<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "test".
 *
 * @property int $test_id
 * @property string $test_name ทดสอบข้อความ
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test';
    }
    public $name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['test_name'], 'required'],
            [['test_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'test_id' => 'Test ID',
            'test_name' => 'Test Name',
        ];
    }
}
