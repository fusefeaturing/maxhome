<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mh_job_route".
 *
 * @property int $job_route_id รหัสเส้นทาง
 * @property int $user_id ชื่อผู้ใช้
 * @property int $job_route_province_id พื้นที่ให้บริการจังหวัด
 * @property int $job_route_amphoe_id พื้นที่ให้บริการอำเภอ
 * @property string $created_date วันเวลาสร้าง
 * @property string $updated_date วันเวลาแก้ไข
 *
 * @property MhProvince $jobRouteProvince
 * @property MhAmphoe $jobRouteAmphoe
 * @property MhUser $user
 */
class MhJobRoute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_job_route';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_route_province_id', 'job_route_amphoe_id', 'created_date'], 'required'],
            [['job_id', 'job_route_province_id', 'job_route_amphoe_id'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['job_route_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhProvince::className(), 'targetAttribute' => ['job_route_province_id' => 'province_id']],
            [['job_route_amphoe_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhAmphoe::className(), 'targetAttribute' => ['job_route_amphoe_id' => 'amphoe_id']],
            //[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhUser::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhJob::className(), 'targetAttribute' => ['job_id' => 'job_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_route_id' => 'รหัสเส้นทาง',

            'job_id' => 'ชื่อกิจการ',
            'job_route_province_id' => 'พื้นที่ให้บริการจังหวัด',
            'job_route_amphoe_id' => 'พื้นที่ให้บริการอำเภอ',
            'created_date' => 'วันเวลาสร้าง',
            'updated_date' => 'วันเวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobRouteProvince()
    {
        return $this->hasOne(MhProvince::className(), ['province_id' => 'job_route_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobRouteAmphoe()
    {
        return $this->hasOne(MhAmphoe::className(), ['amphoe_id' => 'job_route_amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getJob()
    {
        return $this->hasOne(MhJob::className(), ['job_id' => 'job_id']);
    }
}
