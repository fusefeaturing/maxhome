<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mh_job_sub_pt".
 *
 * @property int $job_sub_pt_id รหัสหมู่งานรับเหมา
 * @property string $job_sub_pt_name ชื่อหมู่งานรับเหมา
 * @property int|null $job_pt_id ชื่อหมวดงานรับเหมา
 * @property int|null $updated_id ชื่อผู้แก้ไข
 * @property string $created_time วันเวลาสร้าง
 * @property string $updated_time วันเวลาแก้ไข
 *
 * @property MhConstructionProductType $jobPt
 */
class MhJobSubPt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_job_sub_pt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_sub_pt_name', 'created_time'], 'required'],
            [['job_pt_id', 'updated_id', 'job_sub_pt_level'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['job_sub_pt_name'], 'string', 'max' => 150],
            [['job_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhJobProductType::className(), 'targetAttribute' => ['job_pt_id' => 'job_pt_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_sub_pt_id' => 'รหัสหมู่งานรับเหมา',
            'job_sub_pt_name' => 'ชื่อหมู่งานรับเหมา',
            'job_pt_id' => 'ชื่อหมวดงานรับเหมา',
            'job_sub_pt_level' => 'ลำดับ',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'วันเวลาสร้าง',
            'updated_time' => 'วันเวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobPt()
    {
        return $this->hasOne(MhJobProductType::className(), ['job_pt_id' => 'job_pt_id']);
    }



}
