<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mh_ae_route".
 *
 * @property int $ae_route_id รหัสเส้นทาง
 * @property int $user_id ชื่อผู้ใช้
 * @property int $ae_route_province_id พื้นที่ให้บริการจังหวัด
 * @property int $ae_route_amphoe_id พื้นที่ให้บริการอำเภอ
 * @property string $created_date วันเวลาสร้าง
 * @property string $updated_date วันเวลาแก้ไข
 *
 * @property MhProvince $aeRouteProvince
 * @property MhAmphoe $aeRouteAmphoe
 * @property MhUser $user
 */
class MhAeRoute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_ae_route';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ae_route_province_id', 'ae_route_amphoe_id', 'created_date'], 'required'],
            [['ae_id', 'ae_route_province_id', 'ae_route_amphoe_id'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['ae_route_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhProvince::className(), 'targetAttribute' => ['ae_route_province_id' => 'province_id']],
            [['ae_route_amphoe_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhAmphoe::className(), 'targetAttribute' => ['ae_route_amphoe_id' => 'amphoe_id']],
            //[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhUser::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [['ae_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhArchitectEngineer::className(), 'targetAttribute' => ['ae_id' => 'ae_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ae_route_id' => 'รหัสเส้นทาง',
            'user_id' => 'ชื่อผู้ใช้',
            'ae_id' => 'ชื่อกิจการ',
            'ae_route_province_id' => 'พื้นที่ให้บริการจังหวัด',
            'ae_route_amphoe_id' => 'พื้นที่ให้บริการอำเภอ',
            'created_date' => 'วันเวลาสร้าง',
            'updated_date' => 'วันเวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAeRouteProvince()
    {
        return $this->hasOne(MhProvince::className(), ['province_id' => 'ae_route_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAeRouteAmphoe()
    {
        return $this->hasOne(MhAmphoe::className(), ['amphoe_id' => 'ae_route_amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MhUser::className(), ['user_id' => 'user_id']);
    }

    public function getAe()
    {
        return $this->hasOne(MhArchitectEngineer::className(), ['ae_id' => 'ae_id']);
    }
}
