<?php

namespace common\models;

use backend\models\BackendLogin;
use Yii;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mh_cons_product".
 *
 * @property int $cons_product_id
 * @property string|null $cons_product_name
 * @property string|null $cons_product_description
 * @property string|null $cons_product_pic
 * @property int $cons_pt_id
 * @property int $cons_sub_pt_id
 * @property int|null $cons_sub_type_id
 * @property int|null $updated_id
 * @property string $created_time
 * @property string $updated_time
 *
 * @property MhConsItems[] $mhConsItems
 * @property MhConstructionProductType $consPt
 * @property MhConsSubPt $consSubPt
 * @property MhConsSubType $consSubType
 */
class MhConsProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_cons_product';
    }

    public $upload_foler = 'uploads';
    public $picture = 'none.png';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_product_description'], 'string'],
            [['cons_pt_id', 'cons_sub_pt_id', 'created_time'], 'required'],
            [['cons_pt_id', 'cons_sub_pt_id', 'cons_sub_type_id', 'updated_id'], 'integer'],
            [['cons_product_price'], 'number'],
            [['created_time', 'updated_time'], 'safe'],
            [['cons_product_name'], 'string', 'max' => 255],
            [['cons_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhConstructionProductType::className(), 'targetAttribute' => ['cons_pt_id' => 'cons_pt_id']],
            [['cons_sub_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhConsSubPt::className(), 'targetAttribute' => ['cons_sub_pt_id' => 'cons_sub_pt_id']],
            [['cons_sub_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhConsSubType::className(), 'targetAttribute' => ['cons_sub_type_id' => 'cons_sub_type_id']],
            [
                ['cons_product_pic'], 'file',
                'skipOnEmpty' => true,
                'maxFiles' => 5,
                'extensions' => 'png,jpg,jpeg,gif',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cons_product_id' => 'รหัสสินค้า',
            'cons_product_name' => 'ชื่อสินค้า',
            'cons_product_description' => 'รายละเอียดสินค้า',
            'cons_product_pic' => 'รูปภาพสินค้า',
            'cons_product_price' => 'ราคาสินค้าส่งร้าน',
            'cons_pt_id' => 'ประเภทสินค้า',
            'cons_sub_pt_id' => 'ประเภทสินค้าย่อย',
            'cons_sub_type_id' => 'ประเภทสินค้าย่อยๆ',
            'updated_id' => 'ชื่อ',
            'created_time' => 'วันเวลาสร้าง',
            'updated_time' => 'วันเวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhConsItems()
    {
        return $this->hasMany(MhConsItems::className(), ['cons_product_id' => 'cons_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsPt()
    {
        return $this->hasOne(MhConstructionProductType::className(), ['cons_pt_id' => 'cons_pt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsSubPt()
    {
        return $this->hasOne(MhConsSubPt::className(), ['cons_sub_pt_id' => 'cons_sub_pt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsSubType()
    {
        return $this->hasOne(MhConsSubType::className(), ['cons_sub_type_id' => 'cons_sub_type_id']);
    }

    public function getAdmin()
    {
        return $this->hasOne(BackendLogin::className(), ['id' => 'updated_id']);
    }

    public function subtype()
    {
        $result = '';
        if (($this->cons_sub_type_id != null) && ($this->cons_sub_type_id != '')) {
            $resultcoll = MhConsSubType::find()
                ->where(['cons_sub_type_id' => $this->cons_sub_type_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->cons_sub_type_name;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function upload($model, $attribute)
    {
        $picture  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $picture !== null) {

            $fileName = md5($picture->baseName . time()) . '.' . $picture->extension;
            if ($picture->saveAs($path . $fileName)) {
                return $fileName;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUploadPath()
    {
        return Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/';
    }

    public function getUploadUrl()
    {
        return Yii::getAlias('../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewer()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrl() . $this->picture;
    }

    public function getUploadUrlback()
    {
        return Yii::getAlias('../../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewerback()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrlback() . $this->picture;
    }

    public function uploadMultiple($model, $attribute)
    {
        $photos  = UploadedFile::getInstances($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $photos !== null) {
            $filenames = [];
            foreach ($photos as $file) {
                $filename = md5($file->baseName . time()) . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $filenames[] = $filename;
                }
            }

            if ($model->isNewRecord) {
                return implode(',', $filenames);
            } else {
                if (sizeof($filenames) == 0) {
                    return implode(',', (ArrayHelper::merge($filenames, $model->getOwnPhotosToArray())));
                } else {
                    return implode(',', $filenames);
                }
            }
        }

        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getPhotosViewer()
    {
        $photos = $this->cons_product_pic ? @explode(',', $this->cons_product_pic) : [];
        $img = '';
        /*var_dump($photos);
            die();*/
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrl() . $photo, ['class' => 'img-thumbnail commany-display-mobile items-display-mobile']);
        }
        return $img;
    }

    public function getPhotosViewerback()
    {
        $photos = $this->cons_product_pic ? @explode(',', $this->cons_product_pic) : [];
        $img = '';

        /*var_dump($photos);
            die();*/

        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrlback() . $photo, ['class' => 'img-thumbnail', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotos()
    {
        $cons_product_pic = $this->cons_product_pic ? @explode(',', $this->cons_product_pic) : [];
        $img = Html::img(Yii::getAlias('../common/web') . '/img/none.png', ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile items-display-mobile']);
        if (isset($cons_product_pic[0])) {
            $img = Html::img($this->getUploadUrl() . $cons_product_pic[0], ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile items-display-mobile ']);
        }

        /*var_dump($img);
            die();*/
        return $img;
    }

    public function getFirstPhotoURL()
    {
        $cons_product_pic = $this->cons_product_pic ? @explode(',', $this->cons_product_pic) : [];
        $img = Yii::getAlias('../common/web') . '/img/none.png';
        if (isset($cons_product_pic[0])) {
            $img = $this->getUploadUrl() . $cons_product_pic[0];
        }
        return $img;
    }


    public function getFirstPhotosback()
    {
        $cons_product_pic = $this->cons_product_pic ? @explode(',', $this->cons_product_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile items-display-mobile']);
        if (isset($cons_product_pic[0])) {
            $img = Html::img($this->getUploadUrlback() . $cons_product_pic[0], ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile items-display-mobile']);
        }
        return $img;
    }

    public function getFirstPhotoURLback()
    {
        $cons_product_pic = $this->cons_product_pic ? @explode(',', $this->cons_product_pic) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($cons_product_pic[0])) {
            $img = $this->getUploadUrlback() . $cons_product_pic[0];
        }
        return $img;
    }


    public function getOwnPhotosToArray()
    {
        return $this->getOldAttribute('cons_product_pic') ? @explode(',', $this->getOldAttribute('cons_product_pic')) : [];
    }

}
