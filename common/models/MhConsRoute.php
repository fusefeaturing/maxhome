<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mh_cons_route".
 *
 * @property int $cons_route_id รหัสเส้นทาง
 * @property int $user_id ชื่อผู้ใช้
 * @property int $cons_route_province_id พื้นที่ให้บริการจังหวัด
 * @property int $cons_route_amphoe_id พื้นที่ให้บริการอำเภอ
 * @property string $created_date วันเวลาสร้าง
 * @property string $updated_date วันเวลาแก้ไข
 *
 * @property MhUser $user
 * @property MhProvince $consRouteProvince
 * @property MhAmphoe $consRouteAmphoe
 */
class MhConsRoute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_cons_route';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_route_province_id', 'cons_route_amphoe_id', 'created_date'], 'required'],
            [['cons_id', 'cons_route_province_id', 'cons_route_amphoe_id'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            //[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhUser::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [['cons_route_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhProvince::className(), 'targetAttribute' => ['cons_route_province_id' => 'province_id']],
            [['cons_route_amphoe_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhAmphoe::className(), 'targetAttribute' => ['cons_route_amphoe_id' => 'amphoe_id']],
            [['cons_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhConstruction::className(), 'targetAttribute' => ['cons_id' => 'cons_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cons_route_id' => 'รหัสเส้นทาง',
            'user_id' => 'ชื่อผู้ใช้',
            'cons_id' => 'ชื่อกิจการ',
            'cons_route_province_id' => 'พื้นที่ให้บริการจังหวัด',
            'cons_route_amphoe_id' => 'พื้นที่ให้บริการอำเภอ',
            'created_date' => 'วันเวลาสร้าง',
            'updated_date' => 'วันเวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MhUser::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsRouteProvince()
    {
        return $this->hasOne(MhProvince::className(), ['province_id' => 'cons_route_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsRouteAmphoe()
    {
        return $this->hasOne(MhAmphoe::className(), ['amphoe_id' => 'cons_route_amphoe_id']);
    }

    public function getCons()
    {
        return $this->hasOne(MhConstruction::className(), ['cons_id' => 'cons_id']);
    }
}
