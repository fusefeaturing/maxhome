<?php

namespace common\models;

use backend\models\BackendLogin;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\Html;
use Yii;

/**
 * This is the model class for table "mh_job".
 *
 * @property int $job_id รหัสกิจการ
 * @property int $job_search_level ลำดับการค้นหา
 * @property int $user_id ชื่อผู้ใช้
 * @property string $job_name ชื่อกิจการ
 * @property string $job_pic รูปกิจการ
 * @property string $job_gps พิกัด gps
 * @property string $job_address ที่อยู่กิจการ
 * @property string $job_km รัศมีพื้นที่ให้บริการ
 * @property int $job_province_id พื้นที่ให้บริการจังหวัด
 * @property int $job_amphoe_id พื้นที่ให้บริการอำเภอ
 * @property string $job_tel1 เบอร์ติดต่อ 1
 * @property string $job_tel2 เบอร์ติดต่อ 2
 * @property string $job_line_id Line Id
 * @property string $job_line_ad Line @
 * @property string $job_fb_fp Facebook fanpage
 * @property string $job_messenger Messenger
 * @property string $job_email Email
 * @property string $job_website Website
 * @property string $job_other อื่น ๆ
 * @property string $job_pt_id ประเภทสินค้าร้าน
 * @property string $job_data_other ข้อมูลเพิ่มเติม
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 *
 * @property MhProvince $jobProvince
 * @property MhAmphoe $jobAmphoe
 * @property MhUser $user
 */
class MhJob extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_job';
    }

    public $upload_foler = 'uploads';
    public $picture = 'nonee.png';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_province_id', 'job_amphoe_id', 'job_district_id', 'updated_id', 'job_search_level', 'job_sub_pt_id'], 'integer'],
            [['user_id', 'job_name', 'job_province_id', 'job_amphoe_id', 'job_tel1', /*'job_product_id', 'job_pic'*/], 'required'],
            [['created_time', 'updated_time'], 'safe'],
            [['job_data_other'], 'string'],
            [['job_name', 'job_gps', 'job_fb_fp', 'job_messenger', 'job_email', 'job_website'], 'string', 'max' => 150],
            [['job_address', 'job_other', 'job_pt_id', 'job_product_id'], 'string', 'max' => 255],
            [['job_km'], 'string', 'max' => 30],
            [['job_tel1', 'job_tel2'], 'string', 'max' => 15],
            [['job_line_id', 'job_line_ad'], 'string', 'max' => 50],
            [['job_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhProvince::className(), 'targetAttribute' => ['job_province_id' => 'province_id']],
            [['job_amphoe_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhAmphoe::className(), 'targetAttribute' => ['job_amphoe_id' => 'amphoe_id']],
            [['job_district_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhDistrict::className(), 'targetAttribute' => ['job_district_id' => 'district_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhUser::className(), 'targetAttribute' => ['user_id' => 'user_id']],

            [
                ['job_pic'], 'file',
                'skipOnEmpty' => true,
                'maxFiles' => 5,
                'extensions' => 'png,jpg,jpeg,gif',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'รหัสกิจการ',
            'job_name' => 'ชื่อกิจการ',
            'job_search_level' => 'ลำดับการค้นหา',
            'user_id' => 'ชื่อผู้ใช้งาน',
            'job_pic' => 'รูปกิจการ',

            'job_gps' => 'พิกัด gps',
            'job_address' => 'ที่อยู่กิจการ',
            'job_km' => 'รัศมีพื้นที่ให้บริการ',
            'job_province_id' => 'จังหวัดที่ตั้งกิจการ',
            'job_amphoe_id' => 'อำเภอที่ตั้งกิจการ',
            'job_district_id' => 'รหัสไปรษณีย์',
            'job_tel1' => 'เบอร์ติดต่อ 1',
            'job_tel2' => 'เบอร์ติดต่อ 2',
            'job_line_id' => 'Line Id',
            'job_line_ad' => 'Line @',
            'job_fb_fp' => 'Facebook fanpage',
            'job_messenger' => 'Messenger',
            'job_email' => 'Email',
            'job_website' => 'Website',
            'job_other' => 'อื่น ๆ',
            'job_pt_id' => 'หมวดงานรับเหมาก่อสร้าง',
            'job_sub_pt_id' => 'หมู่งานรับเหมาก่อสร้าง',
            'job_product_id' => 'หมวดหมู่งานรับเหมาก่อสร้าง',
            'job_data_other' => 'รายละเอียดอื่นๆ',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'เวลาสร้าง',
            'updated_time' => 'เวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobProvince()
    {
        return $this->hasOne(MhProvince::className(), ['province_id' => 'job_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobAmphoe()
    {
        return $this->hasOne(MhAmphoe::className(), ['amphoe_id' => 'job_amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobProduct()
    {
        return $this->hasOne(MhJobProduct::className(), ['job_product_id' => 'job_product_id']);
    }

    public function getJobPt()
    {
        return $this->hasOne(MhJobProductType::className(), ['job_pt_id' => 'job_pt_id'])->andOnCondition('job_pt_id!=:job_pt_id', [':job_pt_id' => 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobSubPt()
    {
        return $this->hasOne(MhJobSubPt::className(), ['job_sub_pt_id' => 'job_sub_pt_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(MhDistrict::className(), ['district_id' => 'job_district_id']);
    }

    public static function itemsAlias($key)
    {

        $items =
            ArrayHelper::map(MhConsSubPt::find()->all(), 'cons_sub_pt_id', 'cons_sub_pt_name');

        /*var_dump($items);
        die();*/
        return ArrayHelper::getValue($items, $key, []);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemSkill()
    {
        return self::itemsAlias('job_product_id');
    }

    public function getSkillName()
    {
        $skills = $this->getItemSkill();
        $skillSelected = explode(',', $this->job_product_id);
        $skillSelectedName = [];
        foreach ($skills as $key => $skillName) {
            foreach ($skillSelected as $skillKey) {
                if ($key === $skillKey) {
                    $skillSelectedName[] = $skillName;
                }
            }
        }

        return implode(', ', $skillSelectedName);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MhUser::className(), ['user_id' => 'user_id']);
    }

    public function getAdmin()
    {
        return $this->hasOne(BackendLogin::className(), ['id' => 'updated_id']);
    }

    public function getDefaultLevel()
    {

        $result = '';
        if (($this->job_search_level != null) && ($this->job_search_level != '')) {
            $resultcoll = MhJob::find()
                ->where(['job_search_level' => $this->job_search_level])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->job_search_level;
            } else {
                $result = '100';
            }
        } else {
            $result = '100';
        }
        return $result;
    }


    public function pickup()
    {
        $result = '';
        if (($this->job_province_id != null) && ($this->job_province_id != '')) {
            $resultcoll = MhProvince::find()
                ->where(['province_id' => $this->job_province_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->province_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function deliver()
    {
        $result = '';
        if (($this->job_amphoe_id != null) && ($this->job_amphoe_id != '')) {
            $resultcoll = MhAmphoe::find()
                ->where(['amphoe_id' => $this->job_amphoe_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->amphoe_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function zipcode()
    {
        $result = '';
        if (($this->job_district_id != null) && ($this->job_district_id != '')) {
            $resultcoll = MhDistrict::find()
                ->where(['district_id' => $this->job_district_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->district_zip_code;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function usercontact()
    {
        $result = '';

        $userdata = MhUser::find()
            ->where(['user_id' => $this->user_id])
            ->one();

        if ($userdata) {
            $result = $userdata->user_tel1;
        }

        return $result;
    }

    public function upload($model, $attribute)
    {
        $picture  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $picture !== null) {

            $fileName = md5($picture->baseName . time()) . '.' . $picture->extension;
            if ($picture->saveAs($path . $fileName)) {
                return $fileName;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUploadPath()
    {
        return Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/';
    }

    public function getUploadUrl()
    {
        return Yii::getAlias('../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewer()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrl() . $this->picture;
    }


    public function getUploadUrlback()
    {
        return Yii::getAlias('../../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewerback()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrlback() . $this->picture;
    }

    public function uploadMultiple($model, $attribute)
    {
        $photos  = UploadedFile::getInstances($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $photos !== null) {
            $filenames = [];
            foreach ($photos as $file) {
                $filename = md5($file->baseName . time()) . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $filenames[] = $filename;
                }
            }

            if ($model->isNewRecord) {
                return implode(',', $filenames);
            } else {
                if (sizeof($filenames) == 0) {
                    return implode(',', (ArrayHelper::merge($filenames, $model->getOwnPhotosToArray())));
                } else {
                    return implode(',', $filenames);
                }
            }
        }

        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getPhotosViewer()
    {
        $photos = $this->job_pic ? @explode(',', $this->job_pic) : [];
        $img = '';
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrl() . $photo, ['class' => 'img-thumbnail commany-display-mobile', 'style' => 'width:345px; height:300px;']);
        }
        return $img;
    }

    public function getPhotosViewerback()
    {
        $photos = $this->job_pic ? @explode(',', $this->job_pic) : [];
        $img = '';

        /*var_dump($photos);
            die();*/

        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrlback() . $photo, ['class' => 'img-thumbnail', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getOwnPhotosToArray()
    {
        return $this->getOldAttribute('job_pic') ? @explode(',', $this->getOldAttribute('job_pic')) : [];
    }


    public function getFirstPhotos()
    {
        $job_pic = $this->job_pic ? @explode(',', $this->job_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile ']);
        if (isset($job_pic[0])) {
            $img = Html::img($this->getUploadUrl() . $job_pic[0], ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile ']);
        }
        return $img;
    }

    public function getFirstPhotoURL()
    {
        $job_pic = $this->job_pic ? @explode(',', $this->job_pic) : [];
        $img = Yii::getAlias('../common/web') . '/img/none.png';
        if (isset($job_pic[0])) {
            $img = $this->getUploadUrl() . $job_pic[0];
        }
        return $img;
    }


    public function getFirstPhotosback()
    {
        $job_pic = $this->job_pic ? @explode(',', $this->job_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile']);
        if (isset($job_pic[0])) {
            $img = Html::img($this->getUploadUrlback() . $job_pic[0], ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotoURLback()
    {
        $job_pic = $this->job_pic ? @explode(',', $this->job_pic) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($job_pic[0])) {
            $img = $this->getUploadUrlback() . $job_pic[0];
        }
        return $img;
    }

    public function jobtype_text()
    {
        $separator = '<br>';

        $jobtypeRefArr = ArrayHelper::map(
            MhJobProductType::find()->asArray()->all(),
            'job_pt_id',

            function ($model) {
                if (' - ' . $model['job_pt_name']) return ' - ' . $model['job_pt_name'];
                else return  ' - ' . $model['job_pt_name'];
            }
        );

        $jobtypeRowArr = $this->jobtypeToArray();

        //echo '<pre>'.var_dump($jobtypeRowArr).'</pre>'; exit();

        $txt = 'ไม่ได้ระบุ กรุณาสอบถามผู้ประกาศ';

        if (!empty($jobtypeRowArr)) {
            foreach ($jobtypeRowArr as $jobtypesingleid) {
                if (isset($jobtypeRefArr[$jobtypesingleid])) {
                    $txtarr[] = $jobtypeRefArr[$jobtypesingleid];
                }
            }

            if (isset($txtarr)) {
                $txt = implode($separator, $txtarr);
            }
        }

        return $txt;
    }

    public function jobtypeToArray()
    {
        return $this->job_pt_id = explode(',', $this->job_pt_id);
    }

    public function jobproducttype_text()
    {
        $separator = '<br>';

        $jobtypeRefArr = ArrayHelper::map(
            MhJobSubPt::find()->all(),
            'job_sub_pt_id',
            function ($model) {
                if ($model['job_pt_id'] == $model['job_sub_pt_id']) return $model['job_pt_id'];
                else return $model->jobPt->job_pt_name . ' - ' . $model->job_sub_pt_name;
            }
        );

        $jobtypeRowArr = $this->jobproducttypeToArray();

        //echo '<pre>'.var_dump($jobtypeRowArr).'</pre>'; exit();

        $txt = 'ไม่ได้ระบุ กรุณาสอบถามผู้ประกาศ';

        if (!empty($jobtypeRowArr)) {
            foreach ($jobtypeRowArr as $jobtypesingleid) {
                if (isset($jobtypeRefArr[$jobtypesingleid])) {
                    $txtarr[] = $jobtypeRefArr[$jobtypesingleid];
                }
            }

            if (isset($txtarr)) {
                $txt = implode($separator, $txtarr);
            }
        }

        return $txt;
    }

    public function jobproducttypeToArray()
    {
        return $this->job_product_id = explode(',', $this->job_product_id);
    }

    public function location_province($province)
    {
        $result = '';

        if ($province != null) {
            $pv = MhProvince::find()
                ->where(['province_id' => $province])
                ->one();

            if ($pv) {
                $result .= $pv->province_name_th;
            }
        }

        return $result;
    }

    public function location_amphoe($amphoe)
    {
        $result = '';


        if ($amphoe != null) {
            $am = MhAmphoe::find()
                ->where(['amphoe_id' => $amphoe])
                ->one();

            if ($am) {
                $result .= ' ' . $am->amphoe_name_th;
            }
        }


        return $result;
    }
}
