<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mh_contact".
 *
 * @property int $contact_id
 * @property string $lat
 * @property string $lng
 * @property string $firstname
 * @property string $lastname
 * @property string $address
 */
class MhContact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lat', 'lng', 'firstname', 'lastname', 'address'], 'required'],
            [['lat', 'lng', 'firstname', 'lastname', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'contact_id' => 'Contact ID',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'address' => 'Address',
        ];
    }
}
