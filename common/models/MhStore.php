<?php

namespace common\models;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\Html;
use Yii;

/**
 * This is the model class for table "mh_store".
 *
 * @property int $store_id
 * @property int $user_id ชื่อผู้ใช้
 * @property string $store_name ชื่อกิจการ
 * @property string $store_pic รูปถ่ายกิจการ
 * @property string $store_address ที่อยู่กิจการ
 * @property string $store_gps พิกัด GPS
 * @property string $store_km รัศมีพื้นที่ให้บริการ
 * @property int $store_province_id จังหวัดที่ตั้งกิจการ
 * @property int $store_amphoe_id อำเภอที่ตั้งกิจการ
 * @property string $store_tel1 เบอร์ติดต่อ 1
 * @property string $store_tel2 เบอร์ติดต่อ 2
 * @property string $store_line_id Line Id
 * @property string $store_line_ad Line @
 * @property string $store_fb_fp Facebook fanpage
 * @property string $store_messenger Messenger
 * @property string $store_email Email
 * @property string $store_website Website
 * @property string $store_other อื่น ๆ
 * @property string $store_data_other ข้อมูลเพิ่มเติม
 * @property string $store_pt_id ประเภทร้านค้า
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 *
 * @property MhProvince $storeProvince
 * @property MhAmphoe $storeAmphoe
 * @property MhUser $user
 */
class MhStore extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_store';
    }

    public $upload_foler = 'uploads';
    public $picture = 'nonee.png';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'store_province_id', 'store_amphoe_id', 'updated_id'], 'integer'],
        [['store_name', 'store_address', 'store_province_id', 'store_amphoe_id', 'store_tel1'/*, 'store_pt_id'*/], 'required'],
            [['created_time', 'updated_time'], 'safe'],
            [['store_name', 'store_gps', 'store_fb_fp', 'store_messenger', 'store_email', 'store_website'], 'string', 'max' => 150],
            [['store_address', 'store_other', 'store_data_other', 'store_pt_id'], 'string', 'max' => 255],
            [['store_km'], 'string', 'max' => 30],
            [['store_tel1', 'store_tel2'], 'string', 'max' => 15],
            [['store_line_id', 'store_line_ad'], 'string', 'max' => 50],
            [['store_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhProvince::className(), 'targetAttribute' => ['store_province_id' => 'province_id']],
            [['store_amphoe_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhAmphoe::className(), 'targetAttribute' => ['store_amphoe_id' => 'amphoe_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhUser::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [
                ['store_pic'], 'file',
                'skipOnEmpty' => true,
                'maxFiles' => 5,
                'extensions' => 'png,jpg,jpeg,gif',
            ]

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'store_id' => 'รหัสร้านค้า',
            'store_name' => 'ชื่อกิจการ',
            'user_id' => 'ชื่อผู้ใช้งาน',
            'store_pic' => 'รูปถ่ายกิจการ',
            'store_address' => 'ที่อยู่กิจการ',
            'store_gps' => 'พิกัด GPS',
            'store_km' => 'รัศมีพื้นที่ให้บริการ',
            'store_province_id' => 'พื้นที่ให้บริการจังหวัด',
            'store_amphoe_id' => 'พื้นที่ให้บริการอำเภอ',
            'store_tel1' => 'เบอร์ติดต่อ 1',
            'store_tel2' => 'เบอร์ติดต่อ 2',
            'store_line_id' => 'Line Id',
            'store_line_ad' => 'Line @',
            'store_fb_fp' => 'Facebook fanpage',
            'store_messenger' => 'Messenger',
            'store_email' => 'Email',
            'store_website' => 'Website',
            'store_other' => 'อื่น ๆ',
            'store_pt_id' => 'ประเภทสินค้าร้าน',
            'store_data_other' => 'ข้อมูลเพิ่มเติม',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'เวลาสร้าง',
            'updated_time' => 'เวลาแก้ไข',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreProvince()
    {
        return $this->hasOne(MhProvince::className(), ['province_id' => 'store_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreAmphoe()
    {
        return $this->hasOne(MhAmphoe::className(), ['amphoe_id' => 'store_amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MhUser::className(), ['user_id' => 'user_id']);
    }

    public function getStoreType()
    {
        return $this->hasOne(MhStoreProductType::className(), ['store_pt_id' => 'store_pt_id']);
    }

    public function storetype_text()
    {
        $separator = '<br>';

        $storetypeRefArr = ArrayHelper::map(
            MhStoreProductType::find()->asArray()->all(),
                'store_pt_id',

                function ($model) {
                    if ($model['store_pt_name'] ) return $model['store_pt_name'];
                    else return $model['store_pt_name'];
                }
            );

        $storetypeRowArr = $this->storetypeToArray();

        //echo '<pre>'.var_dump($storetypeRowArr).'</pre>'; exit();

        $txt = 'ไม่ได้ระบุ กรุณาสอบถามผู้ประกาศ';

        if (!empty($storetypeRowArr)) {
            foreach ($storetypeRowArr as $storetypesingleid) {
                if (isset($storetypeRefArr[$storetypesingleid])) {
                    $txtarr[] = $storetypeRefArr[$storetypesingleid];
                }
            }

            if (isset($txtarr)) {
                $txt = implode($separator, $txtarr);
            }
        }

        return $txt;
    }

    public function storetypeToArray()
    {
        return $this->store_pt_id = explode(',', $this->store_pt_id);
    }


    public function pickup()
    {
        $result = '';
        if (($this->store_province_id != null) && ($this->store_province_id != '')) {
            $resultcoll = MhProvince::find()
                ->where(['province_id' => $this->store_province_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->province_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function deliver()
    {
        $result = '';
        if (($this->store_amphoe_id != null) && ($this->store_amphoe_id != '')) {
            $resultcoll = MhAmphoe::find()
                ->where(['amphoe_id' => $this->store_amphoe_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->amphoe_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    
/*
    public function storetype_text()
    {
        $separator = '<br>';
        $separator = '<br>';
        $storetypeRefArr = ArrayHelper::map(
            MhStoreProductType::find()->asArray()->all(),
                'store_pt_id',

                function ($model) {
                    if ($model['store_pt_name'] ) return $model['store_pt_name'];
                    else return $model['store_pt_name'];
                }
            );

        $storetypeRowArr = $this->storetypeToArray();

        //echo '<pre>'.var_dump($storetypeRowArr).'</pre>'; exit();

        $txt = 'ไม่ได้ระบุ กรุณาสอบถามผู้ประกาศ';

        if (!empty($storetypeRowArr)) {
            foreach ($storetypeRowArr as $storetypesingleid) {
                if (isset($storetypeRefArr[$storetypesingleid])) {
                    $txtarr[] = $storetypeRefArr[$storetypesingleid];
                }
            }

            if (isset($txtarr)) {
                $txt = implode(",", $txtarr);
            }
        }

        return $txt;
    }

    public function storetypeToArray()
    {
        return $this->store_pt_id = explode(',', $this->store_pt_id);
    }
*/
    public function upload($model, $attribute)
    {
        $picture  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $picture !== null) {

            $fileName = md5($picture->baseName . time()) . '.' . $picture->extension;
            if ($picture->saveAs($path . $fileName)) {
                return $fileName;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUploadPath()
    {
        return Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/';
    }

    public function getUploadUrl()
    {
        return Yii::getAlias('../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewer()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrl() . $this->picture;
    }

    public function getUploadUrlback()
    {
        return Yii::getAlias('../../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewerback()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrlback() . $this->picture;
    }




    public function uploadMultiple($model, $attribute)
    {
        $photos  = UploadedFile::getInstances($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $photos !== null) {
            $filenames = [];
            foreach ($photos as $file) {
                $filename = md5($file->baseName . time()) . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $filenames[] = $filename;
                }
            }

            if ($model->isNewRecord) {
                return implode(',', $filenames);
            } else {
                if (sizeof($filenames) == 0) {
                    return implode(',', (ArrayHelper::merge($filenames, $model->getOwnPhotosToArray())));
                } else {
                    return implode(',', $filenames);
                }
            }
        }

        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getPhotosViewer()
    {
        $photos = $this->store_pic ? @explode(',', $this->store_pic) : [];
        $img = '';
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrl() . $photo, ['class' => 'img-thumbnail', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getPhotosViewerback()
    {
        $photos = $this->store_pic ? @explode(',', $this->store_pic) : [];
        $img = '';
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrlback() . $photo, ['class' => 'img-thumbnail', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotos()
    {
        $store_pic = $this->store_pic ? @explode(',', $this->store_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        if (isset($store_pic[0])) {
            $img = Html::img($this->getUploadUrl() . $store_pic[0], ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotoURL()
    {
        $store_pic = $this->store_pic ? @explode(',', $this->store_pic) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($store_pic[0])) {
            $img = $this->getUploadUrl() . $store_pic[0];
        }
        return $img;
    }


    public function getFirstPhotosback()
    {
        $store_pic = $this->store_pic ? @explode(',', $this->store_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        if (isset($store_pic[0])) {
            $img = Html::img($this->getUploadUrlback() . $store_pic[0], ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotoURLback()
    {
        $store_pic = $this->store_pic ? @explode(',', $this->store_pic) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($store_pic[0])) {
            $img = $this->getUploadUrlback() . $store_pic[0];
        }
        return $img;
    }


    public function getOwnPhotosToArray()
    {
        return $this->getOldAttribute('store_pic') ? @explode(',', $this->getOldAttribute('store_pic')) : [];
    }

    public function location_province($province)
    {
        $result = '';

        if ($province != null) {
            $pv = MhProvince::find()
                ->where(['province_id' => $province])
                ->one();

            if ($pv) {
                $result .= $pv->province_name_th;
            }
        }

        return $result;
    }

    public function location_amphoe($amphoe)
    {
        $result = '';


        if ($amphoe != null) {
            $am = MhAmphoe::find()
                ->where(['amphoe_id' => $amphoe])
                ->one();

            if ($am) {
                $result .= ' ' . $am->amphoe_name_th;
            }
        }


        return $result;
    }

}
