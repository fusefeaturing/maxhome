<?php

namespace common\models;

use Yii;
use backend\models\BackendLogin;

/**
 * This is the model class for table "mh_province".
 *
 * @property int $province_id รหัสจังหวัด
 * @property string $province_name_th ชื่อจังหวัดภาษาไทย
 * @property string $province_name_en ชื่อจังหวัดภาษาอังกฤษ
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 *
 * @property MhAmphoe[] $mhAmphoes
 * @property MhArchitectEngineer[] $mhArchitectEngineers
 * @property MhConcrete[] $mhConcretes
 * @property MhJob[] $mhJobs
 * @property MhStore[] $mhStores
 */
class MhProvince extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_province';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['province_name_th', 'province_name_en'], 'required'],
            [['updated_id'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['province_name_th', 'province_name_en'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'province_id' => 'รหัสจังหวัด',
            'province_name_th' => 'ชื่อจังหวัดภาษาไทย',
            'province_name_en' => 'ชื่อจังหวัดภาษาอังกฤษ',
            'updated_id' => 'รหัสเวลาแก้ไข',
            'created_time' => 'เวลาสร้าง',
            'updated_time' => 'เวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhAmphoes()
    {
        return $this->hasMany(MhAmphoe::className(), ['province_id' => 'province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhArchitectEngineers()
    {
        return $this->hasMany(MhArchitectEngineer::className(), ['ae_province_id' => 'province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhConcretes()
    {
        return $this->hasMany(MhConcrete::className(), ['con_province_id' => 'province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhJobs()
    {
        return $this->hasMany(MhJob::className(), ['job_province_id' => 'province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMhStores()
    {
        return $this->hasMany(MhStore::className(), ['store_province_id' => 'province_id']);
    }

    public function getUser_backend() {
        return $this->hasOne(BackendLogin::className(),['id'=>'updated_id']);
    }
}
