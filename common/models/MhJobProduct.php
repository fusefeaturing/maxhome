<?php

namespace common\models;

use backend\models\BackendLogin;
use Yii;

/**
 * This is the model class for table "mh_job_product".
 *
 * @property int $job_product_id
 * @property string $job_product_name
 * @property string $job_product_description
 * @property int|null $job_pt_id
 * @property int|null $job_sub_pt_id
 * @property int|null $updated_id
 * @property string $created_time
 * @property string $updated_time
 *
 * @property MhJobProductType $jobPt
 * @property MhJobSubPt $jobSubPt
 */
class MhJobProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_job_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_product_name', 'job_product_description', 'created_time'], 'required'],
            [['job_product_description'], 'string'],
            [['job_pt_id', 'job_sub_pt_id', 'updated_id'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['job_product_name'], 'string', 'max' => 255],
            [['job_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhJobProductType::className(), 'targetAttribute' => ['job_pt_id' => 'job_pt_id']],
            [['job_sub_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhJobSubPt::className(), 'targetAttribute' => ['job_sub_pt_id' => 'job_sub_pt_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_product_id' => 'รหัสงานรับเหมาก่อสร้าง',
            'job_product_name' => 'ชื่องานรับเหมาก่อสร้าง',
            'job_product_description' => 'รายละเอียดงานรับเหมาก่อสร้าง',
            'job_pt_id' => 'ชื่อหมวด',
            'job_sub_pt_id' => 'ชื่อหมู่',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'วันเวลาสร้าง',
            'updated_time' => 'วันเวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobPt()
    {
        return $this->hasOne(MhJobProductType::className(), ['job_pt_id' => 'job_pt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobSubPt()
    {
        return $this->hasOne(MhJobSubPt::className(), ['job_sub_pt_id' => 'job_sub_pt_id']);
    }

    public function getAdmin()
    {
        return $this->hasOne(BackendLogin::className(), ['id' => 'updated_id']);
    }
}
