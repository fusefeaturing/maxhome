<?php

namespace common\models;

use Yii;
use backend\models\BackendLogin;

/**
 * This is the model class for table "mh_job_product_type".
 *
 * @property int $job_pt_id รหัสประเภทสินค้าก่อสร้าง
 * @property string $job_pt_name ชื่อประเภทสินค้าก่อสร้าง
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 */
class MhJobProductType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_job_product_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_pt_name', 'updated_id', 'created_time', 'updated_time'], 'required'],
            [['updated_id', 'job_pt_level'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['job_pt_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_pt_id' => 'รหัสประเภทงานรับเหมา',
            'job_pt_name' => 'ชื่อประเภทงานรับเหมา',
            'job_pt_level' => 'ลำดับประเภท',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'เวลาสร้าง',
            'updated_time' => 'เวลาแก้ไข',
        ];
    }

    public function getUser_backend() {
        return $this->hasOne(BackendLogin::className(),['id'=>'updated_id']);
    }
}
