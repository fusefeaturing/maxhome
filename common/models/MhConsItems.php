<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mh_cons_items".
 *
 * @property int $cons_items_id รหัสสินค้า
 * @property string|null $cons_item_name ชื่อสินค้า
 * @property int|null $cons_item_level ลำดับสินค้า
 * @property int|null $cons_product_id ประเภทสินค้า
 * @property float|null $cons_price ราคาสินค้า
 * @property int|null $cons_id รหัสกิจการ
 * @property int|null $user_id ผู้ใช้
 * @property int|null $cons_pt_id
 * @property int|null $cons_sub_pt_id
 * @property int|null $cons_sub_type_id
 * @property int|null $cons_province_id
 * @property int|null $cons_amphoe_id
 * @property int|null $updated_id
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 *
 * @property MhConstruction $cons
 * @property MhProvince $consProvince
 * @property MhAmphoe $consAmphoe
 * @property MhUser $user
 * @property MhConsProduct $consProduct
 * @property MhConstructionProductType $consPt
 * @property MhConsSubPt $consSubPt
 * @property MhConsSubType $consSubType
 */
class MhConsItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_cons_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_item_level', 'cons_product_id', 'cons_id', 'user_id', 'cons_pt_id', 'cons_sub_pt_id', 'cons_sub_type_id', 'cons_province_id', 'cons_amphoe_id', 'updated_id'], 'integer'],
            [['cons_price'], 'number'],
            [['created_time', 'cons_id'], 'required'],
            [['created_time', 'updated_time'], 'safe'],
            [['cons_item_name'], 'string', 'max' => 255],
            [['cons_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhConstruction::className(), 'targetAttribute' => ['cons_id' => 'cons_id']],
            [['cons_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhProvince::className(), 'targetAttribute' => ['cons_province_id' => 'province_id']],
            [['cons_amphoe_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhAmphoe::className(), 'targetAttribute' => ['cons_amphoe_id' => 'amphoe_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhUser::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [['cons_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhConsProduct::className(), 'targetAttribute' => ['cons_product_id' => 'cons_product_id']],
            [['cons_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhConstructionProductType::className(), 'targetAttribute' => ['cons_pt_id' => 'cons_pt_id']],
            [['cons_sub_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhConsSubPt::className(), 'targetAttribute' => ['cons_sub_pt_id' => 'cons_sub_pt_id']],
            [['cons_sub_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhConsSubType::className(), 'targetAttribute' => ['cons_sub_type_id' => 'cons_sub_type_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cons_items_id' => 'รหัสสินค้า',
            'cons_item_name' => 'ชื่อสินค้า',
            'cons_item_level' => 'ลำดับสินค้า',
            'cons_product_id' => 'ประเภทสินค้า',
            'cons_price' => 'ราคาสินค้า',
            'cons_id' => 'ชื่อกิจการ',
            'user_id' => 'ชื่อเจ้าของกิจการ',
            'cons_pt_id' => 'ประเภทสินค้า',
            'cons_sub_pt_id' => 'ประเภทสินค้าย่อย',
            'cons_sub_type_id' => 'ประเภทสินค้าย่อยๆ',
            'cons_province_id' => 'พื้นที่จังหวัด',
            'cons_amphoe_id' => 'พื้นที่อำเภอ',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'วันเวลาสร้าง',
            'updated_time' => 'วันเวลาแก้ไข',
        ];
    }

    public function getCons()
    {
        return $this->hasOne(MhConstruction::className(), ['cons_id' => 'cons_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsProvince()
    {
        return $this->hasOne(MhProvince::className(), ['province_id' => 'cons_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsAmphoe()
    {
        return $this->hasOne(MhAmphoe::className(), ['amphoe_id' => 'cons_amphoe_id']);
    }

       /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsRouteProvince()
    {
        return $this->hasOne(MhConsRoute::className(), ['cons_route_province_id' => 'cons_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsRouteAmphoe()
    {
        return $this->hasOne(MhConsRoute::className(), ['cons_route_amphoe_id' => 'cons_amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MhUser::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsProduct()
    {
        return $this->hasOne(MhConsProduct::className(), ['cons_product_id' => 'cons_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsPt()
    {
        return $this->hasOne(MhConstructionProductType::className(), ['cons_pt_id' => 'cons_pt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsSubPt()
    {
        return $this->hasOne(MhConsSubPt::className(), ['cons_sub_pt_id' => 'cons_sub_pt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsSubType()
    {
        return $this->hasOne(MhConsSubType::className(), ['cons_sub_type_id' => 'cons_sub_type_id']);
    } 

    public function getDefaultLevel() {

        $result = '';
        if (($this->cons_item_level != null) && ($this->cons_item_level != '')) {
            $resultcoll = MhConsItems::find()
                ->where(['cons_item_level' => $this->cons_item_level])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->cons_item_level;
            } else {
                $result = '100';
            }
        } else {
            $result = '100';
        }
        return $result;
        
   }

    
    public function getProductTypes()
    {
        if (($model = MhConsProduct::find()) !== null) {
            return ArrayHelper::map(
                MhConsProduct::find()->all(),
                'cons_product_id',
                function ($model) {
                    if ( $model['cons_pt_id'] == $model['cons_sub_pt_id']) return $model['cons_pt_id'];
                    else return $model->consPt->cons_pt_name . ' - ' . $model->consSubPt->cons_sub_pt_name;
                }
            );
        }

        return array(-1, 'No car data');
    }

    public function pickup()
    {
        $result = '';
        if (($this->cons_province_id != null) && ($this->cons_province_id != '')) {
            $resultcoll = MhProvince::find()
                ->where(['province_id' => $this->cons_province_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->province_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function deliver()
    {
        $result = '';
        if (($this->cons_amphoe_id != null) && ($this->cons_amphoe_id != '')) {
            $resultcoll = MhAmphoe::find()
                ->where(['amphoe_id' => $this->cons_amphoe_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->amphoe_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    


}
