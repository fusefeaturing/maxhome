<?php

namespace common\models;

use Yii;
use backend\models\BackendLogin;

/**
 * This is the model class for table "mh_construction_product_type".
 *
 * @property int $cons_pt_id รหัสประเภทสินค้าวัสดุก่อสร้าง
 * @property string $cons_pt_name ชื่อประเภทสินค้าวัสดุก่อสร้าง
 * @property int $cons_pt_level ลำดับประเภท
 * @property string $updated_id ชื่อผู้แก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 */
class MhConstructionProductType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_construction_product_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_pt_name'], 'required'],
            [['cons_pt_level'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['cons_pt_name'], 'string', 'max' => 100],
            [['updated_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cons_pt_id' => 'รหัสประเภทสินค้าก่อสร้าง',
            'cons_pt_name' => 'ชื่อหมวดสินค้า',
            'cons_pt_level' => 'ลำดับประเภท',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'เวลาสร้าง',
            'updated_time' => 'เวลาแก้ไข',
        ];
    }

    public function getUser_backend() {
        return $this->hasOne(BackendLogin::className(),['id'=>'updated_id']);
    }
}
