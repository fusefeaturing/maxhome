<?php

namespace common\models;

use Yii;
use backend\models\BackendLogin;

/**
 * This is the model class for table "mh_store_product_type".
 *
 * @property int $store_pt_id รหัสประเภทสินค้าร้าน
 * @property string $store_pt_name ชื่อประเภทสินค้าร้าน
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 */
class MhStoreProductType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_store_product_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['store_pt_name', 'updated_id', 'created_time', 'updated_time'], 'required'],
            [['updated_id'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['store_pt_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'store_pt_id' => 'รหัสประเภทสินค้าร้านค้า',
            'store_pt_name' => 'ชื่อประเภทสินค้าร้านค้า',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'เวลาสร้าง',
            'updated_time' => 'เวลาแก้ไข',
        ];
    }

    public function getUser_backend() {
        return $this->hasOne(BackendLogin::className(),['id'=>'updated_id']);
    }
}
