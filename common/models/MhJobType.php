<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mh_job_type".
 *
 * @property int $job_type_id
 * @property int|null $job_id
 * @property int|null $job_type_pt_id
 * @property int|null $job_type_sub_pt_id
 * @property string $created_date
 * @property string $updated_time
 *
 * @property MhJob $job
 * @property MhJobProductType $jobTypePt
 * @property MhJobSubPt $jobTypeSubPt
 */
class MhJobType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_job_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id', 'job_type_pt_id', 'job_type_sub_pt_id'], 'integer'],
            [['created_date'], 'required'],
            [['created_date', 'updated_time'], 'safe'],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhJob::className(), 'targetAttribute' => ['job_id' => 'job_id']],
            [['job_type_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhJobProductType::className(), 'targetAttribute' => ['job_type_pt_id' => 'job_pt_id']],
            [['job_type_sub_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhJobSubPt::className(), 'targetAttribute' => ['job_type_sub_pt_id' => 'job_sub_pt_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_type_id' => 'รหัสประเภท',
            'job_id' => 'ชื่อกิจการ',
            'job_type_pt_id' => 'ชื่อหมวด',
            'job_type_sub_pt_id' => 'ชื่อหมู่',
            'created_date' => 'วันเวลาสร้าง',
            'updated_time' => 'วันเวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(MhJob::className(), ['job_id' => 'job_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobTypePt()
    {
        return $this->hasOne(MhJobProductType::className(), ['job_pt_id' => 'job_type_pt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobTypeSubPt()
    {
        return $this->hasOne(MhJobSubPt::className(), ['job_sub_pt_id' => 'job_type_sub_pt_id']);
    }
}
