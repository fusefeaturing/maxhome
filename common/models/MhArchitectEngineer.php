<?php

namespace common\models;

use backend\models\BackendLogin;
use Yii;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "mh_architect_engineer".
 *
 * @property int $ae_id รหัสงานเขียนแบบออกแบบ
 * @property int $ae_search_level ลำดับการค้นหา
 * @property int $user_id ชื่อผู้ใช้
 * @property string $ae_name ชื่อกิจการ
 * @property string $ae_pic รูปกิจการ
 * @property string $ae_gps พิกัด gps
 * @property string $ae_address ที่อยู่กิจการ
 * @property string $ae_km รัศมีพื้นที่ให้บริการ
 * @property int $ae_province_id พื้นที่ให้บริการจังหวัด 
 * @property int $ae_amphoe_id พื้นที่ให้บริการอำเภอ
 * @property string $ae_tel1 เบอร์ติดต่อ 1
 * @property string $ae_tel2 เบอร์ติดต่อ 2
 * @property string $ae_line_id Line Id
 * @property string $ae_line_ad Line @
 * @property string $ae_fb_fp Facebook fanpage
 * @property string $ae_messenger Messenger
 * @property string $ae_email Email
 * @property string $ae_website Website
 * @property string $ae_other อื่น ๆ
 * @property string $ae_data_other ข้อมูลเพิ่มเติม
 * @property string $ae_pt_id ประเภทสินค้าเขียนแบบออกแบบ
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 *
 * @property MhProvince $aeProvince
 * @property MhAmphoe $aeAmphoe
 * @property MhUser $user
 */
class MhArchitectEngineer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_architect_engineer';
    }

    public $upload_foler = 'uploads';
    public $picture = 'nonee.png';

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'ae_province_id', 'ae_amphoe_id', 'ae_district_id', 'updated_id', 'ae_search_level'], 'integer'],
            [['ae_name', 'ae_province_id', 'ae_amphoe_id', 'ae_tel1', 'ae_pt_id'/*, 'ae_pic'*/], 'required'],
            [['created_time', 'updated_time'], 'safe'],
            [['ae_name', 'ae_gps', 'ae_fb_fp', 'ae_messenger', 'ae_email', 'ae_website'], 'string', 'max' => 150],
            [['ae_address', 'ae_other', 'ae_data_other', 'ae_pt_id'], 'string', 'max' => 255],
            [['ae_km'], 'string', 'max' => 30],
            [['ae_tel1', 'ae_tel2'], 'string', 'max' => 15],
            [['ae_line_id', 'ae_line_ad'], 'string', 'max' => 50],
            [['ae_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhProvince::className(), 'targetAttribute' => ['ae_province_id' => 'province_id']],
            [['ae_amphoe_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhAmphoe::className(), 'targetAttribute' => ['ae_amphoe_id' => 'amphoe_id']],
            [['ae_district_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhDistrict::className(), 'targetAttribute' => ['ae_district_id' => 'district_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhUser::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [
                ['ae_pic'], 'file',
                'skipOnEmpty' => true,
                'maxFiles' => 5,
                'extensions' => 'png,jpg,jpeg,gif',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ae_id' => 'รหัสงานเขียนแบบออกแบบ',
            'ae_search_level' => 'ลำดับการค้นหา',
            'ae_name' => 'ชื่อกิจการ',
            'user_id' => 'ชื่อผู้ใช้งาน',
            'ae_pic' => 'รูปกิจการ',
            'ae_gps' => 'พิกัด gps',
            'ae_address' => 'ที่อยู่กิจการ',
            'ae_km' => 'รัศมีพื้นที่ให้บริการ',
            'ae_province_id' => 'พื้นที่ให้บริการจังหวัด ',
            'ae_amphoe_id' => 'พื้นที่ให้บริการอำเภอ',
            'ae_district_id' => 'รหัสไปรษณีย์',
            'ae_tel1' => 'เบอร์ติดต่อ 1',
            'ae_tel2' => 'เบอร์ติดต่อ 2',
            'ae_line_id' => 'Line Id',
            'ae_line_ad' => 'Line @',
            'ae_fb_fp' => 'Facebook fanpage',
            'ae_messenger' => 'Messenger',
            'ae_email' => 'Email',
            'ae_website' => 'Website',
            'ae_other' => 'อื่น ๆ',
            'ae_data_other' => 'รายละเอียดอื่นๆ',
            'ae_pt_id' => 'ประเภทเขียนแบบออกแบบ',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'เวลาสร้าง',
            'updated_time' => 'เวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAeProvince()
    {
        return $this->hasOne(MhProvince::className(), ['province_id' => 'ae_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAeAmphoe()
    {
        return $this->hasOne(MhAmphoe::className(), ['amphoe_id' => 'ae_amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MhUser::className(), ['user_id' => 'user_id']);
    }

    public function getAdmin()
    {
        return $this->hasOne(BackendLogin::className(), ['id' => 'updated_id']);
    }

        /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(MhDistrict::className(), ['district_id' => 'ae_district_id']);
    }


    public static function itemsAlias($key){

        $items = [
          

      ];
      
        return ArrayHelper::getValue($items,$key,[]);
        //return array_key_exists($key, $items) ? $items[$key] : [];
      }

    public function getItemSkill(){
        return self::itemsAlias('ae_pt_id');
    }

    public function pickup()
    {
        $result = '';
        if (($this->ae_province_id != null) && ($this->ae_province_id != '')) {
            $resultcoll = MhProvince::find()
                ->where(['province_id' => $this->ae_province_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->province_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function deliver()
    {
        $result = '';
        if (($this->ae_amphoe_id != null) && ($this->ae_amphoe_id != '')) {
            $resultcoll = MhAmphoe::find()
                ->where(['amphoe_id' => $this->ae_amphoe_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->amphoe_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function zipcode()
    {
        $result = '';
        if (($this->ae_district_id != null) && ($this->ae_district_id != '')) {
            $resultcoll = MhDistrict::find()
                ->where(['district_id' => $this->ae_district_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->district_zip_code;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function upload($model, $attribute)
    {
        $picture  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $picture !== null) {

            $fileName = md5($picture->baseName . time()) . '.' . $picture->extension;
            if ($picture->saveAs($path . $fileName)) {
                return $fileName;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUploadPath()
    {
        return Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/';
    }

    public function getUploadUrl()
    {
        return Yii::getAlias('../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewer()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrl() . $this->picture;
    }

    public function getUploadUrlback()
    {
        return Yii::getAlias('../../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewerback()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrlback() . $this->picture;
    }




    public function uploadMultiple($model, $attribute)
    {
        $ae_pic  = UploadedFile::getInstances($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $ae_pic !== null) {
            $filenames = [];
            foreach ($ae_pic as $file) {
                $filename = md5($file->baseName . time()) . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $filenames[] = $filename;
                }
            }

            if ($model->isNewRecord) {
                return implode(',', $filenames);
            } else {
                if (sizeof($filenames) == 0) {
                    return implode(',', (ArrayHelper::merge($filenames, $model->getOwnPhotosToArray())));
                } else {
                    return implode(',', $filenames);
                }
            }
        }

        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getPhotosViewer()
    {
        $ae_pic = $this->ae_pic ? @explode(',', $this->ae_pic) : [];
        $img = '';
        foreach ($ae_pic as $ae_pics) {
            $img .= ' ' . Html::img($this->getUploadUrl() . $ae_pics, ['class' => 'img-thumbnail commany-display-mobile']);
        }
        return $img;
    }

    public function getPhotosViewerback()
    {
        $ae_pic = $this->ae_pic ? @explode(',', $this->ae_pic) : [];
        $img = '';

        /*var_dump($ae_pic);
        die();*/

        foreach ($ae_pic as $ae_pics) {
            $img .= ' ' . Html::img($this->getUploadUrlback() . $ae_pics, ['class' => 'img-thumbnail' , 'style' => 'max-width:200px;']);
        }
           
        return $img;
    }

    public function getFirstPhotos()
    {
        $ae_pic = $this->ae_pic ? @explode(',', $this->ae_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile',]);
        if (isset($ae_pic[0])) {
            $img = Html::img($this->getUploadUrl() . $ae_pic[0], ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile ']);
        }
        return $img;
    }

    public function getFirstPhotoURL()
    {
        $ae_pic = $this->ae_pic ? @explode(',', $this->ae_pic) : [];
        $img = Yii::getAlias('../common/web') . '/img/none.png';
        if (isset($ae_pic[0])) {
            $img = $this->getUploadUrl() . $ae_pic[0];
        }
        return $img;
    }


    public function getFirstPhotosback()
    {
        $ae_pic = $this->ae_pic ? @explode(',', $this->ae_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile']);
        if (isset($ae_pic[0])) {
            $img = Html::img($this->getUploadUrlback() . $ae_pic[0], ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile']);
        }
        return $img;
    }

    public function getFirstPhotoURLback()
    {
        $ae_pic = $this->ae_pic ? @explode(',', $this->ae_pic) : [];
        $img = Yii::getAlias('../common/web') . '/img/none.png';
        if (isset($ae_pic[0])) {
            $img = $this->getUploadUrlback() . $ae_pic[0];
        }
        return $img;
    }


    public function getOwnPhotosToArray()
    {
        return $this->getOldAttribute('ae_pic') ? @explode(',', $this->getOldAttribute('ae_pic')) : [];
    }


    public function aetype_text()
    {
        $separator = '<br>';

        $aetypeRefArr = ArrayHelper::map(
            MhAeProductType::find()->asArray()->all(),
                'ae_pt_id',

                function ($model) {
                    if (' - ' . $model['ae_pt_name'] ) return ' - ' . $model['ae_pt_name'];
                    else return ' - ' . $model['ae_pt_name'];
                }
            );

        $aetypeRowArr = $this->aetypeToArray();

        //echo '<pre>'.var_dump($aetypeRowArr).'</pre>'; exit();

        $txt = 'ไม่ได้ระบุ กรุณาสอบถามผู้ประกาศ';

        if (!empty($aetypeRowArr)) {
            foreach ($aetypeRowArr as $aetypesingleid) {
                if (isset($aetypeRefArr[$aetypesingleid])) {
                    $txtarr[] = $aetypeRefArr[$aetypesingleid];
                }
            }

            if (isset($txtarr)) {
                $txt = implode($separator, $txtarr);
            }
        }

        return $txt;
    }


    public function aetypeToArray()
    {
        return $this->ae_pt_id = explode(',', $this->ae_pt_id);
    }



    public function location_province($province)
    {
        $result = '';

        if ($province != null) {
            $pv = MhProvince::find()
                ->where(['province_id' => $province])
                ->one();

            if ($pv) {
                $result .= $pv->province_name_th;
            }
        }

        return $result;
    }

    public function location_amphoe($amphoe)
    {
        $result = '';


        if ($amphoe != null) {
            $am = MhAmphoe::find()
                ->where(['amphoe_id' => $amphoe])
                ->one();

            if ($am) {
                $result .= ' ' . $am->amphoe_name_th;
            }
        }

        return $result;
    }
}
