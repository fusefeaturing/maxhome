<?php

namespace common\models;

use Yii;
use backend\models\BackendLogin;

/**
 * This is the model class for table "mh_ae_product_type".
 *
 * @property int $ae_pt_id รหัสประเภทสินค้าออกแบบเขียนแบบ
 * @property string $ae_pt_name ชื่อประเภทสินค้าออกแบบเขียนแบบ
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 */
class MhAeProductType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_ae_product_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ae_pt_name', 'updated_id', 'created_time', 'updated_time'], 'required'],
            [['updated_id', 'ae_pt_level'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['ae_pt_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ae_pt_id' => 'รหัสประเภทสินค้าออกแบบเขียนแบบ',
            'ae_pt_name' => 'ชื่อประเภทสินค้าออกแบบเขียนแบบ',
            'ae_pt_level' => 'ลำดับประเภท',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'เวลาสร้าง',
            'updated_time' => 'เวลาแก้ไข',
        ];
    }

    public function getUser_backend() {
        return $this->hasOne(BackendLogin::className(),['id'=>'updated_id']);
    }
}
