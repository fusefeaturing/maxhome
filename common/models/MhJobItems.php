<?php

namespace common\models;

use backend\models\BackendLogin;
use Yii;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "mh_job_items".
 *
 * @property int $job_items_id รหัสงานรับเหมาก่อสร้าง
 * @property string|null $job_item_name ชื่อสินค้า
 * @property int|null $job_item_level ลำดับสินค้า
 * @property int|null $job_product_id ประเภทสินค้า
 * @property float|null $job_price ราคาสินค้า
 * @property int|null $job_id รหัสกิจการ
 * @property int|null $user_id ผู้ใช้
 * @property int|null $job_pt_id
 * @property int|null $job_sub_pt_id
 * @property int|null $job_province_id
 * @property int|null $job_amphoe_id
 * @property int|null $updated_id
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 *
 * @property MhAmphoe $jobAmphoe
 * @property MhProvince $jobProvince
 * @property MhJobProduct $jobProduct
 * @property MhJobProductType $jobPt
 * @property MhJobSubPt $jobSubPt
 * @property MhJob $job
 * @property MhUser $user
 */
class MhJobItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_job_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_item_level', 'job_product_id', 'job_id', 'user_id', 'job_pt_id', 'job_sub_pt_id', 'job_province_id', 'job_amphoe_id', 'updated_id'], 'integer'],
            [['job_price'], 'number'],
            [['job_id', 'user_id'], 'required'],
            [['job_item_date_start', 'job_item_date_end', 'created_time', 'updated_time'], 'safe'],
            [['job_item_name'], 'string', 'max' => 255],
            [['job_item_description'], 'string'],
            [['job_amphoe_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhAmphoe::className(), 'targetAttribute' => ['job_amphoe_id' => 'amphoe_id']],
            [['job_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhProvince::className(), 'targetAttribute' => ['job_province_id' => 'province_id']],
            [['job_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhJobProduct::className(), 'targetAttribute' => ['job_product_id' => 'job_product_id']],
            [['job_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhJobProductType::className(), 'targetAttribute' => ['job_pt_id' => 'job_pt_id']],
            [['job_sub_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhJobSubPt::className(), 'targetAttribute' => ['job_sub_pt_id' => 'job_sub_pt_id']],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhJob::className(), 'targetAttribute' => ['job_id' => 'job_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhUser::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [
                ['job_item_pic'], 'file',
                'skipOnEmpty' => true,
                'maxFiles' => 5,
                'extensions' => 'png,jpg,jpeg,gif',
            ]
        ];
    }

    public $upload_foler = 'uploads';
    public $picture = 'nonee.png';

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_items_id' => 'รหัสงานรับเหมา',
            'job_item_name' => 'ชื่อผลงาน',
            'job_item_pic' => 'รูปผลงาน',
            'job_item_description' => 'รายละเอียดผลงาน',
            'job_item_date_start' => 'วันที่เริ่มงาน',
            'job_item_date_end' => 'วันที่สิ้นสุดงาน',
            'job_item_level' => 'ลำดับสินค้า',
            'job_product_id' => 'ชื่องานรับเหมาาา',
            'job_price' => 'ราคา',
            'job_id' => 'ชื่อกิจการ',
            'user_id' => 'ชื่อผู้ใช้งาน',
            'job_pt_id' => 'หมวดงานรับเหมา',
            'job_sub_pt_id' => 'หมู่งานรับเหมา',
            'job_province_id' => 'ที่ตั้งจังหวัด',
            'job_amphoe_id' => 'ที่ตั้งอำเภอ',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'วันเวลาสร้าง',
            'updated_time' => 'วันเวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobAmphoe()
    {
        return $this->hasOne(MhAmphoe::className(), ['amphoe_id' => 'job_amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobProvince()
    {
        return $this->hasOne(MhProvince::className(), ['province_id' => 'job_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobProduct()
    {
        return $this->hasOne(MhJobProduct::className(), ['job_product_id' => 'job_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobPt()
    {
        return $this->hasOne(MhJobProductType::className(), ['job_pt_id' => 'job_pt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJobSubPt()
    {
        return $this->hasOne(MhJobSubPt::className(), ['job_sub_pt_id' => 'job_sub_pt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(MhJob::className(), ['job_id' => 'job_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MhUser::className(), ['user_id' => 'user_id']);
    }

    public function getAdmin()
    {
        return $this->hasOne(BackendLogin::className(), ['id' => 'updated_id']);
    }

    public function getProductTypes()
    {
        if (($model = MhJobProduct::find()) !== null) {
            return ArrayHelper::map(
                MhJobProduct::find()->all(),
                'job_product_id',
                function ($model) {
                    if ( $model['job_pt_id'] == $model['job_sub_pt_id']) return $model['job_pt_id'];
                    else return $model->jobPt->job_pt_name . ' - ' . $model->jobSubPt->job_sub_pt_name;
                }
            );
        }

        return array(-1, 'No car data');
    }

    public function getDefaultLevel() {

        $result = '';
        if (($this->job_item_level != null) && ($this->job_item_level != '')) {
            $resultcoll = MhJobItems::find()
                ->where(['job_item_level' => $this->job_item_level])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->job_item_level;
            } else {
                $result = '100';
            }
        } else {
            $result = '100';
        }
        return $result;
        
   }

   public function upload($model, $attribute)
    {
        $picture  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $picture !== null) {

            $fileName = md5($picture->baseName . time()) . '.' . $picture->extension;
            if ($picture->saveAs($path . $fileName)) {
                return $fileName;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUploadPath()
    {
        return Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/';
    }

    public function getUploadUrl()
    {
        return Yii::getAlias('../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewer()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrl() . $this->picture;
    }

    public function getUploadUrlback()
    {
        return Yii::getAlias('../../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewerback()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrlback() . $this->picture;
    }

    public function uploadMultiple($model,$attribute)
    {
        $photos  = UploadedFile::getInstances($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $photos !== null) 
        {
            $filenames = [];
            foreach ($photos as $file) 
            {
                $filename = md5($file->baseName.time()) . '.' . $file->extension;
                if($file->saveAs($path . $filename))
                {
                    $filenames[] = $filename;
                }
            }

            if($model->isNewRecord)
            {
                return implode(',', $filenames);
            }
            else
            {
                if(sizeof($filenames) == 0)
                {
                    return implode(',', (ArrayHelper::merge($filenames,$model->getOwnPhotosToArray())));
                }
                else
                {
                    return implode(',', $filenames);
                }
            }
        }

      return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getPhotosViewer()
    {
        $photos = $this->job_item_pic ? @explode(',', $this->job_item_pic) : [];
        $img = '';
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrl() . $photo, ['class' => 'img-thumbnail commany-display-mobile', 'style' => 'width:345px; height:300px;']);
        }
        return $img;
    }

    public function getPhotosViewerback()
    {
        $photos = $this->job_item_pic ? @explode(',', $this->job_item_pic) : [];
        $img = '';
        
            /*var_dump($photos);
            die();*/
        
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrlback() . $photo, ['class' => 'img-thumbnail', 'style' => 'max-width:200px;']);
        }
        return $img;

        
       
    }

    public function getOwnPhotosToArray()
    {
        return $this->getOldAttribute('job_item_pic') ? @explode(',', $this->getOldAttribute('job_item_pic')) : [];
    }


    public function getFirstPhotos()
    {
        $job_item_pic = $this->job_item_pic ? @explode(',', $this->job_item_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile ']);
        if (isset($job_item_pic[0])) {
            $img = Html::img($this->getUploadUrl() . $job_item_pic[0], ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile ']);
        }
        return $img;
    }

    public function getFirstPhotoURL()
    {
        $job_item_pic = $this->job_item_pic ? @explode(',', $this->job_item_pic) : [];
        $img = Yii::getAlias('../common/web') . '/img/none.png';
        if (isset($job_item_pic[0])) {
            $img = $this->getUploadUrl() . $job_item_pic[0];
        }
        return $img;
    }


    public function getFirstPhotosback()
    {
        $job_item_pic = $this->job_item_pic ? @explode(',', $this->job_item_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile']);
        if (isset($job_item_pic[0])) {
            $img = Html::img($this->getUploadUrlback() . $job_item_pic[0], ['class' => 'img-responsive', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotoURLback()
    {
        $job_item_pic = $this->job_item_pic ? @explode(',', $this->job_item_pic) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($job_item_pic[0])) {
            $img = $this->getUploadUrlback() . $job_item_pic[0];
        }
        return $img;
    }
}
