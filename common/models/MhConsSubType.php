<?php

namespace common\models;

use backend\models\BackendLogin;
use Yii;

/**
 * This is the model class for table "mh_cons_sub_type".
 *
 * @property int $cons_sub_type_id
 * @property string $cons_sub_type_name
 * @property int $cons_sub_pt_id
 * @property int|null $updated_id
 * @property string $created_time
 * @property string $updated_time
 *
 * @property MhConsItemsOld[] $mhConsItemsOlds
 * @property MhConsProduct[] $mhConsProducts
 * @property MhConsSubPt $consSubPt
 */
class MhConsSubType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_cons_sub_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_sub_type_name', 'cons_sub_pt_id', 'created_time'], 'required'],
            [['cons_sub_pt_id', 'updated_id', 'cons_sub_type_level'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['cons_sub_type_name'], 'string', 'max' => 255],
            [['cons_sub_pt_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhConsSubPt::className(), 'targetAttribute' => ['cons_sub_pt_id' => 'cons_sub_pt_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cons_sub_type_id' => 'รหัสประเภทย่อย',
            'cons_sub_type_name' => 'ชื่อประเภทย่อย',
            'cons_sub_pt_id' => 'ประเภทย่อยของสินค้าวัสดุก่อสร้าง',
            'cons_sub_type_level' => 'ลำดับการค้นหา',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'วันเวลาสร้าง',
            'updated_time' => 'วันเวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser_backend() {
        return $this->hasOne(BackendLogin::className(),['id'=>'updated_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsSubPt()
    {
        return $this->hasOne(MhConsSubPt::className(), ['cons_sub_pt_id' => 'cons_sub_pt_id']);
    }

    public function getDefaultLevel() {

        $result = '';
        if (($this->cons_sub_type_level != null) && ($this->cons_sub_type_level != '')) {
            $resultcoll = MhConsSubType::find()
                ->where(['cons_sub_type_level' => $this->cons_sub_type_level])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->cons_sub_type_level;
            } else {
                $result = '100';
            }
        } else {
            $result = '100';
        }
        return $result;
        
   }

}
