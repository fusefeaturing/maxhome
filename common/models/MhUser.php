<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mh_user".
 *
 * @property int $user_id รหัสผู้ใช้งาน
 * @property string $username ชื่อรหัสผู้ใช้งาน
 * @property string $password รหัสผ่าน
 * @property string $user_firstname ชื่อ
 * @property string $user_lastname นามสกุล
 * @property string $user_tel1 เบอร์ติดต่อ 1
 * @property string $user_tel2 เบอร์ติดต่อ 2
 * @property string $user_line_id Line Id
 * @property string $user_line_ad Line @
 * @property int $user_fb_id facebook_id
 * @property string $user_fb_url Facebook ส่วนตัว URL
 * @property string $user_messenger Messenger
 * @property string $user_email Email
 * @property string $user_website Website
 * @property string $user_other อื่น ๆ
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 */
class MhUser extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_user';
    }
    public $rememberMe = true;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_fb_id', 'updated_id'], 'integer'],
            [['created_time', 'updated_time'], 'safe'],
            [['username', 'user_firstname', 'user_lastname', 'user_fb_url', 'user_messenger', 'user_email', 'user_website'], 'string', 'max' => 150],
            [['password', 'user_other'], 'string', 'max' => 255],
            [['user_tel1', 'user_tel2', 'user_line_id', 'user_line_ad'], 'string', 'max' => 50],
            [['username'], 'unique'],
            //['rememberMe', 'boolean']
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => 'ชื่อรหัสผู้ใช้งาน',
            'password' => 'รหัสผ่าน',
            'user_firstname' => 'ชื่อ',
            'user_lastname' => 'นามสกุล',
            'user_tel1' => 'เบอร์ติดต่อ 1',
            'user_tel2' => 'เบอร์ติดต่อ 2',
            'user_line_id' => 'Line ID',
            'user_line_ad' => 'Line Ad',
            'user_fb_id' => 'facebook_id',
            'user_fb_url' => 'Facebook ส่วนตัว URL',
            'user_messenger' => 'Messenger',
            'user_email' => 'Email',
            'user_website' => 'Website',
            'user_other' => 'อื่น ๆ',
            'updated_id' => 'รหัสเวลาแก้ไข',
            'created_time' => 'เวลาสร้าง',
            'updated_time' => 'เวลาแก้ไข',
        ];
    }



    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        $user = self::findOne(['user_id' => $id]);
        if ($user) {
            //return new static($user->user_id);
            return $user;
        } else return null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = self::findOne(['user_fb_id' => $token]);
        if ($user) {
            return new static($user);
        } else return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->user_id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->user_fb_id;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->user_fb_id === $authKey;
    }





    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        return $this;
    }

    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
