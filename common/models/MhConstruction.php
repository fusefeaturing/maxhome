<?php

namespace common\models;

use backend\models\BackendLogin;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\Html;
use common\models\MhUser;


use Yii;

/**
 * This is the model class for table "mh_construction".
 *
 * @property int $cons_id
 * @property int $cons_search_level ลำดับการค้นหา
 * @property int $user_id ชื่อผู้ใช้
 * @property string $cons_name ชื่อกิจการ
 * @property string $cons_pic รูปกิจการ
 * @property string $cons_gps พิกัด gps
 * @property string $cons_address ที่อยู่กิจการ
 * @property string $cons_km รัศมีพื้นที่ให้บริการ
 * @property int $cons_province_id พื้นที่ให้บริการจังหวัด
 * @property int $cons_amphoe_id พื้นที่ให้บริการอำเภอ
 * @property string $cons_tel1 เบอร์ติดต่อ 1
 * @property string $cons_tel2 เบอร์ติดต่อ 2
 * @property string $cons_line_id Line Id
 * @property string $cons_line_ad Line @
 * @property string $cons_fb_fp Facebook fanpage
 * @property string $cons_messenger Messenger
 * @property string $cons_email Email
 * @property string $cons_website Website
 * @property string $cons_other อื่น ๆ
 * @property string $cons_data_other ข้อมูลเพิ่มเติม
 * @property string $cons_pt_id ประเภทสินค้าวัสดุก่อสร้าง
 * @property int $cons_sub_pt_id ประเภทย่อยของสินค้าร้านค้าก่อสร้าง
 * @property int $updated_id รหัสเวลาแก้ไข
 * @property string $created_time เวลาสร้าง
 * @property string $updated_time เวลาแก้ไข
 *
 * @property MhProvince $consProvince
 * @property MhAmphoe $consAmphoe
 * @property MhUser $user
 */
class MhConstruction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mh_construction';
    }

    public $upload_foler = 'uploads';
    public $picture = 'none.png';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_province_id', 'cons_amphoe_id', 'cons_district_id', 'updated_id', 'cons_search_level', 'cons_sub_pt_id'], 'integer'],
        [['user_id', 'cons_name', 'cons_province_id', 'cons_amphoe_id', 'cons_tel1', 'cons_pt_id', /*'cons_pic'*/], 'required'],
            [['created_time', 'updated_time'], 'safe'],
            [['cons_data_other'], 'string'],
            [['cons_name', 'cons_gps', 'cons_fb_fp', 'cons_messenger', 'cons_email', 'cons_website'], 'string', 'max' => 150],
            [['cons_address', 'cons_other', 'cons_pt_id'], 'string', 'max' => 255],
            [['cons_km'], 'string', 'max' => 30],
            [['cons_tel1', 'cons_tel2'], 'string', 'max' => 15],
            [['cons_line_id', 'cons_line_ad'], 'string', 'max' => 50],
            [['cons_province_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhProvince::className(), 'targetAttribute' => ['cons_province_id' => 'province_id']],
            [['cons_amphoe_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhAmphoe::className(), 'targetAttribute' => ['cons_amphoe_id' => 'amphoe_id']],
            [['cons_district_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhDistrict::className(), 'targetAttribute' => ['cons_district_id' => 'district_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MhUser::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [
                ['cons_pic'], 'file',
                'skipOnEmpty' => true,
                'maxFiles' => 5,
                'extensions' => 'png,jpg,jpeg,gif',
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cons_id' => 'Cons ID',
            'cons_search_level' => 'ลำดับการค้นหา',
            'user_id' => 'ชื่อผู้ใช้',
            'cons_name' => 'ชื่อกิจการ',
            'cons_pic' => 'รูปกิจการ',
            'cons_gps' => 'พิกัด gps',
            'cons_address' => 'ที่อยู่กิจการ',
            'cons_km' => 'รัศมีพื้นที่ให้บริการ',
            'cons_province_id' => 'จังหวัดที่ตั้งกิจการ',
            'cons_amphoe_id' => 'อำเภอที่ตั้งกิจการ',
            'cons_district_id' => 'รหัสไปรษณีย์',
            'cons_tel1' => 'เบอร์ติดต่อ 1',
            'cons_tel2' => 'เบอร์ติดต่อ 2',
            'cons_line_id' => 'Line ID',
            'cons_line_ad' => 'Line Ad',
            'cons_fb_fp' => 'Facebook fanpage',
            'cons_messenger' => 'Messenger',
            'cons_email' => 'Email',
            'cons_website' => 'Website',
            'cons_other' => 'อื่น ๆ',
            'cons_pt_id' => 'ประเภทสินค้าร้านค้าก่อสร้าง',
            'cons_sub_pt_id' => 'ประเภทย่อยของสินค้าร้านค้าก่อสร้าง',
            'cons_data_other' => 'รายละเอียดอื่นๆ',
            'updated_id' => 'ชื่อผู้แก้ไข',
            'created_time' => 'เวลาสร้าง',
            'updated_time' => 'เวลาแก้ไข',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsProvince()
    {
        return $this->hasOne(MhProvince::className(), ['province_id' => 'cons_province_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsAmphoe()
    {
        return $this->hasOne(MhAmphoe::className(), ['amphoe_id' => 'cons_amphoe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(MhUser::className(), ['user_id' => 'user_id']);
    }

    public function getAdmin()
    {
        return $this->hasOne(BackendLogin::className(), ['id' => 'updated_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(MhDistrict::className(), ['district_id' => 'cons_district_id']);
    }

    public function getDefaultLevel() {

        $result = '';
        if (($this->cons_search_level != null) && ($this->cons_search_level != '')) {
            $resultcoll = MhConstruction::find()
                ->where(['cons_search_level' => $this->cons_search_level])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->cons_search_level;
            } else {
                $result = '100';
            }
        } else {
            $result = '100';
        }
        return $result;
        
   }

    public function constype_text()
    {
        $separator = '<br>';

        $constypeRefArr = ArrayHelper::map(
            MhConstructionProductType::find()->asArray()->all(),
            'cons_pt_id',

            function ($model) {
                if (' - ' . $model['cons_pt_name']) return ' - ' . $model['cons_pt_name'];
                else return ' - ' . $model['cons_pt_name'];
            }
        );

        $constypeRowArr = $this->constypeToArray();

        //echo '<pre>'.var_dump($constypeRowArr).'</pre>'; exit();

        $txt = 'ไม่ได้ระบุ กรุณาสอบถามผู้ประกาศ';

        if (!empty($constypeRowArr)) {
            foreach ($constypeRowArr as $constypesingleid) {
                if (isset($constypeRefArr[$constypesingleid])) {
                    $txtarr[] = $constypeRefArr[$constypesingleid];
                }
            }

            if (isset($txtarr)) {
                $txt = implode($separator, $txtarr);
            }
        }

        return $txt;
    }

    public function constypeToArray()
    {
        return $this->cons_pt_id = explode(',', $this->cons_pt_id);
    }



    public static function itemsAlias($key)
    {

        $items = [];

        return ArrayHelper::getValue($items, $key, []);
        //return array_key_exists($key, $items) ? $items[$key] : [];
    }

    public function getItemSkill()
    {
        return self::itemsAlias('cons_pt_id');
    }




    public function pickup()
    {
        $result = '';
        if (($this->cons_province_id != null) && ($this->cons_province_id != '')) {
            $resultcoll = MhProvince::find()
                ->where(['province_id' => $this->cons_province_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->province_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function deliver()
    {
        $result = '';
        if (($this->cons_amphoe_id != null) && ($this->cons_amphoe_id != '')) {
            $resultcoll = MhAmphoe::find()
                ->where(['amphoe_id' => $this->cons_amphoe_id])
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->amphoe_name_th;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function zipcode()
    {
        $result = '';
        if (($this->cons_district_id != null) && ($this->cons_district_id != '')) {
            $resultcoll = MhDistrict::find()
                ->where(['district_id' => $this->cons_district_id])
                
                ->one();

            if ($resultcoll) {
                $result = $resultcoll->district_zip_code;
            } else {
                $result = 'ไม่ระบุ';
            }
        } else {
            $result = 'ไม่ระบุ';
        }
        return $result;
    }

    public function usercontact()
    {
        $result = '';

        $userdata = MhUser::find()
            ->where(['user_id' => $this->user_id])
            ->one();

        if ($userdata) {
            $result = $userdata->user_tel1;
        }

        return $result;
    }

    public function upload($model, $attribute)
    {
        $picture  = UploadedFile::getInstance($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $picture !== null) {

            $fileName = md5($picture->baseName . time()) . '.' . $picture->extension;
            if ($picture->saveAs($path . $fileName)) {
                return $fileName;
            }
        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getUploadPath()
    {
        return Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/';
    }

    public function getUploadUrl()
    {
        return Yii::getAlias('../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewer()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrl() . $this->picture;
    }

    public function getUploadUrlback()
    {
        return Yii::getAlias('../../common/web') . '/' . $this->upload_foler . '/';
    }

    public function getPhotoViewerback()
    {
        return empty($this->picture) ? Yii::getAlias('../../common/web') . '/img/none.png' : $this->getUploadUrlback() . $this->picture;
    }

    public function uploadMultiple($model, $attribute)
    {
        $photos  = UploadedFile::getInstances($model, $attribute);
        $path = $this->getUploadPath();
        if ($this->validate() && $photos !== null) {
            $filenames = [];
            foreach ($photos as $file) {
                $filename = md5($file->baseName . time()) . '.' . $file->extension;
                if ($file->saveAs($path . $filename)) {
                    $filenames[] = $filename;
                }
            }

            if ($model->isNewRecord) {
                return implode(',', $filenames);
            } else {
                if (sizeof($filenames) == 0) {
                    return implode(',', (ArrayHelper::merge($filenames, $model->getOwnPhotosToArray())));
                } else {
                    return implode(',', $filenames);
                }
            }
        }

        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getPhotosViewer()
    {
        $photos = $this->cons_pic ? @explode(',', $this->cons_pic) : [];
        $img = '';
        /*var_dump($photos);
            die();*/
        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrl() . $photo, ['class' => 'img-thumbnail  commany-display-mobile', 'style' => 'width:345px; height:300px;']);
        }
        return $img;
    }

    public function getPhotosViewerback()
    {
        $photos = $this->cons_pic ? @explode(',', $this->cons_pic) : [];
        $img = '';

        /*var_dump($photos);
            die();*/

        foreach ($photos as $photo) {
            $img .= ' ' . Html::img($this->getUploadUrlback() . $photo, ['class' => 'img-thumbnail', 'style' => 'max-width:200px;']);
        }
        return $img;
    }

    public function getFirstPhotos()
    {
        $cons_pic = $this->cons_pic ? @explode(',', $this->cons_pic) : [];
        $img = Html::img(Yii::getAlias('../common/web') . '/img/none.png', ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile']);
        if (isset($cons_pic[0])) {
            $img = Html::img($this->getUploadUrl() . $cons_pic[0], ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile ']);
        }

        /*var_dump($img);
            die();*/
        return $img;
    }

    public function getFirstPhotoURL()
    {
        $cons_pic = $this->cons_pic ? @explode(',', $this->cons_pic) : [];
        $img = Yii::getAlias('../common/web') . '/img/none.png';
        if (isset($cons_pic[0])) {
            $img = $this->getUploadUrl() . $cons_pic[0];
        }
        return $img;
    }


    public function getFirstPhotosback()
    {
        $cons_pic = $this->cons_pic ? @explode(',', $this->cons_pic) : [];
        $img = Html::img(Yii::getAlias('../../common/web') . '/img/none.png', ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile']);
        if (isset($cons_pic[0])) {
            $img = Html::img($this->getUploadUrlback() . $cons_pic[0], ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile']);
        }
        return $img;
    }

    public function getFirstPhotoURLback()
    {
        $cons_pic = $this->cons_pic ? @explode(',', $this->cons_pic) : [];
        $img = Yii::getAlias('../../common/web') . '/img/none.png';
        if (isset($cons_pic[0])) {
            $img = $this->getUploadUrlback() . $cons_pic[0];
        }
        return $img;
    }


    public function getOwnPhotosToArray()
    {
        return $this->getOldAttribute('cons_pic') ? @explode(',', $this->getOldAttribute('cons_pic')) : [];
    }
    
    public function location_province($province)
    {
        $result = '';

        if ($province != null) {
            $pv = MhProvince::find()
                ->where(['province_id' => $province])
                ->one();

            if ($pv) {
                $result .= $pv->province_name_th;
            }
        }

        return $result;
    }

    public function location_amphoe($amphoe)
    {
        $result = '';


        if ($amphoe != null) {
            $am = MhAmphoe::find()
                ->where(['amphoe_id' => $amphoe])
                ->one();

            if ($am) {
                $result .= ' ' . $am->amphoe_name_th;
            }
        }


        return $result;
    }
}
