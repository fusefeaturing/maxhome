<?php
return [
    'timeZone' => 'Asia/Bangkok', 
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=maxhome',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        /*'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=159.65.10.199;dbname=qqcfpdzfym',
            'username' => 'qqcfpdzfym',
            'password' => 'KqJhbRF7qg',
            'charset' => 'utf8',
        ],*/
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],

    ],

];
