<?php

namespace backend\controllers;

use Yii;
use common\models\MhJob;
use backend\models\MhJobSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use common\models\MhAmphoe;
use common\models\MhDistrict;
use common\models\MhJobProduct;
use common\models\MhJobProductType;
use common\models\MhJobRoute;
use common\models\MhJobSubPt;
use common\models\MhProvince;
use common\models\MhUser;

/**
 * MhJobController implements the CRUD actions for MhJob model.
 */
class MhJobController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $upload_foler = 'uploads';

    /**
     * Lists all MhJob models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MhJobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAddRoute($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = MhJob::findOne($id);
    
        $province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');
    
        $request = Yii::$app->request;
    
        if ($request->isPost) {
            $selectedprovince = $request->post('selectedprovince');
            $selectedamphoe = $request->post('selectedamphoe');
            $job_id = $request->post('job_id');
        
            if ($request->post('step') == 'provinceselector') {
                $redirect = 'select_amphoe';
            
                $province = ArrayHelper::map(
                    MhProvince::find()
                        ->where(['in', 'province_id', $selectedprovince])
                        ->asArray()
                        ->all(),
                    'province_id',
                    'province_name_th'
                );
            
                $amphoe= ArrayHelper::map(
                    MhAmphoe::find()
                        ->where(['in', 'province_id', $selectedprovince])
                        ->asArray()
                        ->all(),
                    'amphoe_id',
                    'amphoe_name_th'
                );
            
                $amphIDs = ArrayHelper::map(
                    MhAmphoe::find()
                        ->asArray()
                        ->all(),
                    'amphoe_id',
                    'amphoe_id'
                );
            
                $selectedamphoe = $amphIDs;
            }
        
            if ($request->post('step') == 'amphoeselector') {
                //save
                $this->saveUserRoute($job_id, $selectedamphoe);
                //redirect
                //return $this->redirect(['car/route']);
                return $this->redirect(['view', 'id' => $model->job_id]);
            }
        
            return $this->render($redirect, [
                'province' => $province,
                'amphoe' => $amphoe,
                'selectedprovince' => $selectedprovince,
                'selectedamphoe' => $selectedamphoe,
                'id' => $model->job_id,
                'model' => $model,
            ]);
        } else {
            $selectedprovince = [];
        
            return $this->render('select_province', [
                'province' => $province,
                'selectedprovince' => $selectedprovince,
                'model' => $model,
            ]);
        }
    }

    protected function saveUserRoute($job_id, $amphoeds)
    {
        //delete old
        $old_user_route = MhJobRoute::find()
            ->where(['in', 'job_id', $job_id])
            ->all();
    
        if ($old_user_route != null) {
            foreach ($old_user_route as $route) {
                $route->delete();
            }
        }
    
        //find all
        $amphoe = MhAmphoe::find()
            ->where(['in', 'amphoe_id', $amphoeds])
            ->all();
    
        //add new
        foreach ($amphoe as $amphoes) {
            $new_route = new MhJobRoute();
            $new_route->job_id = $job_id;
            $new_route->job_route_province_id = $amphoes->province_id;
            $new_route->job_route_amphoe_id = $amphoes->amphoe_id;
            $new_route->created_date = date('Y-m-d H:i:s');
            $new_route->updated_date = date('Y-m-d H:i:s');
            $new_route->save();
        
            //var_dump($new_route); echo '<br>';
        }
        //exit();
    }

    protected function getUserPermitData($id)
    {
        if (($model = MhUser::findOne($id)) !== null) {
            return $model;
        }
    
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findUserRoute($job_id)
    {
        if (($model = MhJobRoute::find()
            ->where(['in', 'job_id', $job_id])
            ->all()) !== null) {
            return $model;
        }
    }

    public function getAmphoeByProvinceID($provinceID)
    {
        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->where(['in', 'province_id', $provinceID])
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );
    
        return $amphoe;
    }

    public function getAmphoeByProvinceName($provinceName)
    {
        $province = MhProvince::find()
            ->where(['in', 'province_name_th', $provinceName])
            ->one();
    
        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->where(['in', 'province_id', $province->province_id])
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );
    
        return $amphoe;
    }

    /**
     * Displays a single MhJob model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $owner = false;
        $model = $this->findModel($id);

        $user = Yii::$app->user->identity;


        
        $user_id = Yii::$app->user->id;

        $searchModel = new MhJobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);

            $serviceroutes = ArrayHelper::map(
                MhJobRoute::find()
                    ->where(['in', 'job_id', $id])
                    ->orderBy(['job_route_province_id' => SORT_ASC])
                    ->asArray()
                    ->all(),
                'job_route_amphoe_id',
                'job_route_province_id'
            );
    
            $province = ArrayHelper::map(
                MhProvince::find()
                    ->asArray()
                    ->all(),
                'province_id',
                'province_name_th'
            );
    
            $amphoe = ArrayHelper::map(
                MhAmphoe::find()
                    ->asArray()
                    ->all(),
                'amphoe_id',
                'amphoe_name_th'
            );

        if (Yii::$app->user->isGuest) {
           
        } else {
            $user_id = Yii::$app->user->id;
            if ($model->user_id == $user_id) {
                $owner = true;
            }
        }

        return $this->render('view', [
            'model' => $model,
            'owner' => $owner,
           
            'searchModel' => $searchModel,            
            'dataProvider' => $dataProvider,            
            
            'province' => $province,
            'amphoe' => $amphoe,
            'serviceroutes' => $serviceroutes,
        ]);
    }

    /**
     * Creates a new MhJob model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MhJob();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->job_product_id) {
                $model->job_product_id = implode(",", $model->job_product_id);
            }

            if ($model->validate()) {
                $model->job_pic = $model->uploadMultiple($model, 'job_pic');
            }
            
            if ($model->save()) {
                //return $this->redirect(['view', 'id' => $model->job_id]);
                return $this->redirect(['view', 'id' => $model->job_id]);
            }
           
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MhJob model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $amphoe = ArrayHelper::map($this->getAmphoe($model->job_province_id), 'id', 'name');
        $zipcode = ArrayHelper::map($this->getZipcode($model->job_amphoe_id), 'id', 'name');
        

        $oldimage = $model->job_pic;
        $imagesexplode = explode(",", $oldimage);

        if ($model->load(Yii::$app->request->post())) {
          
            //$oldFile = Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $oldimage;


            if ($model->job_product_id) {
                $model->job_product_id = implode(",", $model->job_product_id);
            }

            if ($model->validate()) {
                
                $model->job_pic = $model->uploadMultiple($model, 'job_pic');
                
                $job_pic = $model->job_pic;
               
                //echo $photo;
                //die();

                if ($oldimage == null) {
                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->job_id]);
                    }
                } else if ( $job_pic != $oldimage ) {

                    foreach ($imagesexplode as $images) {
                        unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $images);
                    }
                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->job_id]);
                    }
               
                } else if ($job_pic == $oldimage) {

                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->job_id]);
                    }
                }
            }

        }

        return $this->render('update', [
            'model' => $model,
            'amphoe' => $amphoe,
            'zipcode' => $zipcode,
           
        ]);
    }

    /**
     * Deletes an existing MhJob model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = MhJob::findOne($id);
        $images = explode(",", $model->job_pic);


        if ($model->job_pic != null) {
            
            foreach ($images as $image) {
                unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $image);
            }
            
            $this->findModel($id)->delete();
        } else {
           
            $this->findModel($id)->delete();
            //echo 'ไม่สามารถลบข้อมูลได้';
            //die();
        }

        return $this->redirect(['index']);
    }

    public function actionDeleteall($id)
    {
        $model = $this->findModel($id);

        foreach (MhJobRoute::find($id)
        ->select('job_route_id')
        ->where('job_id')
        ->all() as $user) {
            $user->delete();
        }

        return $this->redirect(['view', 'id' => $model->job_id]);

    }

    /**
     * Finds the MhJob model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MhJob the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MhJob::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetAmphoe()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphoe($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetZipcode()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $province_id = empty($ids[0]) ? null : $ids[0];
            $amphoe_id = empty($ids[1]) ? null : $ids[1];
            if ($province_id != null) {
                $data = $this->getZipcode($amphoe_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getAmphoe($id)
    {
        $datas = MhAmphoe::find()->where(['province_id' => $id])->all();
        return $this->MapData($datas, 'amphoe_id', 'amphoe_name_th');
    }

    protected function getZipcode($id)
    {
        $datas = MhDistrict::find()->where(['amphoe_id' => $id])->all();
        return $this->MapData($datas, 'district_id', 'district_zip_code');
    }


    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }

    public function getJobTypes()
    {
        if (($model = MhJobProductType::find()) !== null) {
            return ArrayHelper::map(
                MhJobProductType::find()->asArray()->all(),
                'job_pt_id',

                function ($model) {
                    if ($model['job_pt_name'] ) return $model['job_pt_name'];
                    else return $model['job_pt_name'];
                }
            );
        }

        return array(-1, 'No car data');
    }

    public function getJobProductTypes()
    {
        if (($model = MhJobSubPt::find()) !== null) {
            return ArrayHelper::map(
                MhJobSubPt::find()
                ->orderBy([
                    'job_pt_id' => SORT_ASC
                ])
                ->all(),
                'job_sub_pt_id',
                function ($model) {
                    if ( $model['job_pt_id'] == $model['job_sub_pt_id']) return $model['job_pt_id'];
                    else return $model->jobPt->job_pt_name . ' - ' . $model->job_sub_pt_name;
                }
            );
        }

        return array(-1, 'No car data');
    }
}
