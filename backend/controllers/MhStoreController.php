<?php

namespace backend\controllers;

use Yii;
use common\models\MhStore;
use backend\models\MhStoreSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\MhStoreProductType;
use yii\helpers\ArrayHelper;
use common\models\MhAmphoe;
use yii\helpers\Json;

/**
 * MhStoreController implements the CRUD actions for MhStore model.
 */
class MhStoreController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MhStore models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MhStoreSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MhStore model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MhStore model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MhStore();

        if ($model->load(Yii::$app->request->post()) ) {
            /*if ($model->store_pt_id) {
                $model->store_pt_id = implode(",", $model->store_pt_id);
            }*/
            
             if ($model->validate()) {
                $model->store_pic = $model->uploadMultiple($model, 'store_pic');
            }

            if ($model->save()) {
                //return $this->redirect(['view', 'id' => $model->job_id]);
                return $this->redirect(['view', 'id' => $model->store_id]);
            }


            
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MhStore model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $amphoe = ArrayHelper::map($this->getAmphoe($model->store_province_id), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) ) {

            /*if ($model->store_pt_id) {
                $model->store_pt_id = implode(",", $model->store_pt_id);
            }*/

            if ($model->validate()) {
                $model->store_pic = $model->uploadMultiple($model, 'store_pic');
            }

            if ($model->save()) {
                //return $this->redirect(['view', 'id' => $model->job_id]);
                return $this->redirect(['view', 'id' => $model->store_id]);
            }

           
        }

        

        return $this->render('update', [
            'model' => $model,
            'amphoe' => $amphoe,
        ]);
    }

    /**
     * Deletes an existing MhStore model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MhStore model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MhStore the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MhStore::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function getStoreTypes()
    {
        if (($model = MhStoreProductType::find()) !== null) {
            return ArrayHelper::map(
                MhStoreProductType::find()->asArray()->all(),
                'store_pt_id',

                function ($model) {
                    if ($model['store_pt_name'] ) return $model['store_pt_name'];
                    else return $model['store_pt_name'];
                }
            );
        }

        return array(-1, 'No car data');
    }


    public function actionGetAmphoe()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphoe($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }



    protected function getAmphoe($id)
    {
        $datas = MhAmphoe::find()->where(['province_id' => $id])->all();
        return $this->MapData($datas, 'amphoe_id', 'amphoe_name_th');
    }


    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }

}
