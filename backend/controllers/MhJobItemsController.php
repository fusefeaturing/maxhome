<?php

namespace backend\controllers;

use Yii;
use common\models\MhJobItems;
use backend\models\MhJobItemsSearch;
use backend\models\MhJobProductSearch;
use backend\models\MhJobSearch;
use common\models\MhAmphoe;
use common\models\MhJob;
use common\models\MhJobProduct;
use common\models\MhJobProductType;
use common\models\MhJobSubPt;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * MhJobItemstController implements the CRUD actions for MhJobItems model.
 */
class MhJobItemsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MhJobItems models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MhJobItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionAddproductlists() {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user_id = Yii::$app->user->identity; 
        $model = new MhJobProductSearch();
        $searchModel = new MhJobProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $subpt = ArrayHelper::map($this->getSubpt($model->job_pt_id), 'id', 'name');

        $jobloops = MhJobProduct::find();                
        $pagination = new Pagination([
            'totalCount' => $jobloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $jobloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $jobloops->orderBy('')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/

        return $this->render('addproductlists', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'jobloops' => $jobloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            'subpt' => $subpt,
        ]);
    }

    public function actionUpdateproduct($id)
    {
        $model = $this->findModel($id);    

        /*$subpt = ArrayHelper::map($this->getSubpt($model->job_pt_id), 'id', 'name');
        $subtype = ArrayHelper::map($this->getSubtype($model->job_sub_pt_id), 'id', 'name');*/
        if ($model->load(Yii::$app->request->post()) ) {            

            if ($model->save()) {

                return $this->redirect(['view', 'id' => $model->job_items_id]);
            }
                
            
        }
        
        return $this->render('updateproduct', [
            'model' => $model,
            //'modelpro' => $modelpro,
            /*'subpt' => $subpt,
            'subtype' => $subtype,*/
            
        ]);
    }

    public function actionMyitemsjob($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user_id = Yii::$app->user->identity;
        
        
        $model = MhJob::findOne($id);
        $modeljob = new MhJobSearch();
        /*var_dump($id);
                die();*/
        $searchModel = new MhJobItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $amphoe = ArrayHelper::map($this->getAmphoe($modeljob->job_amphoe_id), 'id', 'name');
        $subpt = ArrayHelper::map($this->getSubpt($modeljob->job_pt_id), 'id', 'name');
        $dataProvider
            ->query
            ->andFilterWhere(['in', 'job_id', $id]);

        

            $jobloops = MhJobItems::find()
            ->where(['in', 'job_id', $id]);

                
            $pagination = new Pagination([
                'totalCount' => $jobloops->count(),
                'defaultPageSize' => 27
            ]);
    
            /*$countQuery =  clone $jobloops;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
            $modelsLimits = $jobloops->orderBy('job_item_level')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            /*var_dump($countQuery);
            die();*/

        return $this->render('my_job_items', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'jobloops' => $jobloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            'id' => $model->job_id,
            'amphoe' => $amphoe,
            'subpt' => $subpt,
            'modeljob' => $modeljob,
        ]);
    }



    /**
     * Displays a single MhJobItems model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MhJobItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new MhJobItems();
        $modelpro = MhJobProduct::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->job_items_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelpro' => $modelpro,
        ]);
    }

    /**
     * Updates an existing MhJobItems model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $subpt = ArrayHelper::map($this->getSubpt($model->job_pt_id), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->job_items_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'subpt' => $subpt,
        ]);
    }

    /**
     * Deletes an existing MhJobItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MhJobItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MhJobItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MhJobItems::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function getProductTypes()
    {
        if (($model = MhJobProduct::find()) !== null) {
            return ArrayHelper::map(
                MhJobProduct::find()->all(),
                'job_product_id',
                function ($model) {
                    if ( $model['job_pt_id'] == $model['job_sub_pt_id']) return $model['job_pt_id'];
                    else return $model->jobPt->job_pt_name . ' - ' . $model->jobSubPt->job_sub_pt_name;
                }
            );
        }

        return array(-1, 'No car data');
    }

    public function actionGetSubpt()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $job_pt_id = $parents[0];
                $out = $this->getSubpt($job_pt_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getSubpt($id)
    {
        $datas = MhJobSubPt::find()->where(['job_pt_id' => $id])->all();
        return $this->MapData($datas, 'job_sub_pt_id', 'job_sub_pt_name');
    }

    public function actionGetAmphoe()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphoe($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getAmphoe($id)
    {
        $datas = MhAmphoe::find()->where(['province_id' => $id])->orderBy('amphoe_name_th')->all();
        return $this->MapData($datas, 'amphoe_id', 'amphoe_name_th');
    }

    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }

    public function getJobTypes()
    {
        if (($model = MhJobProductType::find()) !== null) {
            return ArrayHelper::map(
                MhJobProductType::find()
                    ->orderBy([
                        'job_pt_level' => SORT_ASC
                    ])
                    ->asArray()
                    ->all(),
                'job_pt_id',

                function ($model) {
                    if ($model['job_pt_name']) return $model['job_pt_name'];
                    else return $model['job_pt_name'];
                }
            );
        }

        return array(-1, 'No car data');
    }
}
