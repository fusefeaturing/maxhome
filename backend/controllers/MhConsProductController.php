<?php

namespace backend\controllers;

use Yii;
use common\models\MhConsProduct;
use backend\models\MhConsProductSearch;
use common\models\MhConsSubPt;
use common\models\MhConsSubType;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * MhConsProductController implements the CRUD actions for MhConsProduct model.
 */
class MhConsProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MhConsProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MhConsProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MhConsProduct model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public $upload_foler = 'uploads';

    /**
     * Creates a new MhConsProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MhConsProduct();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                $model->cons_product_pic = $model->uploadMultiple($model, 'cons_product_pic');
            }
            
            if ($model->save()) {
                //return $this->redirect(['view', 'id' => $model->job_id]);
                return $this->redirect(['view', 'id' => $model->cons_product_id]);
                //return $this->redirect(['mh-job/my_job']);
            }
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MhConsProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $subpt = ArrayHelper::map($this->getSubpt($model->cons_pt_id), 'id', 'name');
        $subtype = ArrayHelper::map($this->getSubtype($model->cons_sub_pt_id), 'id', 'name');
        
        $oldimage = $model->cons_product_pic;
        $imagesexplode = explode(",", $oldimage);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                
                $model->cons_product_pic = $model->uploadMultiple($model, 'cons_product_pic');
                
                $cons_product_pic = $model->cons_product_pic;
               
                //echo $photo;
                //die();

                if ( $cons_product_pic != $oldimage ) {

                    foreach ($imagesexplode as $images) {
                        unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $images);
                    }
                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->cons_product_id]);
                    }
               
                } else if ($cons_product_pic == $oldimage) {

                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->cons_product_id]);
                    }
                }
            }

        }

        return $this->render('update', [
            'model' => $model,
            'subpt' => $subpt,
            'subtype' => $subtype,
        ]);
    }

    /**
     * Deletes an existing MhConsProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = MhConsProduct::findOne($id);
        
        $images = explode(",", $model->cons_product_pic);
        
            //$oldFile = Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $oldimage;


            if ($model->cons_product_pic != null) {
            
                foreach ($images as $image) {
                    unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $image);
                }
                
                $this->findModel($id)->delete();
            } else {
               
                $this->findModel($id)->delete();
                //echo 'ไม่สามารถลบข้อมูลได้';
                //die();
            }

        return $this->redirect(['index']);
    }

    /**
     * Finds the MhConsProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MhConsProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MhConsProduct::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetSubpt()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cons_pt_id = $parents[0];
                $out = $this->getSubpt($cons_pt_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetSuptype()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $cons_pt_id = empty($ids[0]) ? null : $ids[0];
            $cons_sup_pt_id = empty($ids[1]) ? null : $ids[1];
            if ($cons_pt_id != null) {
                $data = $this->getSuptype($cons_sup_pt_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getSubpt($id)
    {
        $datas = MhConsSubPt::find()->where(['cons_pt_id' => $id])->all();
        return $this->MapData($datas, 'cons_sub_pt_id', 'cons_sub_pt_name');
    }

    protected function getSubtype($id)
    {
        $datas = MhConsSubType::find()->where(['cons_sub_pt_id' => $id])->all();
        return $this->MapData($datas, 'cons_sub_type_id', 'cons_sub_type_name');
    }

    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }



}
