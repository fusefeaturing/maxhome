<?php

namespace backend\controllers;

use Yii;
use common\models\MhConsItems;
use backend\models\MhConsItemsSearch;
use backend\models\MhConsProductSearch;
use backend\models\MhConstructionSearch;
use common\models\MhAmphoe;
use common\models\MhConsProduct;
use common\models\MhConsSubPt;
use common\models\MhConsSubType;
use common\models\MhConstruction;
use common\models\MhConstructionProductType;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * MhConsItemsController implements the CRUD actions for MhConsItems model.
 */
class MhConsItemsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MhConsItems models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MhConsItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);




        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    public function actionAddproductlists()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user_id = Yii::$app->user->identity;
        $model = new MhConsProductSearch();
        $searchModel = new MhConsProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $subpt = ArrayHelper::map($this->getSubpt($model->cons_pt_id), 'id', 'name');

        $consloops = MhConsProduct::find();
        $pagination = new Pagination([
            'totalCount' => $consloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $consloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $consloops->orderBy('')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/

        return $this->render('addproductlists', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'consloops' => $consloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            'subpt' => $subpt,
        ]);
    }

    public function actionUpdateproduct($id)
    {
        $model = $this->findModel($id);

        /*$subpt = ArrayHelper::map($this->getSubpt($model->cons_pt_id), 'id', 'name');
        $subtype = ArrayHelper::map($this->getSubtype($model->cons_sub_pt_id), 'id', 'name');*/
        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {

                return $this->redirect(['view', 'id' => $model->cons_items_id]);
            }
        }

        return $this->render('updateproduct', [
            'model' => $model,
            //'modelpro' => $modelpro,
            /*'subpt' => $subpt,
            'subtype' => $subtype,*/

        ]);
    }

    public function actionMyitemscons($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user_id = Yii::$app->user->identity;


        $model = MhConstruction::findOne($id);
        $modelcons = new MhConstructionSearch();
        /*var_dump($id);
                die();*/
        $searchModel = new MhConsItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $amphoe = ArrayHelper::map($this->getAmphoe($modelcons->cons_amphoe_id), 'id', 'name');
        $subpt = ArrayHelper::map($this->getSubpt($modelcons->cons_pt_id), 'id', 'name');
        $dataProvider
            ->query
            ->andFilterWhere(['in', 'cons_id', $id]);

        $consloops = MhConsItems::find()
            ->where(['in', 'cons_id', $id]);


        $pagination = new Pagination([
            'totalCount' => $consloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $consloops;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $consloops->orderBy('cons_item_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
            die();*/

        return $this->render('my_cons_items', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'consloops' => $consloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            'id' => $model->cons_id,
            'amphoe' => $amphoe,
            'subpt' => $subpt,
            'modelcons' => $modelcons,
        ]);
    }

    /**
     * Displays a single MhConsItems model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MhConsItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new MhConsItems();
        $modelpro = MhConsProduct::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cons_items_id]);
        }

        return $this->render('create', [
            'model' => $model,
            'modelpro' => $modelpro,
        ]);
    }

    /**
     * Updates an existing MhConsItems model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $subpt = ArrayHelper::map($this->getSubpt($model->cons_pt_id), 'id', 'name');
        $subtype = ArrayHelper::map($this->getSubtype($model->cons_sub_pt_id), 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->cons_items_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'subpt' => $subpt,
            'subtype' => $subtype,
        ]);
    }

    /**
     * Deletes an existing MhConsItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MhConsItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MhConsItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MhConsItems::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function getProductTypes()
    {
        if (($model = MhConsProduct::find()) !== null) {
            return ArrayHelper::map(
                MhConsProduct::find()->all(),
                'cons_product_id',
                function ($model) {
                    if ($model['cons_pt_id'] == $model['cons_sub_pt_id']) return $model['cons_pt_id'];
                    else return $model->consPt->cons_pt_name . ' - ' . $model->consSubPt->cons_sub_pt_name;
                }
            );
        }

        return array(-1, 'No car data');
    }

    public function actionGetAmphoe()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphoe($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getAmphoe($id)
    {
        $datas = MhAmphoe::find()->where(['province_id' => $id])->orderBy('amphoe_name_th')->all();
        return $this->MapData($datas, 'amphoe_id', 'amphoe_name_th');
    }

    public function actionGetSubpt()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cons_pt_id = $parents[0];
                $out = $this->getSubpt($cons_pt_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetSubtype()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $cons_pt_id = empty($ids[0]) ? null : $ids[0];
            $cons_sup_pt_id = empty($ids[1]) ? null : $ids[1];
            if ($cons_pt_id != null) {
                $data = $this->getSubtype($cons_sup_pt_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getSubpt($id)
    {
        $datas = MhConsSubPt::find()->where(['cons_pt_id' => $id])->all();
        return $this->MapData($datas, 'cons_sub_pt_id', 'cons_sub_pt_name');
    }

    protected function getSubtype($id)
    {
        $datas = MhConsSubType::find()->where(['cons_sub_pt_id' => $id])->all();
        return $this->MapData($datas, 'cons_sub_type_id', 'cons_sub_type_name');
    }

    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }

    public function getConsTypes()
    {
        if (($model = MhConstructionProductType::find()) !== null) {
            return ArrayHelper::map(
                MhConstructionProductType::find()
                    ->orderBy([
                        'cons_pt_level' => SORT_ASC
                    ])
                    ->asArray()
                    ->all(),
                'cons_pt_id',

                function ($model) {
                    if ($model['cons_pt_name']) return $model['cons_pt_name'];
                    else return $model['cons_pt_name'];
                }
            );
        }

        return array(-1, 'No car data');
    }
}
