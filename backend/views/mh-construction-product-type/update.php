<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConstructionProductType */

$this->title = 'แก้ไขข้อมูลหมวดสินค้า: ' . $model->cons_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'หมวดสินค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cons_pt_name, 'url' => ['view', 'id' => $model->cons_pt_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-construction-product-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
