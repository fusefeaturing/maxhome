<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MhConstructionProductTypeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-construction-product-type-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cons_pt_id') ?>

    <?= $form->field($model, 'cons_pt_name') ?>

    <?= $form->field($model, 'updated_id') ?>

    <?= $form->field($model, 'created_time') ?>

    <?= $form->field($model, 'updated_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
