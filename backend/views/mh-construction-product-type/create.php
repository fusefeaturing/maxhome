<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConstructionProductType */

$this->title = 'เพิ่มข้อมูลหมวดสินค้า';
$this->params['breadcrumbs'][] = ['label' => 'หมวดสินค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-construction-product-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
