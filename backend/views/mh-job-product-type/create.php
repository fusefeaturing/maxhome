<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobProductType */

$this->title = 'เพิ่มข้อมูลประเภทงานรับเหมา';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลประเภทสินค้าก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-product-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
