<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobProductType */

$this->title = $model->job_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลประเภทงานรับเหมา', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-job-product-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->job_pt_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->job_pt_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'job_pt_id',
            'job_pt_name',
            'job_pt_level',
            //'updated_id',
            [
                'attribute' => 'updated_id',
                'value' => $model->user_backend->firstname
            ],
            'created_time',
            'updated_time',
        ],
    ]) ?>

</div>
