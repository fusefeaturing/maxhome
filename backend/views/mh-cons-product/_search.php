<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MhConsProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-cons-product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cons_product_id') ?>

    <?= $form->field($model, 'cons_product_name') ?>

    <?= $form->field($model, 'cons_product_description') ?>

    <?= $form->field($model, 'cons_product_pic') ?>

    <?= $form->field($model, 'cons_pt_id') ?>

    <?php // echo $form->field($model, 'cons_sub_pt_id') ?>

    <?php // echo $form->field($model, 'cons_sub_type_id') ?>

    <?php // echo $form->field($model, 'updated_id') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>

    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('รีเซ็ท', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
