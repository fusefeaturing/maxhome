<?php

use backend\models\BackendLogin;
use common\models\MhConstruction;
use common\models\MhConstructionProductType;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsProduct */
/* @var $form yii\widgets\ActiveForm */
$user_backend = ArrayHelper::map(BackendLogin::find()->asArray()->all(), 'id', 'firstname');
$consid = ArrayHelper::map(MhConstruction::find()->asArray()->all(), 'cons_id', 'cons_name');
$conspt = ArrayHelper::map(MhConstructionProductType::find()->asArray()->all(), 'cons_pt_id', 'cons_pt_name');

?>

<div class="mh-cons-product-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]);?>

    <?= $form->field($model, 'cons_product_name')->textInput(['maxlength' => true])
        ->label('ชื่อสินค้า', ['style' => 'color:red']); ?>

    <?= $form->field($model, 'cons_product_description')->textarea(['rows' => 6])
        ->label('รายละเอียดสินค้า', ['style' => 'color:red']); ?>

    <?= $form->field($model, 'cons_product_pic[]')->fileInput(['multiple' => true])
        ->label('รูปภาพสินค้า', ['style' => 'color:red']) ?>
    <div class="well">
        <?= $model->getPhotosViewerback(); ?>    
    </div>

    <?= $form->field($model, 'cons_product_price')->textInput(['maxlength' => true])
        ->label('ราคาสินค้าส่งร้าน'); ?>

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'cons_pt_id')

                    ->dropdownList($conspt, [
                        'id' => 'ddl-province',
                        'prompt' => 'เลือกประเภทสินค้า'
                    ])
                    ->label('ประเภทสินค้า', ['style' => 'color:red']);
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'cons_sub_pt_id')
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => 'ddl-amphoe'],
                        'data' => $subpt,
                        'pluginOptions' => [
                            'depends' => ['ddl-province'],
                            'placeholder' => 'เลือกประเภทสินค้าย่อย',
                            'url' => Url::to(['/mh-cons-product/get-subpt'])
                        ]
                    ])
                    ->label('ประเภทสินค้าย่อย', ['style' => 'color:red']);
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'cons_sub_type_id')
                    ->widget(DepDrop::classname(), [
                        'data' => $subtype,
                        'pluginOptions' => [
                            'depends' => ['ddl-province', 'ddl-amphoe'],
                            'placeholder' => 'เลือกประเภทสินค้าย่อยๆ',
                            'url' => Url::to(['mh-cons-product/get-subtype'])
                        ]                        
                    ])
            ?>
        </div>   
    </div>

    
    <?=
                $form
                    ->field($model, 'updated_id')                    
                    ->dropdownList($user_backend, [
                        'id' => 'ddl-roguser',
                                       
                    ])
                    
    ?>

    <?= $form->field($model, 'created_time')->textInput([
        'readonly' => true,
        'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
    ]) ?>
    
    <?= $form->field($model, 'updated_time')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
