<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsProduct */

$this->title = 'แก้ไขข้อมูลสินค้า: ' . $model->cons_product_name;
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลสินค้าวัสดุก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cons_product_id, 'url' => ['view', 'id' => $model->cons_product_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-cons-product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'subpt' => $subpt,
        'subtype' => $subtype,
    ]) ?>

</div>
