<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsProduct */

$this->title = 'เพิ่มข้อมูลสินค้า';
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลสินค้าวัสดุก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-cons-product-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'subpt' => [],
        'subtype' => [],
    ]) ?>

</div>
