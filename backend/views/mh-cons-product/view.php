<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsProduct */

$this->title = $model->cons_product_name;
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลสินค้าวัสดุก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-cons-product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->cons_product_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->cons_product_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'cons_product_id',
            'cons_product_name',
            'cons_product_description:ntext',
            //'cons_product_pic',
            [
                'attribute'=>'cons_product_pic',
                'value'=> $model->getPhotosViewerback(),
                'format' => 'raw',
            ],
            //'cons_pt_id',
            [
                'attribute' => 'cons_pt_id',
                'value' => $model->consPt->cons_pt_name,
            ],
            [
                'attribute' => 'cons_sub_pt_id',
                'value' => $model->consSubPt->cons_sub_pt_name,
            ],
            [
                'attribute' => 'cons_sub_type_id',
                'value' => $model->subtype() ,
            ],
            'cons_product_price',
            //'cons_sub_pt_id',
            //'cons_sub_type_id',
            //'updated_id',
            [
                'attribute' => 'updated_id',
                'value' => $model->admin->firstname,
            ],
            'created_time',
            'updated_time',
        ],
    ]) ?>

</div>
