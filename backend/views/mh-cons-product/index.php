<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\MhConsSubPt;
use common\models\MhConstructionProductType;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhConsProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลสินค้าวัสดุก่อสร้าง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-cons-product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'cons_product_id',
            'cons_product_name',
            'cons_product_description:ntext',
            //'cons_product_pic',
            
            [
                'attribute' => 'cons_pt_id',
                'value' => 'consPt.cons_pt_name',
                'filter'=>ArrayHelper::map(MhConstructionProductType::find()->orderBy('cons_pt_level')->asArray()->all(), 'cons_pt_id', 'cons_pt_name') 
            ],
            [
                'attribute' => 'cons_sub_pt_id',
                'value' => 'consSubPt.cons_sub_pt_name',
                'filter'=>ArrayHelper::map(MhConsSubPt::find()->asArray()->all(), 'cons_sub_pt_id', 'cons_sub_pt_name')
            ],
            /*[
                'attribute' => 'cons_sub_type_id',
                'value' => 'user.user_firstname',
            ],*/
            'cons_product_price',
           //'cons_pt_id',
           //'cons_sub_pt_id',
            //'cons_sub_type_id',
            //'updated_id',
            //'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
