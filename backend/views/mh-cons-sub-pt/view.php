<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsSubPt */

$this->title = $model->cons_sub_pt_id;
$this->params['breadcrumbs'][] = ['label' => 'หมู่สินค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-cons-sub-pt-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success  ']) ?>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->cons_sub_pt_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->cons_sub_pt_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
           // 'cons_sub_pt_id',
            'cons_sub_pt_name',
            //'cons_pt_id',
            [
                'attribute' => 'cons_pt_id',
                'value' => $model->consPt->cons_pt_name
            ],
            [
                'attribute' => 'updated_id',
                'value' => $model->user_backend->firstname
            ],
            'created_time',
            'updated_time',
        ],
    ]) ?>

</div>
