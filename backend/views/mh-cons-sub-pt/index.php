<?php

use common\models\MhConstructionProductType;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhConsSubPtSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'หมู่สินค้า';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-cons-sub-pt-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'cons_sub_pt_id',
           
            //'cons_pt_id',
            [
                'attribute' => 'cons_pt_id',
                'value' => 'consPt.cons_pt_name',
                'filter'=>ArrayHelper::map(MhConstructionProductType::find()->asArray()->all(), 'cons_pt_id', 'cons_pt_name') 
            ],
            'cons_sub_pt_name',
            'cons_sub_pt_level',
            

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
