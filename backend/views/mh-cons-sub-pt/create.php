<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsSubPt */

$this->title = 'เพิ่มข้อมูลหมู่สินค้า';
$this->params['breadcrumbs'][] = ['label' => 'หมู่สินค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-cons-sub-pt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
