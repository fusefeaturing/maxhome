<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsSubPt */

$this->title = 'แก้ไขข้อมูลหมู่สินค้า: ' . $model->cons_sub_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'หมู่สินค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cons_sub_pt_name, 'url' => ['view', 'id' => $model->cons_sub_pt_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-cons-sub-pt-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
