<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MhUserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'user_name') ?>

    <?= $form->field($model, 'user_tel1') ?>

    <?= $form->field($model, 'user_tel2') ?>

    <?= $form->field($model, 'user_line_id') ?>

    <?php // echo $form->field($model, 'user_line_ad') ?>

    <?php // echo $form->field($model, 'user_fb_url') ?>

    <?php // echo $form->field($model, 'user_messenger') ?>

    <?php // echo $form->field($model, 'user_email') ?>

    <?php // echo $form->field($model, 'user_website') ?>

    <?php // echo $form->field($model, 'user_other') ?>

    <?php // echo $form->field($model, 'updated_id') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>

    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('รีเซ็ท', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
