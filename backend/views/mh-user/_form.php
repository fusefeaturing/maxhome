<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MhUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'user_firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_tel1')->textInput() ?>

    <?= $form->field($model, 'user_tel2')->textInput() ?>

    <?= $form->field($model, 'user_line_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_line_ad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_fb_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_fb_url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_messenger')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_other')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_id')->textInput() ?>

    <?= $form->field($model, 'created_time')->textInput([
        'readonly' => true,
        'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
    ]) ?>

    <?= $form->field($model, 'updated_time')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>