<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhUser */

$this->title = 'เพิ่มข้อมูลผู้ใช้งาน';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลผู้ใช้งาน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
