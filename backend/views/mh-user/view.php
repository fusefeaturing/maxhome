<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhUser */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลผู้ใช้งาน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->user_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->user_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'username',
            'user_firstname',
            'user_lastname',
            'user_tel1',
            'user_tel2',
            'user_line_id',
            'user_line_ad',
            'user_fb_url:url',
            'user_messenger',
            'user_email:email',
            'user_website',
            'user_other',
            'updated_id',
            'created_time:datetime',
            'updated_time:datetime',
        ],
    ]) ?>

</div>