<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobRoute */

$this->title = $model->job->job_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลพื้นที่ให้บริการงานรับเหมาก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-job-route-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->job_route_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->job_route_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'job_route_id',
            
            [
                'attribute' => 'job_id',
                'value' => $model->job->job_name
            ],
            [
                'attribute' => 'job_route_province_id',
                'value' => $model->jobRouteProvince->province_name_th
            ],
            [
                'attribute' => 'job_route_amphoe_id',
                'value' => $model->jobRouteAmphoe->amphoe_name_th
            ],
            'created_date',
            'updated_date',
        ],
    ]) ?>

</div>
