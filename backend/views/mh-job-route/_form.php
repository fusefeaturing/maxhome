<?php

use common\models\MhJob;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use common\models\MhProvince;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobRoute */
/* @var $form yii\widgets\ActiveForm */
$province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');

$jobid = ArrayHelper::map(MhJob::find()->asArray()->all(), 'job_id', 'job_name');

?>

<div class="mh-job-route-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
                $form
                    ->field($model, 'job_id')
                    
                    ->dropdownList($jobid, [
                        'id' => 'ddl-roguser',
                        'prompt'=>'เลือกกิจการ'
                    ])
                    ->label('เลือกกิจการ', ['style' => 'color:red'])
                    
    ?>


<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'job_route_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
                ->label('จังหวัดที่ตั้งกิจการ', ['style' => 'color:red'])
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'job_route_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ...',
                        'url' => Url::to(['/mh-job/get-amphoe'])
                    ]
                ])
                ->label('อำเภอที่ตั้งกิจการ', ['style' => 'color:red']);
        ?>
    </div>   
</div>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'updated_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
