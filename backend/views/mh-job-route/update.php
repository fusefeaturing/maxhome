<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobRoute */

$this->title = 'แก้ไขพื้นที่ให้บริการงานรับเหมาก่อสร้าง: ' . $model->job->job_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลพื้นที่ให้บริการงานรับเหมาก่อสร้าง:', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->job->job_name, 'url' => ['view', 'id' => $model->job_route_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-job-route-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
    ]) ?>

</div>
