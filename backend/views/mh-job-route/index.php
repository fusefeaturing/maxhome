<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhJobRouteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลพื้นที่ให้บริการงานรับก่อสร้าง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-route-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'job_route_id',
            [
                'attribute' => 'job_id',
                'value' => 'job.job_name',
            ],
            [
                'attribute' => 'job_route_province_id',
                'value' => 'jobRouteProvince.province_name_th',
            ],
            [
                'attribute' => 'job_route_amphoe_id',
                'value' => 'jobRouteAmphoe.amphoe_name_th',
            ],
            'created_date',
            'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
