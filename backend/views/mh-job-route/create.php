<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobRoute */

$this->title = 'เพิ่มพื้นที่ให้บริการงานรับเหมาก่อสร้าง';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลพื้นที่ให้บริการงานรับเหมาก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-route-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
    ]) ?>

</div>
