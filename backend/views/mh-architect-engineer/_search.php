<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MhArchitectEngineerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-architect-engineer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ae_id') ?>

    <?= $form->field($model, 'ae_name') ?>

    <?= $form->field($model, 'ae_pic') ?>

    <?= $form->field($model, 'ae_gps') ?>

    <?= $form->field($model, 'ae_address') ?>

    <?php // echo $form->field($model, 'ae_km') ?>

    <?php // echo $form->field($model, 'ae_province_id') ?>

    <?php // echo $form->field($model, 'ae_amphoe_id') ?>

    <?php // echo $form->field($model, 'ae_tel1') ?>

    <?php // echo $form->field($model, 'ae_tel2') ?>

    <?php // echo $form->field($model, 'ae_line_id') ?>

    <?php // echo $form->field($model, 'ae_line_ad') ?>

    <?php // echo $form->field($model, 'ae_fb_fp') ?>

    <?php // echo $form->field($model, 'ae_messenger') ?>

    <?php // echo $form->field($model, 'ae_email') ?>

    <?php // echo $form->field($model, 'ae_website') ?>

    <?php // echo $form->field($model, 'ae_other') ?>

    <?php // echo $form->field($model, 'ae_data_other') ?>

    <?php // echo $form->field($model, 'ae_pt_id') ?>

    <?php // echo $form->field($model, 'updated_id') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>

    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('รีเซ็ท', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
