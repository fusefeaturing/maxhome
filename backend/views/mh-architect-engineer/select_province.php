<?php

use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = 'เลือกจังหวัดที่ให้บริการ';
//$userid = yii::$app->user->identity->id;

$aeid = Yii::$app->getRequest()->getQueryParam('id');
//var_dump($aeid); echo '<br>';
?>

<div class="rog-car-post-form">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>


<h1>เลือกพื้นที่ให้บริการจังหวัด</h1>

<div class="custom-control custom-checkbox">
    <input id="customCheck1" class="custom-control-input" type="checkbox" value="เลือกทั้งหมด">
    <label class="custom-control-label" for="customCheck1">เลือกทั้งหมด</label>
</div>


<hr>

    
    <?= Html::beginForm(['/mh-architect-engineer/add-route', 'id' => $model->ae_id], 'POST'); ?>
    <?= Html::hiddenInput('step', 'provinceselector'); ?>
    <?= Html::hiddenInput('ae_id', $aeid);  ?>
    <?= Html::checkboxList('selectedprovince', $selectedprovince, $province, ['class' => 'test',
   
    ]
    
    ) ?>




    <div class="form-group">
        <?= Html::submitButton('ต่อไป', ['class' => 'btn btn-primary']); ?>
    </div>
    <?= Html::endForm(); ?>


    <script type="text/javascript">
        $("#customCheck1").click(function() {
            $("input[type=checkbox]").prop("checked", $(this).prop("checked"));
        });

        $("input[type=checkbox]").click(function() {
            if (!$(this).prop("checked")) {
                $("#customCheck1").prop("checked", false);
            }
        });

        jackHarnerSig();
    
    </script>



</div>

