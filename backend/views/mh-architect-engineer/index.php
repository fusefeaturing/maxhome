<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhArchitectEngineerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลสถาปนิค / วิศวกร';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-architect-engineer-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ae_id',
            'ae_name',
            //'ae_pic',
            'ae_gps',
            'ae_address',
            //'ae_km',
            //'ae_province_id',
            //'ae_amphoe_id',
            'ae_tel1',
            //'ae_tel2',
            'ae_line_id',
            'ae_search_level',
            //'ae_line_ad',
            //'ae_fb_fp',
            //'ae_messenger',
            //'ae_email:email',
            //'ae_website',
            //'ae_other',
            //'ae_data_other',
            //'ae_pt_id',
            //'updated_id',
            //'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
