<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhArchitectEngineer */

$this->title = 'แก้ไขข้อมูลสถาปนิค / วิศวกร: ' . $model->ae_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลสถาปนิค / วิศวกร', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ae_name, 'url' => ['view', 'id' => $model->ae_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-architect-engineer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
        'zipcode' => $zipcode,
    ]) ?>

</div>
