<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhArchitectEngineer */

$this->title = 'เพิ่มข้อมูลสถาปนิค / วิศวกร';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลสถาปนิค / วิศวกร', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-architect-engineer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
        'zipcode' => [],
    ]) ?>

</div>
