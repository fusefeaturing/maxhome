<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
use common\models\MhProvince;
use backend\models\BackendLogin;
use common\models\MhAeProductType;
use common\models\MhUser;
use kartik\select2\Select2;

$user_backend = ArrayHelper::map(BackendLogin::find()->asArray()->all(), 'id', 'firstname');
$province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');
$userid = ArrayHelper::map(MhUser::find()->asArray()->all(), 'user_id', 'user_firstname');
$aetype = ArrayHelper::map(MhAeProductType::find()->asArray()->all(),'ae_pt_id', 'ae_pt_name')



/* @var $this yii\web\View */
/* @var $model common\models\MhArchitectEngineer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-architect-engineer-form">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

<!--<script type="text/javascript">
    $(document).ready(function() {

        function showTextStatus(status) {
            return status == 'true' ? "(แตะเพื่อย่อ)" : "(แตะเพื่อขยาย)";
        }

        function loopCheckedValue() {
            var selectedLanguage = []
            $('input[type="checkbox"]:checked').each(function() {

                var text = $('span#boxitem' + this.value).html();
                selectedLanguage.push(text);
            });
            return selectedLanguage;
        }

        function showCollapsHead(selected, textStat) {
            $('#divResult').html(selected.length + "\n" + "รายการ / ประเภทสินค้าที่เลือก: " + selected + textStat);
        }

        function event(type) {
            var stat = $("#divResult[aria-expanded]").attr('aria-expanded') == "true";
            var stringStat = type == "exp" ? showTextStatus(""+!stat+"") : showTextStatus(""+stat+"");
            var selected = loopCheckedValue();
            
            if (type == "init") {
                if (selected.length != 0)  { showCollapsHead(selected, stringStat); } 
                else { return; }
            } else { showCollapsHead(selected, stringStat); }  
        }

        $("#divResult[aria-expanded]").click(function() {
            event("exp")
        });

        $('input[type="checkbox"]').click(function() {
            event("chk")
        });
        event("init")
        
    });

</script>-->

<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data']
]);?>

<?= $form->field($model, 'ae_search_level')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ae_name')->textInput(['maxlength' => true])->label('ชื่อกิจการ', ['style' => 'color:red']) ?>

    <?=
                $form
                    ->field($model, 'user_id')
                    ->widget(Select2::classname(), [
                        'data' => $userid,
                        'language' => 'th',
                        'options' => [/*'multiple' => true,*/ 'placeholder' => 'เลือกผู้ใช้งาน'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],                    
                ])    
                ->label('เลือกผู้ใช้งาน', ['style' => 'color:red']);       
                 
            ?>

    <?='' //$form->field($model, 'ae_pic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ae_pic[]')->fileInput(['multiple' => true])->label('รูปถ่ายกิจการ', ['style' => 'color:red']) ?>
    <div class="well">
        <?= $model->getPhotosViewerback(); ?>
        
    </div>

    <?= $form->field($model, 'ae_gps')->textInput(['maxlength' => true]) ?>

    

    <?= $form->field($model, 'ae_km')->textInput() ?>

    <?='' ///$form->field($model, 'ae_province_id')->textInput() ?>

    <?='' //$form->field($model, 'ae_amphoe_id')->textInput() ?>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'ae_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
                ->label('จังหวัดที่ตั้งกิจการ', ['style' => 'color:red'])
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'ae_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ...',
                        'url' => Url::to(['/mh-architect-engineer/get-amphoe'])
                    ]
                ])->label('อำเภอที่ตั้งกิจการ', ['style' => 'color:red']);
        ?>
    </div>   

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'ae_district_id')
                ->widget(DepDrop::classname(), [
                    'data' => $zipcode,
                    'pluginOptions' => [
                        'depends' => ['ddl-province', 'ddl-amphoe'],
                        'placeholder' => 'เลือกรหัสไปรษณีย์...',
                        'url' => Url::to(['mh-architect-engineer/get-zipcode'])
                    ]
                ])
                ->label('รหัสไปรษณีย์');
        ?>
    </div> 
</div>

    <?= $form->field($model, 'ae_address')
    ->textInput(['maxlength' => true])
    ->input('', ['placeholder' => "ตัวอย่าง 1/2 ม. 3 ต. 4"])
    ->label('ที่อยู่กิจการ')
     ?>

    <?= $form->field($model, 'ae_tel1')->textInput()->label('เบอร์ติดต่อ 1', ['style' => 'color:red']) ?>

    <?= $form->field($model, 'ae_tel2')->textInput() ?>

    <?= $form->field($model, 'ae_line_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ae_line_ad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ae_fb_fp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ae_messenger')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ae_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ae_website')->textInput(['maxlength' => true]) ?>

    <?='' //$form->field($model, 'ae_other')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ae_data_other')->textarea(['rows' => 6]) ?>

   

    
    <!--<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div id="w1" class="panel-group collapse in" aria-expanded="true" style="">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">

                            <a class="collapse-toggle" href="#w1-collapse1" data-toggle="collapse" data-parent="#w1" aria-expanded="false"  class="divResult" id="divResult">เลือกประเภทรถขนส่ง (แตะเพื่อขยาย)</a>


                        </h4>
                    </div>

                    <div id="w1-collapse1" class="panel-collapse collapse" style="">
                        <div class="panel-body" id="divR"> 
                            <?=

                                $form
                                    ->field($model, 'ae_pt_id')
                                    ->checkBoxList(
                                        $this->context->getaeTypes(),
                                        [
                                            'separator' => '<br>',
                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                $checked = $checked ? 'checked' : '';
                                                return '<div> <span id="boxitem' . $value . '"> <input type="checkbox" value=' . $value . ' name=' . $name . ' ' . $checked  . '>' . $label . '</span></div>';
                                            }
                                        ]
                                    )
                                    
                                    ->label(false)
                                    ->hint('เลือกได้หลายรายการ')

                            ?>

                        </div>
                    </div>




                </div>
            </div>

        </div>
    </div>-->


    
    
    <?=$form->field($model, 'ae_pt_id')->checkboxList($model->getItemSkill())->label(false) ?>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <strong><h4 style="color:red">เลือกประเภทงานออกแบบเขียนแบบ</h4></strong>
            <div class="custom-control custom-checkbox">
                <input id="customCheck1" class="custom-control-input" type="checkbox" >
                <label class="custom-control-label" style="color:green" for="customCheck1"><strong>เลือกทั้งหมด</strong></label>
            </div>
            <p></p>
            <?=
                $form
                    ->field($model, 'ae_pt_id')
                    ->checkBoxList(
                        $this->context->getAeTypes(),
                        ['itemOptions' => $model->aetypeToArray(), 'separator' => '<br>']
                    )
                    ->hint(Html::a('คลิกดูประเภทรถ', ['mh-job-product-type/index'], ['target' => '_blank', 'class' => 'linksWithTarget']))
                    ->label(false)
                    ->hint('เลือกได้หลายรายการ')
                    
            ?>
        </div>
    </div>

    <!--<div>
                       <?= $form->field($model, 'ae_pt_id')->checkboxList($this->context->getAeTypes(), [
                                   'separator' => '<br>',
                                   'item' => function ($index, $label, $name, $checked, $value) {
                                       $checked = $checked ? 'checked' : '';
                                       return '<div><input type="checkbox" value=' . $value . ' name=' . $name . ' ' . $checked  . '>' . $label . '</div>';
                                   }
                               ])
                               ->label(false);    
                        ?>

    </div>-->

    

    <?='' //$form->field($model, 'updated_id')->textInput() ?>

    <?=
                $form
                    ->field($model, 'updated_id')
                    
                    ->dropdownList($user_backend, [
                        'id' => 'ddl-roguser',
                        'prompt' => ''
                        
                    ])
                    
    ?>

    <?= $form->field($model, 'created_time')->textInput([
        'readonly' => true,
        'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
    ]) ?>
    
    <?= $form->field($model, 'updated_time')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <script type="text/javascript">
        $("#customCheck1").click(function() {
            $("input[type=checkbox]").prop("checked", $(this).prop("checked"));
        });

        $("input[type=checkbox]").click(function() {
            if (!$(this).prop("checked")) {
                $("#customCheck1").prop("checked", false);
            }
        });

        jackHarnerSig();
    
    </script>


</div>
