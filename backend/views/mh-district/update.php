<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhDistrict */

$this->title = 'แก้ไขข้อมูลตำบล: ' . $model->district_name_th;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลตำบล', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->district_name_th, 'url' => ['view', 'id' => $model->district_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-district-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
