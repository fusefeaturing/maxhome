<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BackendLogin */

$this->title = 'เพิ่มผู้ใช้งานเบื้องหลัง';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลผู้ใช้งานเบื้องหลัง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backend-login-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
