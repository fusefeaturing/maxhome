<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BackendLogin */

$this->title = 'แก้ไข้อมูลผู้ใช้งานเบื้องหลัง: ' . $model->firstname;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลผู้ใช้งานเบื้องหลัง', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->firstname, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แกไข';
?>
<div class="backend-login-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
