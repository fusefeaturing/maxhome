<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhAeRouteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลพื้นที่ให้บริการสถาปนิค / วิศวกร';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-ae-route-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'ae_route_id',
            [
                'attribute' => 'ae_id',
                'value' => 'ae.ae_name',
            ],
            [
                'attribute' => 'ae_route_province_id',
                'value' => 'aeRouteProvince.province_name_th',
            ],
            [
                'attribute' => 'ae_route_amphoe_id',
                'value' => 'aeRouteAmphoe.amphoe_name_th',
            ],
            'created_date',
            //'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
