<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhAeRoute */

$this->title = 'เพิ่มพื้นที่ให้บริการสถาปนิค / วิศวกร';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลพื้นที่ให้บริการสถาปนิค / วิศวกร', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-ae-route-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
    ]) ?>

</div>
