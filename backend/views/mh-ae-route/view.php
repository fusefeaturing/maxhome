<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhAeRoute */

$this->title = $model->ae->ae_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลพื้นที่ให้บริการสถาปนิค / วิศวกร', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-ae-route-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->ae_route_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->ae_route_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ae_route_id',
            [
                'attribute' => 'ae_id',
                'value' => $model->ae->ae_name
            ],
            
            [
                'attribute' => 'ae_route_province_id',
                'value' => $model->aeRouteProvince->province_name_th
            ],
            [
                'attribute' => 'ae_route_amphoe_id',
                'value' => $model->aeRouteAmphoe->amphoe_name_th
            ],
            'created_date',
            'updated_date',
        ],
    ]) ?>

</div>
