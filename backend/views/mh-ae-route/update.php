<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhAeRoute */

$this->title = 'แก้ไขพื้นที่ให้บริการสถาปนิค / วิศวกร: ' . $model->ae->ae_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลพื้นที่ให้บริการสถาปนิค / วิศวกร', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ae->ae_name, 'url' => ['view', 'id' => $model->ae_route_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-ae-route-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
    ]) ?>

</div>
