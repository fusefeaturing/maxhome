<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhStoreProductType */

$this->title = 'เพิ่มข้อมูลประเภทสินค้าร้านค้า';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลประเภทสินค้าร้านค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-store-product-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
