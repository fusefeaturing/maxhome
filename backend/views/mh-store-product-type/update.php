<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhStoreProductType */

$this->title = 'แก้ไขข้อมูลประเภทสินค้าร้านค้า: ' . $model->store_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลประเภทสินค้าร้านค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->store_pt_name, 'url' => ['view', 'id' => $model->store_pt_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-store-product-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
