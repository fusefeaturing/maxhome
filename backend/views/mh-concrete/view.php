<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhConcrete */

$this->title = 'สั่งคอนกรีต';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลคอนกรีต', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-concrete-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->con_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->con_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <h1><?= Html::encode($this->title) ?></h1>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <p><?= $model->getPhotosViewerback() ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>รหัสรายละเอียดคอนกรีต</strong> : <?= $model->con_id ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>รายละเอียดคอนกรีต</strong> : <?= $model->con_desciption_product ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>กำลังแรงอัด</strong> : <?= $model->con_strength ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>ปริมาณ</strong> : <?= $model->con_desciption_product ?> <?= $model->con_unit ?></p>
                </div>
                <!--<div class="col-xs-12 col-sm-12 col-md-12">
                            <p><strong>Cube</strong> : <?= $model->con_cube ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                            <p><strong>Cylender</strong> : <?= $model->con_cylender ?></p>
                </div>-->
                <div class="col-xs-12 col-sm-12 col-md-12">
                            <p><strong>พิกัด GPS</strong> : <?= $model->con_gps_transpot ?></p>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>พื้นที่ให้บริการจังหวัด</strong> : <?= $model->location_province($model->con_province_id) ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>พื้นที่ให้บริการอำเภอ</strong> : <?= $model->location_amphoe($model->con_amphoe_id) ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>พื้นที่ให้บริการตำบล</strong> : <?= $model->location_district($model->con_district_id) ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>วันที่</strong> : <?= $model->con_datetime ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>ช่วงเวลา</strong> : <?= $model->con_duration ?></p>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>ชื่อติดต่อ</strong> : <?= $model->con_name_contact ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>เบอร์ติดต่อ</strong> : <?= $model->con_tel_contact ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>อื่น ๆ</strong> : <?= $model->con_other ?></p>
                </div>







                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>ชื่อผู้แก้ไข</strong> : <?= $model->updated_id ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>เวลาสร้าง</strong> : <?= $model->created_time ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p><strong>เวลาแก้ไข</strong> : <?= $model->updated_time ?></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <!-- Your share button code -->
                    <div class="fb-share-button" 
                        data-size="large"
                        data-href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>"
                        data-layout="button_count">
                    </div>
                    <div class="line-it-button" 
                        data-lang="en" 
                        data-type="share-a" 
                        data-ver="2" 
                        data-url="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                        style="display: none;">
                    </div>
                </div>      
                </div>



    <?='' /*DetailView::widget([
        'model' => $model,
        'attributes' => [
            'con_id',
            'user_id',
            'con_desciption_product',
            'con_strength',
            'con_volume',
            'con_unit',
            'con_cube',
            'con_cylender',
            'con_gps_transpot',
            'con_province_id',
            'con_amphoe_id',
            'con_district_id',
            'con_pic_map',
            'con_datetime',
            'con_duration',
            'con_name_contact',
            'con_tel_contact',
            'con_other',
            'updated_id',
            'created_time',
            'updated_time',
        ],
    ])*/ ?>

</div>
