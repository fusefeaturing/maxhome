<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConcrete */

$this->title = 'แก้ไขข้อมูลคอนกรีต: ' . $model->con_id;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลคอนกรีต', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->con_id, 'url' => ['view', 'id' => $model->con_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-concrete-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
        'district' => $district,
    ]) ?>

</div>
