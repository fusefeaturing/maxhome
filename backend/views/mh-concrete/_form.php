<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
use common\models\MhProvince;
use yii\jui\DatePicker;
use backend\models\BackendLogin;
use common\models\MhUser;

/* @var $this yii\web\View */
/* @var $model common\models\MhConcrete */
/* @var $form yii\widgets\ActiveForm */


$user_backend = ArrayHelper::map(BackendLogin::find()->asArray()->all(), 'id', 'firstname');
$province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');
$userid = ArrayHelper::map(MhUser::find()->asArray()->all(), 'user_id', 'user_firstname');

?>

<div class="mh-concrete-form">

<?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'con_desciption_product')->textInput(['maxlength' => true]) ?>

    <?=
                $form
                    ->field($model, 'user_id')
                    
                    ->dropdownList($userid, [
                        'id' => 'ddl-roguser',
                        
                    ])
                    
    ?>

    <?= $form->field($model, 'con_strength')->textInput() ?>

    <?= $form->field($model, 'con_volume')->textInput() ?>

    <?= $form->field($model, 'con_unit')->radioList([ 1 => 'cube', 2 => 'cylender', ])
    ->label('หน่วย', ['style' => 'color:red']) ?>

    <?='' //$form->field($model, 'con_cube')->radio(['cube' => 'cube']) ?>

    <?='' //$form->field($model, 'con_cylender')->radio([0 => 'cylender']) ?>

    <?= $form->field($model, 'con_gps_transpot')->textInput(['maxlength' => true]) ?>

    

    <?='' //$form->field($model, 'con_province_id')->textInput() ?>
   
    <?='' //$form->field($model, 'con_amphoe_id')->textInput() ?>
   
    <?='' //$form->field($model, 'con_district_id')->textInput() ?>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'con_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'con_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ...',
                        'url' => Url::to(['/mh-concrete/get-amphoe'])
                    ]
                ]);
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'con_district_id')
                ->widget(DepDrop::classname(), [
                    'data' => $district,
                    'pluginOptions' => [
                        'depends' => ['ddl-province', 'ddl-amphoe'],
                        'placeholder' => 'เลือกตำบล...',
                        'url' => Url::to(['mh-concrete/get-district'])
                    ]
                ]);
        ?>
    </div>   
</div> 

    <?= $form->field($model, 'con_pic_map[]')->fileInput(['multiple' => true]) ?>
    <div class="well">
        <?= $model->getPhotosViewerback(); ?>
    </div>

    <?='' //$form->field($model, 'con_datetime')->textInput() ?>

    
    <div class="">
            <?= $form
                ->field($model, 'con_datetime')
                ->widget(DatePicker::classname(), [
                    'language' => 'th',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                    ],
                    'options' => ['class' => 'form-control']
                ])
            ?>
    </div>

    <?= $form->field($model, 'con_duration')->radioList([ 1 => 'เช้า', 2 => 'บ่าย', ])
    ->label('ช่วงเวลาที่ต้องการส่ง', ['style' => 'color:red']) ?>

    <?= $form->field($model, 'con_name_contact')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'con_tel_contact')->textInput() ?>

    <?= $form->field($model, 'con_other')->textInput(['maxlength' => true]) ?>

    <?='' //$form->field($model, 'updated_id')->textInput() ?>

    <?=
                $form
                    ->field($model, 'updated_id')
                    
                    ->dropdownList($user_backend, [
                        'id' => 'ddl-roguser',
                        
                    ])
                    
    ?>

    <?= $form->field($model, 'created_time')->textInput([
        'readonly' => true,
        'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
    ]) ?>
    
    <?= $form->field($model, 'updated_time')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
