<?php
use backend\models\BackendLogin;
use common\models\MhJobProductType;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\MhUser;
use kartik\widgets\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobProduct */
/* @var $form yii\widgets\ActiveForm */
$userid = ArrayHelper::map(MhUser::find()->asArray()->all(), 'user_id', 'user_firstname');

$user_backend = ArrayHelper::map(BackendLogin::find()->asArray()->all(), 'id', 'firstname');
$jobpt = ArrayHelper::map(MhJobProductType::find()->asArray()->all(), 'job_pt_id', 'job_pt_name');
?>

<div class="mh-job-product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'job_product_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'job_product_description')->textarea(['rows' => 6]) ?>

  

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'job_pt_id')

                    ->dropdownList($jobpt, [
                        'id' => 'ddl-job_pt_id',
                        'prompt' => 'เลือกประเภท'
                    ])
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'job_sub_pt_id')
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => 'ddl-subpt'],
                        'data' => $subpt,
                        'pluginOptions' => [
                            'depends' => ['ddl-job_pt_id'],
                            'placeholder' => 'เลือกประเภทย่อย',
                            'url' => Url::to(['/mh-job-product/get-subpt'])
                        ]
                    ]);
            ?>
        </div>
    </div>

    

    <?=
                $form
                    ->field($model, 'updated_id')                    
                    ->dropdownList($user_backend, [
                        'id' => 'ddl-roguser',
                                       
                    ])
                    
    ?>

    <?= $form->field($model, 'created_time')->textInput([
        'readonly' => true,
        'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
    ]) ?>
    
    <?= $form->field($model, 'updated_time')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
