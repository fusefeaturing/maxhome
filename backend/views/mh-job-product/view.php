<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobProduct */

$this->title = $model->job_product_name;
$this->params['breadcrumbs'][] = ['label' => 'งานรับเหมาก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-job-product-view">

    <h1>ชื่องานรับเหมา : <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->job_product_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->job_product_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'job_product_id',
            'job_product_name',
            'job_product_description:ntext',
            //'job_pt_id',
            //'job_sub_pt_id',
            [
                'attribute' => 'job_pt_id',
                'value' => $model->jobPt->job_pt_name,
            ],
            /*[
                'attribute' => 'job_sub_pt_id',
                'value' => $model->jobSubPt->job_sub_pt_name,
            ],*/
            //'updated_id',
            [
                'attribute' => 'updated_id',
                'value' => $model->admin->firstname,
            ],
            'created_time',
            'updated_time',
        ],
    ]) ?>

</div>
