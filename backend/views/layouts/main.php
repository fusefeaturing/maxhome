<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        //['label' => 'หน้าหลัก', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {


        
        $menuItems[] = ['label' => 'เข้าสู่ระบบ', 'url' => ['/site/login']];
    } else {

        /*$menuItems[]=['label' => ('การจัดการข้อมูลกิจการ'),
        'items' => [

                    ['label' => 'ข้อมูลร้านค้า', 'url' => ['/mh-store/index']],
                    ['label' => 'ดูบทความ', 'url' => ['/site/contact']],
                    ['label' => 'ข้อมูลผู้รับเหมาก่อสร้าง', 'url' => ['/mh-job/index']],
                    ['label' => 'ข้อมูลร้านวัสดุก่อสร้าง', 'url' => ['/mh-construction/index']],
                    ['label' => 'ข้อมูลสินค้าร้านวัสดุก่อสร้าง', 'url' => ['/mh-cons-items-old/index']],
                    ['label' => 'ข้อมูลสถาปนิก / วิศวกร', 'url' => ['/mh-architect-engineer/index']],
                    ['label' => 'ข้อมูลคอนกรีต', 'url' => ['/mh-concrete/index']],                   

                    ],
                ];*/


        $menuItems[]=['label' => ('การจัดการผู้รับเหมาก่อสร้าง'),
        'items' => [

                    ['label' => 'ข้อมูลผู้รับเหมาก่อสร้าง', 'url' => ['/mh-job/index']],
                    ['label' => ('กลุ่มงานผู้รับเหมา'), 'url' => ['/mh-job-product-type/index']],
                    ['label' => ('ชนิดงานผู้รับเหมา'), 'url' => ['/mh-job-sub-pt/index']],
                    //['label' => ('การจัดการข้อมูลประเภทย่อย'), 'url' => ['/mh-cons-sub-type/index']],        
                    //['label' => 'ข้อมูลสินค้า', 'url' => ['/mh-job-product/index']],
                    
                    ['label' => ('หมวดหมู่ประเภทงานรับเหมาก่อสร้าง'), 'url' => ['/mh-job-type/index']],
                    ['label' => ('พื้นที่ให้บริการงานรับเหมาก่อสร้าง'), 'url' => ['/mh-job-route/index']],                   
        
                    ],
                ];


        $menuItems[]=['label' => ('การจัดการร้านค้าวัสดุก่อสร้าง'),
        'items' => [

                    ['label' => 'ข้อมูลร้านวัสดุก่อสร้าง', 'url' => ['/mh-construction/index']],
                    ['label' => ('หมวดสินค้า'), 'url' => ['/mh-construction-product-type/index']],
                    ['label' => ('หมู่สินค้า'), 'url' => ['/mh-cons-sub-pt/index']],
                    ['label' => ('การจัดการข้อมูลประเภทย่อย'), 'url' => ['/mh-cons-sub-type/index']],
                    ['label' => 'ข้อมูลสินค้า', 'url' => ['/mh-cons-product/index']],
                    ['label' => 'ข้อมูลสินค้าของผู้ใช้', 'url' => ['/mh-cons-items/index']],   
                    
                    ['label' => ('การจัดการข้อมูลพื้นที่ให้บริการร้านค้าวัสดุก่อสร้าง'), 'url' => ['/mh-cons-route/index']],
        
                    ],
                ];


        $menuItems[]=['label' => ('การจัดการสถาปนิก / วิศวกร'),
        'items' => [
                    ['label' => 'ข้อมูลสถาปนิก / วิศวกร', 'url' => ['/mh-architect-engineer/index']],
                    ['label' => ('การจัดการข้อมูลประเภทงานออกแบบเขียนแบบ'), 'url' => ['/mh-ae-product-type/index']],       
                    ['label' => ('การจัดการข้อมูลพื้นที่ให้บริการสถาปนิก / วิศวกร'), 'url' => ['/mh-ae-route/index']],
                    
                    //['label' => ('การจัดการข้อมูลประเภทย่อยของสินค้าวัสดุก่อสร้าง'), 'url' => ['/mh-cons-sub-pt/index']],
                    //['label' => ('การจัดการข้อมูลประเภทย่อย'), 'url' => ['/mh-cons-sub-type/index']],
                    

        
                    ],
                ];

       
        


        $menuItems[]=['label' => ('การจัดการข้อมูลพื้นฐาน'),
        'items' => [
            ['label' => 'ข้อมูลคอนกรีต', 'url' => ['/mh-concrete/index']], 
            ['label' => ('การจัดการข้อมูลผู้ใช้งาน'), 'url' => ['/mh-user/index']],
            ['label' => ('การจัดการข้อมูลผู้ใช้งานเบื้องหลัง'), 'url' => ['/backendlogin/index']],   
            ['label' => ('การจัดการข้อมูลจังหวัด'), 'url' => ['/mh-province/index']],
            ['label' => ('การจัดการข้อมูลอำเภอ'), 'url' => ['/mh-amphoe/index']],
            ['label' => ('การจัดการข้อมูลตำบล'), 'url' => ['/mh-district/index']],
            ],
        ];

        

        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'ออกจากระบบ (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
