<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhStore */

$this->title = 'แก้ไขข้อมูลร้านค้า: ' . $model->store_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลร้านค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->store_name, 'url' => ['view', 'id' => $model->store_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-store-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
    ]) ?>

</div>
