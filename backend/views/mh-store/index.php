<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhStoreSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการร้านค้า';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-store-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'store_id',
            'user_id',
            'store_name',
            'store_pic',
            'store_address',
            //'store_gps',
            //'store_km',
            //'store_province_id',
            //'store_amphoe_id',
            //'store_tel1',
            //'store_tel2',
            //'store_line_id',
            //'store_line_ad',
            //'store_fb_fp',
            //'store_messenger',
            //'store_email:email',
            //'store_website',
            //'store_other',
            //'store_data_other',
            //'updated_id',
            //'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
