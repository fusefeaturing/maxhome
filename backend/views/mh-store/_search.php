<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MhStoreSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-store-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'store_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'store_name') ?>

    <?= $form->field($model, 'store_pic') ?>

    <?= $form->field($model, 'store_address') ?>

    <?php // echo $form->field($model, 'store_gps') ?>

    <?php // echo $form->field($model, 'store_km') ?>

    <?php // echo $form->field($model, 'store_province_id') ?>

    <?php // echo $form->field($model, 'store_amphoe_id') ?>

    <?php // echo $form->field($model, 'store_tel1') ?>

    <?php // echo $form->field($model, 'store_tel2') ?>

    <?php // echo $form->field($model, 'store_line_id') ?>

    <?php // echo $form->field($model, 'store_line_ad') ?>

    <?php // echo $form->field($model, 'store_fb_fp') ?>

    <?php // echo $form->field($model, 'store_messenger') ?>

    <?php // echo $form->field($model, 'store_email') ?>

    <?php // echo $form->field($model, 'store_website') ?>

    <?php // echo $form->field($model, 'store_other') ?>

    <?php // echo $form->field($model, 'store_data_other') ?>

    <?php // echo $form->field($model, 'updated_id') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
