<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhStore */

$this->title = 'เพิ่มข้อมูลร้านค้า';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลร้านค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-store-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
    ]) ?>

</div>
