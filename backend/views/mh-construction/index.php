<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhConstructionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการร้านค้าวัสดุก่อสร้าง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-construction-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'cons_id',
            'cons_name',
            //'cons_pic',
            'cons_gps',
            'cons_address',
            //'cons_km',
            //'cons_province_id',
            //'cons_amphoe_id',
            'cons_tel1',
            'cons_search_level',
            //'cons_tel2',
            //'cons_line_id',
            //'cons_line_ad',
            //'cons_fb_fp',
            //'cons_messenger',
            //'cons_email:email',
            //'cons_website',
            //'cons_other',
            //'cons_pt_id',
            //'cons_data_other',
            //'updated_id',
            //'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
