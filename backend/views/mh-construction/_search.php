<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MhConstructionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-construction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cons_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'cons_name') ?>

    <?= $form->field($model, 'cons_pic') ?>

    <?= $form->field($model, 'cons_gps') ?>

    <?php // echo $form->field($model, 'cons_address') ?>

    <?php // echo $form->field($model, 'cons_km') ?>

    <?php // echo $form->field($model, 'cons_province_id') ?>

    <?php // echo $form->field($model, 'cons_amphoe_id') ?>

    <?php // echo $form->field($model, 'cons_tel1') ?>

    <?php // echo $form->field($model, 'cons_tel2') ?>

    <?php // echo $form->field($model, 'cons_line_id') ?>

    <?php // echo $form->field($model, 'cons_line_ad') ?>

    <?php // echo $form->field($model, 'cons_fb_fp') ?>

    <?php // echo $form->field($model, 'cons_messenger') ?>

    <?php // echo $form->field($model, 'cons_email') ?>

    <?php // echo $form->field($model, 'cons_website') ?>

    <?php // echo $form->field($model, 'cons_other') ?>

    <?php // echo $form->field($model, 'cons_data_other') ?>

    <?php // echo $form->field($model, 'cons_pt_id') ?>

    <?php // echo $form->field($model, 'updated_id') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
