<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConstruction */

$this->title = 'เพิ่มร้านวัสดุก่อสร้าง';
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลร้านวัสดุก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-construction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
        'zipcode' => [],
    ]) ?>

</div>
