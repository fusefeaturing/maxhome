<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConstruction */

$this->title = 'แก้ไขข้อมูลร้านวัสดุก่อสร้าง: ' . $model->cons_name;
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลร้านวัสดุก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cons_name, 'url' => ['view', 'id' => $model->cons_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-construction-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
        'zipcode' => $zipcode,
    ]) ?>

</div>
