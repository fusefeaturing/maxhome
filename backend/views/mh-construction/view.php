<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhConstruction */

$this->title = $model->cons_name;
$this->params['breadcrumbs'][] = ['label' => 'รายการร้านวัสดุก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$consid = Yii::$app->getRequest()->getQueryParam('id');
\yii\web\YiiAsset::register($this);

//echo $consid;
?>
<div class="mh-construction-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success ']) ?>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->cons_id], ['class' => 'btn btn-primary ']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->cons_id], [
            'class' => 'btn btn-danger  ',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('+ ระบุพื้นที่ฉันให้บริการ', ['add-route', 'id' => $model->cons_id], ['class' => 'btn btn-info ']); ?>
        <?= Html::a('+ เพิ่มสินค้าร้านวัสดุก่อสร้าง', ['mh-cons-items/addproductlists', 'id' => $consid], ['class' => 'btn btn-success ']) ?>
        <?= Html::a('ดูสินค้าของร้านสำหรับเจ้าของกิจการ', ['mh-cons-items/myitemscons', 'id' => $consid], ['class' => 'btn btn-primary ']) ?>

    </p>

    <div class="panel panel-default" style="padding-top:30px">
        <div class="text-center ">
            <div class="gImg ">
                <p class="card-img-top"><?= $model->getPhotosViewerback() ?></p>
            </div>
        </div>
    </div>

    <div class="panel panel-default">

        <div class="panel-body">
            <h3 class="card-title"><strong>ชื่อกิจการ : <?= $model->cons_name ?></strong></h3>
            <p class="card-text"><strong>ที่อยู่กิจการ</strong> : <?= $model->cons_address ?> อ. <?= $model->deliver() ?> จ. <?= $model->pickup() ?> <?= $model->zipcode() ?></p>
            <!--<p class="card-text"><strong>จังหวัดที่ตั้งกิจการ</strong> : <?= $model->pickup() ?></p>
            <p class="card-text"><strong>อำเภอที่ตั้งกิจการ</strong> : <?= $model->deliver() ?></p>-->
            <p class="card-text"><strong>พิกัด GPS</strong> : <?= $model->cons_gps ?></p>
            <p class="card-text"><strong>รัศมีพื้นที่ให้บริการ</strong> : <?= $model->cons_km ?> กิโลเมตร</p>
            <p class="card-text"><strong>ประเภทสินค้าร้านวัสดุก่อสร้าง</strong> <br> <?= $model->constype_text() ?></p>
            <p class="card-text"><strong>facebook fanpage</strong> : <a href="<?= $model->cons_fb_fp ?>"><?= $model->cons_fb_fp ?></a> </p>
            <p class="card-text"><strong>รายละเอียดอื่นๆ</strong> <br> <?= nl2br($model->cons_data_other) ?></p>

        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <p></p>
    </div>

    <div class="panel panel-default">

        <div class="panel-body">



            <h4 class="card-text"><strong>ชื่อเจ้าของกิจการ</strong> : <?= $model->user->user_firstname ?></h4>
            <h6 class="card-text"><strong>เพิ่มโดย</strong> :
                <?php if ($model->updated_id) {
                    echo ' Admin';
                } else {
                    echo $model->user->user_firstname;
                }
                ?>
            </h6>

            <button class="btn btn-outline-dark btn-block" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                <i class="far fa-address-book"></i> ข้อมูลติดต่ออื่นๆ
            </button>

            <div class="collapse" id="collapseExample">
                <div class="card panel-body">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>เบอร์ติดต่อ 1</strong> : <?= $model->cons_tel1 ?></p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>เบอร์ติดต่อ 2</strong> : <?= $model->cons_tel2 ?></p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>line ID</strong> : <?= $model->cons_line_id ?></p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>line @</strong> : <?= $model->cons_line_ad ?></p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>facebook fanpage</strong> : <a href="<?= $model->cons_fb_fp ?>"><?= $model->cons_fb_fp ?></a></p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>messenger</strong> : <?= $model->cons_messenger ?></p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>email</strong> : <?= $model->cons_email ?></p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>เว็บไซต์</strong> : <?= $model->cons_website ?></p>
                    </div>
                    <!--<div class="col-xs-12 col-sm-12 col-md-12">
                              <p><strong>อื่นๆ</strong> : <?= $model->cons_other ?></p>
                          </div>-->
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <p><strong>วันเวลาที่สร้าง</strong> : <?= $model->created_time ?></p>
                    </div>

                </div>
            </div>


            <div class="col-xs-12 col-sm-12 col-md-12">
                <p></p>
            </div>

            <div class="">



                <button class="btn btn-outline-dark btn-block" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fas fa-map-marker-alt"></i> พื้นที่ให้บริการ
                </button>

                <div class="collapse" id="collapseExample2">
                    <div class="card panel-body">

                        <div class="row">



                        </div>

                        <?php

                        $provtran = [];
                        foreach ($serviceroutes as $key => $servicearea) {
                            $provtran[$province[$servicearea]][$key] = $amphoe[$key];
                        }

                        if (!empty($serviceroutes)) {


                            echo '<div class="col-lg-4 col-md-5 col-sm-3 col-sx-3">';
                            echo '<div class="form-group">';
                            echo Html::a('ลบพื้นที่ให้บริการทั้งหมด', ['deleteall', 'id' => $model->cons_id], [
                                'class' => 'btn btn-danger btn-block',
                                'data' => [
                                    'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                    'method' => 'post',
                                ],
                            ]);
                            echo '</div>';
                            echo '</div>';
                            echo '<br>';
                            echo '<br>';
                        } else {

                            echo ' <div class="alert alert-danger" role="alert">';
                            echo "ไม่มีข้อมูล !!";
                            echo '</div>';
                        }

                        foreach ($provtran as $key => $protran) {
                            echo '<h4>' . $key . '</h4>';
                            echo '<p>';
                            foreach ($provtran[$key] as $distran) {
                                echo $distran . ' ';
                            }
                            echo '</p>';
                            echo '<hr>';
                        }
                        ?>



                    </div>
                </div>
            </div>

            <br>



            <div class="">
                <?php



                echo '<p>';
                echo Html::a(
                    '<i class="fas fa-phone-alt"></i> ติดต่อกิจการ : ' . $model->cons_name,
                    ($model->cons_tel1 != '')
                        ? 'tel:' . $model->cons_tel1
                        : ['mh-user/view', 'id' => $model->user_id],
                    ['class' => 'btn btn-primary']
                );
                //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                echo '</p>';


                ?>
            </div>



            <div class="col-xs-12 col-sm-12 col-md-12">
                <p></p>
            </div>
        </div>

    </div>

</div>