<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhJob */

$this->title = $model->job_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลงานรับเหมา', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$jobid = Yii::$app->getRequest()->getQueryParam('id');
\yii\web\YiiAsset::register($this);
?>
<div class="mh-job-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->job_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->job_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('+ เพิ่มสินค้าร้านวัสดุก่อสร้าง', ['mh-job-items/addproductlists', 'id' => $jobid], ['class' => 'btn btn-success']) ?>
        <?= Html::a('+ ระบุพื้นที่ฉันให้บริการ', ['add-route', 'id' => $model->job_id], ['class' => 'btn btn-info']); ?>
        <?='' //Html::a('ดูงานรับเหมาก่อสร้างทั้งหมด', ['mh-job-items/myitemsjob', 'id' => $jobid], ['class' => 'btn btn-primary']) ?>


    </p>
    

    <div class="panel panel-default" style="padding-top:30px">
            <div class="text-center ">
                <div class="gImg ">
                    <p class="card-img-top"><?= $model->getPhotosViewerback() ?></p>
                </div>
            </div>
        </div>

<div class="panel panel-default">
  
  <div class="panel-body">
    <h3 class="card-title"><strong>ชื่อกิจการ : <?= $model->job_name ?></strong></h3>
      <p class="card-text"><strong>ที่อยู่กิจการ</strong> : <?= $model->job_address ?> อ. <?= $model->deliver() ?> จ. <?= $model->pickup() ?> <?= $model->zipcode() ?></p>
      <!--<p class="card-text"><strong>จังหวัดที่ตั้งกิจการ</strong> : <?= $model->pickup() ?></p>
      <p class="card-text"><strong>อำเภอที่ตั้งกิจการ</strong> : <?= $model->deliver() ?></p>-->
      <p class="card-text"><strong>พิกัด GPS</strong> : <?= $model->job_gps ?></p>
      <p class="card-text"><strong>รัศมีพื้นที่ให้บริการ</strong> : <?= $model->job_km ?> กิโลเมตร</p>
      <p class="card-text"><strong>ประเภทสินค้าร้านวัสดุก่อสร้าง</strong> <br> <?= $model->jobproducttype_text() ?></p>
      <p><strong>facebook fanpage</strong> : <a href="<?= $model->job_fb_fp ?>"><?= $model->job_fb_fp ?></a> </p>
      <p class="card-text"><strong>รายละเอียดอื่นๆ</strong> <br> <?= nl2br($model->job_data_other) ?></p>
                              
                              
      </div>
  </div>

  <div class="">
      <p></p>
  </div>


  <div class="panel panel-default"> 
                                          
      <div class="panel-body">           
      <h4 class="card-text"><strong>ชื่อเจ้าของกิจการ</strong> : <?= $model->user->user_firstname ?></h4>
      <h6 class="card-text"><strong>เพิ่มโดย</strong> : 
                    <?php if($model->updated_id) {
                              echo ' Admin';
                    } else {
                        echo $model->user->user_firstname;
                    }  
                    ?>      
        </h6>

              <button class="btn btn-outline-dark btn-block" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                  <i class="far fa-address-book"></i> ข้อมูลติดต่ออื่นๆ
              </button>

              <div class="collapse" id="collapseExample">
                  <div class=" panel-body">
                  <div class="">
                          <p><strong>เบอร์ติดต่อ 1</strong> : <?= $model->job_tel1 ?></p>
                      </div>
                      <div class="">
                          <p><strong>เบอร์ติดต่อ 2</strong> : <?= $model->job_tel2 ?></p>
                      </div>
                      <div class="">
                          <p><strong>line ID</strong> : <?= $model->job_line_id ?></p>
                      </div>
                      <div class="">
                          <p><strong>line @</strong> : <?= $model->job_line_ad ?></p>
                      </div>
                      <div class="">
                          <p><strong>facebook fanpage</strong> : <a href="<?= $model->job_fb_fp ?>"><?= $model->job_fb_fp ?></a></p>
                      </div>
                      <div class="">
                          <p><strong>messenger</strong> : <?= $model->job_messenger ?></p>
                      </div>
                      <div class="">
                          <p><strong>email</strong> : <?= $model->job_email ?></p>
                      </div>
                      <div class="">
                          <p><strong>เว็บไซต์</strong> : <?= $model->job_website ?></p>
                      </div>
                      <!--<div class="">
                          <p><strong>อื่นๆ</strong> : <?= $model->job_other ?></p>
                      </div>-->
                      <div class="">
                          <p><strong>วันเวลาที่สร้าง</strong> : <?= $model->created_time ?></p>
                      </div>

                  </div>
              </div>
  

      <div class="">
              <p></p>
      </div>
  
      <div class="">            

          <button class="btn btn-outline-dark btn-block" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">
          <i class="fas fa-map-marker-alt"></i> พื้นที่ให้บริการ
          </button>

          <div class="collapse" id="collapseExample2">
              <div class=" panel-body">
                  <div class="row">


          </div>   
                  

              <?php

                              $provtran = [];
                              foreach($serviceroutes as $key => $servicearea)
                              {
                                  $provtran[$province[$servicearea]][$key] = $amphoe[$key];
                              }

                              if (!empty($serviceroutes)) {
                                      
                                         
                                              
                                                  echo '<div class="col-lg-4 col-md-5 col-sm-3 col-sx-3">';
                                                  echo '<div class="form-group">';
                                                  echo Html::a('ลบพื้นที่ให้บริการทั้งหมด', ['deleteall', 'id' => $model->job_id], [
                                                      'class' => 'btn btn-danger btn-block',
                                                      'data' => [
                                                          'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                                          'method' => 'post',
                                                      ],
                                                  ]);    
                                                  echo '</div>';
                                                  echo '</div>';    echo '<br>';echo '<br>';         
                                                  
                                             
                                           
                              }else {
                                  
                                            echo ' <div class="alert alert-danger" role="alert">';
                                                  echo "ไม่มีข้อมูล !!"; 
                                            echo '</div>';
                              }

                              foreach($provtran as $key => $protran)
                              {
                                  echo '<h4>' . $key . '</h4>';
                                  echo '<p>';
                                  foreach($provtran[$key] as $distran)
                                  {
                                      echo $distran . ' ';
                                  }
                                  echo '</p>';
                                  echo '<hr>';
                              }
              ?>



              </div>
          </div>
      </div>

<br>



                      <div class="col-sm-4 ">
                          <?php
                          if(!$owner)
                              {
                              
                              
                              
                                      echo '<p>';
                                      echo Html::a('<i class="fas fa-phone-alt"></i> ติดต่อกิจการ : ' . $model->job_name, 
                                      ($model->job_tel1 != '') 
                                      ? 'tel:'.$model->job_tel1 
                                      : ['mh-user/view', 'id' => $model->user_id] , ['class' => 'btn btn-primary btn-block']);
                                      //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                                      echo '</p>';
                              
                              }
                          ?>
                      </div>

<!-- Your share button code -->
<div class="">  
<!-- Your share button code -->
  <div class="fb-share-button" 
      data-size="large"
      data-href=<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>
      data-layout="button_count">
  </div>
  <div class="line-it-button" 
      data-lang="en" 
      data-type="share-a" 
      
      data-ver="2" 
      data-url="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
      style="display: none;">
  </div>
</div>



  <div class="">
      <p></p>
 
      </div>
      </div>          
</div>

    <?='' /*DetailView::widget([
        'model' => $model,
        'attributes' => [
            'job_id',
            'job_name',
            'job_pic',
            [
                'attribute'=>'job_pic',
                'value'=>$model->getPhotosViewerback(),
                'format' => 'html',
            ],
            'job_gps',
            'job_address',
            'job_km',
            'job_province_id',
            'job_amphoe_id',
            [
                'attribute' => 'job_province_id',
                'value' => $model->jobProvince->province_name_th
            ],
            [
                'attribute' => 'job_amphoe_id',
                'value' => $model->jobAmphoe->amphoe_name_th
            ],
            'job_tel1',
            'job_tel2',
            'job_line_id',
            'job_line_ad',
            'job_fb_fp',
            'job_messenger',
            'job_email:email',
            'job_website',
            'job_other',
            'job_pt_id',
            'job_data_other',
            'updated_id',
            'created_time',
            'updated_time',
        ],
    ])*/?>

</div>
