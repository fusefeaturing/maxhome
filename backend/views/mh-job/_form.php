<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
use common\models\MhProvince;
use backend\models\BackendLogin;
use common\models\MhUser;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\MhJob */
/* @var $form yii\widgets\ActiveForm */
$user_backend = ArrayHelper::map(BackendLogin::find()->asArray()->all(), 'id', 'firstname');
$province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');
$userid = ArrayHelper::map(MhUser::find()->asArray()->all(), 'user_id', 'user_firstname');
?>

<div class="mh-job-form">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data']
]);?>

    

    <?= 
                $form
                    ->field($model, 'job_search_level')
                    ->textInput(['maxlength' => true, 'value' => $model->getDefaultLevel()])
                    //->input('', ['placeholder' => ""])
                    ->label(false) 
            ?>

    <?= $form->field($model, 'job_name')->textInput(['maxlength' => true])->label('ชื่อกิจการ', ['style' => 'color:red']) ?>

    <?=
                $form
                    ->field($model, 'user_id')
                    ->widget(Select2::classname(), [
                        'data' => $userid,
                        'language' => 'th',
                        'options' => [/*'multiple' => true,*/ 'placeholder' => 'เลือกผู้ใช้งาน'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],                    
                ])    
                ->label('เลือกผู้ใช้งาน', ['style' => 'color:red']);       
                 
            ?>

    <?='' //$form->field($model, 'job_pic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'job_pic[]')->fileInput(['multiple' => true])->label('รูปถ่ายกิจการ', ['style' => 'color:red']) ?>
    <div class="well">
        <?= $model->getPhotosViewerback(); ?>
        
    </div>

    <?= $form->field($model, 'job_gps')->textInput(['maxlength' => true]) ?>    

    <?= $form->field($model, 'job_km')->textInput() ?>    

    <?='' //$form->field($model, 'job_province_id')->textInput() ?>

    <?='' //$form->field($model, 'job_amphoe_id')->textInput() ?>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'job_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
                ->label('จังหวัดที่ตั้งกิจการ', ['style' => 'color:red'])
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'job_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ...',
                        'url' => Url::to(['/mh-job/get-amphoe'])
                    ]
                ])
                ->label('อำเภอที่ตั้งกิจการ', ['style' => 'color:red']);
        ?>
    </div>
    
    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'job_district_id')
                ->widget(DepDrop::classname(), [
                    'data' => $zipcode,
                    'pluginOptions' => [
                        'depends' => ['ddl-province', 'ddl-amphoe'],
                        'placeholder' => 'เลือกรหัสไปรษณีย์...',
                        'url' => Url::to(['mh-job/get-zipcode'])
                    ]
                ])
                ->label('รหัสไปรษณีย์');
        ?>
    </div> 
</div>

    <?= $form->field($model, 'job_address')
    ->textInput(['maxlength' => true])
    ->input('', ['placeholder' => "ตัวอย่าง 1/2 ม. 3 ต. 4"])
    ->label('ที่อยู่กิจการ') ?>

    <?= $form->field($model, 'job_tel1')->textInput()->label('เบอร์ติดต่อ 1', ['style' => 'color:red']) ?>

    <?= $form->field($model, 'job_tel2')->textInput() ?>

    <?= $form->field($model, 'job_line_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'job_line_ad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'job_fb_fp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'job_messenger')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'job_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'job_website')->textInput(['maxlength' => true]) ?>

    <?='' //$form->field($model, 'job_other')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model, 'job_product_id')->checkboxList($model->getItemSkill())->label(false) ?>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <strong><h4 style="color:red">เลือกประเภทงานรับเหมา</h4></strong>
            <div class="custom-control custom-checkbox">
                <input id="customCheck1" class="custom-control-input" type="checkbox" >
                <label class="custom-control-label" style="color:green" for="customCheck1"><strong>เลือกทั้งหมด</strong></label>
            </div>
            <p></p>
            <?=
                $form
                    ->field($model, 'job_product_id')
                    ->checkBoxList(
                        $this->context->getJobProductTypes(),
                        ['itemOptions' => $model->jobproducttypeToArray(), 'separator' => '<br>']
                    )
                    ->hint(Html::a('คลิกดูประเภทรถ', ['mh-job-product-type/index'], ['target' => '_blank', 'class' => 'linksWithTarget']))
                    ->label(false)
                    ->hint('เลือกได้หลายรายการ')
                    ->label('เลือกประเภทสินค้าก่อสร้าง', ['style' => 'color:red'])
            ?>
        </div>
    </div>


    
    <?= $form->field($model, 'job_data_other')->textarea(['rows' => 6]) ?>

    <?='' //$form->field($model, 'updated_id')->textInput() ?>

    <?=
                $form
                    ->field($model, 'updated_id')
                    
                    ->dropdownList($user_backend, [
                        'id' => 'ddl-roguser',
                        'prompt' => ''
                        
                    ])
                    
    ?>

    <?='' //$form->field($model, 'created_time')->textInput() ?>

    <?='' //$form->field($model, 'updated_time')->textInput() ?>

    <?= $form->field($model, 'created_time')->textInput([
        'readonly' => true,
        'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
    ]) ?>

    <?= $form->field($model, 'updated_time')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <script type="text/javascript">
        $("#customCheck1").click(function() {
            $("input[type=checkbox]").prop("checked", $(this).prop("checked"));
        });

        $("input[type=checkbox]").click(function() {
            if (!$(this).prop("checked")) {
                $("#customCheck1").prop("checked", false);
            }
        });

        jackHarnerSig();
    
    </script>

</div>
