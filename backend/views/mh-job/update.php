<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJob */

$this->title = 'แก้ไขข้อมูลงานรับเหมา: ' . $model->job_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลงานรับเหมา', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->job_name, 'url' => ['view', 'id' => $model->job_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-job-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
        'zipcode' => $zipcode,
    ]) ?>

</div>
