<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJob */

$this->title = 'เพิ่มข้อมูลงานรับเหมา';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลงานรับเหมา', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
        'zipcode' => [],
    ]) ?>

</div>
