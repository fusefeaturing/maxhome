<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
use common\models\MhProvince;

/* @var $this yii\web\View */
/* @var $model backend\models\MhJobSearch */
/* @var $form yii\widgets\ActiveForm */
$province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');


?>

<div class="mh-job-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        function showTextStatus(status) {
            return status == 'true' ? "(แตะเพื่อย่อ)" : "(แตะเพื่อขยาย)";
        }

        function loopCheckedValue() {
            var selectedLanguage = []
            $('input[type="checkbox"]:checked').each(function() {

                var text = $('span#boxitem' + this.value).html();
                selectedLanguage.push(text);
            });
            return selectedLanguage;
        }

        function showCollapsHead(selected, textStat) {
            $('#divResult').html(selected.length + "\n" + "รายการ / ประเภทรถที่เลือก: " + selected + textStat);
        }

        function event(type) {
            var stat = $("#divResult[aria-expanded]").attr('aria-expanded') == "true";
            var stringStat = type == "exp" ? showTextStatus(""+!stat+"") : showTextStatus(""+stat+"");
            var selected = loopCheckedValue();
            
            if (type == "init") {
                if (selected.length != 0)  { showCollapsHead(selected, stringStat); } 
                else { return; }
            } else { showCollapsHead(selected, stringStat); }  
        }

        $("#divResult[aria-expanded]").click(function() {
            event("exp")
        });

        $('input[type="checkbox"]').click(function() {
            event("chk")
        });
        event("init")
        
    });

</script>

    


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div id="w1" class="panel-group collapse in" aria-expanded="true" style="">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">

                            <a class="collapse-toggle" href="#w1-collapse1" data-toggle="collapse" data-parent="#w1" aria-expanded="false"  class="divResult" id="divResult">เลือกประเภทงานรับเหมา (แตะเพื่อขยาย)</a>


                        </h4>
                    </div>

                    <div id="w1-collapse1" class="panel-collapse collapse" style="">
                        <div class="panel-body" id="divR"> 
                        <?=
                $form
                    ->field($model, 'job_pt_id')
                    ->checkBoxList(
                        $this->context->getJobTypes(),
                            [ 'separator' => '<br>',
                                'item' => function ($index, $label, $name, $checked, $value) {
                                    $checked = $checked ? 'checked' : '';
                                    return '<div> <span id="boxitem' . $value . '"> <input type="checkbox" value=' . $value . ' name=' . $name . ' ' . $checked  . '>' . $label . '</span></div>';
                                }
                            ]
                    )
                    ->hint(Html::a('คลิกดูประเภทรถ', ['mh-job-product-type/index'], ['target' => '_blank', 'class' => 'linksWithTarget']))
                    ->label(false)
                    ->hint('เลือกได้หลายรายการ')
            ?>

                        </div>
                    </div>




                </div>
            </div>

        </div>
    </div>

    
<div class="">
    <div class="row">

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'job_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
                ->label('จังหวัดที่ตั้งกิจการ', ['style' => 'color:red'])
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'job_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ...',
                        'url' => Url::to(['/mh-job/get-amphoe'])
                    ]
                ])
                ->label('อำเภอที่ตั้งกิจการ', ['style' => 'color:red']);
        ?>
    </div>



        <div class="col-sm-3">
            <div class="form-group">
                <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary btn-block', 'id' => 'btnSubmit']) ?>
            </div>
        </div>

        <div class="col-sm-3">    
            <div class="form-group">       
                <?= Html::a(
                    'รีเซ็ตค่าค้นหา',
                    ['mh-job/index'],
                    ['class' => 'btn btn-danger btn-block', 'style' => '']
                ) ?>
            </div>
        </div>

        <?php ActiveForm::end();
        ?>

    </div>
</div>



    <?='' //$form->field($model, 'job_id') ?>

    <?='' //$form->field($model, 'job_name') ?>

    <?='' //$form->field($model, 'job_pic') ?>

    <?='' //$form->field($model, 'job_gps') ?>

    <?='' //$form->field($model, 'job_address') ?>

    <?php // echo $form->field($model, 'job_km') ?>

    <?php // echo $form->field($model, 'job_province_id') ?>

    <?php // echo $form->field($model, 'job_amphoe_id') ?>

    <?php // echo $form->field($model, 'job_tel1') ?>

    <?php // echo $form->field($model, 'job_tel2') ?>

    <?php // echo $form->field($model, 'job_line_id') ?>

    <?php // echo $form->field($model, 'job_line_ad') ?>

    <?php // echo $form->field($model, 'job_fb_fp') ?>

    <?php // echo $form->field($model, 'job_messenger') ?>

    <?php // echo $form->field($model, 'job_email') ?>

    <?php // echo $form->field($model, 'job_website') ?>

    <?php // echo $form->field($model, 'job_other') ?>

    <?php // echo $form->field($model, 'job_pt_id') ?>

    <?php // echo $form->field($model, 'job_data_other') ?>

    <?php // echo $form->field($model, 'updated_id') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>



</div>
