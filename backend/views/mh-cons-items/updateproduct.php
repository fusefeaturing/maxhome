<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsItems */

$this->title = 'แก้ไขข้อมูลสินค้า: ' . $model->consProduct->cons_product_name;
$consid = Yii::$app->getRequest()->getQueryParam('id');
$this->params['breadcrumbs'][] = ['label' => 'ร้านค้า' , 'url' => ['mh-construction/view','id' => $consid]];
if (Yii::$app->user->isGuest) {
    
    $this->params['breadcrumbs'][] = ['label' => 'สินค้าภายในร้าน', 'url' => ['mh-cons-items/itemsstore', 'id' => $model->cons_id]];
    
} else {    

    $this->params['breadcrumbs'][] = ['label' => 'สินค้าภายในร้านของฉัน', 'url' => ['mh-cons-items/myitems', 'id' => $consid, ]];

}
$this->params['breadcrumbs'][] = ['label' => $model->consProduct->cons_product_name, 'url' => ['view', 'id' => $model->cons_items_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-cons-items-update">

    <div class="card2">
        <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_formupdateproduct', [
        'model' => $model,
        //'modelpro' => $modelpro,
    ]) ?>

</div>
