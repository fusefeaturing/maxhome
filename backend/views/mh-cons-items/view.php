<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsItems */

$this->title = $model->consPt->cons_pt_name .' - '. $model->consSubPt->cons_sub_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลสินค้าวัสดุก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$cons_id = Yii::$app->getRequest()->getQueryParam('id');
\yii\web\YiiAsset::register($this);
?>
<div class="mh-cons-items-view">

    <h1>ชื่อสินค้า : <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['addproductlists'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('แก้ไข', ['updateproduct', 'id' => $model->cons_items_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->cons_items_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('+ เพิ่มสินค้าร้านวัสดุก่อสร้าง', ['mh-cons-items/addproductlists'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="panel panel-default" style="padding-top:20px">
        <div class="text-center ">
            <div class="gImg ">
                <p class="card-img-top"><?= $model->consProduct->getPhotosViewerback() ?></p>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <p class="card-text"><strong>ลำดับการค้นหา</strong> : <?= $model->cons_item_level ?> </p>           
            <p class="card-text"><strong>ประเภทสินค้า</strong> : <?= $model->consProduct->consPt->cons_pt_name ?> - <?= $model->consProduct->consSubPt->cons_sub_pt_name ?></p>
            <p class="card-text"><strong>รายละเอียดสินค้า : <br> <?= nl2br($model->consProduct->cons_product_description) ?></strong></p>
            <p class="card-text"><strong>ราคาสินค้า</strong> : <?= $model->cons_price ?> </p>                     
            <p class="card-text"><strong>ชื่อกิจการ</strong> : <?= $model->cons->cons_name ?> </p>  
                <br>          
            <h4 class="card-text"><strong>ชื่อเจ้าของกิจการ</strong> : <?= $model->user->user_firstname ?></h4>
            <p class="card-text"><strong>เวลาสร้าง</strong> : <?= $model->created_time ?></p>
            <p class="card-text"><strong>เวลาแก้ไข</strong> : <?= $model->updated_time ?></p>
        </div>
    </div>



</div>
