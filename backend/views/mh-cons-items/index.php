<?php

use common\models\MhConsSubPt;
use common\models\MhConstructionProductType;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhConsItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลสินค้าวัสดุก่อสร้าง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-cons-items-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['addproductlists'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'cons_item_name',
            [
                'attribute' => 'cons_product_id',
                'value' => 'consProduct.cons_product_name',
                'label' => 'ชื่อสินค้า'
            ],
            [
                'attribute' => 'cons_pt_id',
                'value' => 'consPt.cons_pt_name',
                'filter'=>ArrayHelper::map(MhConstructionProductType::find()->asArray()->all(), 'cons_pt_id', 'cons_pt_name') 
            ],

            [
                'attribute' => 'cons_sub_pt_id',
                'value' => 'consSubPt.cons_sub_pt_name',
                'filter'=>ArrayHelper::map(MhConsSubPt::find()->asArray()->all(), 'cons_sub_pt_id', 'cons_sub_pt_name') 
            ],

            'cons_price',
            [
                'attribute' => 'cons_id',
                'value' => 'cons.cons_name',
            ],
            [
                'attribute' => 'user_id',
                'value' => 'user.user_firstname',
            ],
            'cons_item_level',
            //'cons_product_id',            
            //'cons_id',
            //'user_id',
            //'updated_id',
            //'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
