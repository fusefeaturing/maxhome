<?php

use common\models\MhConstruction;
use yii\bootstrap4\Html;
use yii\grid\GridView;
use yii\bootstrap4\LinkPager;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$consid = Yii::$app->getRequest()->getQueryParam('id');

$this->title = 'สินค้าที่สามารถเพิ่มได้';
$this->params['breadcrumbs'][] = ['label' => 'ร้านค้า', 'url' => ['mh-construction/view','id' => $consid]];
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['og_title']['content'] = $this->title;


Yii::$app->params['og_url']['content'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
Yii::$app->params['og_keyword']['content'] = 'ร้านวัสดุก่อสร้าง ,hub.maxhome ,สินค้าร้านวัสดุก่อสร้าง';
Yii::$app->params['og_revisit_after']['content'] = '1 Days';


?>

<body>
  


<div class="mh-job-index">



<?php echo $this->render('_searchproduct', ['model' => $searchModel, 'subpt' => $subpt,]); ?>
  

  <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

        
        <div class="container">
    <div class=" row ">

      <?php foreach ($dataProvider->models as $model) { ?>
        
        <?= Html::a('<div class="">
                          <div class="panel panel-default" >
                            <div class=""> 
                            ' . Html::img($model->getFirstPhotoURL(), ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile commany-image576px commany-image768px']) . '                      
                            </div>

                            <div class="panel-body">
                              <h6 class="card-title">' . $model->cons_product_name . ' </h6>
                              
                              
                              
                              
                            </div>  

                          </div>
                        </div>', ['create', 'id' => $model->cons_product_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
        ?>

      <?php } ?>



    </div>
    <br>
  </div>


      <?php if (!empty($dataProvider->models)) : ?>            
        <?php
                    echo \yii\bootstrap4\LinkPager::widget([
                    'pagination' => $pagination,
                ]);
        ?>
      <?php else : ?>
            <div class="alert alert-danger" role="alert">
              <?php echo "ไม่พบข้อมูล !!"; ?>
            </div>
      <?php endif; ?>







      </div>
</body>