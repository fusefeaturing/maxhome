<?php

use backend\models\BackendLogin;
use common\models\MhConsProduct;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\MhConstruction;
use common\models\MhConstructionProductType;
use common\models\MhUser;
use kartik\widgets\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsItems */
/* @var $form yii\widgets\ActiveForm */
$cons_product_id = Yii::$app->getRequest()->getQueryParam('id');
$userid = ArrayHelper::map(MhUser::find()->asArray()->all(), 'user_id', 'user_firstname');
$consproduct = ArrayHelper::map(MhConsProduct::find()->asArray()->all(), 'cons_product_id', 'cons_product_name');
$consid = ArrayHelper::map(MhConstruction::find()->asArray()->all(), 'cons_id', 'cons_name');
$user_backend = ArrayHelper::map(BackendLogin::find()->asArray()->all(), 'id', 'firstname');
$conspt = ArrayHelper::map(MhConstructionProductType::find()->asArray()->all(), 'cons_pt_id', 'cons_pt_name');
?>

<div class="mh-cons-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <p class="card-text">ชื่อสินค้าวัสดุก่อสร้าง : <?= $modelpro->cons_product_name ?></p>
            <p class="card-text">รายละเอียด : <?= $modelpro->cons_product_description ?></p>    
            <p class="card-text">หมวด : <?= $modelpro->consPt->cons_pt_name ?></p>
            <p class="card-text">หมู่ : <?= $modelpro->consSubPt->cons_sub_pt_name ?></p>

        </div>
    </div>

   
    <div class="row">             

        <div class="col-xs-12 col-sm-12 col-md-12">
            
        
            <?= 
                $form
                    ->field($model, 'cons_item_level')
                    ->hiddenInput(['maxlength' => true, 'value' => $model->getDefaultLevel()])
                    //->input('', ['placeholder' => ""])
                    ->label(false) 
            ?>


            <?= 
                $form
                    ->field($model, 'cons_product_id')
                    ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $cons_product_id])
                    ->label(false)
            ?>

            <?= $form->field($model, 'cons_pt_id')
                            ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $modelpro->consPt->cons_pt_id])
                            ->label(false)
            ?>  

            <?= $form->field($model, 'cons_sub_pt_id')
                            ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $modelpro->consSubPt->cons_sub_pt_id])
                            ->label(false)
            ?>  

        </div>
    </div>

    <?= $form->field($model, 'cons_price')->textInput(['maxlength' => true]) ?>



            <?=
                $form
                    ->field($model, 'cons_id')
                    ->widget(Select2::classname(), [
                        'data' => $consid,
                        'language' => 'th',
                        'options' => [/*'multiple' => true,*/ 'placeholder' => 'เลือกร้านค้า'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],                    
                ])    
                ->label('เลือกร้านค้า', ['style' => 'color:red']);       
                 
            ?>


            <?=
                $form
                    ->field($model, 'user_id')
                    ->widget(Select2::classname(), [
                        'data' => $userid,
                        'language' => 'th',
                        'options' => [/*'multiple' => true,*/ 'placeholder' => 'เลือกผู้ใช้งาน'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],                    
                ])    
                ->label('เลือกผู้ใช้งาน', ['style' => 'color:red']);       
                 
            ?>

    <?=
                $form
                    ->field($model, 'updated_id')
                    
                    ->dropdownList($user_backend, [
                        'id' => 'ddl-roguser',
                        'prompt' => ''
                        
                    ])
                    
    ?>

            <?= $form->field($model, 'created_time')->textInput([
                'readonly' => true,
                'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
            ]) ?>

            <?= $form->field($model, 'updated_time')->textInput([
                'readonly' => true,
                'value' => date('Y-m-d H:i:s')
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
