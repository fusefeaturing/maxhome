<?php

use yii\bootstrap4\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'สินค้าวัสดุก่อสร้างของร้าน';
$userid = yii::$app->user->identity->id;
$consid = Yii::$app->getRequest()->getQueryParam('id');
$this->params['breadcrumbs'][] = ['label' => $model->cons_name, 'url' => ['mh-construction/view', 'id' => $consid]];
$this->params['breadcrumbs'][] = $this->title;

?>

<body>

<div class="rog-user-car-index">


    
    <div class="panel panel-default">
        <div class="panel-body">
            <h1><?= Html::encode($this->title) ?></h1>
            <p>1.) เพิ่มสินค้าวัสดุก่อสร้างของฉัน</p>

            <p>2.) แก้ข้อมูลสำหรับติดต่อของฉัน<?=Html::a('แก้ข้อมูลสำหรับติดต่อของฉัน', ['mh-user/update', 'id' => $userid], ['class' => 'btn btn-danger']) ?></p>
            <p>ระบุข้อมูลครบถ้วนเพื่อให้ผู้ใช้บริการจะค้นหาและติดต่อคุณได้โดยตรง</p>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

    <div class="panel panel-default">
        <div class="panel-body">
        <h2>1.) สินค้าวัสดุก่อสร้างของฉัน</h2>

            <p>
                <?= Html::a('+ เพิ่มสินค้าร้านวัสดุก่อสร้าง', ['addproductlists', 'id' => $consid], ['class' => 'btn btn-success']) ?>
            </p>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

    <div class="panel panel-default">
            <?php echo $this->render('_searchmyitems', ['model' => $searchModel, 'amphoe' => $amphoe, 'subpt' => $subpt, 'consid' => $consid, ]); ?>

        <div class="panel-body">
            <div class="">
                <div class="">
                    <?= Html::hiddenInput('cons_id', $consid);  ?>
                      <?php foreach ($dataProvider->models as $model) { ?>
                        
                        <?= Html::a('<div class="card-group">
                                          <div class="card card-mobile" >
                                            <div class="cardd"> 
                                            ' . Html::img($model->consProduct->getFirstPhotoURL(), ['class' => 'img-responsive rounded mx-auto d-block items-display-mobile']) . '                      
                                            </div>
                    
                                            <div class="panel-body">
                                            <h6 class="card-title">' . $model->consProduct->cons_product_name . ' </h6>
                                            <p style="font-size:14px; color:green; " class="card-title ">' . $model->cons_price . ' </p>
                                              <p style="font-size:12px; color:blue" class="card-title"><i class="fas fa-map-marker-alt"></i> ' . $model->cons->consProvince->province_name_th . ' ' . $model->cons->consAmphoe->amphoe_name_th . ' </p>
                                              
                                            </div>  
                    
                                          </div>
                                        </div>', ['view', 'id' => $model->cons_items_id ], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
                        ?>
                <hr>
      <?php } ?>



                </div>
    
  </div>

  <?php if (!empty($dataProvider->models)) : ?>
                <?php
                        echo \yii\bootstrap4\LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
    <?php else : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo "ไม่มีข้อมูล !!"; ?>
                </div>
    <?php endif; ?>


        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>


</div>

</body>
