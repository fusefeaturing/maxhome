<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MhJobSubPtSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-job-sub-pt-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'job_sub_pt_id') ?>

    <?= $form->field($model, 'job_sub_pt_name') ?>

    <?= $form->field($model, 'job_pt_id') ?>

    <?= $form->field($model, 'updated_id') ?>

    <?= $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>

    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('รีเซ็ต', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
