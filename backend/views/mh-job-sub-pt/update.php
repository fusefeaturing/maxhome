<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobSubPt */

$this->title = 'แก้ไขหมวดงานรับเหมา: ' . $model->job_sub_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'หมวดงานรับเหมา', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->job_sub_pt_name, 'url' => ['view', 'id' => $model->job_sub_pt_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-job-sub-pt-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
