<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhJobSubPtSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ชนิดงานรับเหมา';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-sub-pt-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'job_sub_pt_id',
            [
                'attribute' => 'job_pt_id',
                'value' => 'jobPt.job_pt_name',
            ],
            'job_sub_pt_name',
            //'job_pt_id',
            'job_sub_pt_level',
            //'updated_id',
            'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
