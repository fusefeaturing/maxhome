<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobSubPt */

$this->title = $model->job_sub_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'หมวดงานรับเหมา', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-job-sub-pt-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->job_sub_pt_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->job_sub_pt_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'job_sub_pt_id',
            'job_sub_pt_name',
            'job_sub_pt_level',
            //'job_pt_id',
            [
                'attribute' => 'job_pt_id',
                'value' => $model->jobPt->job_pt_name
            ],
            //'updated_id',
            
            'created_time',
            'updated_time',
        ],
    ]) ?>

</div>
