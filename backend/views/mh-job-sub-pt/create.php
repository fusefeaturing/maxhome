<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobSubPt */

$this->title = 'เพิ่มหมวดงานรับเหมา';
$this->params['breadcrumbs'][] = ['label' => 'หมวดงานรับเหมา', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-sub-pt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
