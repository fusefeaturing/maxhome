<?php

use backend\models\BackendLogin;
use common\models\MhConstructionProductType;
use common\models\MhJobProductType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobSubPt */
/* @var $form yii\widgets\ActiveForm */
$user_backend = ArrayHelper::map(BackendLogin::find()->asArray()->all(), 'id', 'firstname');
$jobpt = ArrayHelper::map(MhJobProductType::find()->asArray()->all(), 'job_pt_id', 'job_pt_name');
?>

<div class="mh-job-sub-pt-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= 
            $form
                ->field($model, 'job_sub_pt_level')
                ->textInput(['maxlength' => true, 'value' => '100'])
                //->input('', ['placeholder' => ""])
                ->label(false) 
    ?>

    <?=
                $form
                    ->field($model, 'job_pt_id')                    
                    ->dropdownList($jobpt, [
                        'id' => 'ddl-roguser',
                        'prompt' => 'เลือกหมวดงานรับเหมา'
                        
                    ])
                    
    ?>

    <?= $form->field($model, 'job_sub_pt_name')->textInput(['maxlength' => true]) ?>



    <?=
                $form
                    ->field($model, 'updated_id')                    
                    ->dropdownList($user_backend, [
                        'id' => 'ddl-roguser',
                                       
                    ])
                    
    ?>

    <?= $form->field($model, 'created_time')->textInput([
        'readonly' => true,
        'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
    ]) ?>
    
    <?= $form->field($model, 'updated_time')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
