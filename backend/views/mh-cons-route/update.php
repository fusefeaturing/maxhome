<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsRoute */

$this->title = 'แก้ไขพื้นที่ให้บริการร้านค้าวัสดุก่อสร้าง: ' . $model->cons->cons_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลพื้นที่ให้บริหารร้านค้าวัสดุก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cons->cons_name, 'url' => ['view', 'id' => $model->cons_route_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-cons-route-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
    ]) ?>

</div>
