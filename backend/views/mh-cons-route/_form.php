<?php

use common\models\MhArchitectEngineer;
use common\models\MhConstruction;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MhUser;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use common\models\MhProvince;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsRoute */
/* @var $form yii\widgets\ActiveForm */

$province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');
$userid = ArrayHelper::map(MhUser::find()->asArray()->all(), 'user_id', 'user_firstname');
$consid = ArrayHelper::map(MhConstruction::find()->asArray()->all(), 'cons_id', 'cons_name');

?>

<div class="mh-cons-route-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
                $form
                    ->field($model, 'cons_id')
                    
                    ->dropdownList($consid, [
                        'id' => 'ddl-roguser',
                        'prompt'=>'เลือกกิจการ'
                    ])
                    ->label('เลือกกิจการ', ['style' => 'color:red'])
                    
    ?>

    
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'cons_route_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
                ->label('จังหวัดที่ตั้งกิจการ', ['style' => 'color:red'])
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'cons_route_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ...',
                        'url' => Url::to(['/mh-job/get-amphoe'])
                    ]
                ])
                ->label('อำเภอที่ตั้งกิจการ', ['style' => 'color:red']);
        ?>
    </div>   
</div>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'updated_date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
