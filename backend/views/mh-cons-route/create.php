<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsRoute */

$this->title = 'เพิ่มพื้นที่ให้บริการร้านค้าวัสดุก่อสร้าง';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลพื้นที่ให้บริการร้านค้าวัสดุก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-cons-route-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
    ]) ?>

</div>
