<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsRoute */

$this->title = $model->cons->cons_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลพื้นที่ให้บริหารร้านค้าวัสดุก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<div class="mh-cons-route-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->cons_route_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->cons_route_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cons_route_id',
            //'user_id',
       
            [
                'attribute' => 'cons_id',
                'value' => $model->cons->cons_name
            ],
            //'cons_route_province_id',
            [
                'attribute' => 'cons_route_province_id',
                'value' => $model->consRouteProvince->province_name_th
            ],
            [
                'attribute' => 'cons_route_amphoe_id',
                'value' => $model->consRouteAmphoe->amphoe_name_th
            ],
            //'cons_route_amphoe_id',
            'created_date',
            'updated_date',
        ],
    ]) ?>

</div>
