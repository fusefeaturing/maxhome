<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhConsRouteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลพื้นที่ให้บริการร้านค้าวัสดุก่อสร้าง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-cons-route-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'cons_route_id',
            //'user_id',
            /*[
                'attribute' => 'user_id',
                'value' => 'user.user_firstname',
            ],*/

            [
                'attribute' => 'cons_id',
                'value' => 'cons.cons_name',
            ],
            [
                'attribute' => 'cons_route_province_id',
                'value' => 'consRouteProvince.province_name_th',
            ],
            [
                'attribute' => 'cons_route_amphoe_id',
                'value' => 'consRouteAmphoe.amphoe_name_th',
            ],
            //'cons_route_province_id',
            //'cons_route_amphoe_id',
            'created_date',
            'updated_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
