<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Admin Hub.MAXHOME.co';
?>
<div class="site-index">

<?php

    if (Yii::$app->user->isGuest) {

        echo '<h1>URL Admin NEW</h1>';
        echo '<div class="row">';
        echo '<div class="col-md-3" style="justify-content: center; align-items: center; ">';
        echo Html::a(
            '<div class="card-counterv4 black">
            <i class="fas fa-user-secret"></i>
            <span class="count-numbers">เข้าสู่ระบบ</span>
            <span class="count-name">ผู้ดูแลระบบ</span>
            </div>',
            ['site/login'],
            ['class' => '', 'style' => 'background-color: #000000;']
        );
        echo '</div>';
        
        echo '</div>';
        
        echo '<br>';

      
    } else {

        echo '<h1>การจัดการข้อมูลกิจการ</h1>';
        echo '<hr>';


echo '<div class="container">';
    echo '<div class="row">';
        echo '<div class="col-md-3">';
            echo Html::a(
                 '<div class="card-counterv4 pink">
                 <i class="fas fa-shipping-fast fa-flip-horizontal"></i>
                 <span class="count-numbers">ข้อมูล</span>
                 <span class="count-name">ช่างรับเหมาก่อสร้าง</span>
                 </div>',
                 ['mh-job/index'],
                 [/*'style' => 'background-color:#cb06ff'*/]
                 );
                
                                    
        echo '</div>';
        
        echo '<div class="col-md-3">';
            echo Html::a(
                 '<div class="card-counterv4 danger">
                 <i class="fas fa-tools"></i>
                 <span class="count-numbers">ข้อมูล</span>
                 <span class="count-name">ร้านวัสดุก่อสร้าง</span>
                 </div>',
                 ['mh-construction/index'],
                 [/*'style' => 'background-color:#cb06ff'*/]
                 );          
            echo '</div>';

        echo '<div class="col-md-3">';
            echo Html::a(
                 '<div class="card-counterv4 success">
                 <i class="fas fa-pencil-ruler"></i>
                 <span class="count-numbers">ข้อมูล</span>
                 <span class="count-name">สถาปนิก/วิศวกร</span>
                 </div>',
                 ['mh-architect-engineer/index'],
                 [/*'style' => 'background-color:#cb06ff'*/]
                 );          
            echo '</div>';

        echo '<div class="col-md-3">';
            echo Html::a(
                 '<div class="card-counterv4 info">
                 <i class="fas fa-cube"></i>
                 <span class="count-numbers">ข้อมูล</span>
                 <span class="count-name">คอนกรีต</span>
                 </div>',
                 ['mh-concrete/index'],
                 [/*'style' => 'background-color:#cb06ff'*/]
                 );          
            echo '</div>';
        
        
        
    echo '</div>';
echo '</div>';
        
        
        echo '<h1>การจัดการข้อมูลพื้นฐาน</h1>';
        echo '<hr>';


        echo '<div class="container">';
        echo '<div class="row">';
            echo '<div class="col-md-3">';
                echo Html::a(
                     '<div class="card-counterv4 pink">
                     <i class="fas fa-users"></i>
                     <span class="count-numbers">ข้อมูล</span>
                     <span class="count-name">ผู้ใช้งาน</span>
                     </div>',
                     ['mh-user/index'],
                     [/*'style' => 'background-color:#cb06ff'*/]
                     );
                    
                                        
            echo '</div>';
            
            echo '<div class="col-md-3">';
                echo Html::a(
                     '<div class="card-counterv4 danger">
                     <i class="fas fa-user-tie"></i>
                     <span class="count-numbers">ข้อมูล</span>
                     <span class="count-name">ผู้ใช้งานเบื้องหลัง</span>
                     </div>',
                     ['backendlogin/index'],
                     [/*'style' => 'background-color:#cb06ff'*/]
                     );          
                echo '</div>';

            echo '<div class="col-md-3">';
                echo Html::a(
                     '<div class="card-counterv4 success ">
                     <i class="fas fa-shipping-fast fa-flip-horizontal"></i>
                     <span class="count-numbers">ข้อมูล</span>
                     <span class="count-name">ประเภทงานรับเหมา</span>
                     </div>',
                     ['mh-job-product-type/index'],
                     [/*'style' => 'background-color:#cb06ff'*/]
                     );          
                echo '</div>';
    
            echo '<div class="col-md-3">';
                echo Html::a(
                     '<div class="card-counterv4 info">
                     <i class="fas fa-tools"></i>
                     <span class="count-numbers">ข้อมูล</span>
                     <span class="count-name">ประเภทสินค้าวัสดุก่อสร้าง</span>
                     </div>',
                     ['mh-construction-product-type/index'],
                     [/*'style' => 'background-color:#cb06ff'*/]
                     );          
                echo '</div>';

                echo '<hr>';

                echo '<div class="col-md-3">';
                echo Html::a(
                     '<div class="card-counterv4 pink">
                     <i class="fas fa-pencil-ruler"></i>
                     <span class="count-numbers">ข้อมูล</span>
                     <span class="count-name">ประเภทงานออกแบบเขียนแบบ</span>
                     </div>',
                     ['mh-ae-product-type/index'],
                     [/*'style' => 'background-color:#cb06ff'*/]
                     );
                    
                                        
            echo '</div>';

            echo '<div class="col-md-3">';
            echo Html::a(
                 '<div class="card-counterv4 danger">
                 <i class="fas fa-route"></i>
                 <span class="count-numbers">ข้อมูล</span>
                 <span class="count-name">พื้นที่ให้บริการร้านค้าวัสดุก่อสร้าง</span>
                 </div>',
                 ['mh-cons-route/index'],
                 [/*'style' => 'background-color:#cb06ff'*/]
                 );          
            echo '</div>';

            echo '<div class="col-md-3">';
            echo Html::a(
                 '<div class="card-counterv4 success">
                 <i class="fas fa-route"></i>
                 <span class="count-numbers">ข้อมูล</span>
                 <span class="count-name">พื้นที่ให้บริการงานรับเหมา</span>
                 </div>',
                 ['mh-job-route/index'],
                 [/*'style' => 'background-color:#cb06ff'*/]
                 );          
            echo '</div>';

            echo '<div class="col-md-3">';
            echo Html::a(
                 '<div class="card-counterv4 info">
                 <i class="fas fa-route"></i>
                 <span class="count-numbers">ข้อมูล</span>
                 <span class="count-name">พื้นที่ให้บริการสถาปนิก / วิศวกร</span>
                 </div>',
                 ['mh-ae-route/index'],
                 [/*'style' => 'background-color:#cb06ff'*/]
                 );          
            echo '</div>';
            
            echo '<div class="col-md-3">';
                echo Html::a(
                     '<div class="card-counterv4 pink">
                     <i class="fas fa-map-marker-alt"></i>
                     <span class="count-numbers">ข้อมูล</span>
                     <span class="count-name">จังหวัด</span>
                     </div>',
                     ['mh-province/index'],
                     [/*'style' => 'background-color:#cb06ff'*/]
                     );          
                echo '</div>';
    
            echo '<div class="col-md-3">';
                echo Html::a(
                     '<div class="card-counterv4 danger">
                     <i class="fas fa-map-marker-alt"></i>
                     <span class="count-numbers">ข้อมูล</span>
                     <span class="count-name">อำเภอ</span>
                     </div>',
                     ['mh-amphoe/index'],
                     [/*'style' => 'background-color:#cb06ff'*/]
                     );          
                echo '</div>';
    
            echo '<div class="col-md-3">';
                echo Html::a(
                     '<div class="card-counterv4 success">
                     <i class="fas fa-map-marker-alt"></i>
                     <span class="count-numbers">ข้อมูล</span>
                     <span class="count-name">ตำบล</span>
                     </div>',
                     ['mh-district/index'],
                     [/*'style' => 'background-color:#cb06ff'*/]
                     );          
                echo '</div>';
            
            
            
        echo '</div>';
    echo '</div>';
    
    
    
    

    }

    ?>


</div>
