<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsSubType */

$this->title = 'เพิ่มการข้อมูลประเภทย่อย';
$this->params['breadcrumbs'][] = ['label' => 'การข้อมูลประเภทย่อย', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-cons-sub-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
