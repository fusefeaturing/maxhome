<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsSubType */

$this->title = 'แก้ไขการข้อมูลประเภทย่อย: ' . $model->cons_sub_type_name;
$this->params['breadcrumbs'][] = ['label' => 'การข้อมูลประเภทย่อย', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cons_sub_type_name, 'url' => ['view', 'id' => $model->cons_sub_type_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-cons-sub-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
