<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhConsSubTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การข้อมูลประเภทย่อย';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-cons-sub-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'cons_sub_type_id',
            'cons_sub_type_name',
            [
                'attribute' => 'cons_sub_pt_id',
                'value' => 'consSubPt.cons_sub_pt_name',
            ],
            //'cons_sub_pt_id',
            'cons_sub_type_level',
           

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
