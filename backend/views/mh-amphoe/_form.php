<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MhProvince;
use yii\helpers\ArrayHelper;
use backend\models\BackendLogin;

/* @var $this yii\web\View */
/* @var $model common\models\MhAmphoe */
/* @var $form yii\widgets\ActiveForm */
$user_backend = ArrayHelper::map(BackendLogin::find()->asArray()->all(), 'id', 'firstname');
$province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');
?>

<div class="mh-amphoe-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'amphoe_name_th')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amphoe_name_en')->textInput(['maxlength' => true]) ?>

    <?='' //$form->field($model, 'updated_id')->textInput() ?>

    <?=
                $form
                    ->field($model, 'updated_id')
                    
                    ->dropdownList($user_backend, [
                        'id' => 'ddl-roguser',
                        
                    ])
            ?>


    <?= $form->field($model, 'created_time')->textInput([
        'readonly' => true,
        'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
    ]) ?>
    
    <?= $form->field($model, 'updated_time')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <?='' //$form->field($model, 'province_id')->textInput() ?>

    <?=
                $form
                    ->field($model, 'province_id')
                    ->label('ชื่อจังหวัด')
                    ->dropdownList($province, [
                        'id' => 'ddl-province',
                        'prompt' => 'เลือกจังหวัด'
                    ])
            ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
