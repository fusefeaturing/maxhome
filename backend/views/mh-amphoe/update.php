<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhAmphoe */

$this->title = 'แก้ไขข้อมูลอำเภอ: ' . $model->amphoe_name_th;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลอำเภอ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->amphoe_name_th, 'url' => ['view', 'id' => $model->amphoe_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-amphoe-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
