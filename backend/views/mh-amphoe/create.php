<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhAmphoe */

$this->title = 'เพิ่มข้อมูลอำเภอ';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลอำเภอ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-amphoe-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
