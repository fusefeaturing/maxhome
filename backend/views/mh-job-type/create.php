<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobType */

$this->title = 'เพิ่มข้อมูลหมวดหมู่ประเภทงานรับเหมาก่อสร้าง';
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลหมวดหมู่ประเภทงานรับเหมาก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'subpt' => [],
    ]) ?>

</div>
