<?php

use common\models\MhJob;
use common\models\MhJobProductType;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\MhJobType */
/* @var $form yii\widgets\ActiveForm */
$jobpt = ArrayHelper::map(MhJobProductType::find()->asArray()->all(), 'job_pt_id', 'job_pt_name');
$jobid = ArrayHelper::map(MhJob::find()->asArray()->all(), 'job_id', 'job_name');
?>

<div class="mh-job-type-form">

    <?php $form = ActiveForm::begin(); ?>

  
    

        <?=     
                $form
                    ->field($model, 'job_id')
                    ->widget(Select2::classname(), [
                        'data' => $jobid,
                        'model' => $model,
                        'attribute' => 'job_id',
                        'language' => 'th',
                        'options' => ['placeholder' => 'เลือกกิจการ'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            //'multiple' => true,
                        ],                    
                ])    
                ->label(false);                  
            ?>
    
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'job_type_pt_id')

                    ->dropdownList($jobpt, [
                        'id' => 'ddl-job_type_pt_id',
                        'prompt' => 'เลือกประเภท'
                    ])
            ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-4">
            <?=
                $form
                    ->field($model, 'job_type_sub_pt_id')
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => 'ddl-subpt'],
                        'data' => $subpt,
                        'pluginOptions' => [
                            'depends' => ['ddl-job_type_pt_id'],
                            'placeholder' => 'เลือกประเภทย่อย',
                            'url' => Url::to(['/mh-job-type/get-subpt'])
                        ]
                    ]);
            ?>
        </div>
    </div>

    

    <?= $form->field($model, 'created_date')->textInput([
        'readonly' => true,
        'value' => (($model->created_date != null) && ($model->created_date != '0000-00-00 00:00:00')) ? $model->created_date : date('Y-m-d H:i:s')
    ]) ?>

    <?= $form->field($model, 'updated_data')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
