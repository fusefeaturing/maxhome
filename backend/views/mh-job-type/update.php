<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobType */

$this->title = 'แก้ไขข้อมูลหมวดหมู่ประเภทงานรับเหมาก่อสร้าง: ' . $model->jobTypePt->job_pt_name. ' - ' .$model->jobTypeSubPt->job_sub_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลหมวดหมู่ประเภทงานรับเหมาก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->job_type_id, 'url' => ['view', 'id' => $model->job_type_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-job-type-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
        'subpt' => $subpt,
    ]) ?>

</div>
