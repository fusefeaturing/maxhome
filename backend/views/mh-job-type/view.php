<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobType */

$this->title = $model->jobTypePt->job_pt_name. ' - ' .$model->jobTypeSubPt->job_sub_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลหมวดหมู่ประเภทงานรับเหมาก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-job-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->job_type_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->job_type_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'job_type_id',
            //'job_id',
            [
                'attribute' => 'job_id',
                'value' => $model->job->job_name
            ],
            //'job_type_pt_id',
            [
                'attribute' => 'job_type_pt_id',
                'value' => $model->jobTypePt->job_pt_name
            ],
            //'job_type_sub_pt_id',
            [
                'attribute' => 'job_type_sub_pt_id',
                'value' => $model->jobTypeSubPt->job_sub_pt_name
            ],
            'created_date',
            'updated_time',
        ],
    ]) ?>

</div>
