<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhJobTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'หมวดหมู่ประเภทงานรับเหมาก่อสร้าง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'job_type_id',
            //'job_id',
            [
                'attribute' => 'job_id',
                'value' => 'job.job_name',
            ],
            //'job_type_pt_id',
            [
                'attribute' => 'job_type_pt_id',
                'value' => 'jobTypePt.job_pt_name',
            ],
            //'job_type_sub_pt_id',
            [
                'attribute' => 'job_type_sub_pt_id',
                'value' => 'jobTypeSubPt.job_sub_pt_name',
            ],
            'created_date',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
