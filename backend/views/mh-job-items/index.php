<?php

use common\models\MhJobProductType;
use common\models\MhJobSubPt;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhJobItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'งานรับเหมาก่อสร้าง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-items-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['addproductlists'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'job_items_id',
            //'job_item_name',
            [
                'attribute' => 'job_product_id',
                'value' => 'jobProduct.job_product_name',
                'label' => 'ชื่องานรับเหมา'
            ],
            'job_item_level',

            [
                'attribute' => 'job_pt_id',
                'value' => 'jobPt.job_pt_name',
                'filter'=>ArrayHelper::map(MhJobProductType::find()->asArray()->all(), 'job_pt_id', 'job_pt_name') 
            ],

            [
                'attribute' => 'job_sub_pt_id',
                'value' => 'jobSubPt.job_sub_pt_name',
                'filter'=>ArrayHelper::map(MhJobSubPt::find()->asArray()->all(), 'job_sub_pt_id', 'job_sub_pt_name') 
            ],

            //'job_price',
            [
                'attribute' => 'job_id',
                'value' => 'job.job_name',
            ],
            [
                'attribute' => 'user_id',
                'value' => 'user.user_firstname',
            ],
            //'job_product_id',
            //'job_price',

            //'job_id',
            //'user_id',
            //'job_pt_id',
            //'job_sub_pt_id',
            //'job_province_id',
            //'job_amphoe_id',
            //'updated_id',
            //'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
