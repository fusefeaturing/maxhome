<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobItems */

$this->title = $model->jobPt->job_pt_name .' - '. $model->jobSubPt->job_sub_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'งานรับเหมาก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-job-items-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['addproductlists'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('แก้ไข', ['updateproduct', 'id' => $model->job_items_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->job_items_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <div class="panel panel-default">        
  
  <div class="panel-body">
      <p class="card-text"><strong>ประเภทสินค้า</strong> : <?= $model->jobPt->job_pt_name ?> - <?= $model->jobSubPt->job_sub_pt_name ?></p>
      <p class="card-text"><strong>รายละเอียดสินค้า : <br> <?= nl2br($model->jobProduct->job_product_description) ?></strong></p>
      <!--<p class="card-text"><strong>ราคาสินค้า</strong> : <?='' //$model->job_price ?> </p>-->                     
      <p class="card-text"><strong>ชื่อกิจการ</strong> : <?= $model->job->job_name ?> </p>            
      <h4 class="card-text"><strong>ชื่อเจ้าของกิจการ</strong> : <?= $model->user->user_firstname ?></h4>
      <p class="card-text"><strong>เวลาสร้าง</strong> : <?= $model->created_time ?></p>
      <p class="card-text"><strong>เวลาแก้ไข</strong> : <?= $model->updated_time ?></p>
</div>

</div>

    <?='' /*DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'job_items_id',
            //'job_item_name',
            'job_item_level',
            'job_product_id',
            
            'job_price',
            //'job_id',
            [
                'attribute' => 'job_id',
                'value' => $model->job->job_name,
            ],
            //'user_id',
            [
                'attribute' => 'user_id',
                'value' => $model->user->user_firstname,
            ],
            //'job_pt_id',
            [
                'attribute' => 'job_pt_id',
                'value' => $model->jobPt->job_pt_name,
            ],
            //'job_sub_pt_id',
            [
                'attribute' => 'job_sub_pt_id',
                'value' => $model->jobSubPt->job_sub_pt_name,
            ],
            //'job_province_id',
            //'job_amphoe_id',
            //'updated_id',
            [
                'attribute' => 'updated_id',
                'value' => $model->admin->firstname,
            ],
            'created_time',
            'updated_time',
        ],
    ]) */?>

</div>
