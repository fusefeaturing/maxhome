<?php

use backend\models\BackendLogin;
use common\models\MhJob;
use common\models\MhJobProduct;
use common\models\MhJobProductType;
use common\models\MhUser;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobItems */
/* @var $form yii\widgets\ActiveForm */
$job_product_id = Yii::$app->getRequest()->getQueryParam('id');
$userid = ArrayHelper::map(MhUser::find()->asArray()->all(), 'user_id', 'user_firstname');
$jobproduct = ArrayHelper::map(MhJobProduct::find()->asArray()->all(), 'job_product_id', 'job_product_name');
$jobid = ArrayHelper::map(MhJob::find()->asArray()->all(), 'job_id', 'job_name');
$user_backend = ArrayHelper::map(BackendLogin::find()->asArray()->all(), 'id', 'firstname');
$jobpt = ArrayHelper::map(MhJobProductType::find()->asArray()->all(), 'job_pt_id', 'job_pt_name');
?>

<div class="mh-job-items-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="panel panel-default">
        <div class="panel-body">
            <p class="card-text">ชื่องานรับเหมาก่อสร้าง : <?= $modelpro->job_product_name ?></p>
            <p class="card-text">รายละเอียด : <?= $modelpro->job_product_description ?></p>    
            <p class="card-text">หมวด : <?= $modelpro->jobPt->job_pt_name ?></p>
            <p class="card-text">หมู่ : <?= $modelpro->jobSubPt->job_sub_pt_name ?></p>

        </div>
    </div>

    

    <div class="row">             

        <div class="col-xs-12 col-sm-12 col-md-12">
            
        
            <?= 
                $form
                    ->field($model, 'job_item_level')
                    ->textInput(['maxlength' => true, 'value' => '0'])
                    //->input('', ['placeholder' => ""])
                    ->label(false) 
            ?>


            <?= 
                $form
                    ->field($model, 'job_product_id')
                    ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $job_product_id])
                    ->label(false)
            ?>


            <?= 
                $form
                    ->field($model, 'job_pt_id')
                    ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $modelpro->jobPt->job_pt_id])
                    ->label(false)
            ?>  

            <?= 
                $form
                    ->field($model, 'job_sub_pt_id')
                    ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $modelpro->jobSubPt->job_sub_pt_id])
                    ->label(false)
            ?>  



        </div>
    </div>

    <?='' //$form->field($model, 'job_price')->textInput(['maxlength' => true]) ?>



            <?=
                $form
                    ->field($model, 'job_id')
                    ->widget(Select2::classname(), [
                        'data' => $jobid,
                        'language' => 'th',
                        'options' => [/*'multiple' => true,*/ 'placeholder' => 'เลือกร้านค้า'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],                    
                ])    
                ->label('เลือกร้านค้า', ['style' => 'color:red']);       
                 
            ?>


            <?=
                $form
                    ->field($model, 'user_id')
                    ->widget(Select2::classname(), [
                        'data' => $userid,
                        'language' => 'th',
                        'options' => [/*'multiple' => true,*/ 'placeholder' => 'เลือกผู้ใช้งาน'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],                    
                ])    
                ->label('เลือกผู้ใช้งาน', ['style' => 'color:red']);       
                 
            ?>

    <?=
                $form
                    ->field($model, 'updated_id')
                    
                    ->dropdownList($user_backend, [
                        'id' => 'ddl-roguser',
                        'prompt' => ''
                        
                    ])
                    
    ?>

            <?= $form->field($model, 'created_time')->textInput([
                'readonly' => true,
                'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
            ]) ?>

            <?= $form->field($model, 'updated_time')->textInput([
                'readonly' => true,
                'value' => date('Y-m-d H:i:s')
            ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
