<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobItems */

$this->title = 'แก้ไขงานรับเหมาก่อสร้าง: ' . $model->job_items_id;
$this->params['breadcrumbs'][] = ['label' => 'งานรับเหมาก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->job_items_id, 'url' => ['view', 'id' => $model->job_items_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-job-items-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'subpt' => $subpt,
       
    ]) ?>

</div>
