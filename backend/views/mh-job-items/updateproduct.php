<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhjobItems */

$this->title = 'แก้ไขข้อมูลสินค้า: ' . $model->jobProduct->job_product_name;
$jobid = Yii::$app->getRequest()->getQueryParam('id');
$this->params['breadcrumbs'][] = ['label' => 'ร้านค้า' , 'url' => ['mh-jobtruction/view','id' => $jobid]];
if (Yii::$app->user->isGuest) {
    
    $this->params['breadcrumbs'][] = ['label' => 'สินค้าภายในร้าน', 'url' => ['mh-job-items/itemsstore', 'id' => $model->job_id]];
    
} else {    

    $this->params['breadcrumbs'][] = ['label' => 'สินค้าภายในร้านของฉัน', 'url' => ['mh-job-items/myitems', 'id' => $jobid, ]];

}
$this->params['breadcrumbs'][] = ['label' => $model->jobProduct->job_product_name, 'url' => ['view', 'id' => $model->job_items_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-job-items-update">

    <div class="card2">
        <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_formupdateproduct', [
        'model' => $model,
        //'modelpro' => $modelpro,
    ]) ?>

</div>
