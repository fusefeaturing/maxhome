<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MhJobItemsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-job-items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'job_items_id') ?>

    <?= $form->field($model, 'job_item_name') ?>

    <?= $form->field($model, 'job_item_level') ?>

    <?= $form->field($model, 'job_product_id') ?>

    <?= $form->field($model, 'job_price') ?>

    <?php // echo $form->field($model, 'job_id') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'job_pt_id') ?>

    <?php // echo $form->field($model, 'job_sub_pt_id') ?>

    <?php // echo $form->field($model, 'job_province_id') ?>

    <?php // echo $form->field($model, 'job_amphoe_id') ?>

    <?php // echo $form->field($model, 'updated_id') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
