<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use common\models\MhJob;
use common\models\MhJobProduct;
use common\models\MhUser;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\MhjobItems */
/* @var $form yii\widgets\ActiveForm */
$id = yii::$app->user->identity->id;
$job_product_id = Yii::$app->getRequest()->getQueryParam('id');
$userid = ArrayHelper::map(MhUser::find()->asArray()->all(), 'user_id', 'user_firstname');
$jobproduct = ArrayHelper::map(MhJobProduct::find()->asArray()->all(), 'job_product_id', 'job_product_name');
$jobid = ArrayHelper::map(MhJob::find()->andWhere('job_id')->asArray()->all(), 'job_id', 'job_name');


?>

<body>

<div class="mh-job-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <p class="card-text">ชื่องานรับเหมาก่อสร้าง : <?= $model->jobProduct->job_product_name ?></p>
            <p class="card-text">รายละเอียด : <?= $model->jobProduct->job_product_description ?></p>    
            <p class="card-text">หมวด : <?= $model->jobProduct->jobPt->job_pt_name ?></p>
            <p class="card-text">หมู่ : <?= $model->jobProduct->jobSubPt->job_sub_pt_name ?></p>
        </div>
    </div>



    <div class="panel panel-default" style="padding-left:20px; padding-top:20px;">


        <div class="panel-body">

        <?= 
                $form
                    ->field($model, 'job_product_id')
                    ->hiddenInput(['maxlength' => true  ])
                    ->label(false)
        ?>

            <?= $form->field($model, 'job_pt_id')
                            ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $model->jobPt->job_pt_id])
                            ->label(false)
            ?>  

            <?= $form->field($model, 'job_sub_pt_id')
                            ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $model->jobSubPt->job_sub_pt_id])
                            ->label(false)
            ?>  


            
            <?= 
                $form
                    ->field($model, 'job_item_level')
                    ->hiddenInput(['maxlength' => true, 'value' => '0'])
                    //->input('', ['placeholder' => ""])
                    ->label(false) 
            ?>
            





            <?='' //$form->field($model, 'job_price')->textInput(['maxlength' => true]) ?>


          


            <?= $form->field($model, 'job_id')
                            ->dropdownList($jobid, [
                                'id' => 'ddl-roguser',        
                                'prompt' => 'เลือกร้านของฉัน'                
                            ])
                            ->label('ร้านค้าของฉัน')
            ?>

<?=
                $form
                    ->field($model, 'user_id')
                    ->widget(Select2::classname(), [
                        'data' => $userid,
                        'language' => 'th',
                        'options' => [/*'multiple' => true,*/ 'placeholder' => 'เลือกผู้ใช้งาน'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],                    
                ])    
                ->label('เลือกผู้ใช้งาน', ['style' => 'color:red']);       
                 
            ?>       

            

            <?='' /*$form->field($model, 'job_sub_type_id')
                            ->textInput(['maxlength' => true, 'readonly' => true, 'value' => $modelpro->subtype()])
                            ->label(false)*/
            ?>  



            <?='' /*$form->field($model, 'job_id')
                            ->textInput(['maxlength' => true, 'readonly' => true, 'value' => $jobid])
                            ->label(false)*/
            ?>




                            
            <?= $form->field($model, 'created_time')->textInput([
                'readonly' => true,
                'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
            ]) ?>

            <?= $form->field($model, 'updated_time')->textInput([
                'readonly' => true,
                'value' => date('Y-m-d H:i:s')
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
            </div>
        
        </div>
    </div>
    
<br>
    <?php ActiveForm::end(); ?>

</div>
<script>
    // Get the modal
var modal = document.getElementById('myModal');

modal.addEventListener('click',function(){
this.style.display="none";
})

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// Get all images and insert the clicked image inside the modal
// Get the content of the image description and insert it inside the modal image caption
var images = document.getElementsByTagName('img');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
var i;
for (i = 0; i < images.length; i++) {
  images[i].onclick = function() {
    modal.style.display = "block";
    modalImg.src = this.src;
    modalImg.alt = this.alt;
    captionText.innerHTML = this.nextElementSibling.innerHTML;
  }
}
</script>

</body>
