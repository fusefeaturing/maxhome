<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhJobType;

/**
 * MhJobTypeSearch represents the model behind the search form of `common\models\MhJobType`.
 */
class MhJobTypeSearch extends MhJobType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_type_id' ], 'integer'],
            [['created_date', 'updated_time', 'job_id', 'job_type_pt_id', 'job_type_sub_pt_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhJobType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->joinWith(['job']);
        $query->joinWith(['jobTypePt']);
        $query->joinWith(['jobTypeSubPt']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_type_id' => $this->job_type_id,
            //'job_id' => $this->job_id,
            //'job_type_pt_id' => $this->job_type_pt_id,
            //'job_type_sub_pt_id' => $this->job_type_sub_pt_id,
            'created_date' => $this->created_date,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'Mh_job_product_type.job_pt_name', $this->job_type_pt_id])
        ->andFilterWhere(['like', 'Mh_job_sub_pt.job_sub_pt_name', $this->job_type_sub_pt_id])        
        ->andFilterWhere(['like', 'mh_job.job_name', $this->job_id]);


        return $dataProvider;
    }
}
