<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhConsSubType;

/**
 * MhConsSubTypeSearch represents the model behind the search form of `common\models\MhConsSubType`.
 */
class MhConsSubTypeSearch extends MhConsSubType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_sub_type_id', 'cons_sub_pt_id', 'updated_id'], 'integer'],
            [['cons_sub_type_name', 'created_time', 'updated_time', 'cons_sub_type_level'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhConsSubType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['cons_sub_type_level' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cons_sub_type_id' => $this->cons_sub_type_id,
            //'cons_sub_pt_id' => $this->cons_sub_pt_id,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'cons_sub_type_name', $this->cons_sub_type_name])
        ->andFilterWhere(['like', 'Mh_cons_sub_pt.cons_sub_pt_name', $this->cons_sub_pt_id]);

        return $dataProvider;
    }
}
