<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhJobProduct;

/**
 * MhJobProductSearch represents the model behind the search form of `common\models\MhJobProduct`.
 */
class MhJobProductSearch extends MhJobProduct
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_product_id', 'job_pt_id', 'job_sub_pt_id', 'updated_id'], 'integer'],
            [['job_product_name', 'job_product_description', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhJobProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_product_id' => $this->job_product_id,
            'job_pt_id' => $this->job_pt_id,
            'job_sub_pt_id' => $this->job_sub_pt_id,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'job_product_name', $this->job_product_name])
            ->andFilterWhere(['like', 'job_product_description', $this->job_product_description]);

        return $dataProvider;
    }
}
