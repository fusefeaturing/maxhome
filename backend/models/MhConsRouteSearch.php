<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhConsRoute;


/**
 * MhConsRouteSearch represents the model behind the search form of `common\models\MhConsRoute`.
 */
class MhConsRouteSearch extends MhConsRoute
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_route_id'], 'integer'],
            [['created_date', 'updated_date', 'cons_route_province_id', 'cons_route_amphoe_id', 'user_id', 'cons_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhConsRoute::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        
        $query->joinWith(['user']);
        $query->joinWith(['cons']);
        $query->joinWith(['consRouteProvince']);
        $query->joinWith(['consRouteAmphoe']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cons_route_id' => $this->cons_route_id,
            //'user_id' => $this->user_id,
            
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);


        $query->andFilterWhere(['like', 'mh_province.province_name_th', $this->cons_route_province_id])
        ->andFilterWhere(['like', 'mh_amphoe.amphoe_name_th', $this->cons_route_amphoe_id])
        ->andFilterWhere(['like', 'mh_user.user_firstname', $this->user_id])
        ->andFilterWhere(['like', 'mh_construction.cons_name', $this->cons_id]);

        return $dataProvider;
    }
}
