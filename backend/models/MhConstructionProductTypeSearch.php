<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhConstructionProductType;

/**
 * MhConstructionProductTypeSearch represents the model behind the search form of `common\models\MhConstructionProductType`.
 */
class MhConstructionProductTypeSearch extends MhConstructionProductType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_pt_id', 'cons_pt_level'], 'integer'],
            [['cons_pt_name', 'updated_id', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhConstructionProductType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['cons_pt_level' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cons_pt_id' => $this->cons_pt_id,
            'cons_pt_level' => $this->cons_pt_level,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'cons_pt_name', $this->cons_pt_name])
            ->andFilterWhere(['like', 'updated_id', $this->updated_id]);

        return $dataProvider;
    }
}
