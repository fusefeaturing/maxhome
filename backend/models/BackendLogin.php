<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "backend_login".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $username
 * @property string $password
 * @property string $auth_key
 * @property string $password_reset_token
 */
class BackendLogin extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'backend_login';
    }

    public $authKey;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'username', 'password', 'auth_key', 'password_reset_token'], 'required'],
            [['firstname', 'lastname', 'password_reset_token'], 'string', 'max' => 255],
            [['username', 'password'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {

        return [
            'id' => Yii::t('app', 'ID'),
            'firstName' => Yii::t('app', 'First Name'),
            'lastName' => Yii::t('app', 'Last Name'),
            'userName' => Yii::t('app', 'User Name'),
            'password' => Yii::t('app', 'Password'),
            'authKey' => Yii::t('app', 'Auth Key'),
            'password_reset_token' => Yii::t('app', 'password reset token'),
        ];
    }

    public static function findIdentity($id){
		return static::findOne($id);
	}
 
	public static function findIdentityByAccessToken($token, $type = null){
		throw new \yii\base\NotSupportedException();
	}
 
	public function getId(){
		return $this->id;
	}
 
	public function getAuthKey()
    {
	return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
    return $this->authKey === $authKey;
    }
	public static function findByUsername($username){
		return self::findOne(['username'=>$username]);
	}
 
	public function validatePassword($password){
		return $this->password === $password;
    }
}
