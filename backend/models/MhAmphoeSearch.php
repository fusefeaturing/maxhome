<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhAmphoe;

/**
 * MhAmphoeSearch represents the model behind the search form of `common\models\MhAmphoe`.
 */
class MhAmphoeSearch extends MhAmphoe
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amphoe_id', 'updated_id'], 'integer'],
            [['amphoe_name_th', 'amphoe_name_en', 'created_time', 'updated_time', 'province_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhAmphoe::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->joinWith(['province']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'amphoe_id' => $this->amphoe_id,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
           
        ]);

        $query->andFilterWhere(['like', 'amphoe_name_th', $this->amphoe_name_th])
            ->andFilterWhere(['like', 'amphoe_name_en', $this->amphoe_name_en])
            ->andFilterWhere(['like', 'Mh_province.province_name_th', $this->province_id]);

        return $dataProvider;
    }
}
