<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhStore;

/**
 * MhStoreSearch represents the model behind the search form of `common\models\MhStore`.
 */
class MhStoreSearch extends MhStore
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['store_id', 'user_id', 'store_province_id', 'store_amphoe_id', 'updated_id'], 'integer'],
            [['store_name', 'store_pic', 'store_address', 'store_gps', 'store_km', 'store_tel1', 'store_tel2', 'store_line_id', 'store_line_ad', 'store_fb_fp', 'store_messenger', 'store_email', 'store_website', 'store_other', 'store_data_other', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhStore::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'store_id' => $this->store_id,
            'user_id' => $this->user_id,
            'store_province_id' => $this->store_province_id,
            'store_amphoe_id' => $this->store_amphoe_id,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'store_name', $this->store_name])
            ->andFilterWhere(['like', 'store_pic', $this->store_pic])
            ->andFilterWhere(['like', 'store_address', $this->store_address])
            ->andFilterWhere(['like', 'store_gps', $this->store_gps])
            ->andFilterWhere(['like', 'store_km', $this->store_km])
            ->andFilterWhere(['like', 'store_tel1', $this->store_tel1])
            ->andFilterWhere(['like', 'store_tel2', $this->store_tel2])
            ->andFilterWhere(['like', 'store_line_id', $this->store_line_id])
            ->andFilterWhere(['like', 'store_line_ad', $this->store_line_ad])
            ->andFilterWhere(['like', 'store_fb_fp', $this->store_fb_fp])
            ->andFilterWhere(['like', 'store_messenger', $this->store_messenger])
            ->andFilterWhere(['like', 'store_email', $this->store_email])
            ->andFilterWhere(['like', 'store_website', $this->store_website])
            ->andFilterWhere(['like', 'store_other', $this->store_other])
            ->andFilterWhere(['like', 'store_data_other', $this->store_data_other]);

        return $dataProvider;
    }
}
