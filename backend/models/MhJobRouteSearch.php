<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhJobRoute;

/**
 * MhJobRouteSearch represents the model behind the search form of `common\models\MhJobRoute`.
 */
class MhJobRouteSearch extends MhJobRoute
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_route_id'], 'integer'],
            [['created_date', 'updated_date' ,'job_id' , 'user_id', 'job_route_province_id', 'job_route_amphoe_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhJobRoute::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        
        $query->joinWith(['job']);
        $query->joinWith(['jobRouteProvince']);
        $query->joinWith(['jobRouteAmphoe']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_route_id' => $this->job_route_id,
            //'user_id' => $this->user_id,
            //'job_route_province_id' => $this->job_route_province_id,
            //'job_route_amphoe_id' => $this->job_route_amphoe_id,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'mh_province.province_name_th', $this->job_route_province_id])
        ->andFilterWhere(['like', 'mh_amphoe.amphoe_name_th', $this->job_route_amphoe_id])
        
        ->andFilterWhere(['like', 'mh_job.job_name', $this->job_id]);


        return $dataProvider;
    }
}
