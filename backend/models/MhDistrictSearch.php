<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhDistrict;

/**
 * MhDistrictSearch represents the model behind the search form of `common\models\MhDistrict`.
 */
class MhDistrictSearch extends MhDistrict
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['district_id', 'district_zip_code', 'updated_id'], 'integer'],
            [['district_name_th', 'district_name_en', 'created_time', 'updated_time', 'amphoe_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhDistrict::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->joinWith(['amphoe']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'district_id' => $this->district_id,
            'district_zip_code' => $this->district_zip_code,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
            
        ]);

        $query->andFilterWhere(['like', 'district_name_th', $this->district_name_th])
            ->andFilterWhere(['like', 'district_name_en', $this->district_name_en])
            ->andFilterWhere(['like', 'Mh_amphoe.amphoe_name_th', $this->amphoe_id]);

        return $dataProvider;
    }
}
