<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhConstruction;

/**
 * MhConstructionSearch represents the model behind the search form of `common\models\MhConstruction`.
 */
class MhConstructionSearch extends MhConstruction
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_id', 'user_id', 'cons_province_id', 'cons_amphoe_id', 'cons_pt_id', 'updated_id', 'cons_search_level'], 'integer'],
            [['cons_name', 'cons_pic', 'cons_gps', 'cons_address', 'cons_km', 'cons_tel1', 'cons_tel2', 'cons_line_id', 'cons_line_ad', 'cons_fb_fp', 'cons_messenger', 'cons_email', 'cons_website', 'cons_other', 'cons_data_other', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhConstruction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['cons_search_level' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cons_id' => $this->cons_id,
            'cons_search_level' => $this->cons_search_level,
            'user_id' => $this->user_id,
            'cons_province_id' => $this->cons_province_id,
            'cons_amphoe_id' => $this->cons_amphoe_id,
            'cons_pt_id' => $this->cons_pt_id,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'cons_name', $this->cons_name])
            ->andFilterWhere(['like', 'cons_pic', $this->cons_pic])
            ->andFilterWhere(['like', 'cons_gps', $this->cons_gps])
            ->andFilterWhere(['like', 'cons_address', $this->cons_address])
            ->andFilterWhere(['like', 'cons_km', $this->cons_km])
            ->andFilterWhere(['like', 'cons_tel1', $this->cons_tel1])
            ->andFilterWhere(['like', 'cons_tel2', $this->cons_tel2])
            ->andFilterWhere(['like', 'cons_line_id', $this->cons_line_id])
            ->andFilterWhere(['like', 'cons_line_ad', $this->cons_line_ad])
            ->andFilterWhere(['like', 'cons_fb_fp', $this->cons_fb_fp])
            ->andFilterWhere(['like', 'cons_messenger', $this->cons_messenger])
            ->andFilterWhere(['like', 'cons_email', $this->cons_email])
            ->andFilterWhere(['like', 'cons_website', $this->cons_website])
            ->andFilterWhere(['like', 'cons_other', $this->cons_other])
            ->andFilterWhere(['like', 'cons_data_other', $this->cons_data_other]);

        return $dataProvider;
    }
}
