<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhAeRoute;

/**
 * MhAeRouteSearch represents the model behind the search form of `common\models\MhAeRoute`.
 */
class MhAeRouteSearch extends MhAeRoute
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ae_route_id'], 'integer'],
            [['created_date', 'updated_date', 'user_id', 'ae_id', 'ae_route_province_id', 'ae_route_amphoe_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhAeRoute::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->joinWith(['user']);
        $query->joinWith(['ae']);
        $query->joinWith(['aeRouteProvince']);
        $query->joinWith(['aeRouteAmphoe']);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ae_route_id' => $this->ae_route_id,
            //'user_id' => $this->user_id,
            //'ae_route_province_id' => $this->ae_route_province_id,
            //'ae_route_amphoe_id' => $this->ae_route_amphoe_id,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);


        $query->andFilterWhere(['like', 'mh_province.province_name_th', $this->ae_route_province_id])
        ->andFilterWhere(['like', 'mh_amphoe.amphoe_name_th', $this->ae_route_amphoe_id])
        ->andFilterWhere(['like', 'mh_user.user_firstname', $this->user_id])
        ->andFilterWhere(['like', 'mh_architect_engineer.ae_name', $this->ae_id]);

        return $dataProvider;
    }
}
