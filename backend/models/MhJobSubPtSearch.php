<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhJobSubPt;

/**
 * MhJobSubPtSearch represents the model behind the search form of `common\models\MhJobSubPt`.
 */
class MhJobSubPtSearch extends MhJobSubPt
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_sub_pt_id', 'updated_id'], 'integer'],
            [['job_sub_pt_name', 'created_time', 'updated_time', 'job_pt_id', 'job_sub_pt_level'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhJobSubPt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['job_sub_pt_level' => SORT_ASC]]
        ]);

        $this->load($params);
        $query->joinWith(['jobPt']);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_sub_pt_id' => $this->job_sub_pt_id,
            //'job_pt_id' => $this->job_pt_id,
            'updated_id' => $this->updated_id,
            'job_sub_pt_level' => $this->job_sub_pt_level,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'job_sub_pt_name', $this->job_sub_pt_name])
                ->andFilterWhere(['like', 'Mh_job_product_type.job_pt_name', $this->job_pt_id]);

        return $dataProvider;
    }
}
