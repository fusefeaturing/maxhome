<?php

namespace frontend\controllers;

use Yii;
use common\models\MhJob;
use frontend\models\MhJobSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use common\models\MhAmphoe;
use common\models\MhDistrict;
use common\models\MhJobItems;
use common\models\MhJobProduct;
use common\models\MhJobProductType;
use common\models\MhJobRoute;
use common\models\MhJobSubPt;
use common\models\MhJobType;
use common\models\MhProvince;
use common\models\MhUser;
use yii\data\Pagination;

/**
 * MhJobController implements the CRUD actions for MhJob model.
 */
class MhJobController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $upload_foler = 'uploads';

    /**
     * Lists all MhJob models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new MhJob();
        $jobsub = MhJobSubPt::find();
        $searchModel = new MhJobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $amphoe = ArrayHelper::map($this->getAmphoe($model->job_amphoe_id), 'amphoe_id', 'amphoe_name_th');
        $subpt = ArrayHelper::map($this->getSubpt($model->job_pt_id), 'id', 'name');

        $jobloops = MhJob::find();
        $pagination = new Pagination([
            'totalCount' => $jobloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $jobloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $jobloops->orderBy('job_search_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/

        return $this->render('view_all_joblist', [
            'model' => $model,
            'jobsub' => $jobsub,
            'amphoe' => $amphoe,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'jobloops' => $jobloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            'subpt' => $subpt,
        ]);
    }

    public function actionMyjob($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = MhJob::find($id);

        $user_id = Yii::$app->user->identity;
        $searchModel = new MhJobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);


        $jobloops = MhJob::find()
            ->where(['in', 'user_id', $user_id]);
        $pagination = new Pagination([
            'totalCount' => $jobloops->count(),
            'defaultPageSize' => 6

        ]);

        /*$countQuery =  clone $jobloops;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $jobloops->orderBy('job_search_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
            die();*/


        return $this->render('my_job', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'jobloops' => $jobloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            'model' => $model,
        ]);
    }


    public function actionAddRoute($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = MhJob::findOne($id);


        $province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');

        $request = Yii::$app->request;

        if ($request->isPost) {
            $selectedprovince = $request->post('selectedprovince');
            $selectedamphoe = $request->post('selectedamphoe');
            $job_id = $request->post('job_id');

            if ($request->post('step') == 'provinceselector') {
                $redirect = 'select_amphoe';

                $province = ArrayHelper::map(
                    MhProvince::find()
                        ->where(['in', 'province_id', $selectedprovince])
                        ->orderBy(['province_name_th' => SORT_ASC])
                        ->asArray()
                        ->all(),
                    'province_id',
                    'province_name_th'
                );

                $amphoe = ArrayHelper::map(
                    MhAmphoe::find()
                        ->where(['in', 'province_id', $selectedprovince])
                        ->asArray()
                        ->orderBy(['amphoe_name_th' => SORT_ASC])
                        ->all(),
                    'amphoe_id',
                    'amphoe_name_th'
                );

                $amphIDs = ArrayHelper::map(
                    MhAmphoe::find()
                        ->asArray()
                        ->all(),
                    'amphoe_id',
                    'amphoe_id'
                );

                $selectedamphoe = $amphIDs;
            }

            if ($request->post('step') == 'amphoeselector') {
                //save
                $this->saveUserRoute($job_id, $selectedamphoe);
                //redirect
                //return $this->redirect(['car/route']);
                return $this->redirect(['view', 'id' => $model->job_id]);
            }

            return $this->render($redirect, [
                'province' => $province,
                'amphoe' => $amphoe,
                'selectedprovince' => $selectedprovince,
                'selectedamphoe' => $selectedamphoe,
                'id' => $model->job_id,
                'model' => $model,
            ]);
        } else {
            $selectedprovince = [];


            $user_id = Yii::$app->user->id;

            $searchModel = new MhJobSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider
                ->query
                ->where(['in', 'user_id', $user_id]);

            $serviceroutes = ArrayHelper::map(
                MhJobRoute::find()
                    ->where(['in', 'job_id', $id])
                    ->orderBy(['job_route_province_id' => SORT_ASC])
                    ->asArray()
                    ->all(),
                'job_route_amphoe_id',
                'job_route_province_id'
            );


            $province = ArrayHelper::map(
                MhProvince::find()
                    ->asArray()
                    ->orderBy(['province_name_th' => SORT_ASC])
                    ->all(),
                'province_id',
                'province_name_th'
            );

            $amphoe = ArrayHelper::map(
                MhAmphoe::find()
                    ->asArray()
                    ->orderBy(['amphoe_name_th' => SORT_ASC])
                    ->all(),
                'amphoe_id',
                'amphoe_name_th'
            );

            return $this->render('select_province', [
                'province' => $province,
                'selectedprovince' => $selectedprovince,
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

                'province' => $province,
                'amphoe' => $amphoe,
                'serviceroutes' => $serviceroutes,
            ]);
        }
    }

    protected function saveUserRoute($job_id, $amphoeds)
    {
        //delete old
        $old_user_route = MhJobRoute::find()
            ->where(['in', 'job_id', $job_id])
            ->all();

        if ($old_user_route != null) {
            foreach ($old_user_route as $route) {
                $route->delete();
            }
        }

        //find all
        $amphoe = MhAmphoe::find()
            ->where(['in', 'amphoe_id', $amphoeds])
            ->all();

        //add new
        foreach ($amphoe as $amphoes) {
            $new_route = new MhJobRoute();
            $new_route->job_id = $job_id;
            $new_route->job_route_province_id = $amphoes->province_id;
            $new_route->job_route_amphoe_id = $amphoes->amphoe_id;
            $new_route->created_date = date('Y-m-d H:i:s');
            $new_route->updated_date = date('Y-m-d H:i:s');
            //$new_route->save();
            if ($new_route->save()) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => 'เพิ่มพื้นที่บริการเรียบร้อย',
                    'options' => ['class' => 'alert alert-success']
                ]);
            } else {
                Yii::$app->session->setFlash('error', "ไม่สามารถเพิ่มพื้นที่บริการได้ !!!");
            }

            //var_dump($new_route); echo '<br>';
        }
        //exit();
    }

    protected function getUserPermitData($id)
    {
        if (($model = MhUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findUserRoute($job_id)
    {
        if (($model = MhJobRoute::find()
            ->where(['in', 'job_id', $job_id])
            ->all()) !== null) {
            return $model;
        }
    }

    public function getAmphoeByProvinceID($provinceID)
    {
        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->where(['in', 'province_id', $provinceID])
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );

        return $amphoe;
    }

    public function getAmphoeByProvinceName($provinceName)
    {
        $province = MhProvince::find()
            ->where(['in', 'province_name_th', $provinceName])
            ->one();

        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->where(['in', 'province_id', $province->province_id])
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );

        return $amphoe;
    }



    /**
     * Displays a single MhJob model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $owner = false;
        $model = $this->findModel($id);

        $user = Yii::$app->user->identity;



        $user_id = Yii::$app->user->id;

        $searchModel = new MhJobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);

        $serviceroutes = ArrayHelper::map(
            MhJobRoute::find()
                ->where(['in', 'job_id', $id])
                ->orderBy(['job_route_province_id' => SORT_ASC])
                ->asArray()
                ->all(),
            'job_route_amphoe_id',
            'job_route_province_id'
        );

        $province = ArrayHelper::map(
            MhProvince::find()
                ->asArray()
                ->orderBy(['province_name_th' => SORT_ASC])
                ->all(),
            'province_id',
            'province_name_th'
        );

        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->asArray()
                ->orderBy(['amphoe_name_th' => SORT_ASC])
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );

        if (Yii::$app->user->isGuest) {
        } else {
            $user_id = Yii::$app->user->id;
            if ($model->user_id == $user_id) {
                $owner = true;
            }
        }

//select  รหัส, ชื่อ, ตำแหย่งงาน from table พนักงาน;
        // add job-type


        $servicetype = ArrayHelper::map(
            MhJobType::find()
                ->where(['in', 'job_id', $id])
                ->orderBy(['job_type_pt_id' => SORT_ASC])
                ->asArray()
                ->all(),
            'job_type_sub_pt_id',
            'job_type_pt_id'
        );

        $pt = ArrayHelper::map(
            MhJobProductType::find()
                ->asArray()
                ->orderBy(['job_pt_name' => SORT_ASC])
                ->all(),
            'job_pt_id',
            'job_pt_name'
        );

        $subpt = ArrayHelper::map(
            MhJobSubPt::find()
                ->asArray()
                ->orderBy(['job_sub_pt_name' => SORT_ASC])
                ->all(),
            'job_sub_pt_id',
            'job_sub_pt_name'
        );

        if (Yii::$app->user->isGuest) {
        } else {
            $user_id = Yii::$app->user->id;
            if ($model->user_id == $user_id) {
                $owner = true;
            }
        }

        $jobloops = MhJobItems::find()
            ->where(['in', 'job_id', $id]);


        $pagination = new Pagination([
            'totalCount' => $jobloops->count(),
            'defaultPageSize' => 9
        ]);

        /*$countQuery =  clone $jobloops;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $jobloops->orderBy('job_item_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
            die();*/


        return $this->render('view', [
            'model' => $model,
            'owner' => $owner,

            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,

            'province' => $province,
            'amphoe' => $amphoe,
            'serviceroutes' => $serviceroutes,


            'pt' => $pt,
            'subpt' => $subpt,
            'servicetype' => $servicetype,
        ]);
    }


    /**
     * Creates a new MhJob model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = new MhJob();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->job_product_id) {
                $model->job_product_id = implode(",", $model->job_product_id);
            }

            if ($model->validate()) {
                $model->job_pic = $model->uploadMultiple($model, 'job_pic');
            }

            if ($model->save()) {
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('alert', [
                        'body' => 'เพิ่มข้อมูลเรียบร้อย เชิญเพิ่มพื้นที่บริการด้านล่าง',
                        'options' => ['class' => 'alert alert-success']
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', "ไม่สามารถเพิ่มข้อมูลได้ !!!");
                }
                //return $this->redirect(['view', 'id' => $model->job_id]);
                return $this->redirect(['view', 'id' => $model->job_id]);
                //return $this->redirect(['mh-job/my_job']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MhJob model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $amphoe = ArrayHelper::map($this->getAmphoe($model->job_province_id), 'id', 'name');
        $zipcode = ArrayHelper::map($this->getZipcode($model->job_amphoe_id), 'id', 'name');
        $subpt = ArrayHelper::map($this->getSubpt($model->job_pt_id), 'id', 'name');

        $oldimage = $model->job_pic;
        $imagesexplode = explode(",", $oldimage);

        if ($model->load(Yii::$app->request->post())) {

            //$oldFile = Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $oldimage;


            if ($model->job_product_id) {
                $model->job_product_id = implode(",", $model->job_product_id);
            }

            if ($model->validate()) {

                $model->job_pic = $model->uploadMultiple($model, 'job_pic');

                $job_pic = $model->job_pic;

                //echo $photo;
                //die();

                if ($oldimage == null) {
                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->job_id]);
                    }
                } else if ($job_pic != $oldimage) {

                    foreach ($imagesexplode as $images) {
                        unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $images);
                    }
                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->job_id]);
                    }
                } else if ($job_pic == $oldimage) {

                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->job_id]);
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'amphoe' => $amphoe,
            'zipcode' => $zipcode,
            'subpt' => $subpt,


        ]);
    }

    /**
     * Deletes an existing MhJob model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = MhJob::findOne($id);
        $images = explode(",", $model->job_pic);


        if ($model->job_pic != null) {

            foreach ($images as $image) {
                unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $image);
            }

            $this->findModel($id)->delete();
        } else {

            $this->findModel($id)->delete();
            //echo 'ไม่สามารถลบข้อมูลได้';
            //die();
        }
        return $this->redirect(['index']);
    }

    public function actionDeleteall($id)
    {
        $model = $this->findModel($id);

        foreach (MhJobRoute::find($id)
            ->select('job_route_id')
            ->where('job_id')
            ->all() as $user) {
            $user->delete();
        }
        return $this->redirect(['view', 'id' => $model->job_id]);

        /*BEGIN
	DELETE FROM mh_jobtruction
    	WHERE job_id = old.job_id;
       END*/
    }

    /**
     * Finds the MhJob model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MhJob the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MhJob::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetAmphoe()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphoe($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetZipcode()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $province_id = empty($ids[0]) ? null : $ids[0];
            $amphoe_id = empty($ids[1]) ? null : $ids[1];
            if ($province_id != null) {
                $data = $this->getZipcode($amphoe_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getAmphoe($id)
    {
        $datas = MhAmphoe::find()->where(['province_id' => $id])->orderBy('amphoe_name_th')->all();
        return $this->MapData($datas, 'amphoe_id', 'amphoe_name_th');
    }

    protected function getZipcode($id)
    {
        $datas = MhDistrict::find()->where(['amphoe_id' => $id])->groupBy('district_zip_code')->all();
        return $this->MapData($datas, 'district_id', 'district_zip_code');
    }

    public function actionGetSubpt()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $job_pt_id = $parents[0];
                $out = $this->getSubpt($job_pt_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getSubpt($id)
    {
        $datas = MhJobSubPt::find()->where(['job_pt_id' => $id])->all();
        return $this->MapData($datas, 'job_sub_pt_id', 'job_sub_pt_name');
    }


    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }

    public function getJobTypes()
    {
        if (($model = MhJobProductType::find()) !== null) {
            return ArrayHelper::map(
                MhJobProductType::find()
                    ->orderBy([
                        'job_pt_level' => SORT_ASC
                    ])
                    ->asArray()
                    ->all(),
                'job_pt_id',

                function ($model) {
                    if ($model['job_pt_name']) return $model['job_pt_name'];
                    else return $model['job_pt_name'];
                }
            );
        }

        return array(-1, 'No car data');
    }

    public function getJobProductTypes()
    {
        if (($model = MhJobSubPt::find()) !== null) {
            return ArrayHelper::map(
                MhJobSubPt::find()
                    ->orderBy([
                        'job_pt_id' => SORT_ASC
                    ])

                    ->all(),
                'job_sub_pt_id',
                function ($model) {
                    if ($model['job_pt_id'] == $model['job_sub_pt_id']) return $model['job_pt_id'];
                    else return $model->jobPt->job_pt_name . ' - ' . $model->job_sub_pt_name;
                }
            );
        }

        return array(-1, 'No car data');
    }

    public static function getJobSearch()
    {
        $options = [];

        $parents = MhJobProductType::find()->orderBy('job_pt_level')->all();
        //$parents = MhJobSubPt::find()->where('job_pt_id')->orderBy('job_pt_id')->all();
        foreach ($parents as $id => $p) {
            $children = MhJobSubPt::find()->where('job_pt_id=:job_pt_id', [':job_pt_id' => $p->job_pt_id])->all();
            $child_options = [];
            foreach ($children as $child) {
                $child_options[$child->job_pt_id] = $child->job_sub_pt_name;
            }
            //$options[$p->jobPt->job_pt_name] = $child_options;
            $options[$p->job_pt_name] = ArrayHelper::map($children, 'job_sub_pt_id', 'job_sub_pt_name');
        }
        return $options;
    }


    public function actionAddJobType($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = MhJob::findOne($id);


        $jobloops = MhJobSubPt::find();
        $pagination = new Pagination([
            'totalCount' => $jobloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $jobloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $jobloops->orderBy('job_pt_id')
            ->where('job_pt_id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/

        $pt = ArrayHelper::map(MhJobProductType::find()->asArray()->all(), 'job_pt_id', 'job_pt_name');

        $request = Yii::$app->request;

        if ($request->isPost) {
            $selectedpt = $request->post('selectedpt');
            $selectedsubpt = $request->post('selectedsubpt');
            $job_id = $request->post('job_id');

            if ($request->post('step') == 'ptselector') {
                $redirect = 'select_job_sub_pt';

                $pt = ArrayHelper::map(
                    MhJobProductType::find()
                        ->where(['in', 'job_pt_id', $selectedpt])
                        ->orderBy(['job_pt_level' => SORT_ASC])
                        ->asArray()
                        ->all(),
                    'job_pt_id',
                    'job_pt_name'
                );

                $subpt = ArrayHelper::map(
                    MhJobSubPt::find()
                        ->where(['in', 'job_pt_id', $selectedpt])
                        ->asArray()
                        ->all(),
                    'job_sub_pt_id',
                    'job_sub_pt_name'
                );

                $subptIDs = ArrayHelper::map(
                    MhJobSubPt::find()
                        ->asArray()
                        ->all(),
                    'job_sub_pt_id',
                    'job_sub_pt_id'
                );

                $selectedsubpt = $subptIDs;
            }

            if ($request->post('step') == 'subptselector') {
                //save
                $this->saveUserType($job_id, $selectedsubpt);
                //redirect
                //return $this->redirect(['car/route']);
                return $this->redirect(['view', 'id' => $model->job_id]);
            }

            return $this->render($redirect, [
                'pt' => $pt,
                'subpt' => $subpt,
                'selectedpt' => $selectedpt,
                'selectedsubpt' => $selectedsubpt,
                'id' => $model->job_id,
                'model' => $model,
            ]);
        } else {
            $selectedpt = [];


            $user_id = Yii::$app->user->id;

            $searchModel = new MhJobSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider
                ->query
                ->where(['in', 'user_id', $user_id]);

            $servicetype = ArrayHelper::map(
                MhJobType::find()
                    ->where(['in', 'job_id', $id])
                    ->orderBy(['job_type_pt_id' => SORT_ASC])
                    ->asArray()
                    ->all(),
                'job_type_sub_pt_id',
                'job_type_pt_id'
            );

            $pt = ArrayHelper::map(
                MhJobProductType::find()
                    ->orderBy(['job_pt_level' => SORT_ASC])
                    ->asArray()
                    ->all(),
                'job_pt_id',
                'job_pt_name'
            );

            $subpt = ArrayHelper::map(
                MhJobSubPt::find()
                    ->asArray()
                    ->orderBy(['job_sub_pt_level' => SORT_ASC])
                    ->all(),
                'job_sub_pt_id',
                'job_sub_pt_name'
            );



            return $this->render('select_job_pt', [

                'selectedpt' => $selectedpt,
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'pt' => $pt,
                'subpt' => $subpt,
                'servicetype' => $servicetype,
                'modelsLimits' => $modelsLimits,
                'jobloops' => $jobloops,
            ]);
        }
    }

    protected function saveUserType($job_id, $subpts)
    {
        //delete old
        $old_user_type = MhJobType::find()
            ->where(['in', 'job_id', $job_id])
            ->all();

        if ($old_user_type != null) {
            foreach ($old_user_type as $type) {
                $type->delete();
            }
        }

        //find all
        $subpt = MhJobSubPt::find()
            ->where(['in', 'job_sub_pt_id', $subpts])
            ->all();

        //add new
        foreach ($subpt as $subpts) {
            $new_type = new MhJobType();
            $new_type->job_id = $job_id;
            $new_type->job_type_pt_id = $subpts->job_pt_id;
            $new_type->job_type_sub_pt_id = $subpts->job_sub_pt_id;
            $new_type->created_date = date('Y-m-d H:i:s');
            $new_type->updated_time = date('Y-m-d H:i:s');
            //$new_type->save();
            if ($new_type->save()) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => 'เพิ่มพื้นที่บริการเรียบร้อย',
                    'options' => ['class' => 'alert alert-success']
                ]);
            } else {
                Yii::$app->session->setFlash('error', "ไม่สามารถเพิ่มพื้นที่บริการได้ !!!");
            }

            //var_dump($new_route); echo '<br>';
        }
        //exit();
    }



    protected function findUserType($job_id)
    {
        if (($model = MhJobType::find()
            ->where(['in', 'job_id', $job_id])
            ->all()) !== null) {
            return $model;
        }
    }

    public function getSubPtByPtID($ptID)
    {
        $subpt = ArrayHelper::map(
            MhJobSubPt::find()
                ->where(['in', 'job_pt_id', $ptID])
                ->asArray()
                ->all(),
            'job_sub_pt_id',
            'job_sub_pt_name'
        );

        return $subpt;
    }

    public function getSubPtByPtName($ptName)
    {
        $pt = MhJobProductType::find()
            ->where(['in', 'job_pt_name', $ptName])
            ->one();

        $subpt = ArrayHelper::map(
            MhJobSubPt::find()
                ->where(['in', 'job_pt_id', $pt->job_pt_id])
                ->asArray()
                ->all(),
            'job_sub_pt_id',
            'job_sub_pt_name'
        );

        return $subpt;
    }

    public function actionAddJobSubType($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = MhJob::findOne($id);


        $jobloops = MhJobSubPt::find();
        $pagination = new Pagination([
            'totalCount' => $jobloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $jobloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $jobloops->orderBy('job_pt_id')
            ->where('job_pt_id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/

        $pt = ArrayHelper::map(MhJobProductType::find()->asArray()->all(), 'job_pt_id', 'job_pt_name');

        $request = Yii::$app->request;

        if ($request->isPost) {
            $selectedpt = $request->post('selectedpt');
            $selectedsubpt = $request->post('selectedsubpt');
            $job_id = $request->post('job_id');
            /*var_dump($job_id);
        die();*/
            if ($request->post('step') == 'ptselector') {
                $redirect = 'select_job_sub_pt';

                $pt = ArrayHelper::map(
                    MhJobProductType::find()
                        ->where(['in', 'job_pt_id', $selectedpt])
                        ->orderBy(['job_pt_level' => SORT_ASC])
                        ->asArray()
                        ->all(),
                    'job_pt_id',
                    'job_pt_name'
                );

                $subpt = ArrayHelper::map(
                    MhJobSubPt::find()
                        ->where(['in', 'job_pt_id', $selectedpt])
                        ->asArray()
                        ->all(),
                    'job_sub_pt_id',
                    'job_sub_pt_name'
                );

                $subptIDs = ArrayHelper::map(
                    MhJobSubPt::find()
                        ->asArray()
                        ->all(),
                    'job_sub_pt_id',
                    'job_sub_pt_id'
                );

                $selectedsubpt = $subptIDs;
            }

            if ($request->post('step') == 'subptselector') {
                //save
                $this->saveUserType($job_id, $selectedsubpt);
                //redirect
                //return $this->redirect(['car/route']);
                return $this->redirect(['view', 'id' => $model->job_id]);
            }

            return $this->render($redirect, [
                'pt' => $pt,
                'subpt' => $subpt,
                'selectedpt' => $selectedpt,
                'selectedsubpt' => $selectedsubpt,
                'id' => $model->job_id,
                'model' => $model,
            ]);
        } else {
            $selectedpt = [];


            $user_id = Yii::$app->user->id;

            $searchModel = new MhJobSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider
                ->query
                ->where(['in', 'user_id', $user_id]);

            $servicetype = ArrayHelper::map(
                MhJobType::find()
                    ->where(['in', 'job_id', $id])
                    ->orderBy(['job_type_pt_id' => SORT_ASC])
                    ->asArray()
                    ->all(),
                'job_type_sub_pt_id',
                'job_type_pt_id'
            );

            $pt = ArrayHelper::map(
                MhJobProductType::find()
                    ->orderBy(['job_pt_level' => SORT_ASC])
                    ->asArray()
                    ->all(),
                'job_pt_id',
                'job_pt_name'
            );

            $subpt = ArrayHelper::map(
                MhJobSubPt::find()
                    ->orderBy('job_sub_pt_level')
                    ->asArray()
                    ->all(),
                'job_sub_pt_id',
                'job_sub_pt_name'
            );



            return $this->render('select_job_sub_pt2', [

                'selectedpt' => $selectedpt,
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'pt' => $pt,
                'subpt' => $subpt,
                'servicetype' => $servicetype,
                'modelsLimits' => $modelsLimits,
                'jobloops' => $jobloops,
            ]);
        }
    }
}
