<?php

namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\MhContact;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\MhUser;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
        ];
    }


    public function oAuthSuccess($client)
    {
        // get user data from client


        // do some thing with user data. for example with $userAttributes['email']

        $unit_debug = false;
        Yii::$app->user->logout();

        // get user data from client
        $userAttributes = $client->getUserAttributes();

        if ($unit_debug) {
            echo '<pre>';
            var_dump($userAttributes);
            echo '</pre>';
        }

        if (empty($userAttributes['email'])) {
            Yii::$app->session->setFlash('error', 'กรุณากด Allow Access ใน Facebook เพื่อใช้งาน Facebook Login');
            return $this->redirect('/site/login');
        }

        //$user = MhUser::findOne(['user_email' => $userAttributes['email']]);
        $user = MhUser::findOne(['user_fb_id' => $userAttributes['id']]);
        if ($user) {

            echo 'Welcome back';
            Yii::$app->getUser()->login($user, Yii::$app->params['user.remmberMeDuration']);
            //print_r($user);
            // die();



            if ($unit_debug) {
                echo '<pre>';
                var_dump($user);
                echo '</pre>';
                exit();
            }
        } else {
            //echo 'New User';
            //getusername from mail
            $uname = explode("@", $userAttributes['email']);
            $getuser = MhUser::findOne(['username' => $uname[0]]);
            if ($getuser) {
                //echo 'dupe username';
                $rand = rand(10, 99);
                $username = $uname[0] . $rand;
            } else {
                //echo 'usable username';
                $username = $uname[0];
            }


            echo $username;
            echo '<br>';

            $name = explode(" ", $userAttributes['name']);

            $new_user = new MhUser();
            $new_user->username = $username;
            $new_user->user_password = Yii::$app->security->generatePasswordHash($username);
            $new_user->user_email = $userAttributes['email'];
            //$new_user->email_contact = $userAttributes['email'];
            $new_user->user_fb_id = $userAttributes['id'];
            //$new_user->is_user_verified = 0;
            $new_user->created_time = date('Y-m-d H:i:s');
            $new_user->updated_time = date('Y-m-d H:i:s');

            $new_user->user_tel1 = 'กรุณาใส่เบอร์ติดต่อ';



            if ($new_user->save()) {
                $name = explode(" ", $userAttributes['name']);
                $new_user->user_firstname = $name[0];
                $new_user->user_lastname = $name[1];
                $new_user->save();

                Yii::$app->getUser()->login($new_user, Yii::$app->params['user.remmberMeDuration']);
            } else {
                $name = explode(" ", $userAttributes['name']);
                echo '<br>';
                echo 'Register Failed <br>';
                print_r($new_user->getErrors());
                echo '<br>';
                echo '<br>';

                echo '<br>';
                print_r($name[0]);
                echo '<br>';
                print_r($name[1]);

                var_dump($new_user);
                die();
            }

            if ($unit_debug) {
                echo '<pre>';
                var_dump($new_user);
                echo '</pre>';
                exit();
            }
        }
    }


    /*
public function oAuthSuccess($client) {
    // get user data from client
    $userAttributes = $client->getUserAttributes();
    echo '<pre>';
    var_dump($userAttributes);
    echo '</pre>';
    die();
}
*/


    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new MhUser();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionMap()
    {

        $contacts = MhContact::find()->all();

        return $this->render('map', [
            'contacts' => $contacts
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        //$model = new MhUser();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
                //'modeluser' => $modeluser,
            ]);
        }
    }



    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


    public function actionGroupface()
    {
        return $this->render('groupface');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
                return $this->goHome();
            }
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    // public function actionRegister()
    // {

    //     $model = new MhUser();

    //     if ($model->load(Yii::$app->request->post())) {

    //         $model->user_password = password_hash($_POST['password']['password'], PASSWORD_ARGON2I);

    //         if ($model->save()) {
    //             return $this->redirect(['site/login']);
    //         }

    //         return $this->redirect(['site/login']);
    //     }

    //     return $this->render('_form_register', [
    //         'model' => $model,
    //     ]);
    // }
}
