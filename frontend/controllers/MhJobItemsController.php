<?php

namespace frontend\controllers;

use backend\models\MhJobProductSearch;
use common\models\MhAmphoe;
use common\models\MhJob;
use Yii;
use common\models\MhJobItems;
use common\models\MhJobProduct;
use common\models\MhJobProductType;
use common\models\MhJobRoute;
use common\models\MhJobSubPt;
use common\models\MhProvince;
use frontend\models\MhJobItemsSearch;
use frontend\models\MhJobSearch;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * MhJobItemsController implements the CRUD actions for MhJobItems model.
 */
class MhJobItemsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $upload_foler = 'uploads';

    /**
     * Lists all MhJobItems models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new MhJobItemsSearch();
        $modeljob = new MhJobSearch();
        $searchModel = new MhJobItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $amphoe = ArrayHelper::map($this->getAmphoe($model->job_amphoe_id), 'id', 'name');
        $subpt = ArrayHelper::map($this->getSubpt($model->job_pt_id), 'id', 'name');

        $jobloops = MhJobItems::find();                
        $pagination = new Pagination([
            'totalCount' => $jobloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $jobloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $jobloops->orderBy('job_item_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/


        return $this->render('index', [
            'model' => $model,
            'modeljob' => $modeljob,
            'amphoe' => $amphoe,
            'subpt' => $subpt,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modeljob' => $modeljob,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
        ]);
    }


    public function actionAddproductlists() {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $modeljob = new MhJobSearch();
        $user_id = Yii::$app->user->identity; 
        $model = new MhJobProductSearch();
        $searchModel = new MhJobProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $subpt = ArrayHelper::map($this->getSubpt($model->job_pt_id), 'id', 'name');

        $jobloops = MhJobProduct::find();                
        $pagination = new Pagination([
            'totalCount' => $jobloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $jobloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $jobloops->orderBy('')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/

        return $this->render('addproductlists', [
            'model' => $model,
            'modeljob' => $modeljob,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'jobloops' => $jobloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            'subpt' => $subpt,
        ]);
    }


    public function actionUpdateproduct($id)
    {
        $model = $this->findModel($id);    

        /*$subpt = ArrayHelper::map($this->getSubpt($model->job_pt_id), 'id', 'name');
        $subtype = ArrayHelper::map($this->getSubtype($model->job_sub_pt_id), 'id', 'name');*/
        $modelpro = MhJobProduct::findAll($id);
        if ($model->load(Yii::$app->request->post()) ) {            

            if ($model->save()) {

                return $this->redirect(['view', 'id' => $model->job_items_id]);
            }
                
            
        }
        
        return $this->render('updateproduct', [
            'model' => $model,
            'modelpro' => $modelpro,
            /*'subpt' => $subpt,
            'subtype' => $subtype,*/
            
        ]);
    }


    protected function findModelProduct($id)
    {
        if (($model = MhJobProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionMyitems($id)
    {


        $user_id = Yii::$app->user->identity;
        
        
        $model = MhJobItems::findOne($id);
        
        /*var_dump($id);
        die();*/
        $searchModel = new MhJobItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$amphoe = ArrayHelper::map($this->getAmphoe($modeljob->job_amphoe_id), 'id', 'name');
        //$subpt = ArrayHelper::map($this->getSubpt($modeljob->job_pt_id), 'id', 'name');
        $dataProvider
            ->query
            ->andFilterWhere(['in', 'job_id', $id]);

        

            $jobloops = MhJobItems::find()
            ->where(['in', 'job_id', $id]);
     
                
            $pagination = new Pagination([
                'totalCount' => $jobloops->count(),
                'defaultPageSize' => 27
            ]);
    
            /*$countQuery =  clone $jobloops;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
            $modelsLimits = $jobloops->orderBy('job_item_level')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            /*var_dump($countQuery);
            die();*/

        return $this->render('my_job_items', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'jobloops' => $jobloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            //'id' => $model->job_id,
            //'amphoe' => $amphoe,
            //'subpt' => $subpt,
            //'modeljob' => $modeljob,
        ]);
    }

    public function actionItemsstore($id)
    {
       

        $user_id = Yii::$app->user->identity;
        $request = Yii::$app->request;
        $modeljob = new MhJobSearch();
        $job_id = $request->post('job_id');
        
        /*var_dump($job_id);
                die();*/

        $model = MhJob::findOne($id);
        /*var_dump($id);
                die();*/
        $searchModel = new MhJobItemsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $amphoe = ArrayHelper::map($this->getAmphoe($modeljob->job_amphoe_id), 'id', 'name');
        $subpt = ArrayHelper::map($this->getSubpt($modeljob->job_pt_id), 'id', 'name');
        $dataProvider
            ->query
            ->andFilterWhere(['in', 'job_id', $id]);

        

            $jobloops = MhJobItems::find()
            ->where(['in', 'job_id', $id]);
                
            $pagination = new Pagination([
                'totalCount' => $jobloops->count(),
                'defaultPageSize' => 27
            ]);
    
            /*$countQuery =  clone $jobloops;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
            $modelsLimits = $jobloops->orderBy('job_item_level')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            /*var_dump($countQuery);
            die();*/

            $modelsLimitss = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('itemsstore', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'jobloops' => $jobloops,
            'modelsLimits' => $modelsLimits,
            'modelsLimitss' => $modelsLimitss,
            'pagination' => $pagination,
            'amphoe' => $amphoe,
            'subpt' => $subpt,
            'modeljob' => $modeljob,
        ]);
    }




    /**
     * Displays a single MhJobItems model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $owner = false;
        $model = $this->findModel($id);
        $user_id = Yii::$app->user->id;

        $searchModel = new MhJobSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);

        $serviceroutes = ArrayHelper::map(
            MhJobRoute::find()
                ->where(['in', 'job_id', $id])
                ->orderBy(['job_route_province_id' => SORT_ASC])
                ->asArray()
                ->all(),
            'job_route_amphoe_id',
            'job_route_province_id'
        );

        $province = ArrayHelper::map(
            MhProvince::find()
                ->asArray()
                ->all(),
            'province_id',
            'province_name_th'
        );

        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );
        
        if ($model->user_id == $user_id) {
            $owner = true;
        }      

        return $this->render('view', [
            'model' => $model,
            'owner' => $owner,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

            'province' => $province,
            'amphoe' => $amphoe,
            'serviceroutes' => $serviceroutes,
        ]);
    }

    /**
     * Creates a new MhJobItems model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MhJobItems();

        if ($model->load(Yii::$app->request->post())) {



            if ($model->validate()) {
                $model->job_item_pic = $model->uploadMultiple($model, 'job_item_pic');
            }

            if ($model->save()) {
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('alert', [
                        'body' => 'เพิ่มข้อมูลเรียบร้อย เชิญเพิ่มพื้นที่บริการด้านล่าง',
                        'options' => ['class' => 'alert alert-success']
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', "ไม่สามารถเพิ่มข้อมูลได้ !!!");
                }
                //return $this->redirect(['view', 'id' => $model->job_id]);
                return $this->redirect(['view', 'id' => $model->job_items_id]);
                //return $this->redirect(['mh-job/my_job']);
            }
        }
        


        return $this->render('create', [
            'model' => $model,
            
        ]);
    }

    /**
     * Updates an existing MhJobItems model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $oldimage = $model->job_item_pic;
        $imagesexplode = explode(",", $oldimage);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->validate()) {
                
                $model->job_item_pic = $model->uploadMultiple($model, 'job_item_pic');
                
                $job_item_pic = $model->job_item_pic;
               
                //echo $photo;
                //die();

                if ($oldimage == null) {
                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->job_items_id]);
                    }

                } else if ( $job_item_pic != $oldimage ) {

                    foreach ($imagesexplode as $images) {
                        unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $images);
                    }
                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->job_items_id]);
                    }
               
                } else if ($job_item_pic == $oldimage) {

                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->job_items_id]);
                    }
                }
            }

        }
        

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
        }

        return $this->render('update', [
            'model' => $model,
            
        ]);
    }

    /**
     * Deletes an existing MhJobItems model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = MhJobItems::findOne($id);
        $images = explode(",", $model->job_item_pic);

        if ($model->job_item_pic != null) {
            
            foreach ($images as $image) {
                unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $image);
            }
            
            $this->findModel($id)->delete();
        } else {
           
            $this->findModel($id)->delete();
            //echo 'ไม่สามารถลบข้อมูลได้';
            //die();
        }

        return $this->redirect(['my_job_items']);
    }

    /**
     * Finds the MhJobItems model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MhJobItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MhJobItems::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function getjobTypes()
    {
        if (($model = MhJobProductType::find()) !== null) {
            return ArrayHelper::map(
                MhJobProductType::find()
                    ->orderBy([
                        'job_pt_level' => SORT_ASC
                    ])
                    ->asArray()
                    ->all(),
                'job_pt_id',

                function ($model) {
                    if ($model['job_pt_name']) return $model['job_pt_name'];
                    else return $model['job_pt_name'];
                }
            );
        }

        return array(-1, 'No car data');
    }

    public function actionGetAmphoe()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphoe($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getAmphoe($id)
    {
        $datas = MhAmphoe::find()->where(['province_id' => $id])->orderBy('amphoe_name_th')->all();
        return $this->MapData($datas, 'amphoe_id', 'amphoe_name_th');
    }

    public function actionGetSubpt()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $job_pt_id = $parents[0];
                $out = $this->getSubpt($job_pt_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getSubpt($id)
    {
        $datas = MhJobSubPt::find()->where(['job_pt_id' => $id])->all();
        return $this->MapData($datas, 'job_sub_pt_id', 'job_sub_pt_name');
    }

    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }

    public function getProductTypes()
    {
        if (($model = MhJobSubPt::find()) !== null) {
            return ArrayHelper::map(
                MhJobProduct::find()->all(),
                'job_product_id',
                function ($model) {
                    if ( $model['job_pt_id'] == $model['job_sub_pt_id']) return $model['job_pt_id'];
                    else return $model->jobPt->job_pt_name . ' - ' . $model->jobSubPt->job_sub_pt_name;
                }
            );
        }

        return array(-1, 'No car data');
    }

}
