<?php

namespace frontend\controllers;

use Yii;
use common\models\MhArchitectEngineer;
use frontend\models\MhArchitectEngineerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\MhAeProductType;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\MhAmphoe;
use common\models\MhProvince;
use common\models\MhUser;
use common\models\MhAeRoute;
use common\models\MhDistrict;
use yii\data\Pagination;

/**
 * MhArchitectEngineerController implements the CRUD actions for MhArchitectEngineer model.
 */
class MhArchitectEngineerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MhArchitectEngineer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new MhArchitectEngineer();
        $searchModel = new MhArchitectEngineerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $amphoe = ArrayHelper::map($this->getAmphoe($model->ae_amphoe_id), 'id', 'name');


        $aeloops = MhArchitectEngineer::find();
        $pagination = new Pagination([
            'totalCount' => $aeloops->count(),
            'defaultPageSize' => 27
        ]);


        /*$countQuery =  clone $aeloops;


        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $aeloops->orderBy('ae_search_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/


        return $this->render('view_all_aelist', [
            'model' => $model,
            'amphoe' => $amphoe,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'aeloops' => $aeloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
        ]);
    }


    public function actionMyae()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user_id = Yii::$app->user->identity;

        $searchModel = new MhArchitectEngineerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);

        
            $aeloops = MhArchitectEngineer::find()
                ->where(['in', 'user_id', $user_id]);
            
            $pagination = new Pagination([
                'totalCount' => $aeloops->count(),
                'defaultPageSize' => 6
                
            ]);
    
    
            /*$countQuery =  clone $aeloops;
    
    
            $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
            $modelsLimits = $aeloops->orderBy('ae_search_level')
                ->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            /*var_dump($countQuery);
            die();*/

        return $this->render('my_ae', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'aeloops' => $aeloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
        ]);
    }



    public function actionAddRoute($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = MhArchitectEngineer::findOne($id);

        $province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');

        $request = Yii::$app->request;

        if ($request->isPost) {
            $selectedprovince = $request->post('selectedprovince');
            $selectedamphoe = $request->post('selectedamphoe');
            $ae_id = $request->post('ae_id');

            if ($request->post('step') == 'provinceselector') {
                $redirect = 'select_amphoe';

                $province = ArrayHelper::map(
                    MhProvince::find()
                        ->where(['in', 'province_id', $selectedprovince])
                        ->asArray()
                        ->all(),
                    'province_id',
                    'province_name_th'
                );

                $amphoe = ArrayHelper::map(
                    MhAmphoe::find()
                        ->where(['in', 'province_id', $selectedprovince])
                        ->asArray()
                        ->all(),
                    'amphoe_id',
                    'amphoe_name_th'
                );

                $amphIDs = ArrayHelper::map(
                    MhAmphoe::find()
                        ->asArray()
                        ->all(),
                    'amphoe_id',
                    'amphoe_id'
                );

                $selectedamphoe = $amphIDs;
            }

            if ($request->post('step') == 'amphoeselector') {
                //save
                $this->saveUserRoute($ae_id, $selectedamphoe);
                //redirect
                //return $this->redirect(['car/route']);
                return $this->redirect(['view', 'id' => $model->ae_id]);
            }

            return $this->render($redirect, [
                'province' => $province,
                'amphoe' => $amphoe,
                'selectedprovince' => $selectedprovince,
                'selectedamphoe' => $selectedamphoe,
                'id' => $model->ae_id,
                'model' => $model,
            ]);
        } else {
            $selectedprovince = [];


            $user_id = Yii::$app->user->id;

        $searchModel = new MhArchitectEngineerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);

        $serviceroutes = ArrayHelper::map(
            MhAeRoute::find()
                ->where(['in', 'ae_id', $id])
                ->orderBy(['ae_route_province_id' => SORT_ASC])
                ->asArray()
                ->all(),
            'ae_route_amphoe_id',
            'ae_route_province_id'
        );

        $province = ArrayHelper::map(
            MhProvince::find()
                ->asArray()
                ->all(),
            'province_id',
            'province_name_th'
        );

        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );

            return $this->render('select_province', [
                'province' => $province,
                'selectedprovince' => $selectedprovince,
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'province' => $province,
                'amphoe' => $amphoe,
                'serviceroutes' => $serviceroutes,
            ]);
        }
    }

    protected function saveUserRoute($ae_id, $amphoeds)
    {
        //delete old
        $old_user_route = MhAeRoute::find()
            ->where(['in', 'ae_id', $ae_id])
            ->all();

        if ($old_user_route != null) {
            foreach ($old_user_route as $route) {
                $route->delete();
            }
        }

        //find all
        $amphoe = MhAmphoe::find()
            ->where(['in', 'amphoe_id', $amphoeds])
            ->all();

        //add new
        foreach ($amphoe as $amphoes) {
            $new_route = new MhAeRoute();
            $new_route->ae_id = $ae_id;
            $new_route->ae_route_province_id = $amphoes->province_id;
            $new_route->ae_route_amphoe_id = $amphoes->amphoe_id;
            $new_route->created_date = date('Y-m-d H:i:s');
            $new_route->updated_date = date('Y-m-d H:i:s');
            //$new_route->save();
            if ($new_route->save()) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => 'เพิ่มพื้นที่บริการเรียบร้อย',
                    'options' => ['class' => 'alert alert-success']
                ]);
            } else {
                Yii::$app->session->setFlash('error', "ไม่สามารถเพิ่มพื้นที่บริการได้ !!!");
            }

            //var_dump($new_route); echo '<br>';
        }
        //exit();
    }

    protected function getUserPermitData($id)
    {
        if (($model = MhUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findUserRoute($ae_id)
    {
        if (($model = MhAeRoute::find()
            ->where(['in', 'ae_id', $ae_id])
            ->all()) !== null) {
            return $model;
        }
    }

    public function getAmphoeByProvinceID($provinceID)
    {
        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->where(['in', 'province_id', $provinceID])
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );

        return $amphoe;
    }

    public function getAmphoeByProvinceName($provinceName)
    {
        $province = MhProvince::find()
            ->where(['in', 'province_name_th', $provinceName])
            ->one();

        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->where(['in', 'province_id', $province->province_id])
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );

        return $amphoe;
    }



    /**
     * Displays a single MhArchitectEngineer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $owner = false;
        $model = $this->findModel($id);

        $user = Yii::$app->user->identity;



        $user_id = Yii::$app->user->id;

        $searchModel = new MhArchitectEngineerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);

        $serviceroutes = ArrayHelper::map(
            MhAeRoute::find()
                ->where(['in', 'ae_id', $id])
                ->orderBy(['ae_route_province_id' => SORT_ASC])
                ->asArray()
                ->all(),
            'ae_route_amphoe_id',
            'ae_route_province_id'
        );

        $province = ArrayHelper::map(
            MhProvince::find()
                ->asArray()
                ->all(),
            'province_id',
            'province_name_th'
        );

        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );

        if (Yii::$app->user->isGuest) {
        } else {
            $user_id = Yii::$app->user->id;
            if ($model->user_id == $user_id) {
                $owner = true;
            }
        }

        return $this->render('view', [
            'model' => $model,
            'owner' => $owner,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'province' => $province,
            'amphoe' => $amphoe,
            'serviceroutes' => $serviceroutes,
        ]);
    }

    /**
     * Creates a new MhArchitectEngineer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = new MhArchitectEngineer();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->ae_pt_id) {
                $model->ae_pt_id = implode(",", $model->ae_pt_id);
            }
            if ($model->validate()) {
                $model->ae_pic = $model->uploadMultiple($model, 'ae_pic');
            }

            if ($model->save()) {
                //return $this->redirect(['view', 'id' => $model->job_id]);
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('alert', [
                        'body' => 'เพิ่มข้อมูลเรียบร้อย เชิญเพิ่มพื้นที่บริการด้านล่าง',
                        'options' => ['class' => 'alert alert-success']
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', "ไม่สามารถเพิ่มข้อมูลได้ !!!");
                }
                return $this->redirect(['view', 'id' => $model->ae_id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MhArchitectEngineer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $amphoe = ArrayHelper::map($this->getAmphoe($model->ae_province_id), 'id', 'name');
        $zipcode = ArrayHelper::map($this->getZipcode($model->ae_amphoe_id), 'id', 'name');

        if ($model->load(Yii::$app->request->post())) {

            if ($model->ae_pt_id) {
                $model->ae_pt_id = implode(",", $model->ae_pt_id);
            }
            if ($model->validate()) {
                $model->ae_pic = $model->uploadMultiple($model, 'ae_pic');
            }

            if ($model->save()) {
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('alert', [
                        'body' => 'แก้ไขข้อมูลเรียบร้อย',
                        'options' => ['class' => 'alert alert-success']
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                }
                //return $this->redirect(['view', 'id' => $model->job_id]);
                return $this->redirect(['view', 'id' => $model->ae_id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'amphoe' => $amphoe,
            'zipcode' => $zipcode,
        ]);
    }

    /**
     * Deletes an existing MhArchitectEngineer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteall($id)
    {
        $model = $this->findModel($id);

        foreach (MhAeRoute::find($id)
            ->select('ae_route_id')
            ->where('ae_id')
            ->all() as $user) {
            $user->delete();
        }
        return $this->redirect(['view', 'id' => $model->ae_id]);

        /*BEGIN
	DELETE FROM mh_construction
    	WHERE cons_id = old.cons_id;
       END*/
    }

    /**
     * Finds the MhArchitectEngineer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MhArchitectEngineer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MhArchitectEngineer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetAmphoe()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphoe($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetZipcode()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $province_id = empty($ids[0]) ? null : $ids[0];
            $amphoe_id = empty($ids[1]) ? null : $ids[1];
            if ($province_id != null) {
                $data = $this->getZipcode($amphoe_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getAmphoe($id)
    {
        $datas = MhAmphoe::find()->where(['province_id' => $id])->orderBy('amphoe_name_th')->all();
        return $this->MapData($datas, 'amphoe_id', 'amphoe_name_th');
    }

    protected function getZipcode($id)
    {
        $datas = MhDistrict::find()->where(['amphoe_id' => $id])->groupBy('district_zip_code')->all();
        return $this->MapData($datas, 'district_id', 'district_zip_code');
    }


    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }


    public function getAeTypes()
    {
        if (($model = MhAeProductType::find()) !== null) {
            return ArrayHelper::map(
                MhAeProductType::find()
                    ->orderBy([
                        'ae_pt_level' => SORT_ASC
                    ])
                    ->asArray()
                    ->all(),
                'ae_pt_id',

                function ($model) {
                    if ($model['ae_pt_name']) return $model['ae_pt_name'];
                    else return $model['ae_pt_name'];
                }
            );
        }
        return array(-1, 'No car data');
    }
}
