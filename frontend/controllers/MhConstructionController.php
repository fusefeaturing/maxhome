<?php

namespace frontend\controllers;

use frontend\models\MhConsItemsSearch;
use Yii;
use common\models\MhConstruction;
use frontend\models\MhConstructionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use common\models\MhAmphoe;
use common\models\MhConsItems;
use common\models\MhConsRoute;
use common\models\MhConsSubPt;
use common\models\MhConsSubType;
use common\models\MhConsSupType;
use common\models\MhConstructionProductType;
use common\models\MhDistrict;
use common\models\MhProvince;
use common\models\MhUser;
use yii\data\Pagination;

/**
 * MhConstructionController implements the CRUD actions for MhConstruction model.
 */
class MhConstructionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $upload_foler = 'uploads';

    /**
     * Lists all MhConstruction models.
     * @return mixed
     */

    public function actionIndex()
    {

        $user_id = Yii::$app->user->identity;
        $model = new MhConstructionSearch();
        $searchModel = new MhConstructionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $amphoe = ArrayHelper::map($this->getAmphoe($model->cons_amphoe_id), 'id', 'name');
        $subpt = ArrayHelper::map($this->getSubpt($model->cons_pt_id), 'id', 'name');



        $consloops = MhConstruction::find();
        $pagination = new Pagination([
            'totalCount' => $consloops->count(),
            'defaultPageSize' => 27
        ]);

        /*$countQuery =  clone $consloops;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $consloops->orderBy('cons_search_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
        die();*/

        return $this->render('view_all_conslist', [
            'model' => $model,
            'amphoe' => $amphoe,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'consloops' => $consloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
            'subpt' => $subpt,

        ]);
    }



    public function actionMycons()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user_id = Yii::$app->user->identity;


        $searchModel = new MhConstructionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);



        $consloops = MhConstruction::find()
            ->where(['in', 'user_id', $user_id]);

        $pagination = new Pagination([
            'totalCount' => $consloops->count(),
            'defaultPageSize' => 6
        ]);

        /*$countQuery =  clone $consloops;
            $pages = new Pagination(['totalCount' => $countQuery->count()]);*/
        $modelsLimits = $consloops->orderBy('cons_search_level')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        /*var_dump($countQuery);
            die();*/

        return $this->render('my_cons', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'consloops' => $consloops,
            'modelsLimits' => $modelsLimits,
            'pagination' => $pagination,
        ]);
    }





    public function actionAddRoute($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = MhConstruction::findOne($id);


        $province = ArrayHelper::map(MhProvince::find()->asArray()->orderBy('province_name_th')->all(), 'province_id', 'province_name_th');

        $request = Yii::$app->request;

        if ($request->isPost) {
            $selectedprovince = $request->post('selectedprovince');
            $selectedamphoe = $request->post('selectedamphoe');
            $cons_id = $request->post('cons_id');



            if ($request->post('step') == 'provinceselector') {
                $redirect = 'select_amphoe';

                $province = ArrayHelper::map(
                    MhProvince::find()
                        ->where(['in', 'province_id', $selectedprovince])
                        ->asArray()
                        ->orderBy(['province_name_th' => SORT_ASC])
                        ->all(),
                    'province_id',
                    'province_name_th'
                );

                $amphoe = ArrayHelper::map(
                    MhAmphoe::find()
                        ->where(['in', 'province_id', $selectedprovince])
                        ->orderBy(['amphoe_name_th' => SORT_ASC])
                        ->asArray()
                        ->all(),
                    'amphoe_id',
                    'amphoe_name_th'
                );

                $amphIDs = ArrayHelper::map(
                    MhAmphoe::find()
                        ->asArray()
                        ->all(),
                    'amphoe_id',
                    'amphoe_id'
                );


                $selectedamphoe = $amphIDs;
            }

            if ($request->post('step') == 'amphoeselector') {
                //save
                $model = $this->findModel($id);
                $this->saveUserRoute($cons_id, $selectedamphoe);
                //redirect
                //return $this->redirect(['car/route']);
                return $this->redirect(['view', 'id' => $model->cons_id]);
            }
            /*print_r($cons_id);
                die();*/

            return $this->render($redirect, [
                'province' => $province,
                'amphoe' => $amphoe,
                'selectedprovince' => $selectedprovince,
                'selectedamphoe' => $selectedamphoe,
                'id' => $model->cons_id,
                'model' => $model,

            ]);
        } else {
            $selectedprovince = null;

            $user_id = Yii::$app->user->id;

            $searchModel = new MhConstructionSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider
                ->query
                ->where(['in', 'user_id', $user_id]);

            $serviceroutes = ArrayHelper::map(
                MhConsRoute::find()
                    ->where(['in', 'cons_id', $id])
                    ->orderBy(['cons_route_province_id' => SORT_ASC])
                    ->asArray()
                    ->all(),
                'cons_route_amphoe_id',
                'cons_route_province_id'
            );

            $province = ArrayHelper::map(
                MhProvince::find()
                    ->asArray()
                    ->orderBy(['province_name_th' => SORT_ASC])
                    ->all(),
                'province_id',
                'province_name_th'
            );

            $amphoe = ArrayHelper::map(
                MhAmphoe::find()
                    ->asArray()
                    ->orderBy(['amphoe_name_th' => SORT_ASC])
                    ->all(),
                'amphoe_id',
                'amphoe_name_th'
            );

            return $this->render('select_province', [
                'province' => $province,
                'selectedprovince' => $selectedprovince,
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,

                'province' => $province,
                'amphoe' => $amphoe,
                'serviceroutes' => $serviceroutes,

            ]);
        }
    }

    protected function saveUserRoute($cons_id, $amphoeds)
    {

        //delete old
        $old_user_route = MhConsRoute::find()
            ->where(['in', 'cons_id', $cons_id])
            ->all();

        if ($old_user_route != null) {
            foreach ($old_user_route as $route) {
                $route->delete();
            }
        }
        //var_dump($cons_id); echo '<br>'; die();

        //find all
        $amphoe = MhAmphoe::find()
            ->where(['in', 'amphoe_id', $amphoeds])
            ->all();

        //add new
        foreach ($amphoe as $amphoes) {
            $new_route = new MhConsRoute();
            $new_route->cons_id = $cons_id;
            $new_route->cons_route_province_id = $amphoes->province_id;
            $new_route->cons_route_amphoe_id = $amphoes->amphoe_id;
            $new_route->created_date = date('Y-m-d H:i:s');
            $new_route->updated_date = date('Y-m-d H:i:s');
            //$new_route->save();
            if ($new_route->save()) {
                Yii::$app->getSession()->setFlash('alert', [
                    'body' => 'เพิ่มพื้นที่บริการเรียบร้อย',
                    'options' => ['class' => 'alert alert-success']
                ]);
            } else {
                Yii::$app->session->setFlash('error', "ไม่สามารถเพิ่มพื้นที่บริการได้ !!!");
            }


            //var_dump($new_route); echo '<br>'; die();
            //print_r($new_route); die();
        }
        //exit();
    }

    protected function getUserPermitData($id)
    {
        if (($model = MhUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findUserRoute($cons_id)
    {
        if (($model = MhConsRoute::find()
            ->where(['in', 'cons_id', $cons_id])
            ->all()) !== null) {
            return $model;
        }
    }

    public function getAmphoeByProvinceID($provinceID)
    {
        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->where(['in', 'province_id', $provinceID])
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );

        return $amphoe;
    }

    public function getAmphoeByProvinceName($provinceName)
    {
        $province = MhProvince::find()
            ->where(['in', 'province_name_th', $provinceName])
            ->one();

        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->where(['in', 'province_id', $province->province_id])
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );

        return $amphoe;
    }




    /**
     * Displays a single MhConstruction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */




    public function actionView($id)
    {
        $owner = false;
        $model = $this->findModel($id);

        $user = Yii::$app->user->identity;



        $user_id = Yii::$app->user->id;

        $searchModel = new MhConstructionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);

        $serviceroutes = ArrayHelper::map(
            MhConsRoute::find()
                ->where(['in', 'cons_id', $id])
                ->orderBy(['cons_route_province_id' => SORT_ASC])
                ->asArray()
                ->all(),
            'cons_route_amphoe_id',
            'cons_route_province_id'
        );

        $province = ArrayHelper::map(
            MhProvince::find()
                ->asArray()
                ->all(),
            'province_id',
            'province_name_th'
        );

        $amphoe = ArrayHelper::map(
            MhAmphoe::find()
                ->asArray()
                ->all(),
            'amphoe_id',
            'amphoe_name_th'
        );

        if (Yii::$app->user->isGuest) {
        } else {
            $user_id = Yii::$app->user->id;
            if ($model->user_id == $user_id) {
                $owner = true;
            }
        }

        return $this->render('view', [
            'model' => $model,
            'owner' => $owner,

            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

            'province' => $province,
            'amphoe' => $amphoe,
            'serviceroutes' => $serviceroutes,
        ]);
    }


    /**
     * Creates a new MhConstruction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $model = new MhConstruction();

        if ($model->load(Yii::$app->request->post())) {

            if ($model->cons_pt_id) {
                $model->cons_pt_id = implode(",", $model->cons_pt_id);
            }

            if ($model->validate()) {
                $model->cons_pic = $model->uploadMultiple($model, 'cons_pic');
            }

            if ($model->save()) {
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('alert', [
                        'body' => 'เพิ่มข้อมูลเรียบร้อย เชิญเพิ่มพื้นที่บริการด้านล่าง',
                        'options' => ['class' => 'alert alert-success']
                    ]);
                } else {
                    Yii::$app->session->setFlash('error', "ไม่สามารถเพิ่มข้อมูลได้ !!!");
                }
                return $this->redirect(['view', 'id' => $model->cons_id]);

                //return $this->redirect(['mh-job/my_job']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MhConstruction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $amphoe = ArrayHelper::map($this->getAmphoe($model->cons_province_id), 'id', 'name');
        $zipcode = ArrayHelper::map($this->getZipcode($model->cons_amphoe_id), 'id', 'name');

        $subpt = ArrayHelper::map($this->getSubpt($model->cons_pt_id), 'id', 'name');
        $suptype = ArrayHelper::map($this->getSubtype($model->cons_sub_pt_id), 'id', 'name');

        $oldimage = $model->cons_pic;
        $imagesexplode = explode(",", $oldimage);

        if ($model->load(Yii::$app->request->post())) {

            if ($model->cons_pt_id) {
                $model->cons_pt_id = implode(",", $model->cons_pt_id);
            }

            if ($model->validate()) {

                $model->cons_pic = $model->uploadMultiple($model, 'cons_pic');

                $cons_pic = $model->cons_pic;

                //echo $photo;
                //die();

                if ($oldimage == null) {
                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->cons_id]);
                    }
                } else if ($cons_pic != $oldimage) {

                    foreach ($imagesexplode as $images) {
                        unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $images);
                    }
                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->cons_id]);
                    }
                } else if ($cons_pic == $oldimage) {

                    if ($model->save()) {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('alert', [
                                'body' => 'แก้ไขข้อมูลเรียบร้อย',
                                'options' => ['class' => 'alert alert-success']
                            ]);
                        } else {
                            Yii::$app->session->setFlash('error', "ไม่สามารถแก้ไขข้อมูลได้ !!!");
                        }
                        return $this->redirect(['view', 'id' => $model->cons_id]);
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'amphoe' => $amphoe,
            'zipcode' => $zipcode,
            'subpt' => $subpt,
            'suptype' => $suptype,
        ]);
    }

    /**
     * Deletes an existing MhConstruction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $model = MhConstruction::findOne($id);

        $images = explode(",", $model->cons_pic);

        //$oldFile = Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $oldimage;


        if ($model->cons_pic != null) {

            foreach ($images as $image) {
                unlink(Yii::getAlias('@common/web') . '/' . $this->upload_foler . '/' . $image);
            }

            $this->findModel($id)->delete();
        } else {

            $this->findModel($id)->delete();
            //echo 'ไม่สามารถลบข้อมูลได้';
            //die();
        }

        return $this->redirect(['index']);
    }

    public function actionDeleteall($id)
    {
        $model = $this->findModel($id);

        foreach (MhConsRoute::find($id)
            ->select('cons_route_id')
            ->where('cons_id')
            ->all() as $user) {
            $user->delete();
        }
        return $this->redirect(['view', 'id' => $model->cons_id]);

        /*BEGIN
	DELETE FROM mh_construction
    	WHERE cons_id = old.cons_id;
       END*/
    }

    /**
     * Finds the MhConstruction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MhConstruction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MhConstruction::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function getConsTypes()
    {
        if (($model = MhConstructionProductType::find()) !== null) {
            return ArrayHelper::map(
                MhConstructionProductType::find()
                    ->orderBy([
                        'cons_pt_level' => SORT_ASC
                    ])
                    ->asArray()
                    ->all(),
                'cons_pt_id',

                function ($model) {
                    if ($model['cons_pt_name']) return $model['cons_pt_name'];
                    else return $model['cons_pt_name'];
                }
            );
        }

        return array(-1, 'No car data');
    }

    public function actionGetAmphoe()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $province_id = $parents[0];
                $out = $this->getAmphoe($province_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetZipcode()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $province_id = empty($ids[0]) ? null : $ids[0];
            $amphoe_id = empty($ids[1]) ? null : $ids[1];
            if ($province_id != null) {
                $data = $this->getZipcode($amphoe_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getAmphoe($id)
    {
        $datas = MhAmphoe::find()->where(['province_id' => $id])->orderBy('amphoe_name_th')->all();
        return $this->MapData($datas, 'amphoe_id', 'amphoe_name_th');
    }

    protected function getZipcode($id)
    {
        $datas = MhDistrict::find()->where(['amphoe_id' => $id])->groupBy('district_zip_code')->all();
        return $this->MapData($datas, 'district_id', 'district_zip_code');
    }

    protected function MapData($datas, $fieldId, $fieldName)
    {
        $obj = [];
        foreach ($datas as $key => $value) {
            array_push($obj, ['id' => $value->{$fieldId}, 'name' => $value->{$fieldName}]);
        }
        return $obj;
    }


    public function actionGetSubpt()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cons_pt_id = $parents[0];
                $out = $this->getSubpt($cons_pt_id);
                echo Json::encode(['output' => $out, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionGetSubtype()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $cons_pt_id = empty($ids[0]) ? null : $ids[0];
            $cons_sup_pt_id = empty($ids[1]) ? null : $ids[1];
            if ($cons_pt_id != null) {
                $data = $this->getSuptype($cons_sup_pt_id);
                echo Json::encode(['output' => $data, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    protected function getSubpt($id)
    {
        $datas = MhConsSubPt::find()->where(['cons_pt_id' => $id])->all();
        return $this->MapData($datas, 'cons_sub_pt_id', 'cons_sub_pt_name');
    }

    protected function getSubtype($id)
    {
        $datas = MhConsSubType::find()->where(['cons_sub_pt_id' => $id])->all();
        return $this->MapData($datas, 'cons_sub_type_id', 'cons_sub_type_name');
    }
}
