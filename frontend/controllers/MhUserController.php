<?php

namespace frontend\controllers;

use Yii;
use common\models\MhUser;
use backend\models\MhUserSearch;
use frontend\models\MhStoreSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * MhUserController implements the CRUD actions for MhUser model.
 */
class MhUserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MhUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MhUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProfile()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user = Yii::$app->user->identity;

        return $this->render('profile', [
            'model' => $this->findModel($user->user_id),
        ]);
    }

    public function actionMybusiness()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        $user_id = Yii::$app->user->identity;
        $user = Yii::$app->user->identity;

        $searchModel = new MhStoreSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider
            ->query
            ->where(['in', 'user_id', $user_id]);


        return $this->render('my_business', [
            'model' => $this->findModel($user->id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,



        ]);
    }

    /**
     * Displays a single MhUser model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $owner = false;

        $user_id = Yii::$app->user->id;
        if ($model->user_id == $user_id) {
            $owner = true;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'owner' => $owner,
        ]);
    }

    /**
     * Creates a new MhUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MhUser();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {


            $model->user_password = Yii::$app->security->generatePasswordHash($_POST['user_password']);
            return $this->redirect(['view', 'id' => $model->user_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MhUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['profile', 'id' => $model->user_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MhUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MhUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MhUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MhUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionRegister()
    {

        $model = new MhUser();

        if ($model->load(Yii::$app->request->post())) {

            // $model->password = password_hash($_POST['MhUser']['password'], PASSWORD_ARGON2I);

            if ($model->save()) {
                return $this->redirect(['site/login']);
            }

            return $this->redirect(['site/login']);
        }

        return $this->render('_form_register', [
            'model' => $model,
        ]);
    }

    // public function actionLogin()
    // {
    //     if (!Yii::$app->user->isGuest) {
    //         return $this->goHome();
    //     }

    //     //$model = new LoginForm();
    //     $model = new MhUser();
    //     if ($model->load(Yii::$app->request->post()) && $model->login()) {
    //         return $this->goBack();
    //     } else {
    //         $model->password = '';

    //         return $this->render('login', [
    //             'model' => $model,
    //             //'modeluser' => $modeluser,
    //         ]);
    //     }
    // }
}
