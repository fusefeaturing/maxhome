<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.remmberMeDuration' => 30 * 24 * 60,
    'bsVersion' => '4.x',
    'og_title' => ['property' => 'og:title', 'content' => 'hub.maxhome.co'],

    'og_description' => ['property' => 'og:description', 'content' => 'Hub maxhome คือศูนย์ร่วมกิจการรับเหมาก่อสร้าง ช่วยแนะนำ ผู้รับเหมาก่อสร้าง ร้านวัสดุก่อสร้าง ผู้ผลิต ตัวแทนจำหน่าย ที่พร้อมให้บริการทั่วประเทศไทย ให้กับลูกค้าที่กำลังสนใจสินค้าและบริการ

    เข้าร่วม Hub maxhome ชุมชนวงการธุรกิจก่อสร้าง เสมือนว่าธุรกิจก่อสร้างของท่านตั้งอยู่ทำเลทองที่มีผู้คนสนใจสินค้าและบริการของท่านตลอดเวลา มี traffic จำนวนมากจากลูกค้าที่มีความต้องการใช้สินค้าและบริการของท่านและสามารถติดต่อไปยังธุรกิจของท่านโดยตรง
    • ระบบหน้าร้านค้าวัสดุก่อสร้างออนไลน์สำเร็จรูป ที่ให้เจ้าของธุรกิจในวงการก่อสร้างสามารถเขามาสร้างหน้าร้านของตัวเองได้ง่ายๆ ภายใน 3 นาที
    • หน้าร้านธุรกิจก่อสร้างออนไลน์ที่อยู่ใจกลางชุมชนวงการธุรกิจก่อสร้างที่มีลูกค้าค้นหาบริการของท่านตลอดเวลา
    • ระบบช่วยให้ลูกค้าของคุณสามารถค้นหาสินค้าและบริการของท่าน และติดต่อหาท่านโดยตรงทั้งทาง โทรศัพท์ , Line@ , messenger หรือดูข้อมูลของท่านเพิ่มเติมจากทาง Website หรือ Facebook fanpage ของท่าน'],
    'og_url' => ['property' => 'og:url', 'content' => 'https://hub.maxhome.co'],
    'og_image' => ['property' => 'og:image', 'content' => 'https://hub.maxhome.co/Hub-3.jpg'],
    'og_keyword' => ['property' => 'og:keyword', 'content' => 'hub.maxhome.co ,maxhome ,hub.maxhome ,hub maxhome'],
    'og_robots' => ['property' => 'og:robots', 'content' => 'all'],
    'og_revisit-after' => ['property' => 'og:revisit-after', 'content' => '3 Days'],
];
