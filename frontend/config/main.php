<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

use \yii\web\Request;

$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'on beforeRequest' => function ($event) {
        if (!Yii::$app->request->isSecureConnection) {
            $url = Yii::$app->request->getAbsoluteUrl();
            $url = str_replace('http:', 'https:', $url);
            $url = str_replace('www.', '', $url);
            Yii::$app->getResponse()->redirect($url);
            Yii::$app->end();
        }
    },
    'components' => [

        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => $baseUrl,

        ],
        'user' => [
            'identityClass' => 'common\models\MhUser',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'assetManager' => [
            'bundles' => []
        ],


        // //localhost
        // 'authClientCollection' => [
        //     'class' => 'yii\authclient\Collection',
        //     'clients' => [
        //         'facebook' => [
        //             'class' => 'yii\authclient\clients\Facebook',
        //             'clientId' => '697470570789349',
        //             'clientSecret' => '5e096f8f29905820bdba2cbbb1e47b8d',
        //         ],

        //     ],
        // ],


        //localhost new
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '853252638719135',
                    'clientSecret' => 'd74d1fc107385a5de2e333da991a42c1',
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '736152198058-sen9f2umavnr8qunsdh66fa7kod26ccv.apps.googleusercontent.com',
                    'clientSecret' => 'Vs_3jfkop6AwdMu7EYPK9nAT',
                ],

            ],
        ],



        //host
        // 'authClientCollection' => [
        //     'class' => 'yii\authclient\Collection',
        //     'clients' => [
        //         'facebook' => [
        //             'class' => 'yii\authclient\clients\Facebook',
        //             'clientId' => '159929974583361',
        //             'clientSecret' => '86f5427f9a315609ac3d455bf3d9ea6d',
        //         ], 'google' => [
        //             'class' => 'yii\authclient\clients\Google',
        //             'clientId' => '483258501648-eq61ho2c427q0jpi7h6d8a3tokl5sjd9.apps.googleusercontent.com',
        //             'clientSecret' => 'SEmjZfS0E8PQvgdoLRENbH2j',
        //         ],
        //     ],
        // ],



        'urlManager' => [
            'baseUrl' => $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'module/<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',

                //'<controller>/<action>/<id>' => '<controller>/<action>'  			
            ),
        ],






        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];
