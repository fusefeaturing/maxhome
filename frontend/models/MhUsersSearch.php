<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhUsers;

/**
 * MhUsersSearch represents the model behind the search form of `common\models\MhUsers`.
 */
class MhUsersSearch extends MhUsers
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'facebook_id', 'is_user_findcar', 'is_user_car_owner', 'is_user_verified'], 'integer'],
            [['username', 'password', 'company_name', 'company_address', 'facebook_token', 'citizen_id', 'carlic_id', 'prefix', 'name', 'surname', 'middlename', 'dob', 'email', 'email_contact', 'website', 'line_id', 'tel_1', 'tel_2', 'created_date', 'updated_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhUsers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'facebook_id' => $this->facebook_id,
            'dob' => $this->dob,
            'is_user_findcar' => $this->is_user_findcar,
            'is_user_car_owner' => $this->is_user_car_owner,
            'is_user_verified' => $this->is_user_verified,
            'created_date' => $this->created_date,
            'updated_date' => $this->updated_date,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'company_address', $this->company_address])
            ->andFilterWhere(['like', 'facebook_token', $this->facebook_token])
            ->andFilterWhere(['like', 'citizen_id', $this->citizen_id])
            ->andFilterWhere(['like', 'carlic_id', $this->carlic_id])
            ->andFilterWhere(['like', 'prefix', $this->prefix])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'middlename', $this->middlename])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'email_contact', $this->email_contact])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'line_id', $this->line_id])
            ->andFilterWhere(['like', 'tel_1', $this->tel_1])
            ->andFilterWhere(['like', 'tel_2', $this->tel_2]);

        return $dataProvider;
    }
}
