<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhConsItems;
use common\models\MhConsProduct;
use common\models\MhConsRoute;
use common\models\MhConstruction;

/**
 * MhConsItemsSearch represents the model behind the search form of `common\models\MhConsItems`.
 */
class MhConsItemsSearch extends MhConsItems
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_items_id', 'updated_id', 'cons_item_level'], 'integer'],
            [['cons_price'], 'number'],
            [['created_time', 'updated_time', 'cons_product_id', 'cons_id', 'user_id', 'cons_pt_id', 'cons_sub_pt_id', 'cons_sub_type_id', 'cons_province_id', 'cons_amphoe_id'], 'safe'],
        ];
    }

    public $province = '';
    public $amphoe = '';
    public $subpt = '';


    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhConsItems::find();

        // add conditions that should always apply here

        $this->load($params);

       
       

        // grid filtering conditions
        $query->andFilterWhere([
            'cons_items_id' => $this->cons_items_id,
            'cons_item_level' => $this->cons_item_level,
            'cons_product_id' => $this->cons_product_id,
            'cons_pt_id' => $this->cons_pt_id,
            'cons_sub_pt_id' => $this->cons_sub_pt_id,
            'cons_price' => $this->cons_price,
            //'cons_id' => $this->cons_id,
            'user_id' => $this->user_id,
            //'cons_province_id' => $this->cons_province_id,
            //'cons_amphoe_id' => $this->cons_amphoe_id,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);


        if ($this->cons_province_id != "") {
            echo $this->cons_province_id;
            $routes = MhConsRoute::find()->select("cons_id")->distinct()->where(["cons_route_province_id" => $this->cons_province_id])->all();
            $routeList = [];
            foreach( $routes as $route) {
                array_push($routeList, $route->cons_id);
            }

            if (count($routeList) > 0) {
                $query->andFilterWhere(['in', 'cons_id', $routeList]);
                /*$query->andFilterWhere([
                    'cons_id' => $routeList[0],
                ]);*/
            } else {
                $query->andFilterWhere([
                    'cons_id' => -1,
                ]);   
            }
            //print_r($shopList);
        }

        if ($this->cons_amphoe_id != "") {
            echo $this->cons_amphoe_id;
            $routes = MhConsRoute::find()->select("cons_id")->distinct()->where(["cons_route_amphoe_id" => $this->cons_amphoe_id])->all();
            $routeList = [];
            foreach( $routes as $route) {
                array_push($routeList, $route->cons_id);
            }

            if (count($routeList) > 0) {
                $query->andFilterWhere(['in', 'cons_id', $routeList]);
                /*$query->andFilterWhere([
                    'cons_id' => $routeList[0],
                ]);*/
                /*var_dump($routeList);
                die();*/
            } else {
                $query->andFilterWhere([
                    'cons_id' => -1,
                ]);   
            }
            //print_r($shopList);
        }




        $province = (isset($params['MhConsItemsSearch']['cons_province_id']))
            ? $params['MhConsItemsSearch']['cons_province_id']
            : null;

        $amphoe = (isset($params['MhConsItemsSearch']['cons_amphoe_id']))
            ? $params['MhConsItemsSearch']['cons_amphoe_id']
            : null;

        $conspt = (isset($params['MhConsItemsSearch']['cons_pt_id']))
            ? $params['MhConsItemsSearch']['cons_pt_id']
            : null;

        $subpt = (isset($params['MhConsItemsSearch']['cons_sub_pt_id']))
            ? $params['MhConsItemsSearch']['cons_sub_pt_id']
            : null;


 
    


            if ($province != null) {
                $pickprousers = MhConsRoute::find()
                    ->where(['cons_route_province_id' => $province])
                    ->select(['cons_id'])
                    ->distinct()
                    ->all();
    
                foreach ($pickprousers as $pickprouser) {
                    $picuid[] = $pickprouser->cons_id;
                }
                //echo '<pre>'.var_dump($picuid).'</pre>'; exit();
                if (isset($picuid)) {
                    $query->andFilterWhere(['cons_id' => $picuid]);
                }
            }
    
    
            if ($amphoe != null) {
                $province = MhConsRoute::find()
                    ->where(['cons_route_amphoe_id' => $amphoe])
                    ->select(['cons_id'])
                    ->distinct()
                    ->all();
    
                foreach ($province as $delprouser) {
                    $deluid[] = $delprouser->cons_id;
                }
    
                if (isset($deluid)) {
                    $query->andFilterWhere(['cons_id' => $deluid]);
                }
            }

            if ($conspt != null) {
                $pickprousers = MhConsItems::find()
                    
                    ->where(['cons_pt_id' => $conspt ])                    
                    ->select(['cons_id'])
                    
                    ->distinct()
                    ->all();
    
                foreach ($pickprousers as $pickprouser) {
                    $picuid[] = $pickprouser->cons_id;
                }
                //echo '<pre>'.var_dump($picuid).'</pre>'; exit();
                if (isset($picuid)) {
                    $query->andFilterWhere(['cons_id' => $picuid]);
                }
            }
    
    
            if ($subpt != null) {
                $pickprousers = MhConsItems::find()
                    ->where(['cons_sub_pt_id' => $subpt])
                    ->select(['cons_id'])
                    ->distinct()
                    ->all();
    
                foreach ($pickprousers as $pickprouser) {
                    $deluid[] = $pickprouser->cons_id;
                }
    
                if (isset($deluid)) {
                    $query->andFilterWhere(['cons_id' => $deluid]);
                }
            }
    


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['cons_item_level' => SORT_ASC]],
            
            'pagination' => [
                'pageSize' => 27,
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
