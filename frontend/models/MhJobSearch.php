<?php

namespace frontend\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhJob;
use common\models\MhJobItems;
use common\models\MhJobRoute;
use common\models\MhJobType;

/**
 * MhJobSearch represents the model behind the search form of `common\models\MhJob`.
 */
class MhJobSearch extends MhJob
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id', 'job_km', 'job_tel1', 'job_tel2', 'updated_id', 'job_search_level'], 'integer'],
            [['job_name', 'job_province_id', 'job_amphoe_id', 'job_district_id', 'job_pic', 'job_gps', 'job_address', 'job_line_id', 'job_line_ad', 'job_fb_fp', 'job_messenger', 'job_email', 'job_website', 'job_other', 'job_pt_id', 'job_data_other', 'created_time', 'updated_time', 'job_sub_pt_id', 'job_product_id'], 'safe'],
        ];
    }

    public $province = '';
    public $amphoe = '';

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhJob::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['job_search_level' => SORT_ASC]],

            'pagination' => [
                'pageSize' => 27,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_id' => $this->job_id,
            //'user_id' => $this->user_id,
            'job_km' => $this->job_km,

            'job_tel1' => $this->job_tel1,
            'job_tel2' => $this->job_tel2,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'job_name', $this->job_name])
            ->andFilterWhere(['like', 'job_pic', $this->job_pic])
            ->andFilterWhere(['like', 'job_gps', $this->job_gps])
            ->andFilterWhere(['like', 'job_address', $this->job_address])
            ->andFilterWhere(['like', 'job_line_id', $this->job_line_id])
            ->andFilterWhere(['like', 'job_line_ad', $this->job_line_ad])
            ->andFilterWhere(['like', 'job_fb_fp', $this->job_fb_fp])
            ->andFilterWhere(['like', 'job_messenger', $this->job_messenger])
            ->andFilterWhere(['like', 'job_email', $this->job_email])
            ->andFilterWhere(['like', 'job_website', $this->job_website])
            ->andFilterWhere(['like', 'job_other', $this->job_other])
            //->andFilterWhere(['like', 'job_pt_id', $this->job_pt_id])
            //->andFilterWhere(['like', 'job_sub_pt_id', $this->job_sub_pt_id])
            //->andFilterWhere(['like', 'job_product_id', $this->job_product_id])
            ->andFilterWhere(['like', 'job_data_other', $this->job_data_other]);

            if ($this->job_province_id != "") {
                echo $this->job_province_id;
                $routes = MhJobRoute::find()->select("job_id")->distinct()->where(["job_route_province_id" => $this->job_province_id])->all();
                $routeList = [];
                foreach( $routes as $route) {
                    array_push($routeList, $route->job_id);
                }
    
                if (count($routeList) > 0) {
                    $query->andFilterWhere(['in', 'job_id', $routeList]);
                    /*$query->andFilterWhere([
                        'job_id' => $routeList[0],
                    ]);*/
                } else {
                    $query->andFilterWhere([
                        'job_id' => -1,
                    ]);   
                }
                //print_r($shopList);
            }
    
            if ($this->job_amphoe_id != "") {
                echo $this->job_amphoe_id;
                $routes = MhJobRoute::find()->select("job_id")->distinct()->where(["job_route_amphoe_id" => $this->job_amphoe_id])->all();
                $routeList = [];
                foreach( $routes as $route) {
                    array_push($routeList, $route->job_id);
                }
    
                if (count($routeList) > 0) {
                    $query->andFilterWhere(['in', 'job_id', $routeList]);
                    /*$query->andFilterWhere([
                        'job_id' => $routeList[0],
                    ]);*/
                } else {
                    $query->andFilterWhere([
                        'job_id' => -1,
                    ]);   
                }
                //print_r($shopList);
            }



            if ($this->job_pt_id != "") {
                echo $this->job_pt_id;
               
                $routes = MhJobType::find()->select("job_id")->distinct()->where(["job_type_pt_id" => $this->job_pt_id])->all();
                $routeList = [];
    
                foreach( $routes as $route) {
                    array_push($routeList, $route->job_id);
                }   
                if (count($routeList) > 0) {
                    $query->andFilterWhere(['in', 'job_id', $routeList]);
                    /*$query->andFilterWhere([
                        'job_id' => $routeList[0],
                    ]);*/
    
                } else {
                    $query->andFilterWhere([
                        'job_id' => -1,
                    ]);   
                }
                
                //print_r($shopList);
            } 
    
    
            if ($this->job_sub_pt_id != "") {
                echo $this->job_sub_pt_id;
               
                $routes = MhJobType::find()->select("job_id")->distinct()->where(["job_type_sub_pt_id" => $this->job_sub_pt_id])->all();
                $routeList = [];
    
                foreach( $routes as $route) {
                    array_push($routeList, $route->job_id);
                }   
                if (count($routeList) > 0) {
                    $query->andFilterWhere(['in', 'job_id', $routeList]);
                    /*$query->andFilterWhere([
                        'job_id' => $routeList[0],
                    ]);*/
    
                } else {
                    $query->andFilterWhere([
                        'job_id' => -1,
                    ]);   
                }
                
                //print_r($shopList);
            } 


        $province = (isset($params['MhJobSearch']['job_province_id']))
            ? $params['MhJobSearch']['job_province_id']
            : null;

        $amphoe = (isset($params['MhJobSearch']['job_amphoe_id']))
            ? $params['MhJobSearch']['job_amphoe_id']
            : null;



        if ($province != null) {
            $pickprousers = MhJobRoute::find()
                ->where(['job_route_province_id' => $province])
                ->select(['job_id'])
                ->distinct()
                ->all();

            foreach ($pickprousers as $pickprouser) {
                $picuid[] = $pickprouser->job_id;
            }
            //echo '<pre>'.var_dump($picuid).'</pre>'; exit();
            if (isset($picuid)) {
                $query->andFilterWhere(['job_id' => $picuid]);
            }
        }


        if ($amphoe != null) {
            $amphoee = MhJobRoute::find()
                ->where(['job_route_province_id' => $amphoe])
                ->select(['job_id'])
                ->distinct()
                ->all();

            foreach ($amphoee as $delprouser) {
                $deluid[] = $delprouser->job_id;
            }

            if (isset($deluid)) {
                $query->andFilterWhere(['job_id' => $deluid]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['job_search_level' => SORT_ASC]],

            'pagination' => [
                'pageSize' => 27,
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        return $dataProvider;
    }
}
