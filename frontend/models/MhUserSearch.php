<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhUser;

/**
 * MhUserSearch represents the model behind the search form of `common\models\MhUser`.
 */
class MhUserSearch extends MhUser
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'user_tel1', 'user_tel2', 'updated_id', 'created_time', 'updated_time'], 'integer'],
            [['user_name', 'user_line_id', 'user_line_ad', 'user_fb_url', 'user_messenger', 'user_email', 'user_website', 'user_other'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhUser::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'user_tel1' => $this->user_tel1,
            'user_tel2' => $this->user_tel2,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'user_name', $this->user_name])
            ->andFilterWhere(['like', 'user_line_id', $this->user_line_id])
            ->andFilterWhere(['like', 'user_line_ad', $this->user_line_ad])
            ->andFilterWhere(['like', 'user_fb_url', $this->user_fb_url])
            ->andFilterWhere(['like', 'user_messenger', $this->user_messenger])
            ->andFilterWhere(['like', 'user_email', $this->user_email])
            ->andFilterWhere(['like', 'user_website', $this->user_website])
            ->andFilterWhere(['like', 'user_other', $this->user_other]);

        return $dataProvider;
    }
}
