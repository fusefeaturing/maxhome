<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhJobItems;
use common\models\MhJobRoute;

/**
 * MhJobItemsSearch represents the model behind the search form of `common\models\MhJobItems`.
 */
class MhJobItemsSearch extends MhJobItems
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_items_id', 'job_item_level', 'job_province_id', 'job_amphoe_id', 'updated_id'], 'integer'],
            [['job_item_name', 'job_item_description', 'job_item_date_start', 'job_item_date_end', 'created_time', 'created_time'
            , 'updated_time', 'job_product_id', 'job_id', 'user_id', 'job_pt_id', 'job_sub_pt_id'], 'safe'],
            [['job_price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhJobItems::find();

        // add conditions that should always apply here

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'job_items_id' => $this->job_items_id,
            'job_item_level' => $this->job_item_level,
            'job_item_name' => $this->job_item_name,
            'job_product_id' => $this->job_product_id,
            'job_price' => $this->job_price,
            //'job_id' => $this->job_id,
            'user_id' => $this->user_id,
            'job_pt_id' => $this->job_pt_id,
            'job_sub_pt_id' => $this->job_sub_pt_id,
            //'job_province_id' => $this->job_province_id,
            //'job_amphoe_id' => $this->job_amphoe_id,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'job_item_name', $this->job_item_name]);

        if ($this->job_province_id != "") {
            echo $this->job_province_id;
            $routes = MhJobRoute::find()->select("job_id")->distinct()->where(["job_route_province_id" => $this->job_province_id])->all();
            $routeList = [];
            foreach( $routes as $route) {
                array_push($routeList, $route->job_id);
            }

            if (count($routeList) > 0) {
                $query->andFilterWhere(['in', 'job_id', $routeList]);
                /*$query->andFilterWhere([
                    'job_id' => $routeList[0],
                ]);*/
            } else {
                $query->andFilterWhere([
                    'job_id' => -1,
                ]);   
            }
            //print_r($shopList);
        }

        if ($this->job_amphoe_id != "") {
            echo $this->job_amphoe_id;
            $routes = MhJobRoute::find()->select("job_id")->distinct()->where(["job_route_amphoe_id" => $this->job_amphoe_id])->all();
            $routeList = [];
            foreach( $routes as $route) {
                array_push($routeList, $route->job_id);
            }

            if (count($routeList) > 0) {
                $query->andFilterWhere(['in', 'job_id', $routeList]);
                /*$query->andFilterWhere([
                    'job_id' => $routeList[0],
                ]);*/
                /*var_dump($routeList);
                die();*/
            } else {
                $query->andFilterWhere([
                    'job_id' => -1,
                ]);   
            }
            //print_r($shopList);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['job_item_level' => SORT_ASC]],
            'pagination' => [
                'pageSize' => 27,
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
