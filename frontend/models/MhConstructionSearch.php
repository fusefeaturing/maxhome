<?php

namespace frontend\models;

use common\models\MhConsRoute;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhConstruction;
use common\models\MhConsItems;

/**
 * MhConstructionSearch represents the model behind the search form of `common\models\MhConstruction`.
 */
class MhConstructionSearch extends MhConstruction
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_id', 'user_id', 'updated_id', 'cons_search_level'], 'integer'],
            [['cons_name', 'cons_province_id', 'cons_amphoe_id', 'cons_pic', 'cons_gps', 'cons_address', 'cons_km', 'cons_tel1', 'cons_tel2', 'cons_line_id', 'cons_line_ad', 'cons_fb_fp', 'cons_messenger', 'cons_email', 'cons_website', 'cons_other', 'cons_data_other', 'cons_pt_id', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    public $province = '';
    public $amphoe = '';
    public $subpt = '';


    
    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhConstruction::find();

        // add conditions that should always apply here

        $this->load($params);


        // grid filtering conditions
        $query->andFilterWhere([
            'cons_id' => $this->cons_id,
            'cons_km' => $this->cons_km,
            //'user_id' => $this->user_id,


            'cons_tel1' => $this->cons_tel1,
            'cons_tel2' => $this->cons_tel2,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'cons_name', $this->cons_name])
            ->andFilterWhere(['like', 'cons_pic', $this->cons_pic])
            ->andFilterWhere(['like', 'cons_gps', $this->cons_gps])
            ->andFilterWhere(['like', 'cons_address', $this->cons_address])
            ->andFilterWhere(['like', 'cons_line_id', $this->cons_line_id])
            ->andFilterWhere(['like', 'cons_line_ad', $this->cons_line_ad])
            ->andFilterWhere(['like', 'cons_fb_fp', $this->cons_fb_fp])
            ->andFilterWhere(['like', 'cons_messenger', $this->cons_messenger])
            ->andFilterWhere(['like', 'cons_email', $this->cons_email])
            ->andFilterWhere(['like', 'cons_website', $this->cons_website])
            ->andFilterWhere(['like', 'cons_other', $this->cons_other])
            //->andFilterWhere(['like', 'cons_pt_id', $this->cons_pt_id])
            ->andFilterWhere(['like', 'cons_data_other', $this->cons_data_other]);


        if ($this->cons_province_id != "") {
            echo $this->cons_province_id;
           
            $routes = MhConsRoute::find()->select("cons_id")->distinct()->where(["cons_route_province_id" => $this->cons_province_id])->all();
            $routeList = [];

            foreach( $routes as $route) {
                array_push($routeList, $route->cons_id);
            }   
            if (count($routeList) > 0) {
                $query->andFilterWhere(['in', 'cons_id', $routeList]);
                /*$query->andFilterWhere([
                    'cons_id' => $routeList[0],
                ]);*/

            } else {
                $query->andFilterWhere([
                    'cons_id' => -1,
                ]);   
            }
            
            //print_r($shopList);
        }   
        if ($this->cons_amphoe_id != "") {
            echo $this->cons_amphoe_id;
            $routes = MhConsRoute::find()->select("cons_id")->distinct()->where(["cons_route_amphoe_id" => $this->cons_amphoe_id])->all();
            $routeList = [];
            foreach( $routes as $route) {
                array_push($routeList, $route->cons_id);
            }   
            if (count($routeList) > 0) {
                $query->andFilterWhere(['in', 'cons_id', $routeList]);
                /*$query->andFilterWhere([
                    'cons_id' => $routeList[0],
                ]);*/
            } else {
                $query->andFilterWhere([
                    'cons_id' => -1,
                ]);   
            }
            //print_r($shopList);
        }

        if ($this->cons_pt_id != "") {
            echo $this->cons_pt_id;
           
            $routes = MhConsItems::find()->select("cons_id")->distinct()->where(["cons_pt_id" => $this->cons_pt_id])->all();
            $routeList = [];

            foreach( $routes as $route) {
                array_push($routeList, $route->cons_id);
            }   
            if (count($routeList) > 0) {
                $query->andFilterWhere(['in', 'cons_id', $routeList]);
                /*$query->andFilterWhere([
                    'cons_id' => $routeList[0],
                ]);*/

            } else {
                $query->andFilterWhere([
                    'cons_id' => -1,
                ]);   
            }
            
            //print_r($shopList);
        } 


        if ($this->cons_sub_pt_id != "") {
            echo $this->cons_sub_pt_id;
           
            $routes = MhConsItems::find()->select("cons_id")->distinct()->where(["cons_sub_pt_id" => $this->cons_sub_pt_id])->all();
            $routeList = [];

            foreach( $routes as $route) {
                array_push($routeList, $route->cons_id);
            }   
            if (count($routeList) > 0) {
                $query->andFilterWhere(['in', 'cons_id', $routeList]);
                /*$query->andFilterWhere([
                    'cons_id' => $routeList[0],
                ]);*/

            } else {
                $query->andFilterWhere([
                    'cons_id' => -1,
                ]);   
            }
            
            //print_r($shopList);
        } 


        $conspt = (isset($params['MhConstructionSearch']['cons_pt_id']))
            ? $params['MhConstructionSearch']['cons_pt_id']
            : null;

        $subpt = (isset($params['MhConstructionSearch']['cons_sub_pt_id']))
            ? $params['MhConstructionSearch']['cons_sub_pt_id']
            : null;


       


        if ($conspt != null) {
            $pickprousers = MhConsItems::find()
                ->where(['cons_pt_id' => $conspt])
                ->select(['cons_id'])
                ->distinct()
                ->all();

            foreach ($pickprousers as $pickprouser) {
                $picuid[] = $pickprouser->cons_id;
            }
            //echo '<pre>'.var_dump($picuid).'</pre>'; exit();
            if (isset($picuid)) {
                $query->andFilterWhere(['cons_id' => $picuid]);
            }
          
        }


        if ($subpt != null) {
            $pickprousers = MhConsItems::find()
                ->where(['cons_sub_pt_id' => $subpt])
                ->select(['cons_id'])
                ->distinct()
                ->all();

            foreach ($pickprousers as $pickprouser) {
                $deluid[] = $pickprouser->cons_id;
            }

            if (isset($deluid)) {
                $query->andFilterWhere(['cons_id' => $deluid]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['cons_search_level' => SORT_ASC]],
            'pagination' => [
                'pageSize' => 27,
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');

            return $dataProvider;
        }

        return $dataProvider;
    }
}
