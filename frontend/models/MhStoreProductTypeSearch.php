<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhStoreProductType;
use common\models\MhProvince;
use common\models\MhAmphoe;

/**
 * MhStoreProductTypeSearch represents the model behind the search form of `common\models\MhStoreProductType`.
 */
class MhStoreProductTypeSearch extends MhStoreProductType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['store_pt_id', 'updated_id'], 'integer'],
            [['store_pt_name', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = MhStoreProductTypeSearch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $this->load($params);


        $arrstorety = (isset($params['MhStoreProductTypeSearch']['store_pt_id']))
            ? $params['MhStoreProductTypeSearch']['store_pt_id']
            : null;

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'store_pt_id' => $this->store_pt_id,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'store_pt_name', $this->store_pt_name]);

        $arrstorety = (isset($params['MhStoreProductTypeSearch']['store_pt_id']))
            ? $params['MhStoreProductTypeSearch']['store_pt_id']
            : [];

        $current_car_province = (isset($params['MhStoreProductTypeSearch']['current_car_province']))
            ? $params['MhStoreProductTypeSearch']['current_car_province']
            : null;

        $province = (isset($params['MhStoreProductTypeSearch']['store_province_id']))
            ? $params['MhStoreProductTypeSearch']['store_province_id']
            : null;

        $amphoe = (isset($params['MhStoreProductTypeSearch']['store_amphoe_id']))
            ? $params['MhStoreProductTypeSearch']['store_amphoe_id']
            : null;

        if ($current_car_province != null) {
            $currentpickprousers = RogUserCar::find()
                ->where(['current_car_province' => $current_car_province])
                ->select(['user_id'])
                ->distinct()
                ->all();

            foreach ($currentpickprousers as $currentpickprouser) {
                $curpicuid[] = $currentpickprouser->user_id;
            }
            //echo '<pre>'.var_dump($picuid).'</pre>'; exit();
            if (isset($curpicuid)) {
                $query->andFilterWhere(['user_id' => $curpicuid]);
            }

        }            
            
        if($province != null)
        {
            $pickprousers = MhProvince::find()
            ->where(['province_name_th' => $province])
            ->select(['user_id'])
            ->distinct()
            ->all();
            
            foreach($pickprousers as $pickprouser)
            {

                $picuid[] = $pickprouser->user_id;
            }
            //echo '<pre>'.var_dump($picuid).'</pre>'; exit();
            if(isset($picuid))
            {
                $query->andFilterWhere(['user_id' => $picuid]);
            }
        }
/*
        if($amphoe != null)
        {
            $delprousers = MhAmphoe::find()
            ->where(['amphoe_name_th' => $amphoe])
            ->select(['user_id'])
            ->distinct()
            ->all();

            foreach($delprousers as $delprouser)
            {
                $deluid[] = $delprouser->user_id;
            }

            if(isset($deluid))
            {
                $query->andFilterWhere(['user_id' => $deluid]);
            }
        }
        //deliver_province

        if (!empty($arrstorety)) {
            foreach ($arrstorety as $cartyp) {
                $storee[] = $cartyp;
            }

            if (isset($storee)) {

                $query->andFilterWhere(['store_pt_id' => $storee]);
            }
        }*/

        //$query->andFilterWhere(['-1' => '']);

        //echo '<pre>'.var_dump($arrstorety).'</pre>'; exit();

        return $dataProvider;

       
    }
}
