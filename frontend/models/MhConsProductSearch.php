<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhConsProduct;

/**
 * MhConsProductSearch represents the model behind the search form of `common\models\MhConsProduct`.
 */
class MhConsProductSearch extends MhConsProduct
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cons_product_id', 'updated_id'], 'integer'],
            [['cons_pt_id', 'cons_sub_pt_id', 'cons_sub_type_id', 'cons_product_name', 'cons_product_description', 'cons_product_pic', 'cons_product_price' , 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhConsProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['cons_product_name' => SORT_ASC]],
        ]);

        $this->load($params);

        /*$query->joinWith(['mhConsItems']);
        $query->joinWith(['consPt']);
        $query->joinWith(['consSubPt']);
        $query->joinWith(['consSubType']);*/


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cons_product_id' => $this->cons_product_id,
            'cons_pt_id' => $this->cons_pt_id,
            'cons_sub_pt_id' => $this->cons_sub_pt_id,
            //'cons_sub_type_id' => $this->cons_sub_type_id,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'cons_product_name', $this->cons_product_name])
            ->andFilterWhere(['like', 'cons_product_description', $this->cons_product_description])
            ->andFilterWhere(['like', 'cons_product_pic', $this->cons_product_pic])
           //->andFilterWhere(['like', 'Mh_construction_product_type.cons_pt_name', $this->cons_pt_id])
           //->andFilterWhere(['like', 'Mh_cons_sub_pt.cons_sub_pt_name', $this->cons_sub_pt_id])
           //->andFilterWhere(['like', 'Mh_cons_sub_type.cons_sub_type_name', $this->cons_sub_type_id])
           ;

        return $dataProvider;
    }
}
