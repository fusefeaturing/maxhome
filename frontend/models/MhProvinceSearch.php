<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhProvince;

/**
 * MhProvinceSearch represents the model behind the search form of `common\models\MhProvince`.
 */
class MhProvinceSearch extends MhProvince
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['province_id', 'updated_id'], 'integer'],
            [['province_name_th', 'province_name_en', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhProvince::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'province_id' => $this->province_id,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'province_name_th', $this->province_name_th])
            ->andFilterWhere(['like', 'province_name_en', $this->province_name_en]);

        return $dataProvider;
    }
}
