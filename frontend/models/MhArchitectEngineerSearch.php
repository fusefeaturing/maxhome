<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhArchitectEngineer;
use common\models\MhAeRoute;

/**
 * MhArchitectEngineerSearch represents the model behind the search form of `common\models\MhArchitectEngineer`.
 */
class MhArchitectEngineerSearch extends MhArchitectEngineer
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ae_id', 'ae_search_level', 'user_id', 'updated_id'], 'integer'],
            [['ae_name', 'ae_province_id', 'ae_amphoe_id', 'ae_pic', 'ae_gps', 'ae_address', 'ae_km', 'ae_tel1', 'ae_tel2', 'ae_line_id', 'ae_line_ad', 'ae_fb_fp', 'ae_messenger', 'ae_email', 'ae_website', 'ae_other', 'ae_data_other', 'ae_pt_id', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhArchitectEngineer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,

            'sort' => ['defaultOrder' => ['ae_search_level' => SORT_ASC]],
            'pagination' => [
                'pageSize' => 27,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        

        // grid filtering conditions
        $query->andFilterWhere([
            'ae_id' => $this->ae_id,
            'ae_search_level' => $this->ae_search_level,
            //'user_id' => $this->user_id,

            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'ae_name', $this->ae_name])
            ->andFilterWhere(['like', 'ae_pic', $this->ae_pic])
            ->andFilterWhere(['like', 'ae_gps', $this->ae_gps])
            ->andFilterWhere(['like', 'ae_address', $this->ae_address])
            ->andFilterWhere(['like', 'ae_km', $this->ae_km])
            ->andFilterWhere(['like', 'ae_tel1', $this->ae_tel1])
            ->andFilterWhere(['like', 'ae_tel2', $this->ae_tel2])
            ->andFilterWhere(['like', 'ae_line_id', $this->ae_line_id])
            ->andFilterWhere(['like', 'ae_line_ad', $this->ae_line_ad])
            ->andFilterWhere(['like', 'ae_fb_fp', $this->ae_fb_fp])
            ->andFilterWhere(['like', 'ae_messenger', $this->ae_messenger])
            ->andFilterWhere(['like', 'ae_email', $this->ae_email])
            ->andFilterWhere(['like', 'ae_website', $this->ae_website])
            ->andFilterWhere(['like', 'ae_other', $this->ae_other])
            ->andFilterWhere(['like', 'ae_data_other', $this->ae_data_other])
            ->andFilterWhere(['like', 'ae_pt_id', $this->ae_pt_id]);


        if ($this->ae_province_id != "") {
            echo $this->ae_province_id;
            $routes = MhAeRoute::find()->select("ae_id")->distinct()->where(["ae_route_province_id" => $this->ae_province_id])->all();
            $routeList = [];
            foreach( $routes as $route) {
                array_push($routeList, $route->ae_id);
            }

            if (count($routeList) > 0) {
                $query->andFilterWhere(['in', 'ae_id', $routeList]);
                /*$query->andFilterWhere([
                    'ae_id' => $routeList[0],
                ]);*/
            } else {
                $query->andFilterWhere([
                    'ae_id' => -1,
                ]);   
            }
            //print_r($shopList);
        }

        if ($this->ae_amphoe_id != "") {
            echo $this->ae_amphoe_id;
            $routes = MhAeRoute::find()->select("ae_id")->distinct()->where(["ae_route_amphoe_id" => $this->ae_amphoe_id])->all();
            $routeList = [];
            foreach( $routes as $route) {
                array_push($routeList, $route->ae_id);
            }

            if (count($routeList) > 0) {
                $query->andFilterWhere(['in', 'ae_id', $routeList]);
                /*$query->andFilterWhere([
                    'ae_id' => $routeList[0],
                ]);*/
            } else {
                $query->andFilterWhere([
                    'ae_id' => -1,
                ]);   
            }
            //print_r($shopList);
        }


  





       

 


        return $dataProvider;
    }
}
