<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MhJobProductType;

/**
 * MhJobProductTypeSearch represents the model behind the search form of `common\models\MhJobProductType`.
 */
class MhJobProductTypeSearch extends MhJobProductType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_pt_id', 'updated_id'], 'integer'],
            [['job_pt_name', 'created_time', 'updated_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MhJobProductType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'job_pt_id' => $this->job_pt_id,
            'updated_id' => $this->updated_id,
            'created_time' => $this->created_time,
            'updated_time' => $this->updated_time,
        ]);

        $query->andFilterWhere(['like', 'job_pt_name', $this->job_pt_name]);

        return $dataProvider;
    }
}
