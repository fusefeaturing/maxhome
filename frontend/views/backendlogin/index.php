<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BackendloginSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลผู้ใช้งานเบื้องหลัง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="backend-login-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'firstname',
            'lastname',
            'username',
            //'password',
            //'auth_key',
            //'password_reset_token',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
