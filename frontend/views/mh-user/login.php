<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'เข้าสู่ระบบ hub.maxhome.co';
Yii::$app->params['og_title']['content'] = $this->title;
Yii::$app->params['og_description']['content'] = "Hub maxhome คือศูนย์ร่วมกิจการรับเหมาก่อสร้าง ช่วยแนะนำ ผู้รับเหมาก่อสร้าง ร้านวัสดุก่อสร้าง ผู้ผลิต ตัวแทนจำหน่าย ที่พร้อมให้บริการทั่วประเทศไทย ให้กับลูกค้าที่กำลังสนใจสินค้าและบริการ
เข้าร่วม Hub maxhome ชุมชนวงการธุรกิจก่อสร้าง เสมือนว่าธุรกิจก่อสร้างของท่านตั้งอยู่ทำเลทองที่มีผู้คนสนใจสินค้าและบริการของท่านตลอดเวลา มี traffic จำนวนมากจากลูกค้าที่มีความต้องการใช้สินค้าและบริการของท่านและสามารถติดต่อไปยังธุรกิจของท่านโดยตรง
• ระบบหน้าร้านค้าวัสดุก่อสร้างออนไลน์สำเร็จรูป ที่ให้เจ้าของธุรกิจในวงการก่อสร้างสามารถเขามาสร้างหน้าร้านของตัวเองได้ง่ายๆ ภายใน 3 นาที
• หน้าร้านธุรกิจก่อสร้างออนไลน์ที่อยู่ใจกลางชุมชนวงการธุรกิจก่อสร้างที่มีลูกค้าค้นหาบริการของท่านตลอดเวลา
• ระบบช่วยให้ลูกค้าของคุณสามารถค้นหาสินค้าและบริการของท่าน และติดต่อหาท่านโดยตรงทั้งทาง โทรศัพท์ , Line@ , messenger หรือดูข้อมูลของท่านเพิ่มเติมจากทาง Website หรือ Facebook fanpage ของท่าน"
?>



<div class="site-login">
    <div class="container">

        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <h1><?= Html::encode($this->title) ?></h1>

        <!-- <p>Please fill out the following fields to login:</p> -->




        <!-- <div class="site-login" style="display: flex; justify-content: center; align-items: center;">

            <div class="row">


                <div class="rounded mx-auto d-block ">
                    <img src="../common/web/img/logo-maxhome.png" style="height:150px;" alt="">
                </div>



                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <p>
                        สมัครสมาชิก / เข้าสู่ระบบ ง่ายๆ<br>ด้วยระบบ facebook <br> สะดวก ปลอดภัย
                    </p>


                    <?= Html::a(
                        '  สมัครสมาชิก / เข้าสู่ระบบ',
                        ['site/auth', 'authclient' => 'facebook'],
                        ['class' => 'btn btn-primary btn-block btn-lg fab fa-facebook-square', 'style' => 'background-color: #3b5998;']
                    )
                    ?>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <hr>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <p>
                        สิทธิพิเศษสำหรับสมาชิก เท่านั้น<br>
                        1. ลงประกาศ "งานรับเหมาก่อสร้าง"<br>
                        2. ลงประกาศ "ร้านวัสดุก่อสร้าง"<br>
                        3. ลงประกาศ "สถาปนิก ออกแบบ / วิศวกร เขียนแบบ"
                    <p>

                    </p>
                </div>
            </div>
        </div> -->







        <div class="card2">
            <div class="card-body">
                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <div class="rounded mx-auto d-block ">
                        <img src="../common/web/img/logo-maxhome.png" style="height:150px;" alt="">
                    </div>
                    <p>
                        สมัครสมาชิก / เข้าสู่ระบบ ง่ายๆ<br>ด้วยระบบ facebook และ google<br> สะดวก ปลอดภัย
                    </p>
                </div>

                <div class="row">

                    <div class="col-xs-6 col-sm-6 col-md-6">



                        <?= $form->field($model, 'user_name') ?>
                        <?= $form->field($model, 'user_password')->passwordInput() ?>
                        <?= '' //$form->field($model, 'rememberMe')->checkbox(['id' => 'remember-me-ver', 'custom' => true]) 
                        ?>

                        <div class="form-group ">
                            <?= Html::submitButton('เข้าสู่ระบบ', ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('สมัครสมาชิก', ['mh-user/register'], ['class' => 'btn btn-secondary']) ?>
                        </div>

                        <hr class="hr-text" data-content="OR">
                        <!--<div class="separator">or login social</div>-->

                        <div class="text-center" style="padding-top: 20px;">
                            <?= Html::a(
                                '  สมัครสมาชิก / เข้าสู่ระบบ',
                                ['site/auth', 'authclient' => 'facebook'],
                                ['class' => 'btn btn-primary  btn-lg fab fa-facebook-square ', 'style' => 'background-color: #3b5998; ']
                            )
                            ?>
                            <br>
                            <br>
                            <?= Html::a(
                                '  สมัครสมาชิก / เข้าสู่ระบบ',
                                ['site/auth', 'authclient' => 'google'],
                                ['class' => ' btn btn-danger btn-lg fab fa-google-plus-square ', 'style' => 'background-color: #ef5350; color:#FFFFFF ']
                            )
                            ?>
                        </div>


                    </div>






                    <div class="col-xs-6 col-sm-6 col-md-6">


                        <div class="col-xs-12 col-sm-12 col-md-12">

                            <h6 class="text-center" style="color : red; padding-top:30px;"><strong><u>***สิทธิพิเศษสำหรับสมาชิก เท่านั้น***</u></strong><br></h6><br>
                            <p>
                                สิทธิพิเศษสำหรับสมาชิก เท่านั้น<br>
                                1. ลงประกาศ "งานรับเหมาก่อสร้าง"<br>
                                2. ลงประกาศ "ร้านวัสดุก่อสร้าง"<br>
                                3. ลงประกาศ "สถาปนิก ออกแบบ / วิศวกร เขียนแบบ"
                            <p>
                        </div>

                    </div>

                </div>
            </div>
        </div>




        <?php ActiveForm::end(); ?>






    </div>
</div>