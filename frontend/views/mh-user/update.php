<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhUser */

$this->title = 'แก้ไขข้อมูลผู้ใช้งาน: ' . $model->user_firstname;

$this->params['breadcrumbs'][] = ['label' => $model->user_firstname, 'url' => ['profile', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-user-update">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
