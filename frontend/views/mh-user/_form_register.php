<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\mh-user */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div>
        <p></p>
    </div>

    <div class="card2">
        <h1 align="center" style="padding-top:10px;">กรอกข้อมูล</h1>
        <div class="card-body">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'user_firstname')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'user_lastname')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'user_email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'user_tel1')->textInput(['maxlength' => true]) ?>

            <div class="col-xs-12 col-sm-12 col-md-12" style="margin-bottom :10px">
                <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>