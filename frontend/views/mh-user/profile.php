<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->user_firstname;
$userid = yii::$app->user->identity->id;
//$this->params['breadcrumbs'][] = ['label' => 'ข้อมูลผู้ใช้', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<body style="background-color: #ecf0f3;">

<div class="row">

        

    <div class="container">

        <div class="card2">
            <div class="card-body">
                <h4>เลือกประเภทกิจการของฉัน</h4>
            <br>
                <div class="row">
                    <div class="col-sm-3 col-lg-2">
                        <div class="form-group">
                            <?= Html::a('ผู้รับเหมาก่อสร้าง', ['mh-job/myjob' , 'id' => $model->user_id],
                            ['class' => 'btn button-commany pink btn-sm btn-block']) ?>
                        </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                        <div class="form-group">
                            <?= Html::a('ร้านวัสดุก่อสร้าง', ['mh-construction/mycons' , 'id' => $model->user_id],
                            ['class' => 'btn button-commany danger btn-sm btn-block']) ?>
                        </div>
                    </div>

                    <div class="col-sm-4 col-md-5 col-lg-4">
                        <div class="form-group">
                            <?= Html::a('สถาปนิกออกแบบ / วิศวกรเขียนแบบ', ['mh-architect-engineer/myae'],
                            ['class' => 'btn btn-success btn-sm btn-block']) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!--<div class="rog-user-car-index">
    <?= Html::a('ประวัติการสั่งคอนกรีต', ['mh-concrete/mycon']) ?>
    <br>
    
</div>-->


<div class="">
            <p> </p>
        </div>



        <div class="card2">
            <div class="card-body">
            <h2><?= Html::encode($this->title) ?></h2>

                    <?php 
                    if (yii::$app->user->identity->id == $model->user_id) {
                        echo '<p>';
                        echo Html::a('แก้ไขรายละเอียด', ['mh-user/update', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                        echo '</p>';
                    }
                    ?>

                    <?=DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //'user_id',
                            //'user_name',
                            'user_firstname',
                            'user_lastname',
                            'user_tel1',
                            'user_tel2',
                            'user_line_id',
                            //'user_line_ad',
                            'user_fb_url:url',
                            'user_messenger',
                            'user_email:email',
                            'user_website',
                            'user_other',
                            //'updated_id',
                            'created_time:datetime',
                            'updated_time:datetime',
                        ],
                    ]) ?>
            </div>
        </div>


        <div class="">
            <p> </p>
        </div>


<div class="card2">
    <div class="card-body">
        <div class="">
            <p>เมื่อท่านสมาชิกกรอกข้อมูลติดต่อเรียบร้อย ท่านสามารถใช้บริการลงประกาศใน Hub.MAXHOME.co ได้เลยค่ะ</p>
        </div>


        <div class="">
            <?= Html::a(
                'กลับสู้หน้าหลัก Hub.MAXHOME.co',
                ['site/index'],
                ['class' => 'btn btn-danger btn-block btn-sm', 'style' => '']
            ) ?>
        </div>
    </div>
</div>    

<div class="">
            <p> </p>
        </div>


        
    </div>
</div>

</body>