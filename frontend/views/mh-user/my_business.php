<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
$userid = yii::$app->user->identity->id;
$this->title = $model->user_firstname;
$this->params['breadcrumbs'][] = ['label' => 'รายการกิจการของฉัน', 'url' => ['mybusiness']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<!--
<div class="rog-user-car-index">
    <?= Html::a('ดูเส้นทางที่ให้บริการ', ['car/route']) ?>
    <br>
</div>

<div class="rog-user-car-index">
    <?= Html::a('เพิ่มเส้นทางที่ให้บริการ', ['car/add-route']) ?>
    <br>
</div>

<div class="rog-user-car-index">
    <?= Html::a('รถของฉัน', ['car/my-car']) ?>
    <br>
</div>-->

<div class="rog-user-car-index">
    <?= Html::a('ประกาศหารถของฉัน', ['job/my-job'],['class' => 'btn btn-warning']) ?>

    <hr>
</div>


<div class="mh-store-index">

<h2>1.) รายการร้านค้าของฉัน</h2>

    <p>
        <?= Html::a('+ เพิ่มร้านค้าของฉัน', ['mh-store/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <hr>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showHeader' => false,
        'columns' => [
            [
                
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model)  {
                    $htmlcard = '';
                    $htmlcard .= '<div style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);  padding: 16px; background-color: #f1f1f1;">';
                    /*
                    $htmlcard .= '<div class="row">';
                        $htmlcard .= '<div class="col-xs-12 col-sm-12 col-md-12">';
                        $htmlcard .= '<h4>' . ((array_key_exists($model->car_type, )) ? [$model->car_type] : 'ประเภทรถไม่ถูกต้อง') . ' </h4>';
                        $htmlcard .= '</div>';
                    $htmlcard .= '</div>';
*/
                    $htmlcard .= '<div class="row">';

                    $htmlcard .= '<div class="col-xs-12 col-sm-4 col-md-4">';
                    $htmlcard .= '<p>' . $model->getFirstPhotos() . '</p>';
                    $htmlcard .= '</div>';
                    $htmlcard .= '<div class="col-xs-12 col-sm-6 col-md-6">';                    
                    $htmlcard .= '<p><strong>ชื่อกิจการ</strong> : ' . $model->store_name . ' </p>';                    
                    $htmlcard .= '<p><strong>จังหวัด</strong> : '. $model->pickup() .'</p>';
                    $htmlcard .= '<p><strong>อำเภอ</strong> : '. $model->deliver() .'</p>';
                    $htmlcard .= '<p><strong>ประเภทสินค้าที่ให้บริการ</strong><br>' . $model->storetype_text() . '</p>';
                    //$htmlcard .= '<p>ประเภทสินค้า : ' . $model->store_pt_id . '</p>';
                    $htmlcard .= '</div>';
                    
                    $htmlcard .= '<div class="col-xs-12 col-sm-2 col-md-2" style="text-align: center; vertical-align: middle; margin-top: 10px;">';
                    $htmlcard .= Html::a('ดูรายละเอืยด', 
                    ['mh-store/view', 'id' => $model->store_id], 
                    ['class' => 'btn btn-success glyphicon glyphicon-user', 'style' => 'width: 100%;']);
                    $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    return $htmlcard;
                },
                'contentOptions' => ['style' => 'width:80%; height:20%; text-align: left; margin: 25px; vertical-align: middle;'],
                'headerOptions' => ['style' => 'text-align: center;'],
            ],
        ],
    ]); ?>
</div>

<hr>

<div class="mh-job-index">

<h2>2.) รายการงานรับเหมาของฉัน</h2>
    <p>
        <?= Html::a('+ ระบุเส้นทางที่ฉันให้บริการ', ['car/add-route'], ['class' => 'btn btn-success']) ?>
    </p>

    <hr>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showHeader' => false,
        'columns' => [
            [
                
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model)  {
                    $htmlcard = '';
                    $htmlcard .= '<div style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);  padding: 16px; background-color: #f1f1f1;">';
                    /*
                    $htmlcard .= '<div class="row">';
                        $htmlcard .= '<div class="col-xs-12 col-sm-12 col-md-12">';
                        $htmlcard .= '<h4>' . ((array_key_exists($model->car_type, )) ? [$model->car_type] : 'ประเภทรถไม่ถูกต้อง') . ' </h4>';
                        $htmlcard .= '</div>';
                    $htmlcard .= '</div>';
*/
                    $htmlcard .= '<div class="row">';

                    $htmlcard .= '<div class="col-xs-12 col-sm-4 col-md-4">';
                    $htmlcard .= '<p>' . $model->getFirstPhotos() . '</p>';
                    $htmlcard .= '</div>';
                    $htmlcard .= '<div class="col-xs-12 col-sm-6 col-md-6">';                    
                    $htmlcard .= '<p><strong>ชื่อกิจการ</strong> : ' . $model->job_name . ' </p>';                    
                    $htmlcard .= '<p><strong>จังหวัด</strong> : '. $model->pickup() .'</p>';
                    $htmlcard .= '<p><strong>อำเภอ</strong> : '. $model->deliver() .'</p>';
                    $htmlcard .= '<p><strong>ประเภทสินค้าที่ให้บริการ</strong><br>' . $model->jobtype_text() . '</p>';
                    //$htmlcard .= '<p>ประเภทสินค้า : ' . $model->job_pt_id . '</p>';
                    $htmlcard .= '</div>';
                    
                    $htmlcard .= '<div class="col-xs-12 col-sm-2 col-md-2" style="text-align: center; vertical-align: middle; margin-top: 10px;">';
                    $htmlcard .= Html::a('ดูรายละเอืยด', 
                    ['mh-job/view', 'id' => $model->job_id], 
                    ['class' => 'btn btn-success glyphicon glyphicon-user', 'style' => 'width: 100%;']);
                    $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    return $htmlcard;
                },
                'contentOptions' => ['style' => 'width:80%; height:20%; text-align: left; margin: 25px; vertical-align: middle;'],
                'headerOptions' => ['style' => 'text-align: center;'],
            ],
        ],
    ]); ?>

</div>
