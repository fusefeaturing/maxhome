<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MhUser */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'แก้ไขข้อมูลผู้ใช้งาน: ' . $model->user_firstname;
?>

<body style="background-color: #ecf0f3;">

<div class="mh-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?='' //$form->field($model, 'user_name')->textInput(['maxlength' => true]) ?>

    <div class="card2">

    <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>

        <div class="card-body">
        <?= $form->field($model, 'user_firstname')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'user_lastname')->textInput(['maxlength' => true]) ?>  

        <?= $form->field($model, 'user_tel1')->textInput()->label('เบอร์ติดต่อ 1', ['style' => 'color:red']) ?>

        <?= $form->field($model, 'user_tel2')->textInput() ?>

        <?= $form->field($model, 'user_line_id')->textInput(['maxlength' => true])->label('line id', ['style' => 'color:red']) ?>

        <?='' //$form->field($model, 'user_line_ad')->textInput(['maxlength' => true])->label('line @', ['style' => 'color:red']) ?>

        <?='' //$form->field($model, 'user_fb_id')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'user_fb_url')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'user_messenger')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'user_email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'user_website')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'user_other')->textInput(['maxlength' => true]) ?>

        <?='' //$form->field($model, 'updated_id')->textInput() ?>

        <?= $form->field($model, 'created_time')->textInput([
            'readonly' => true,
            'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
        ]) ?>

        <?= $form->field($model, 'updated_time')->textInput([
            'readonly' => true,
            'value' => date('Y-m-d H:i:s')
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
        </div>
        </div>
    </div>

    

    <?php ActiveForm::end(); ?>

</div>

    <div class="">
        <p></p>
    </div>

</body>
