<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsItems */

$this->title = 'รายละเอียดสินค้า : ' . $modelpro->cons_product_name;
$consid = Yii::$app->getRequest()->getQueryParam('id');
$this->params['breadcrumbs'][] = ['label' => 'สินค้าที่สามารถเพิ่มได้' , 'url' => ['mh-cons-items/addproductlists']];
/*if (Yii::$app->user->isGuest) {
    
    $this->params['breadcrumbs'][] = ['label' => 'สินค้าภายในร้าน', 'url' => ['mh-cons-items/itemsstore', 'id' => $model->cons_id]];

} else {    

    $this->params['breadcrumbs'][] = ['label' => 'สินค้าภายในร้านของฉัน', 'url' => ['mh-cons-items/myitems', 'id' => $model->cons_id, ]];

}*/
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-cons-items-create">

    <div class="card2">
        <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'modelpro' => $modelpro,
        'modelcons' => $modelcons,
    ]) ?>

</div>
