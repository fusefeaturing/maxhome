<?php

use yii\bootstrap4\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการสินค้าวัสดุก่อสร้าง';

$consid = Yii::$app->getRequest()->getQueryParam('id');
$this->params['breadcrumbs'][] = ['label' => $model->cons_name, 'url' => ['mh-construction/view', 'id' => $consid]];


$this->params['breadcrumbs'][] = $this->title;

?>

<body style="background-color: #ecf0f3;">

<div class="rog-user-car-index">



    <div class="card2">
        <div class="card-body">
        <h2>สินค้าหน้าร้านวัสดุก่อสร้างของฉัน</h2>
        <?php                         
                if (Yii::$app->user->isGuest) {
        
                } else {
                
                    echo '<div class="">';
                    echo '<div class="form-group">';
                    echo Html::a('+ เพิ่มสินค้า (จากระบบ)', ['addproductlists', 'id' => $consid], ['class' => 'btn btn-success']); 
                    echo '</div>';
                    echo '</div>';

                }
            ?>

        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

    <div class="card2">
            <?php echo $this->render('_searchmyitems', ['model' => $searchModel, 'amphoe' => $amphoe, 'subpt' => $subpt, 'consid' => $consid, ]); ?>

        <div class="card-body">
            <div class="container">
                <div class=" row row-cols-1 row-cols-md-3">
                    <?= Html::hiddenInput('cons_id', $consid);  ?>
                      <?php foreach ($dataProvider->models as $model) { ?>
                        
                        <?= Html::a('<div class="card-group">
                                          <div class="card card-mobile" >
                                            <div class="cardd"> 
                                            ' . Html::img($model->consProduct->getFirstPhotoURL(), ['class' => 'img-responsive rounded mx-auto d-block items-display-mobile']) . '                      
                                            </div>
                    
                                            <div class="card-body">
                                            <h6 class="card-title">' . $model->consProduct->cons_product_name . ' </h6>
                                            <p style="font-size:14px; color:green; " class="card-title">' . $model->cons_price . ' </p>
                                              <p style="font-size:12px; color:blue" class="card-title"><i class="fas fa-map-marker-alt"></i> ' . $model->cons->consProvince->province_name_th . ' ' . $model->cons->consAmphoe->amphoe_name_th . ' </p>
                                              
                                            </div>  
                    
                                          </div>
                                        </div>', ['view', 'id' => $model->cons_items_id ], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
                        ?>

      <?php } ?>



                </div>
    
  </div>

  <?php if (!empty($dataProvider->models)) : ?>
                <?php
                        echo \yii\bootstrap4\LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
    <?php else : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo "ไม่มีข้อมูล !!"; ?>
                </div>
    <?php endif; ?>


        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

    <!--<div class="card2">
        <div class="card-body">
            

            


        </div>
    </div>-->


</div>

<!--SELECT
    *
FROM
    `mh_cons_product` AS consP
WHERE
    consP.cons_product_id IN(
    SELECT
        consI.cons_product_id
    FROM
        mh_cons_items AS consI
    WHERE
        consI.cons_id = 28
)-->

</body>
