<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'สินค้าภายในร้านค้า';
$this->params['breadcrumbs'][] = ['label' => 'ร้านค้าวัสดุก่อสร้าง', 'url' => ['mh-construction/index']];
$consid = Yii::$app->getRequest()->getQueryParam('id');
$this->params['breadcrumbs'][] = ['label' => $model->cons_name , 'url' => ['mh-construction/view','id' => $consid]];
$this->params['breadcrumbs'][] = $this->title;

?>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


<body style="background-color: #ecf0f3;">

<div class="rog-user-car-index">
    

<?php echo $this->render('_searchitemsstore', ['model' => $searchModel, 'amphoe' => $amphoe, 'subpt' => $subpt, 'consid' => $consid, ]); ?>


<div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

        
        <?= Html::hiddenInput('cons_id', $consid);  ?>

        <?='' //$model->cons->cons_name ?>

            <div class="container">
    <div class=" row row-cols-1 row-cols-md-3">

      <?php foreach ($dataProvider->models as $model) { ?>
        
        <?= Html::a('<div class="card-group">
           
                          <div class="card card-mobile" >
                            <div class="cardd"> 
                            ' . Html::img($model->consProduct->getFirstPhotoURL(), ['class' => 'img-responsive rounded mx-auto d-block items-display-mobile']) . '                      
                            </div>

                            <div class="card-body">
                              <h6 class="card-title">' . $model->consProduct->cons_product_name . ' </h6>
                              <p style="font-size:14px; color:green; " class="card-title ">' . $model->cons_price . ' </p>
                              <p style="font-size:12px; color:blue" class="card-title"><i class="fas fa-map-marker-alt"></i> ' . $model->cons->consProvince->province_name_th . ' ' . $model->cons->consAmphoe->amphoe_name_th . ' </p>
                            </div>  

                          </div>
                        </div>', ['view', 'id' => $model->cons_items_id ], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
        ?>

      <?php } ?>

    </div>
    
  </div>

  <?php if (!empty($dataProvider->models)) : ?>
                <?php
                        echo \yii\bootstrap4\LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
    <?php else : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo "ไม่มีข้อมูล !!"; ?>
                </div>
    <?php endif; ?>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <p> </p>
    </div>

    <div class="card2">
        <div class="card-body">
            
            <div class="row" style="margin-left:10px;">   
                <!-- Your share button code -->
                <div class="fb-share-button" style="margin-right:10px;"
                    data-href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                    data-layout="button_count" 
                    data-size="large">
                        <a target="_blank" href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                            class="fb-xfbml-parse-ignore">แชร์
                        </a>
                </div>

                <div class="line-it-button" style="margin-right:10px;"
                    data-lang="th" 
                    data-type="share-a" 
                    data-ver="3" 
                    data-url=<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>
                    data-color="default" 
                    data-size="large" 
                    style="display: none; ">
                </div>

                <button type="button" style="margin-left:10px;" class="btn btn-secondary btn-copy btn-sm js-tooltip js-copy" data-toggle="tooltip" data-placement="bottom" data-copy="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" title="Copy to clipboard">
                คัดลอก URL <i class="fas fa-copy"></i>
                </button> 

            </div>


            <div class="col-xs-12 col-sm-12 col-md-12">
                <p> </p>
            </div>

            <div align="center">
                <div  class="fb-group" style="margin-bottom:20px;" 
                    data-href="https://www.facebook.com/groups/2000756793524391/" 
                    data-width="280" 
                    data-show-social-context="true" 
                    data-show-metadata="true">
                </div>
            </div>
        </div>
    </div>

       

</div>

<script>

// COPY TO CLIPBOARD
// Attempts to use .execCommand('copy') on a created text field
// Falls back to a selectable alert if not supported
// Attempts to display status in Bootstrap tooltip
// ------------------------------------------------------------------------------

function copyToClipboard(text, el) {
var copyTest = document.queryCommandSupported('copy');
var elOriginalText = el.attr('data-original-title');

if (copyTest === true) {
var copyTextArea = document.createElement("textarea");
copyTextArea.value = text;
document.body.appendChild(copyTextArea);
copyTextArea.select();
try {
  var successful = document.execCommand('copy');
  var msg = successful ? 'Copied!' : 'Whoops, not copied!';
  el.attr('data-original-title', msg).tooltip('show');
} catch (err) {
  console.log('Oops, unable to copy');
}
document.body.removeChild(copyTextArea);
el.attr('data-original-title', elOriginalText);
} else {
// Fallback if browser doesn't support .execCommand('copy')
window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
}
}

$(document).ready(function() {
// Initialize
// ---------------------------------------------------------------------

// Tooltips
// Requires Bootstrap 3 for functionality
$('.js-tooltip').tooltip();

// Copy to clipboard
// Grab any text in the attribute 'data-copy' and pass it to the 
// copy function
$('.js-copy').click(function() {
var text = $(this).attr('data-copy');
var el = $(this);
copyToClipboard(text, el);
});
});
</script>

</body>
