<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsItems */

$this->title = 'แก้ไขข้อมูลสินค้า: ' . $model->cons_product_id;
$consid = Yii::$app->getRequest()->getQueryParam('id');
$this->params['breadcrumbs'][] = ['label' => 'ร้านค้า' , 'url' => ['mh-construction/view','id' => $consid]];
if (Yii::$app->user->isGuest) {
    
    $this->params['breadcrumbs'][] = ['label' => 'สินค้าภายในร้าน', 'url' => ['mh-cons-items/itemsstore', 'id' => $model->cons_id]];
    
} else {    

    $this->params['breadcrumbs'][] = ['label' => 'สินค้าภายในร้านของฉัน', 'url' => ['mh-cons-items/myitems', 'id' => $consid, ]];

}
$this->params['breadcrumbs'][] = ['label' => $model->cons_product_id, 'url' => ['view', 'id' => $model->cons_items_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-cons-items-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelpro' => $modelpro,
        'modelcons' => $modelcons,
    ]) ?>

</div>
