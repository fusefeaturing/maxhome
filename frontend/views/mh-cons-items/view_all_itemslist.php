<?php

use common\models\MhConstruction;
use yii\bootstrap4\Html;
use yii\grid\GridView;
use yii\bootstrap4\LinkPager;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'สินค้าวัสดุก่อสร้าง';
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['og_title']['content'] = $this->title;


Yii::$app->params['og_url']['content'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
Yii::$app->params['og_keyword']['content'] = 'ร้านวัสดุก่อสร้าง ,hub.maxhome ,สินค้าร้านวัสดุก่อสร้าง';
Yii::$app->params['og_revisit_after']['content'] = '1 Days';

$consid = Yii::$app->getRequest()->getQueryParam('id');
?>

<body style="background-color: #ecf0f3;">
  


<div class="mh-job-index">



<?php echo $this->render('_search', ['model' => $searchModel, 'amphoe' => $amphoe, 'subpt' => $subpt,]); ?>
  

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

        
  <div class="container">
    <div class=" row row-cols-1 row-cols-md-3">

      <?php foreach ($dataProvider->models as $model) { ?>
        
        <?= Html::a('<div class="card-group">
                          <div class="card card-mobile" >
                            <div class="cardd"> 
                            ' . Html::img($model->consProduct->getFirstPhotoURL(), ['class' => 'img-responsive rounded mx-auto d-block items-display-mobile']) . '                      
                            </div>

                            <div class="card-body">
                              <h6 class="card-title">' . $model->consProduct->cons_product_name . ' </h6>
                              
                              <p style="font-size:14px; color:green; " class="card-title text-right">' . $model->cons_price . ' </p>
                              <p class="card-text" style="font-size:12px; "><i class="fas fa-map-marker-alt"></i> ' . $model->cons->consProvince->province_name_th . ' ' . $model->cons->consAmphoe->amphoe_name_th . ' </p>
                              
                            </div>

                          </div>
                        </div>', ['view', 'id' => $model->cons_items_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
        ?>

      <?php } ?>

      <!--<div class="card-footer">
                            
                            '.Html::a('<i class="fas fa-phone-alt"></i> ' . '', 
                            ($model->cons->cons_tel1 != '') 
                            ? 'tel:'.$model->cons->cons_tel1 
                            : ['mh-user/view', 'id' => $model->cons->user_id] ).'
                            
      </div>-->

    </div>
    <br>
  </div>


      <?php if (!empty($dataProvider->models)) : ?>            
        <?php
                    echo \yii\bootstrap4\LinkPager::widget([
                    'pagination' => $pagination,
                ]);
        ?>
      <?php else : ?>
            <div class="alert alert-danger" role="alert">
              <?php echo "ไม่พบข้อมูล !!"; ?>
            </div>
      <?php endif; ?>





<br>
  <div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 ">
      <?= Html::a(
        'ลงประกาศ "ร้านวัสดุก่อสร้าง"',
        ['mh-construction/create'],
        ['class' => 'button-commany danger btn-block btn-lg btn', 'style' => '']
      ) ?>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
      <p> </p>
    </div>

  </div>

      </div>
</body>