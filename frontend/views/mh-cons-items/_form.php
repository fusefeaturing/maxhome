<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\MhConsProduct;
use common\models\MhConstruction;
use common\models\MhUser;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\MhConsItems */
/* @var $form yii\widgets\ActiveForm */
$id = yii::$app->user->identity->id;
$cons_product_id = Yii::$app->getRequest()->getQueryParam('id');
$userid = ArrayHelper::map(MhUser::find()->asArray()->all(), 'user_id', 'user_firstname');
$consproduct = ArrayHelper::map(MhConsProduct::find()->asArray()->all(), 'cons_product_id', 'cons_product_name');
$consid = ArrayHelper::map(MhConstruction::find()->where(['user_id' => $id])->andWhere('cons_id')->asArray()->all(), 'cons_id', 'cons_name');
$cons_id = Yii::$app->getRequest()->getQueryParam('id2');
//var_dump($cons_id); echo '<br>';

?>

<body style="background-color: #ecf0f3;">

<div class="mh-cons-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card2" style="padding-top:30px">
        <div class="rounded mx-auto d-block text-center">
                <div class="gImg ">
                    <p class="card-img-top"><?= $modelpro->getPhotosViewer() ?></p>
                </div>
        </div>

        <div id="myModal" class="modal">
                <span class="close">×</span>
                    <img class="modal-content" id="img01">
                <div id="caption"></div>
            </div>

        <div class="card-body">

            <p class="card-text">ชื่อสินค้า : <?= $modelpro->cons_product_name ?></p>
            <p class="card-text">รายละเอียด : <?= nl2br($modelpro->cons_product_description) ?></p>    
            <p class="card-text">หมวด : <?= $modelpro->consPt->cons_pt_name ?></p>
            <p class="card-text">หมู่ : <?= $modelpro->consSubPt->cons_sub_pt_name ?></p>
            <p class="card-text">หมู่ย่อย : <?= $modelpro->subtype() ?></p>
            

        </div>
    </div>

            <br>



    <div class="card2">

            <h2 style="padding-left:20px; padding-top:20px;">เพิ่มสินค้าลงในร้านค้าของฉัน</h2>

        <div class="card-body">
            
            <?= 
                $form
                    ->field($model, 'cons_item_level')
                    ->hiddenInput(['maxlength' => true, 'value' => $model->getDefaultLevel()])
                    //->input('', ['placeholder' => ""])
                    ->label(false) 
            ?>
            


            <?= 
                $form
                    ->field($model, 'cons_product_id')
                    ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $cons_product_id])
                    ->label(false)
            ?>



            <div class="">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <?=''
                        /*$form
                            ->field($model, 'cons_product_id')
                            ->widget(Select2::classname(), [
                                'data' => $this->context->getProductTypes(),
                                'language' => 'th',
                                'options' => ['multiple' => true, 'placeholder' => 'เลือกประเภทสินค้า'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                        ]);*/                 
                    ?>
                </div>
            </div>   



            <?= $form->field($model, 'cons_price')->textInput(['maxlength' => true]) ?>


            <?= $form->field($model, 'cons_id')
                            ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $cons_id])
                            ->label(false)
            ?>


            <?='' /*$form->field($model, 'cons_id')
                            ->dropdownList($consid, [
                                'id' => 'ddl-roguser',        
                                'prompt' => 'เลือกร้านของฉัน'                
                            ])
                            ->label('ร้านค้าของฉัน')*/
            ?>

            <?= $form->field($model, 'user_id')
                            ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $id])
                            ->label(false)
            ?>
            

            <?= $form->field($model, 'cons_pt_id')
                            ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $modelpro->consPt->cons_pt_id])
                            ->label(false)
            ?>  

            <?= $form->field($model, 'cons_sub_pt_id')
                            ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $modelpro->consSubPt->cons_sub_pt_id])
                            ->label(false)
            ?>  

            <?='' /*$form->field($model, 'cons_sub_type_id')
                            ->textInput(['maxlength' => true, 'readonly' => true, 'value' => $modelpro->subtype()])
                            ->label(false)*/
            ?>  

            

            


            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <?=''
                        /*$form
                            ->field($model, 'cons_product_id')
                            ->widget(Select2::classname(), [
                                'data' => $this->context->getProductTypes(),
                                'language' => 'th',
                                'options' => ['multiple' => true, 'placeholder' => 'เลือกประเภทสินค้า'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                        ]);*/                 
                    ?>
                </div>
            </div>



            <?='' /*$form->field($model, 'cons_id')
                            ->textInput(['maxlength' => true, 'readonly' => true, 'value' => $consid])
                            ->label(false)*/
            ?>




                            
            <?= $form->field($model, 'created_time')->textInput([
                'readonly' => true,
                'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
            ]) ?>

            <?= $form->field($model, 'updated_time')->textInput([
                'readonly' => true,
                'value' => date('Y-m-d H:i:s')
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
            </div>
        
        </div>
    </div>
    
<br>
    <?php ActiveForm::end(); ?>

</div>
<script>
    // Get the modal
var modal = document.getElementById('myModal');

modal.addEventListener('click',function(){
this.style.display="none";
})

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// Get all images and insert the clicked image inside the modal
// Get the content of the image description and insert it inside the modal image caption
var images = document.getElementsByTagName('img');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
var i;
for (i = 0; i < images.length; i++) {
  images[i].onclick = function() {
    modal.style.display = "block";
    modalImg.src = this.src;
    modalImg.alt = this.alt;
    captionText.innerHTML = this.nextElementSibling.innerHTML;
  }
}
</script>

</body>
