<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJob */
$userid = yii::$app->user->identity->id;
$this->title = 'เพิ่มผู้รับเหมาก่อสร้าง';
if (Yii::$app->user->isGuest) {
        
    $this->params['breadcrumbs'][] = ['label' => 'รายการผู้รับเหมาก่อสร้าง', 'url' => ['index']];
    
} else {

    $this->params['breadcrumbs'][] = ['label' => 'ผู้รับเหมาก่อสร้างของฉัน', 'url' => ['myjob'], 'id' => $userid ];

}
//$this->params['breadcrumbs'][] = ['label' => 'ผู้รับเหมาก่อสร้างของฉัน', 'url' => ['myjob']];

?>
<div class="mh-job-create">
    
    <div class="card2">
    <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>
    </div>

        

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
        'zipcode' => [],
        'subpt' => [],
    ]) ?>

</div>
