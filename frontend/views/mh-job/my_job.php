<?php

use common\models\MhJob;
use common\models\MhUser;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'กิจการรับเหมาก่อสร้างของฉัน';
$userid = yii::$app->user->identity->id;
$query = MhUser::find($userid)->where(['user_id' => $userid])->one(); 
$name = $query->user_firstname;
$this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['mh-user/profile', 'id' => $userid]];
$this->params['breadcrumbs'][] = $this->title;

?>

<body style="background-color: #ecf0f3;">

<div class="mh-job-index">

        <div class="card2">
            <div class="card-body">
                <h4><?= Html::encode($this->title) ?></h4>
                <p>1.) เพิ่มผู้รับเหมาก่อสร้างของฉัน</p>
                
                <p>2.) แก้ข้อมูลสำหรับติดต่อของฉัน<?= Html::a('แก้ข้อมูลสำหรับติดต่อของฉัน', ['mh-user/update', 'id' => $userid], ['class' => 'btn btn-danger']) ?></p>
                <p>ระบุข้อมูลครบถ้วนเพื่อให้ผู้ใช้บริการจะค้นหาและติดต่อคุณได้โดยตรง</p>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>


        <div class="card2">
            <div class="card-body">
            <h4>กิจการรับเหมาก่อสร้างของฉัน</h4>
                <div class="container">
                    <div class=" row row-cols-1 row-cols-md-3">
                        <?php foreach ($modelsLimits as $modelsLimit) { ?>
                        
                            <?= Html::a('<div class="card-group h-100">
                                      <div class="card card-loop" >
                                        <div class="cardd-job">
                                        ' . Html::img($modelsLimit->getFirstPhotoURL(), ['class' => 'img-responsive rounded mx-auto d-block job-display-loop']) . '                      
                                        </div>
                        
                                        <div class="card-body card-body-list">
                                          <h6 class="card-title">' . "$modelsLimit->job_name" . ' </h6>
                                          <p class="card-text" style="font-size:12px;"><i class="fas fa-map-marker-alt"></i> ' . $modelsLimit->deliver() . ' ' . $modelsLimit->pickup() . '</p>
                                        </div>  
                        
                                      </div>
                                    </div>', ['mh-job/view', 'id' => $modelsLimit->job_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
                            ?>

                        <?php } ?>
                    </div>
        <br>
                </div>

                <?php if (!empty($modelsLimits)) : ?>
                            <?php
                                    echo \yii\bootstrap4\LinkPager::widget([
                                    'pagination' => $pagination,
                                ]);
                            ?>
                <?php else : ?>
                            <div class="alert alert-danger" role="alert">
                                <?php echo "ไม่มีข้อมูล !!"; ?>
                            </div>
                <?php endif; ?>

                <p>
                    <?= Html::a('+ เพิ่มผู้รับเหมาก่อสร้าง', ['create'], ['class' => 'btn btn-success']) ?>
                </p>


            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

</div>

</body>
