<?php

use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'เลือกจังหวัดที่ให้บริการ';
//$userid = yii::$app->user->identity->id;

$jobid = Yii::$app->getRequest()->getQueryParam('id');
//var_dump($jobid); echo '<br>';


?>

<div class="rog-car-post-form">

    
    <?= Html::beginForm(['/mh-job/add-route', 'id' => $model->job_id], 'POST'); ?>

        <?= Html::hiddenInput('step', 'amphoeselector'); ?>
        <?= Html::hiddenInput('job_id', $jobid); ?>

        <?php 
            foreach ($province as $provin)
            {
                echo '<h2>'. $provin .'</h2>';
                echo '<hr>';
                $pov = $this->context->getAmphoeByProvinceName($provin);
                //echo var_dump($pov);
                echo Html::checkboxList('selectedamphoe', $selectedamphoe, $pov, ['class' => 'test']) . '<br><hr>';
            }
        ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-primary']); ?>
    </div>
    <?= Html::endForm(); ?>

</div>