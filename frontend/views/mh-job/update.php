<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJob */
$userid = yii::$app->user->identity->id;
$this->title = 'แก้ไขข้อมูลงานรับเหมาก่อสร้างของฉัน: ' . $model->job_name;
if (Yii::$app->user->isGuest) {
        
    $this->params['breadcrumbs'][] = ['label' => 'รายการงานรับเหมาก่อสร้าง', 'url' => ['index']];
    
} else {

    $this->params['breadcrumbs'][] = ['label' => 'งานรับเหมาก่อสร้างของฉัน', 'url' => ['myjob'], 'id' => $userid];

}
//$this->params['breadcrumbs'][] = ['label' => 'งานรับเหมาก่อสร้างของฉัน', 'url' => ['myjob']];
$this->params['breadcrumbs'][] = ['label' => $model->job_id, 'url' => ['view', 'id' => $model->job_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-job-update">

<div class="card2">
    <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
        'zipcode' => $zipcode,
        'subpt' => $subpt,
    ]) ?>

</div>
