<?php

use common\models\MhJobSubPt;
use prawee\widgets\ButtonAjax;
use yii\bootstrap4\Modal;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = 'เลือกลักษณะงาน';
//$userid = yii::$app->user->identity->id;

$jobid = Yii::$app->getRequest()->getQueryParam('id');
$jobsubs = MhJobSubPt::find()->all();
//var_dump($jobid); echo '<br>';
?>

<div class="rog-car-post-form">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

    <h1>เลือกลักษณะงาน</h1>

    <hr>

    <?= Html::beginForm(['/mh-job/add-job-sub-type', 'id' => $model->job_id], 'POST'); ?>
    <?= Html::hiddenInput('step', 'subptselector'); ?>
    <?= Html::hiddenInput('job_id', $jobid);  ?>
    <div class="d-none d-sm-block">
        <?= Html::checkboxList('selectedsubpt', $selectedpt, $subpt, ['class' => 'test ']) ?>
    </div>

    <div class="d-lg-none d-md-none d-sm-none">
        <?= Html::checkboxList('selectedsubpt', $selectedpt, $subpt, ['class' => 'test btn-group-vertical']) ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton('ต่อไป', ['class' => 'btn btn-primary']); ?>
    </div>
    <?= Html::endForm(); ?>




    <div class="card">

        <h2 style="padding-left:20px; padding-top:20px;">งานของคุณ</h2>

        <hr>

        <div class="card-body">


            <?php

            $provtran = [];
            foreach ($servicetype as $key => $servicearea) {
                $provtran[$pt[$servicearea]][$key] = $subpt[$key];
            }

            if (!empty($servicetype)) {
            } else {

                echo ' <div class="alert alert-danger" role="alert">';
                echo "ไม่มีข้อมูล !!";
                echo '</div>';
            }

            foreach ($provtran as $key => $protran) {
                echo '<h4>' . $key . '</h4>';
                echo '<p>';
                foreach ($provtran[$key] as $distran) {
                    echo $distran . ' ';
                }
                echo '</p>';
                echo '<hr>';
            }
            ?>



        </div>
    </div>

    <br>


</div>