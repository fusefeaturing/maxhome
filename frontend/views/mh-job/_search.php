<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MhProvince;
use common\models\MhAmphoe;
use common\models\MhJobProduct;
use common\models\MhJobProductType;
use common\models\MhJobSubPt;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DepDrop;
use dosamigos\multiselect\MultiSelect;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model frontend\models\MhJobSearch */
/* @var $form yii\widgets\ActiveForm */
$province = ArrayHelper::map(MhProvince::find()->orderBy('province_name_th')->asArray()->all(), 'province_id', 'province_name_th');
$amphoee = ArrayHelper::map(MhAmphoe::find()->asArray()->all(), 'amphoe_id', 'amphoe_name_th');
$jobsearch = $this->context->getJobSearch();
$jobpt = ArrayHelper::map(MhJobProductType::find()->asArray()->orderBy('job_pt_level')->all(), 'job_pt_id', 'job_pt_name');
//$jobid = ArrayHelper::map(MhJobSubPt::find()->asArray()->all(), 'job_sub_pt_id', 'job_sub_pt_name');
//[27-search-form listing_types="place,event,jobs-2,real-estate,cars" tabs_mode="transparent" box_shadow="yes"]
?>


<div class="mh-job-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>


<div class="card2">

<h1 style="padding-left:20px; padding-top:20px;">ผู้รับเหมาก่อสร้าง</h1>

<div class="row card-body">

    <div class="col-sm-5">

        <?=     
                $form
                    ->field($model, 'job_sub_pt_id')
                    ->widget(Select2::classname(), [
                        'data' => $jobsearch,
                        'model' => $model,
                        'attribute' => 'job_sub_pt_id',
                        'language' => 'th',
                        'options' => ['placeholder' => 'เลือกงานรับเหมาก่อสร้าง'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            //'multiple' => true,
                        ],                    
                ])    
                ->label(false);                  
            ?>
    </div>


    <!--<div class="col-md-3">
        <?=
            $form
                ->field($model, 'job_pt_id')
                
                ->dropdownList($jobpt, [
                    'id' => 'ddl-job_pt_id',
                    'prompt' => 'เลือกหมวดงานรับเหมาก่อสร้าง'
                ])
                ->label(false);     
        ?>
    </div>

    <div class="col-md-3">
        <?=
            $form
                ->field($model, 'job_sub_pt_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-job_sub_pt_id'],
                    'data' => $subpt,
                    'pluginOptions' => [
                        'depends' => ['ddl-job_pt_id'],
                        'placeholder' => 'เลือกหมู่งานรับเหมาก่อสร้าง',
                        'url' => Url::to(['/mh-job/get-subpt'])
                    ]
                ])
                ->label(false);
        ?>
    </div>-->


    <div class="col-sm-3">
        <?=
            $form
                ->field($model, 'job_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
                ->label(false);
        ?>
    </div>

    <div class="col-sm-3">
        <?=
            $form
                ->field($model, 'job_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ',
                        'url' => Url::to(['/mh-job/get-amphoe'])
                    ]
                ])
                ->label(false);
        ?>
    </div>

       

        <div class="col-8 col-sm-4">
            <div class="form-group">
                <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary btn-block', 'id' => 'btnSubmit']) ?>
            </div>
        </div>

        <div class="col-4 col-sm-4">    
            <div class="form-group">       
                <?= Html::a(
                    'รีเซ็ต',
                    ['mh-job/index'],
                    ['class' => 'btn btn-danger btn-block', 'style' => '']
                ) ?>
            </div>
        </div>

        <?php ActiveForm::end();
        ?>


    
        <!--<div class="col-xs-12 col-sm-12 col-md-12">
                <?= Html::a(
                    'ลงประกาศ "ผู้รับเหมาก่อสร้าง"',
                    ['mh-job/create'],
                    ['class' => 'button-commany pink btn btn-block btn-lg', 'style' => '']
                ) ?>
        </div>-->
    
    <!--<div class="col-xs-12 col-sm-12 col-md-12">
        <p> </p>
    </div>-->
    
    <!--<div class="col-xs-12 col-sm-12 col-md-12"> 
           
           <?= Html::a(
               '<i class="fab fa-facebook-square"></i> Rogistic.com',
               'https://www.facebook.com/Rogisticcom-359767341103161',
               ['class' => 'btn btn-primary  btn-block btn-lg']
    
               )
           ?>
    
    </div>-->
    


</div>

</div>


</div>
