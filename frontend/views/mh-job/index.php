<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MhJobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลงานรับเหมา';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'job_id',
            'job_name',
            //'job_pic',
            'job_gps',
            'job_address',
            //'job_km',
            //'job_province_id',
            //'job_amphoe_id',
            'job_tel1',
            //'job_tel2',
            //'job_line_id',
            //'job_line_ad',
            //'job_fb_fp',
            //'job_messenger',
            //'job_email:email',
            //'job_website',
            //'job_other',
            //'job_pt_id',
            //'job_data_other',
            //'updated_id',
            //'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
