<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhArchitectEngineer */
$userid = yii::$app->user->identity->id;
$this->title = 'แก้ไขข้อมูลสถาปนิค / วิศวกร: ' . $model->ae_name;
if (Yii::$app->user->isGuest) {
        
    $this->params['breadcrumbs'][] = ['label' => 'รายการงานสถาปนิก / วิศวกรของฉัน', 'url' => ['index']];
    
} else {

    $this->params['breadcrumbs'][] = ['label' => 'งานสถาปนิก / วิศวกรของฉัน', 'url' => ['myae'], 'id' => $userid];

}
//$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลสถาปนิค / วิศวกร', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ae_id, 'url' => ['view', 'id' => $model->ae_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-architect-engineer-update">

    <div class="card2">
        <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
        'zipcode' => $zipcode,
    ]) ?>

</div>
