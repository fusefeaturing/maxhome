<?php

use common\models\MhAeProductType;
use common\models\MhAmphoe;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MhProvince;

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model frontend\models\MhArchitectEngineerSearch */
/* @var $form yii\widgets\ActiveForm */

$province = ArrayHelper::map(MhProvince::find()->orderBy('province_name_th')->asArray()->all(), 'province_id', 'province_name_th');
$amphoee = ArrayHelper::map(MhAmphoe::find()->asArray()->all(), 'amphoe_id', 'amphoe_name_th');
$aetype = ArrayHelper::map(MhAeProductType::find()->asArray()->all(), 'ae_pt_id', 'ae_pt_name');

?>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>


<div class="mh-architect-engineer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

<div class="card2">

<h1 style="padding-left:20px; padding-top:20px;">สถาปนิกออกแบบ / วิศวกรเขียนแบบ</h1>

<div class="row card-body">
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div id="w1" class="panel-group collapse in" aria-expanded="true" >
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">

                            <a class="collapse-toggle" href="#w1-collapse1" data-toggle="collapse" data-parent="#w1" aria-expanded="false"  class="divResult" id="divResult">เลือกประเภทรถขนส่ง (แตะเพื่อขยาย)</a>


                        </h4>
                    </div>

                    <div id="w1-collapse1" class="panel-collapse collapse" >
                        <div class="panel-body" id="divR"> 
                            <?=

                                $form
                                    ->field($model, 'ae_pt_id')
                                    ->checkBoxList(
                                        $this->context->getaeTypes(),
                                        [
                                            'separator' => '<br>',
                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                $checked = $checked ? 'checked' : '';
                                                return '<div> <span id="boxitem' . $value . '"> <input type="checkbox" value=' . $value . ' name=' . $name . ' ' . $checked  . '>' . $label . '</span></div>';
                                            }
                                        ]
                                    )
                                    
                                    ->label(false)
                                    ->hint('เลือกได้หลายรายการ')

                            ?>

                        </div>
                    </div>




                </div>
            </div>

        </div>
    

<div class="col-sm-4">
    <?= $form
        ->field($model, 'ae_pt_id')
        ->dropDownList(
            $aetype,
            [ 'separator' => '<br>',
            'prompt' => 'เลือกประเภทงานออกแบบเขียนแบบ'
        ]
        )
        ->hint(Html::a('', ['mh-store-product-type/index'], ['target' => '_blank', 'class' => 'linksWithTarget']))
        ->label(false)
        
    ?>
</div>

<div class="col-sm-4">
        <?=
            $form
                ->field($model, 'ae_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
                ->label(false)
        ?>
    </div>

    <div class="col-sm-4">
        <?=
            $form
                ->field($model, 'ae_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ...',
                        'url' => Url::to(['/mh-architect-engineer/get-amphoe'])
                    ]
                ])
                ->label(false);
        ?>
    </div>

    <div class="col-8 col-sm-4">
            <div class="form-group">
                <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary btn-block', 'id' => 'btnSubmit']) ?>
            </div>
        </div>

        <div class="col-4 col-sm-4">    
            <div class="form-group">       
                <?= Html::a(
                    'รีเซ็ต',
                    ['mh-architect-engineer/index'],
                    ['class' => 'btn btn-danger btn-block', 'style' => '']
                ) ?>
            </div>
        </div>

        </div>
        <!--<div class="col-xs-12 col-sm-12 col-md-12 ">
            <?= Html::a(
                'ลงประกาศ "สถาปนิก / วิศวกร"',
                ['mh-architect-engineer/create'],
                ['class' => 'button-commany success btn-block btn-lg btn', 'style' => '']
            ) ?>
        </div>-->

<!--<div class="col-xs-12 col-sm-12 col-md-12">
    <p> </p>
</div>-->



    <?php ActiveForm::end(); ?>

</div>

</div>
