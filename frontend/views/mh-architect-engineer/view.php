<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\MhArchitectEngineer */

$this->title = $model->ae_name;
if (Yii::$app->user->isGuest) {
        
    $this->params['breadcrumbs'][] = ['label' => 'งานสถาปนิก / วิศวกร', 'url' => ['index']];
    
} else {

    $this->params['breadcrumbs'][] = ['label' => 'งานสถาปนิก / วิศวกรของฉัน', 'url' => ['myae']];

}

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

Yii::$app->params['og_title']['content'] = $model->ae_name;
Yii::$app->params['og_description']['content'] = $model->ae_data_other;
Yii::$app->params['og_image']['content'] = $model->getFirstPhotoURL();
Yii::$app->params['og_url']['content'] = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
Yii::$app->params['og_keyword']['content'] = $model->ae_name;
?>

<body style="background-color: #ecf0f3;">


<div class="mh-architect-engineer-view">


<?php if(Yii::$app->session->hasFlash('alert')):?>
    <?= \yii\bootstrap4\Alert::widget([
    'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
    'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
    ])?>
<?php endif; ?>

        <div class="card2" style="padding-top:30px">
            <div class="rounded mx-auto d-block ">
                <div class="gImg ">
                    <p class="card-img-top"><?= $model->getPhotosViewer() ?></p>
                </div>
            </div>

            <div id="myModal" class="modal">
                <span class="close">×</span>
                    <img class="modal-content" id="img01">
                <div id="caption"></div>
            </div>
        </div>


            <div class="">
                <p></p>
            </div>


            
    <div class="card2">
        <div class="card-body">
          <h3 class="card-title"><strong>ชื่อกิจการ : <?= $model->ae_name ?></strong></h3>
            <p class="card-text"><strong>ที่อยู่กิจการ</strong> : อ. <?= $model->deliver() ?> จ. <?= $model->pickup() ?> <?= $model->zipcode() ?></p>
            <!--<p class="card-text"><strong>จังหวัดที่ตั้งกิจการ</strong> : <?= $model->pickup() ?></p>
            <p class="card-text"><strong>อำเภอที่ตั้งกิจการ</strong> : <?= $model->deliver() ?></p>-->
            
            <p class="card-text"><strong>ประเภทสินค้าร้านวัสดุก่อสร้าง</strong> <br> <?= $model->aetype_text() ?></p>
            <p class="card-text"><strong>รายละเอียดอื่นๆ</strong> <br> <?= nl2br($model->ae_data_other) ?></p>
        </div>
    </div>
            <div class="">
                <p></p>
            </div>


            <div class="card2">       
                <div class="card-body">

            <button class="btn btn-outline-dark btn-block " type="button" data-toggle="collapse" data-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fas fa-map-marker-alt"></i> ข้อมูลที่ตั้งกิจการ
            </button>

                        <div class="collapse" id="collapseExample1">
                            <div class="card card-body">
                                <p class="card-text"><strong>ที่อยู่กิจการ</strong> : <?= $model->ae_address ?> อ. <?= $model->deliver() ?> จ. <?= $model->pickup() ?> <?= $model->zipcode() ?></p>
                                <p class="card-text"><strong>พิกัด GPS</strong> : <?= $model->ae_gps ?></p>
                                <p class="card-text"><strong>รัศมีพื้นที่ให้บริการ</strong> : <?= $model->ae_km ?> กิโลเมตร</p>
                                

                            </div>
                        </div>
             

                <div class="">
                        <p></p>
                </div>

                <div class="">            

                        <button class="btn btn-outline-dark btn-block" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-route"></i> พื้นที่ให้บริการ
                        </button>

                        <div class="collapse" id="collapseExample2">
                            <div class="card card-body">
                                
        

        <?php

            $provtran = [];
            foreach($serviceroutes as $key => $servicearea)
            {
                $provtran[$province[$servicearea]][$key] = $amphoe[$key];
            }

            if (!empty($serviceroutes)) {
                if($owner)
                {
                
                
                
                    echo '<div class="col-lg-4 col-md-5 col-sm-3 col-sx-3">';
                    echo '<div class="form-group">';
                    echo Html::a('ลบพื้นที่ให้บริการทั้งหมด', ['deleteall', 'id' => $model->ae_id], [
                        'class' => 'btn btn-danger btn-block',
                        'data' => [
                            'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                            'method' => 'post',
                        ],
                    ]);    
                    echo '</div>';
                    echo '</div>';            
                    
                }
            }else {
                
                          echo ' <div class="alert alert-danger" role="alert">';
                                echo "ไม่มีข้อมูล !!"; 
                          echo '</div>';
            }

            foreach($provtran as $key => $protran)
            {
                echo '<h4>' . $key . '</h4>';
                echo '<p>';
                foreach($provtran[$key] as $distran)
                {
                    echo $distran . ' ';
                }
                echo '</p>';
                echo '<hr>';
            }
?>



    </div>
</div>
</div>

                <div class="">
                        <p></p>
                </div>


                        <button class="btn btn-outline-dark btn-block" type="button" data-toggle="collapse" data-target="#collapseExample3" aria-expanded="false" aria-controls="collapseExample">
                            <i class="far fa-address-book"></i> ข้อมูลติดต่ออื่นๆ
                        </button>

                        <div class="collapse" id="collapseExample3">
                            <div class="card card-body">
                                
                            <div class="">
                                    <p><strong>เบอร์ติดต่อ 1</strong> : <?= $model->ae_tel1 ?></p>
                                </div>
                                <div class="">
                                    <p><strong>เบอร์ติดต่อ 2</strong> : <?= $model->ae_tel2 ?></p>
                                </div>
                                <div class="">
                                    <p><strong>line ID</strong> : <?= $model->ae_line_id ?></p>
                                </div>
                                <div class="">
                                    <p><strong>line @</strong> : <?= $model->ae_line_ad ?></p>
                                </div>
                                <div class="">
                                    <p><strong>facebook fanpage</strong> : <?= $model->ae_fb_fp ?></p>
                                </div>
                                <div class="">
                                    <p><strong>messenger</strong> : <?= $model->ae_messenger ?></p>
                                </div>
                                <div class="">
                                    <p><strong>email</strong> : <?= $model->ae_email ?></p>
                                </div>
                                <div class="">
                                    <p><strong>เว็บไซต์</strong> : <?= $model->ae_website ?></p>
                                </div>
                                <!--<div class="">
                                    <p><strong>อื่นๆ</strong> : <?= $model->ae_other ?></p>
                                </div>-->
                                <div class="">
                                    <p><strong>วันเวลาที่สร้าง</strong> : <?= $model->created_time ?></p>
                                </div>
                                <div class="">
                                    <p><strong>วันเวลาที่แก้ไข</strong> : <?= $model->updated_time ?></p>
                                </div>

                            </div>
                        </div>
             

                   

<br>

                                <div class="row ">
                                    <?php
                                    if($owner)
                                        {
                                              
                                            echo '<div class="col-lg-4 col-md-4 col-sm-4 col-sx-4">';
                                            echo '<div class="form-group">';
                                            if (!empty($serviceroutes)) {
                                                echo Html::a('แก้ไขพื้นที่ให้บริการ', ['mh-architect-engineer/add-route', 'id' => $model->ae_id], ['class' => 'btn btn-warning btn-block']);
                                            }else {
                                                echo Html::a('ท่านยังไม่มีพื้นที่ให้บริการ คลิกเพื่อเพิ่ม!', ['mh-architect-engineer/add-route', 'id' => $model->ae_id], ['class' => 'btn btn-danger btn-block']);
                                                
                                                          
                                            }
                                            echo '</div>';
                                            echo '</div>';   
                                            
                                                
                                                
                                            echo '<div class="col-lg-2 col-md-2 col-sm-2 col-sx-2">';
                                            echo '<div class="form-group">';
                                            echo Html::a('แก้ไข', ['update', 'id' => $model->ae_id], ['class' => 'btn btn-primary btn-block']); 
                                            echo '</div>';
                                            echo '</div>';
                                        
                                            echo '<div class="col-lg-1 col-md-1 col-sm-1 col-sx-1">';
                                            echo '<div class="form-group">';
                                            echo Html::a('ลบ', ['delete', 'id' => $model->ae_id], [
                                                'class' => 'btn btn-danger btn-block',
                                                'data' => [
                                                    'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                                    'method' => 'post',
                                                ],
                                            ]);    
                                            echo '</div>';
                                            echo '</div>';  
                                        
                                        }
                                    ?>
                                </div>



                                <div class="col-sm-4 ">
                                    <?php
                                    if(!$owner)
                                        {
                                        
                                        
                                        
                                                echo '<p>';
                                                echo Html::a('<i class="fas fa-phone-alt"></i> ติดต่อกิจการ : ' . $model->ae_name, 
                                                ($model->ae_tel1 != '') 
                                                ? 'tel:'.$model->ae_tel1 
                                                : ['mh-user/view', 'id' => $model->user_id] , ['class' => 'btn btn-primary btn-block']);
                                                //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                                                echo '</p>';
                                        
                                        }
                                    ?>
                                </div>

        <!-- Your share button code -->
        <div class="">  
        <!-- Your share button code -->
            <div class="fb-share-button" 
                data-size="large"
                data-href=<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>
                data-layout="button_count">
            </div>
            <div class="line-it-button" 
                data-lang="en" 
                data-type="share-a" 
                
                data-ver="2" 
                data-url="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                style="display: none;">
            </div>
        </div>

            <div class="">
                <p></p>
            </div>

            <h4 class="card-text"><strong>ชื่อเจ้าของกิจการ</strong> : <?= $model->user->user_firstname ?></h4>
            <h6 class="card-text"><strong>เพิ่มโดย</strong> : 
                <?php if($model->updated_id) {
                    echo 'Admin';
                 } else {
                        echo $model->user->user_firstname;
                 }  
                ?> 
            </h6>

            </div>
          
</div>

                <script>
                    // Get the modal
                var modal = document.getElementById('myModal');

                modal.addEventListener('click',function(){
                this.style.display="none";
                })

                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];

                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                  modal.style.display = "none";
                }

                // Get all images and insert the clicked image inside the modal
                // Get the content of the image description and insert it inside the modal image caption
                var images = document.getElementsByTagName('img');
                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");
                var i;
                for (i = 0; i < images.length; i++) {
                  images[i].onclick = function() {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    modalImg.alt = this.alt;
                    captionText.innerHTML = this.nextElementSibling.innerHTML;
                  }
                }
                </script>

  

</div>

<br>

</body>
