<?php

use yii\bootstrap4\Html;
use yii\grid\GridView;
use yii\bootstrap4\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'สถาปนิกออกแบบ / วิศวกรเขียนแบบ';
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['og_title']['content'] = $this->title;
Yii::$app->params['og_description']['content'] = $model->ae_data_other;
Yii::$app->params['og_image']['content'] = $model->getFirstPhotoURL();
Yii::$app->params['og_url']['content'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
Yii::$app->params['og_keyword']['content'] = 'สถาปนิกออกแบบ ,วิศวกรเขียนแบบ ,hub.maxhome';

?>


<body style="background-color: #ecf0f3;">


<div class="mh-store-index">

    
    <?php echo $this->render('_search', ['model' => $searchModel, 'amphoe' => $amphoe,]); ?>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

    <div class="container">
        <div class=" row row-cols-1 row-cols-md-3">

            <?php foreach ($dataProvider->models as $model) { ?>

                <?= Html::a('<div class="card-group ">
                          <div class="card card-mobile">
                            <div class="cardd">
                            ' . Html::img($model->getFirstPhotoURL(), ['class' => 'img-responsive rounded mx-auto d-block commany-display-mobile commany-image576px commany-image768px commany-image1024px commany-image1200px']) . '                      
                            </div>

                            <div class="card-body">
                              <h6 class="card-title">' . "$model->ae_name" . ' </h6>
                              <p class="card-text" style="font-size:12px;"><i class="fas fa-map-marker-alt"></i> ' . $model->pickup() . ' ' . $model->deliver() . ' </p>
                            </div>  

                          </div>
                        </div>', ['mh-architect-engineer/view', 'id' => $model->ae_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
                ?>


            <?php } ?>  

        </div>
        <br>
    </div>

    <?php if (!empty($dataProvider->models)) : ?>
                <?php
                        echo \yii\bootstrap4\LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
    <?php else : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo "ไม่พบข้อมูล !!"; ?>
                </div>
    <?php endif; ?>





    <!--<?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showHeader' => false,
            'pager' => ['class' => LinkPager::className()],
            'columns' => [
                [

                    'attribute' => 'title',
                    'format' => 'raw',
                    'value' => function ($model) {
                        $htmlcard = '';
                        $htmlcard .= '<div style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);  padding: 9px; background-color: #f1f1f1;">';
                        /*
                    $htmlcard .= '<div class="row">';
                        $htmlcard .= '<div class="col-xs-12 col-sm-12 col-md-12">';
                        $htmlcard .= '<h4>' . ((array_key_exists($model->car_type, )) ? [$model->car_type] : 'ประเภทรถไม่ถูกต้อง') . ' </h4>';
                        $htmlcard .= '</div>';
                    $htmlcard .= '</div>';
*/
                        $htmlcard .= '<div class="row">';

                        $htmlcard .= '<div class="col-xs-12 col-4 col-md-4">';
                        $htmlcard .= '<p>' . $model->getFirstPhotos() . '</p>';
                        $htmlcard .= '</div>';
                        $htmlcard .= '<div class="col-xs-12 col-5 col-md-5 commany-display-mobile">';
                        $htmlcard .= '<p><strong>ชื่อกิจการ</strong> : ' . $model->ae_name . ' </p>';
                        $htmlcard .= '' . $model->deliver() . '';
                        $htmlcard .= ' ' . $model->pickup() . '';
                        //$htmlcard .= '<p><strong>ประเภทงานสถาปนิกออกแบบ</strong><br>' . $model->aetype_text() . '</p>';
                        //$htmlcard .= '<p>ประเภทสินค้า : ' . $model->ae_pt_id . '</p>';
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-3 col-md-3">';
                        $htmlcard .= Html::a(
                            'ดูรายละเอืยด',
                            ['mh-architect-engineer/view', 'id' => $model->ae_id],
                            ['class' => 'btn btn-success glyphicon glyphicon-user btn-sm btn-block', 'style' => 'box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);']
                        );
                        $htmlcard .= '</div>';

                        $htmlcard .= '</div>';

                        $htmlcard .= '</div>';

                        return $htmlcard;
                    },
                    'contentOptions' => ['style' => 'width:80%; height:20%; text-align: left; margin: 25px; vertical-align: middle;'],
                    'headerOptions' => ['style' => 'text-align: center;'],
                ],
            ],
        ]); ?>-->

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 ">
            <?= Html::a(
                'ลงประกาศ "สถาปนิก / วิศวกร"',
                ['mh-architect-engineer/create'],
                ['class' => 'button-commany success btn-block btn-lg btn', 'style' => '']
            ) ?>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

    </div>

</div>

</body>