<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhArchitectEngineer */
$userid = yii::$app->user->identity->id;
$this->title = 'เพิ่มงานสถาปนิก / วิศวกร';
if (Yii::$app->user->isGuest) {
        
    $this->params['breadcrumbs'][] = ['label' => 'รายการงานสถาปนิก / วิศวกรของฉัน', 'url' => ['index']];
    
} else {

    $this->params['breadcrumbs'][] = ['label' => 'งานสถาปนิก / วิศวกรของฉัน', 'url' => ['myae'], 'id' => $userid];

}
//$this->params['breadcrumbs'][] = ['label' => 'งานสถาปนิก / วิศวกรของฉัน', 'url' => ['myae']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-architect-engineer-create">

    <div class="card2">
        <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
        'zipcode' => [],
    ]) ?>

</div>
