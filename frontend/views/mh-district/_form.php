<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\MhAmphoe;
use backend\models\BackendLogin;

/* @var $this yii\web\View */
/* @var $model common\models\MhDistrict */
/* @var $form yii\widgets\ActiveForm */
$user_backend = ArrayHelper::map(BackendLogin::find()->asArray()->all(), 'id', 'firstname');
$amphoe = ArrayHelper::map(MhAmphoe::find()->asArray()->all(), 'amphoe_id', 'amphoe_name_th');
?>

<div class="mh-district-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'district_zip_code')->textInput() ?>

    <?= $form->field($model, 'district_name_th')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'district_name_en')->textInput(['maxlength' => true]) ?>

    <?='' //$form->field($model, 'updated_id')->textInput() ?>

    <?=
                $form
                    ->field($model, 'updated_id')
                    
                    ->dropdownList($user_backend, [
                        'id' => 'ddl-roguser',
                        
                    ])
            ?>

    <?= $form->field($model, 'created_time')->textInput([
        'readonly' => true,
        'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
    ]) ?>
    
    <?= $form->field($model, 'updated_time')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <?='' //$form->field($model, 'amphoe_id')->textInput() ?>

    <?=
                $form
                    ->field($model, 'amphoe_id')
                    ->label('ชื่อจังหวัด')
                    ->dropdownList($amphoe, [
                        'id' => 'ddl-amphoe',
                        'prompt' => 'เลือกจังหวัด'
                    ])
            ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
