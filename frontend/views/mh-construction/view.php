<?php

use yii\bootstrap4\Html;
use yii\widgets\DetailView;


use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model common\models\MhConstruction */

Yii::$app->params['og_title']['content'] = $model->cons_name;

Yii::$app->params['og_description']['content'] = $model->cons_data_other;
Yii::$app->params['og_image']['content'] = $model->getFirstPhotoURL();
Yii::$app->params['og_url']['content'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
Yii::$app->params['og_keyword']['content'] = $model->cons_name;
$consid = Yii::$app->getRequest()->getQueryParam('id');

$this->title = $model->cons_name;

if (Yii::$app->user->isGuest) {

    $this->params['breadcrumbs'][] = ['label' => 'ร้านวัสดุก่อสร้าง', 'url' => ['index']];
} else {

    $this->params['breadcrumbs'][] = ['label' => 'ร้านวัสดุก่อสร้างของฉัน', 'url' => ['mycons']];
}
$this->params['breadcrumbs'][] = $this->title;

?>

<style>
    p>strong {
        font-size: 1.25rem;
    }
</style>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>



<body style="background-color: #ecf0f3;">

    <div class="mh-construction-view">


        <?php if (Yii::$app->session->hasFlash('alert')) : ?>
            <?= \yii\bootstrap4\Alert::widget([
                'body' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
                'options' => ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
            ]) ?>
        <?php endif; ?>

        <div class="card2" style="padding-top:10px">
            <div class="card-body">

                <!-- mobile -->
                <div class="d-lg-none d-md-none d-sm-none">
                    <?php
                    if ($owner) {
                        echo '<button class="btn btn-block " type="button" data-toggle="collapse" data-target="#collapseExampleSetting" aria-expanded="false" aria-controls="collapseExample">';
                        echo '<i class="fas fa-cog"></i> จัดการ้านค้า';
                        echo '</button>';

                        echo '<div class="collapse" id="collapseExampleSetting">';
                        echo '<div class="card card-body">';
                        echo '<div class="row ">';

                        echo '<div class="col-lg-1 col-md-1 col-sm-1 col-sx-1">';
                        echo '<div class="form-group">';
                        echo Html::a('ลบ', ['delete', 'id' => $model->cons_id], [
                            'class' => 'btn btn-block ',
                            'style' => 'background-color: #dc3545;color:#ffffff;',
                            'data' => [
                                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                'method' => 'post',
                            ],
                        ]);
                        echo '</div>';
                        echo '</div>';

                        echo '<div class="col-lg-2 col-md-2 col-sm-2 col-sx-2">';
                        echo '<div class="form-group">';
                        echo Html::a('แก้ไขร้านค้า', ['update', 'id' => $model->cons_id], [
                            'class' => 'btn btn-dark btn-block',
                            //'style' => 'background-color:	#ff6dbe;color:#ffffff;'
                        ]);
                        echo '</div>';
                        echo '</div>';

                        echo '<div class="col-lg-3 col-md-3 col-sm-3 col-sx-3">';
                        echo '<div class="form-group">';
                        echo Html::a('เพิ่ม/ดู สินค้าหน้าร้าน', ['mh-cons-items/myitems', 'id' => $consid], [
                            'class' => 'btn btn-dark btn-block',
                            //'style' => 'background-color:	#be6dff;color:#ffffff;'
                        ]);
                        echo '</div>';
                        echo '</div>';

                        echo '<div class="col-lg-3 col-md-3 col-sm-3 col-sx-3">';
                        echo '<div class="form-group">';

                        if (!empty($serviceroutes)) {
                            echo Html::a('แก้ไขพื้นที่ให้บริการ', ['mh-construction/add-route', 'id' => $model->cons_id], [
                                'class' => 'btn btn-dark btn-block',
                                //'style' => 'background-color:#6dbeff; color:#ffffff;'
                            ]);
                        } else {
                            echo Html::a('แก้ไขพื้นที่ให้บริการ', ['mh-construction/add-route', 'id' => $model->cons_id], [
                                'class' => 'btn btn-dark btn-block',
                                //'style' => 'background-color:#ffbe6d;color:#ffffff;'
                            ]);
                        }
                        echo '</div>';
                        echo '</div>';

                        echo '</div>';
                        echo '</div>';
                        echo '</div>';
                    }

                    ?>

                </div>

                <!-- desktop -->
                <div class="d-none d-sm-block">
                    <div class="container ">
                        <?php if ($owner) { ?>
                            <div class="row">
                                <div class="form-group mr-2">
                                    <?= Html::a('ลบ', ['delete', 'id' => $model->cons_id], [
                                        'class' => 'btn btn-block ',
                                        'style' => 'background-color: #dc3545;color:#ffffff;',
                                        'data' => [
                                            'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                            'method' => 'post',
                                        ],
                                    ]); ?>
                                </div>

                                <div class="form-group mr-2">
                                    <?= Html::a('แก้ไขร้านค้า', ['update', 'id' => $model->cons_id], [
                                        'class' => 'btn btn-dark btn-block',
                                        //'style' => 'background-color: #ff6dbe;color:#ffffff;'
                                    ]); ?>
                                </div>

                                <div class="form-group mr-2">
                                    <?= Html::a('เพิ่ม/ดู สินค้าหน้าร้าน', ['mh-cons-items/myitems', 'id' => $consid], [
                                        'class' => 'btn btn-dark btn-block',
                                        //'style' => 'background-color: #be6dff;color:#ffffff;'
                                    ]); ?>
                                </div>

                                <div class="form-group mr-2">
                                    <?php
                                    if (!empty($serviceroutes)) {
                                        echo Html::a('แก้ไขพื้นที่ให้บริการ', ['mh-construction/add-route', 'id' => $model->cons_id], [
                                            'class' => 'btn btn-dark btn-block',
                                            //'style' => 'background-color:#6dbeff; color:#ffffff;'
                                        ]);
                                    } else {
                                        echo Html::a('แก้ไขพื้นที่ให้บริการ', ['mh-construction/add-route', 'id' => $model->cons_id], [
                                            'class' => 'btn btn-dark btn-block',
                                            //'style' => 'background-color:#ffbe6d;color:#ffffff;'
                                        ]);
                                    } ?>
                                </div>

                            </div>

                        <?php } ?>
                    </div>
                </div>


                <div class="row" style="margin-left:10px; padding-top:10px;">
                    <!-- Your share button code -->
                    <div class="fb-share-button" style="margin-right:10px;" data-href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" data-layout="button_count" data-size="large">
                        <a target="_blank" href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" class="fb-xfbml-parse-ignore">แชร์
                        </a>
                    </div>

                    <div class="line-it-button" style="margin-right:10px;" data-lang="th" data-type="share-a" data-ver="3" data-url=<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?> data-color="default" data-size="large" style="display: none; ">
                    </div>

                    <button type="button" style="margin-left:10px;" class="btn btn-secondary btn-copy btn-sm js-tooltip js-copy " data-toggle="tooltip" data-placement="bottom" data-copy="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" title="Copy to clipboard">
                        คัดลอก URL <i class="fas fa-copy"></i>
                    </button>
                </div>



                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p></p>
                </div>


                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">

                        <?php
                        $imagesexplode = explode(",", $model->cons_pic);
                        $photosImage = $model->cons_pic;

                        foreach ($imagesexplode as $key => $loopImage) { ?>

                            <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>

                        <?php } ?>

                    </ol>

                    <div class="carousel-inner">

                        <?php $count = 0;

                        foreach ($imagesexplode as $key => $loopImage) {
                            $count++ ?>
                            <div class="carousel-item <?= ($count == 1) ? 'active' : '' ?>">
                                <img src="<?php echo Yii::$app->request->baseUrl . '/common/web/uploads/' . $loopImage ?> " class=" img-fluid rounded mx-auto d-block gImg cons-display-mobile">
                            </div>

                            <div id="myModal" class="modal">
                                <span class="close">×</span>
                                <img class="modal-content" id="img01">
                                <div id="caption"></div>
                            </div>
                        <?php } ?>

                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p></p>
                </div>

                <h3 class="card-title"><strong><?= $model->cons_name ?></strong></h3>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-sx-4">
                        <?php
                        echo '<p>';
                        echo Html::a(
                            '<i class="fas fa-phone-alt"></i> ติดต่อ : ' . $model->cons_name,
                            ($model->cons_tel1 != '')
                                ? 'tel:' . $model->cons_tel1
                                : ['mh-user/view', 'id' => $model->user_id],
                            ['class' => 'btn btn-primary btn-block btn-sm rounded-pill']
                        );
                        //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                        echo '</p>';
                        ?>
                    </div>
                    <div class="col-sm-4">
                        <p><strong></strong> <a href="http://line.me/ti/p/~<?= $model->cons_line_id ?>" class="wpfront-button text-center btn-sm rounded-pill btn-sm btn-block" style="text-decoration: none;"><i class="fab fa-line"></i> LINE ID : <?= $model->cons_line_id ?></a></p>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p></p>
        </div>


        <div class="card2">
            <div class="card-body">

                <!--<p class="card-text"><strong>ที่อยู่กิจการ</strong> :  อ. <?= $model->deliver() ?> จ. <?= $model->pickup() ?> <?= $model->zipcode() ?></p>-->
                <!--<p class="card-text"><strong>จังหวัดที่ตั้งกิจการ</strong> : <?= $model->pickup() ?></p>
                    <p class="card-text"><strong>อำเภอที่ตั้งกิจการ</strong> : <?= $model->deliver() ?></p>-->
                <p class="card-text"><strong>หมวดสินค้า</strong> <br> <?= $model->constype_text() ?></p>
                <p class="card-text"><strong>รายละเอียดอื่นๆ</strong> <br> <?= nl2br($model->cons_data_other) ?></p>

                <p class="card-text"><strong>Facebook Fanpage</strong> :
                    <br>
                    <div class="fb-page " data-href="<?= $model->cons_fb_fp ?>" data-tabs="" data-width="280" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="<?= $model->cons_fb_fp ?>" class="fb-xfbml-parse-ignore">
                            <a href="<?= $model->cons_fb_fp ?>">

                            </a>
                        </blockquote>
                    </div>
                </p>

            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p></p>
        </div>

        <div class="card2">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-sx-4">
                        <?php
                        echo '<p>';
                        echo Html::a(
                            '<i class="fas fa-phone-alt"></i> ติดต่อ : ' . $model->cons_name,
                            ($model->cons_tel1 != '')
                                ? 'tel:' . $model->cons_tel1
                                : ['mh-user/view', 'id' => $model->user_id],
                            ['class' => 'btn btn-primary btn-block btn-sm rounded-pill']
                        );
                        //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                        echo '</p>';


                        ?>
                    </div>

                    <div class="col-sm-4">
                        <p><strong></strong> <a href="http://line.me/ti/p/~<?= $model->cons_line_id ?>" class="wpfront-button text-center btn-sm rounded-pill btn-sm btn-block" style="text-decoration: none;"><i class="fab fa-line"></i> LINE ID : <?= $model->cons_line_id ?></a></p>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-sx-3">
                        <div class="form-group">

                            <?= Html::a('ดูสินค้าหน้าร้าน', ['mh-cons-items/itemsstore', 'id' => $model->cons_id], ['class' => 'btn btn-primary btn-block']); ?>


                        </div>
                    </div>

                </div>




                <button class="btn btn-outline-dark btn-block " type="button" data-toggle="collapse" data-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fas fa-map-marker-alt"></i> ข้อมูลที่ตั้งกิจการ
                </button>

                <div class="collapse" id="collapseExample1">
                    <div class="card card-body">
                        <p class="card-text"><strong>ที่อยู่กิจการ</strong> : <?= $model->cons_address ?> อ. <?= $model->deliver() ?> จ. <?= $model->pickup() ?> <?= $model->zipcode() ?></p>
                        <p class="card-text"><strong>พิกัด GPS</strong> : <?= $model->cons_gps ?></p>
                        <p class="card-text"><strong>รัศมีพื้นที่ให้บริการ</strong> : <?= $model->cons_km ?> กิโลเมตร</p>


                    </div>
                </div>


                <div class="">
                    <p></p>
                </div>


                <div class="">

                    <button class="btn btn-outline-dark btn-block" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fas fa-route"></i> พื้นที่ให้บริการ
                    </button>

                    <div class="collapse" id="collapseExample2">
                        <div class="card card-body">
                            <?php

                            $provtran = [];
                            foreach ($serviceroutes as $key => $servicearea) {
                                $provtran[$province[$servicearea]][$key] = $amphoe[$key];
                            }

                            if (!empty($serviceroutes)) {
                                if ($owner) {

                                    echo '<div class="col-lg-4 col-md-5 col-sm-3 col-sx-3">';
                                    echo '<div class="form-group">';
                                    echo Html::a('ลบพื้นที่ให้บริการทั้งหมด', ['deleteall', 'id' => $model->cons_id], [
                                        'class' => 'btn btn-danger btn-block',
                                        'data' => [
                                            'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                            'method' => 'post',
                                        ],
                                    ]);
                                    echo '</div>';
                                    echo '</div>';
                                }
                            } else {

                                echo ' <div class="alert alert-danger" role="alert">';
                                echo "ไม่มีข้อมูล !!";
                                echo '</div>';
                            }

                            foreach ($provtran as $key => $protran) {
                                echo '<h4>' . $key . '</h4>';
                                echo '<p>';
                                foreach ($provtran[$key] as $distran) {
                                    echo $distran . ' ';
                                }
                                echo '</p>';
                                echo '<hr>';
                            }
                            ?>

                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p></p>
                </div>




                <button class="btn btn-outline-dark btn-block" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="far fa-address-book"></i> ข้อมูลติดต่ออื่นๆ
                </button>

                <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                        <p class="card-text"><strong>พิกัด GPS</strong> : <?= $model->cons_gps ?></p>
                        <p class="card-text"><strong>รัศมีพื้นที่ให้บริการ</strong> : <?= $model->cons_km ?> กิโลเมตร</p>
                        <div class="">
                            <p><strong>เบอร์ติดต่อ 1</strong> : <?= $model->cons_tel1 ?></p>
                        </div>
                        <div class="">
                            <p><strong>เบอร์ติดต่อ 2</strong> : <?= $model->cons_tel2 ?></p>
                        </div>
                        <div class="col-sm-3">
                            <p><strong></strong> <a href="http://line.me/ti/p/~<?= $model->cons_line_id ?>" class="wpfront-button text-center btn-block btn-sm rounded-pill btn-sm"><i class="fab fa-line"></i> LINE ID : <?= $model->cons_line_id ?></a></p>
                        </div>
                        <div class="">
                            <p><strong>line @</strong> : <?= $model->cons_line_ad ?></p>
                        </div>
                        <div class="">
                            <p><strong>Facebook Fanpage</strong> :
                                <br>
                                <div class="fb-page " data-href="<?= $model->cons_fb_fp ?>" data-tabs="" data-width="280" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                    <blockquote cite="<?= $model->cons_fb_fp ?>" class="fb-xfbml-parse-ignore">
                                        <a href="<?= $model->cons_fb_fp ?>">
                                            Rogistic.com รถร่วม รวมรถ ขนส่ง ทั่วไทย
                                        </a>
                                    </blockquote>
                                </div>
                            </p>
                        </div>
                        <div class="">
                            <p><strong>messenger</strong> : <?= $model->cons_messenger ?></p>
                        </div>
                        <div class="">
                            <p><strong>email</strong> : <?= $model->cons_email ?></p>
                        </div>
                        <div class="">
                            <p><strong>เว็บไซต์</strong> : <?= $model->cons_website ?></p>
                        </div>
                        <!--<div class="">
                                            <p><strong>อื่นๆ</strong> : <?= $model->cons_other ?></p>
                                        </div>-->
                        <div class="">
                            <p><strong>วันเวลาที่สร้าง</strong> : <?= $model->created_time ?></p>
                        </div>
                        <div class="">
                            <p><strong>วันเวลาที่แก้ไข</strong> : <?= $model->updated_time ?></p>
                        </div>

                    </div>
                </div>


                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p></p>
                </div>





                <!--<h4 class="card-text"><strong>ชื่อเจ้าของกิจการ</strong> : <?= $model->user->user_firstname ?></h4>
                    <h6 class="card-text"><strong>เพิ่มโดย</strong> : 
                        <?php if ($model->updated_id) {
                            echo 'Admin';
                        } else {
                            echo $model->user->user_firstname;
                        }
                        ?> 
                    </h6>-->


                <div class="row" style="margin-left:10px;">
                    <!-- Your share button code -->
                    <div class="fb-share-button" style="margin-right:10px;" data-href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" data-layout="button_count" data-size="large">
                        <a target="_blank" href="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" class="fb-xfbml-parse-ignore">แชร์
                        </a>
                    </div>

                    <div class="line-it-button" style="margin-right:10px;" data-lang="th" data-type="share-a" data-ver="3" data-url=<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?> data-color="default" data-size="large" style="display: none; ">
                    </div>

                    <button type="button" style="margin-left:10px;" class="btn btn-secondary btn-copy btn-sm js-tooltip js-copy" data-toggle="tooltip" data-placement="bottom" data-copy="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" title="Copy to clipboard">
                        คัดลอก URL <i class="fas fa-copy"></i>
                    </button>

                </div>



                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p></p>
                </div>

                <div align="center">
                    <div class="fb-group" data-href="https://www.facebook.com/groups/2000756793524391/" data-width="280" data-show-social-context="true" data-show-metadata="true">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p></p>
                </div>

            </div>


        </div>


        <script>
            // COPY TO CLIPBOARD
            // Attempts to use .execCommand('copy') on a created text field
            // Falls back to a selectable alert if not supported
            // Attempts to display status in Bootstrap tooltip
            // ------------------------------------------------------------------------------

            function copyToClipboard(text, el) {
                var copyTest = document.queryCommandSupported('copy');
                var elOriginalText = el.attr('data-original-title');

                if (copyTest === true) {
                    var copyTextArea = document.createElement("textarea");
                    copyTextArea.value = text;
                    document.body.appendChild(copyTextArea);
                    copyTextArea.select();
                    try {
                        var successful = document.execCommand('copy');
                        var msg = successful ? 'Copied!' : 'Whoops, not copied!';
                        el.attr('data-original-title', msg).tooltip('show');
                    } catch (err) {
                        console.log('Oops, unable to copy');
                    }
                    document.body.removeChild(copyTextArea);
                    el.attr('data-original-title', elOriginalText);
                } else {
                    // Fallback if browser doesn't support .execCommand('copy')
                    window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
                }
            }

            $(document).ready(function() {
                // Initialize
                // ---------------------------------------------------------------------

                // Tooltips
                // Requires Bootstrap 3 for functionality
                $('.js-tooltip').tooltip();

                // Copy to clipboard
                // Grab any text in the attribute 'data-copy' and pass it to the 
                // copy function
                $('.js-copy').click(function() {
                    var text = $(this).attr('data-copy');
                    var el = $(this);
                    copyToClipboard(text, el);
                });
            });
        </script>



        <script>
            // Get the modal
            var modal = document.getElementById('myModal');

            modal.addEventListener('click', function() {
                this.style.display = "none";
            })

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function() {
                modal.style.display = "none";
            }

            // Get all images and insert the clicked image inside the modal
            // Get the content of the image description and insert it inside the modal image caption
            var images = document.getElementsByTagName('img');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("caption");
            var i;
            for (i = 0; i < images.length; i++) {
                images[i].onclick = function() {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    modalImg.alt = this.alt;
                    captionText.innerHTML = this.nextElementSibling.innerHTML;
                }
            }
        </script>

    </div>
</body>