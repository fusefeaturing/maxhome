<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConstruction */
$userid = yii::$app->user->identity->id;
$this->title = 'แก้ไขข้อมูลร้านวัสดุก่อสร้าง: ' . $model->cons_name;
if (Yii::$app->user->isGuest) {
        
    $this->params['breadcrumbs'][] = ['label' => 'รายการร้านวัสดุก่อสร้าง', 'url' => ['index']];
    
} else {

    $this->params['breadcrumbs'][] = ['label' => 'ร้านวัสดุก่อสร้างของฉัน', 'url' => ['mycons'], 'id' => $userid];

}
//$this->params['breadcrumbs'][] = ['label' => 'ร้านค้าของฉัน', 'url' => ['mycons']];
$this->params['breadcrumbs'][] = ['label' => $model->cons_name, 'url' => ['view', 'id' => $model->cons_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-construction-update">

    <div class="card2">
        <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
        'zipcode' => $zipcode,
    ]) ?>

</div>
