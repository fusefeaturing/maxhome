<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MhConstructionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mh Constructions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-construction-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Mh Construction', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cons_id',
            'user_id',
            'cons_name',
            'cons_pic',
            'cons_gps',
            //'cons_address',
            //'cons_km',
            //'cons_province_id',
            //'cons_amphoe_id',
            //'cons_tel1',
            //'cons_tel2',
            //'cons_line_id',
            //'cons_line_ad',
            //'cons_fb_fp',
            //'cons_messenger',
            //'cons_email:email',
            //'cons_website',
            //'cons_other',
            //'cons_data_other',
            //'cons_pt_id',
            //'updated_id',
            //'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
