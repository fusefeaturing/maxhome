<?php

use common\models\MhConstructionProductType;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
use common\models\MhProvince;



/* @var $this yii\web\View */
/* @var $model common\models\MhConstruction */
/* @var $form yii\widgets\ActiveForm */
$province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');
$id = yii::$app->user->identity->id;



?>

<style>
input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}
</style>


<body style="background-color: #ecf0f3;">

<div class="mh-construction-form">

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

       
        

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]);?>

        <div class="card2">
        
            <div class="card-body">

                    <?= $form->field($model, 'user_id')
                       ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $id])
                       ->label(false)
                    ?>

                   <?= $form->field($model, 'cons_name')->textInput(['maxlength' => true])->label('ชื่อกิจการ', ['style' => 'color:red']) ?>

                   <?='' //$form->field($model, 'cons_pic')->textInput(['maxlength' => true]) ?>

                    <?= 
                        $form
                            ->field($model, 'cons_search_level')
                            ->hiddenInput(['maxlength' => true, 'value' => $model->getDefaultLevel()])
                            //->input('', ['placeholder' => ""])
                            ->label(false) 
                    ?>


                   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
                    <div class="field" align="left">
                          <?= $form->field($model, 'cons_pic[]')->fileInput(['multiple' => true])->label('รูปถ่ายกิจการ', ['style' => 'color:red']) ?>
                         <div class="well">
                             <?= $model->getPhotosViewer(); ?>        
                         </div>
                            <hr>
                      <input type="hidden" id="files" name="cons_pic[]" multiple />
                    </div>

                   <?= $form->field($model, 'cons_gps')->textInput(['maxlength' => true]) ?>



                   <?= $form->field($model, 'cons_km')->textInput(['maxlength' => true])
                   ->input('', ['placeholder' => "หน่วยเป็นกิโลเมตร"])
                    ?>

                   <?='' //$form->field($model, 'cons_province_id')->textInput() ?>

                   <?='' //$form->field($model, 'cons_amphoe_id')->textInput() ?>

                <div class="row">
                   <div class="col-xs-12 col-sm-6 col-md-4">
                       <?=
                           $form
                               ->field($model, 'cons_province_id')

                               ->dropdownList($province, [
                                   'id' => 'ddl-province',
                                   'prompt' => 'เลือกจังหวัด'
                               ])
                               ->label('จังหวัดที่ตั้งกิจการ', ['style' => 'color:red'])
                       ?>
                   </div>

                   <div class="col-xs-12 col-sm-6 col-md-4">
                       <?=
                           $form
                               ->field($model, 'cons_amphoe_id')
                               ->widget(DepDrop::classname(), [
                                   'options' => ['id' => 'ddl-amphoe'],
                                   'data' => $amphoe,
                                   'pluginOptions' => [
                                       'depends' => ['ddl-province'],
                                       'placeholder' => 'เลือกอำเภอ...',
                                       'url' => Url::to(['/mh-construction/get-amphoe'])
                                   ]
                               ])
                               ->label('อำเภอที่ตั้งกิจการ', ['style' => 'color:red']);
                       ?>
                   </div>   

                   <div class="col-xs-12 col-sm-6 col-md-4">
                       <?=
                           $form
                               ->field($model, 'cons_district_id')
                               ->widget(DepDrop::classname(), [
                                   'data' => $zipcode,
                                   'pluginOptions' => [
                                       'depends' => ['ddl-province', 'ddl-amphoe'],
                                       'placeholder' => 'เลือกรหัสไปรษณีย์...',
                                       'url' => Url::to(['mh-construction/get-zipcode'])
                                   ]
                               ])
                               ->label('รหัสไปรษณีย์');
                       ?>
                   </div> 
               </div>

                       <?= 
                            $form->field($model, 'cons_address')
                                ->textInput(['maxlength' => true])
                                ->input('', ['placeholder' => "ตัวอย่าง 1/2 ม. 3 ต. 4 *ไม่ต้องกรอก อำเภอ จังหวัด และรหัสไปรษณีย์"])
                                ->label('ที่อยู่กิจการ') 
                       ?>

                   <?= $form->field($model, 'cons_tel1')->textInput()->label('เบอร์ติดต่อ 1', ['style' => 'color:red']) ?>

                   <?= $form->field($model, 'cons_tel2')->textInput(['maxlength' => true]) ?>

                   <?= $form->field($model, 'cons_line_id')->textInput(['maxlength' => true]) ?>

                   <?= $form->field($model, 'cons_line_ad')->textInput(['maxlength' => true]) ?>

                   <?= $form->field($model, 'cons_fb_fp')->textInput(['maxlength' => true]) ?>

                   <?= $form->field($model, 'cons_messenger')->textInput(['maxlength' => true]) ?>

                   <?= $form->field($model, 'cons_email')->textInput(['maxlength' => true]) ?>

                   <?= $form->field($model, 'cons_website')->textInput(['maxlength' => true]) ?>

                   <?='' //$form->field($model, 'cons_other')->textInput(['maxlength' => true]) ?>



                   <?=$form->field($model, 'cons_pt_id')->checkboxList($model->getItemSkill())->label(false) ?>



                   <div class="row">
                       <div class="col-xs-12 col-sm-12 col-md-12">
                           <h6 style="color:red">เลือกหมวดสินค้า</h6>
                           <div class="custom-control custom-checkbox">
                               <input id="customCheck1" class="custom-control-input" type="checkbox" >
                               <label class="custom-control-label" style="color:green" for="customCheck1"><strong>เลือกทั้งหมด</strong></label>
                           </div>
                           <p></p>
                           <?=
                               $form

                                   ->field($model, 'cons_pt_id')
                                   ->checkBoxList(
                                       $this->context->getConsTypes(),
                                       ['itemOptions' => $model->constypeToArray(), 'separator' => '<br>']
                                   )
                                   ->hint(Html::a('คลิกดูประเภทรถ', ['mh-job-product-type/index'], ['target' => '_blank', 'class' => 'linksWithTarget']))
                                   ->label(false)
                                   ->hint('เลือกได้หลายรายการ')

                           ?>

                       </div>
                   </div>



                   <!--<p>    
                       <input type="button" class="btn btn-outline-success btn-sm" onclick='selectAll()' value="เลือกทั้งหมด"/>

                		<input type="button" class="btn btn-outline-danger btn-sm" onclick='UnSelectAll()' value="เอาออกทั้งหมด"/>
                	</p>-->


                   <!--<div class="row">
                       <div class="col-xs-12 col-sm-12 col-md-12">
                       <?=
                               $form->field($model, 'cons_pt_id')->checkboxList($this->context->getConsTypes(),[

                               'item' => function($index, $label, $name, $checked, $value) {
                               $checked = $checked ? 'checked' : '';
                               return "<label class='checkbox col-md-4' style='font-weight: normal;'><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";
                               }
                           ])
                           ->label('เลือกประเภทสินค้าก่อสร้าง', ['style' => 'color:red'])
                       ?>
                       </div>
                   </div>-->
                           
                           
                           
                           
                           
                   <?= $form->field($model, 'cons_data_other')
                       ->textarea(['rows' => 6])
                        ?>

                           
                   <?= $form->field($model, 'created_time')->textInput([
                       'readonly' => true,
                       'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
                   ]) ?>

                   <?= $form->field($model, 'updated_time')->textInput([
                       'readonly' => true,
                       'value' => date('Y-m-d H:i:s')
                   ]) ?>

                   <div class="form-group">
                       <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
                   </div>

            </div>
        </div>

    
    <?php ActiveForm::end(); ?>


    <script type="text/javascript">
        $("#customCheck1").click(function() {
            $("input[type=checkbox]").prop("checked", $(this).prop("checked"));
        });

        $("input[type=checkbox]").click(function() {
            if (!$(this).prop("checked")) {
                $("#customCheck1").prop("checked", false);
            }
        });

        jackHarnerSig();
    
    </script>


<script type="text/javascript">

$(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {
    $("#mhconstruction-cons_pic").on("change", function(e) {
      var files = e.target.files,
        filesLength = files.length;
      for (var i = 0; i < filesLength; i++) {
        var f = files[i]
        var fileReader = new FileReader();
        fileReader.onload = (function(e) {
          var file = e.target;
          $("<span class=\"\">" +
            "<img class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
            "&nbsp;" +
            "</span>").insertAfter("#files");
          $(".remove").click(function(){
            $(this).parent(".pip").remove();
          });
          
          // Old code here
          /*$("<img></img>", {
            class: "imageThumb",
            src: e.target.result,
            title: file.name + " | Click to remove"
          }).insertAfter("#files").click(function(){$(this).remove();});*/
          
        });
        fileReader.readAsDataURL(f);
      }
    });
  } else {
    alert("Your browser doesn't support to File API")
  }
});
</script>


</div>

</body>
