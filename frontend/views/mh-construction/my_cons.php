<?php

use common\models\MhUser;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'กิจการร้านวัสดุก่อสร้างของฉัน';
$userid = yii::$app->user->identity->id;

$query = MhUser::find($userid)->where(['user_id' => $userid])->one(); 
$name = $query->user_firstname;
$this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['mh-user/profile', 'id' => $userid]];
$this->params['breadcrumbs'][] = $this->title;
?>

<body style="background-color: #ecf0f3;">

<div class="rog-user-car-index">
    
    <div class="card2">
        <div class="card-body">
            <h4><?= Html::encode($this->title) ?></h4>
            <p>1.) เพิ่มร้านวัสดุก่อสร้างของฉัน</p>

            <p>2.) แก้ข้อมูลสำหรับติดต่อของฉัน<?=Html::a('แก้ข้อมูลสำหรับติดต่อของฉัน', ['mh-user/update', 'id' => $userid], ['class' => 'btn btn-danger']) ?></p>
            <p>ระบุข้อมูลครบถ้วนเพื่อให้ผู้ใช้บริการจะค้นหาและติดต่อคุณได้โดยตรง</p>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

    <div class="card2">
        <div class="card-body">
        <h4>ร้านวัสดุก่อสร้างของฉัน</h4>


<div class="container">
    <div class=" row row-cols-1 row-cols-md-3">

      <?php foreach ($modelsLimits as $modelsLimit) { ?>

        <?= Html::a('<div class="card-group h-100">
                          <div class="card consd-loop" >
                            <div class="cardd"> 
                            ' . Html::img($modelsLimit->getFirstPhotoURL(), ['class' => 'img-responsive rounded mx-auto d-block cons-display-loop']) . '                      
                            </div>

                            <div class="card-body card-body-list">
                              <h6 class="card-title">' . "$modelsLimit->cons_name" . ' </h6>
                              <p class="card-text" style="font-size:12px;"><i class="fas fa-map-marker-alt"></i> ' . $modelsLimit->deliver() . ' ' . $modelsLimit->pickup() . '</p>
                            </div>  

                          </div>
                        </div>', ['mh-construction/view', 'id' => $modelsLimit->cons_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
        ?>

      <?php } ?>

    </div>
    <br>
  </div>

  <?php if (!empty($modelsLimits)) : ?>
                <?php
                        echo \yii\bootstrap4\LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
    <?php else : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo "ไม่มีข้อมูล !!"; ?>
                </div>
    <?php endif; ?>


    <p>
        <?= Html::a('+ เพิ่มร้านวัสดุก่อสร้าง', ['create'], ['class' => 'btn btn-success']) ?>
    </p>




            <?='' /*GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showHeader'=> false,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'title',
                        'format' => 'raw',
                        'value' => function ($model)
                        {
                            $htmlcard = '';
                            $htmlcard .= '<div style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);  padding: 16px; background-color: #f1f1f1;">';
                        
                            $htmlcard .= '<div class="row">';
                        
                                $htmlcard .= '<div class="col-xs-12 col-sm-6 col-md-6">';
                                $htmlcard .= '<p><strong>ชื่อกิจการ</strong> : ' . $model->cons_name . '</p>';

                                $htmlcard .= '<p><strong>จังหวัดที่ตั้งกิจการ</strong> : ' . $model->pickup() . ' </p>';
                                $htmlcard .= '<p><strong>อำเภอที่ตั้งกิจการ</strong> : ' . $model->deliver() . ' </p>';
                                $htmlcard .= '<p><strong>รัศมีให้บริการ</strong> : ' . $model->cons_km . ' กิโลเมตร</p>';
                                $htmlcard .= '</div>';
                        
                                $htmlcard .= '<div class="col-xs-12 col-sm-4 col-md-4">';
                                $htmlcard .= $model->getFirstPhotos();
                                $htmlcard .= '</div>';
                        
                                $htmlcard .= '<div class="col-xs-12 col-sm-2 col-md-2" style="text-align: center; vertical-align: middle; margin-top: 10px;">';
                                $htmlcard .= Html::a('ดูข้อมูล', 
                                ['mh-construction/view', 'id' => $model->cons_id], 
                                ['class' => 'btn btn-success glyphicon glyphicon-user', 'style' => 'width: 100%;']);
                                $htmlcard .= Html::a('ระบุพื้นที่ที่ให้บริการ', 
                                ['mh-construction/add-route', 'id' => $model->cons_id], 
                                ['class' => 'btn btn-success glyphicon glyphicon-user btn-block','style' => 'margin-top: 10px;'] );
                                $htmlcard .= '</div>';
                        

                        
                            $htmlcard .= '</div>';
                        
                            $htmlcard .= '</div>';
                        
                            return $htmlcard;
                        },
                        'contentOptions' => ['style' => 'width:80%; height:20%; text-align: left; margin: 25px; vertical-align: middle;'],
                        'headerOptions' => ['style' => 'text-align: center;'],
                    ],
                ],
            ]);*/
            ?>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>


</div>

</body>
