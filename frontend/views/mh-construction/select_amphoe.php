<?php

use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'เลือกจังหวัดที่ให้บริการ';
//$userid = yii::$app->user->identity->id;


$consid = Yii::$app->getRequest()->getQueryParam('id');
//var_dump($consid); echo '<br>';


?>

<div class="rog-car-post-form">

   <h1>เลือกพื้นที่ให้บริการอำเภอ</h1>

   <hr>
    
    <?= Html::beginForm(['/mh-construction/add-route', 'id' => $model->cons_id ], 'POST'); ?>

        <?= Html::hiddenInput('step', 'amphoeselector'); ?>
        <?= Html::hiddenInput('cons_id', $consid); ?>

        <?php 
            foreach ($province as $provin)
            {
                echo '<h2>'. $provin .'</h2>';
                echo '<hr>';
                $pov = $this->context->getAmphoeByProvinceName($provin);
                
                echo Html::checkboxList('selectedamphoe', $selectedamphoe, $pov, ['class' => 'test']) . '<br><hr>';
            }            
        ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-primary']); ?>
    </div>
    <?= Html::endForm(); ?>

</div>