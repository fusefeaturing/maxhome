<?php

use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


$this->title = 'เลือกจังหวัดที่ให้บริการ';
//$userid = yii::$app->user->identity->id;

$consid = Yii::$app->getRequest()->getQueryParam('id');
//var_dump($consid); echo '<br>';

?>

<div class="rog-car-post-form">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

    <!--<script type="text/javascript">

        
			    function selectAll(){
			    	var items=document.getElementsByName('selectedprovince[]');
			    	for(var i=0; i<items.length; i++){
			    		if(items[i].type=='checkbox')
			    			items[i].checked=true;
			    	}
			    }
            
			    function UnSelectAll(){
			    	var items=document.getElementsByName('selectedprovince[]');
			    	for(var i=0; i<items.length; i++){
			    		if(items[i].type=='checkbox')
			    			items[i].checked=false;
			    	}
			    }			
        </script>-->




    <h1>เลือกพื้นที่ให้บริการจังหวัด</h1>
    <!--<h6>* เมื่อท่านต้องการเปลี่ยนแปลงพื้นที่ให้บริการ พื้นที่ให้บริการของท่านก่อนหน้านี้จะหายไปทั้งหมด </h6>-->


    <div class="custom-control custom-checkbox">
        <input id="customCheck1" class="custom-control-input" type="checkbox" value="เลือกทั้งหมด">
        <label class="custom-control-label" for="customCheck1">เลือกทั้งหมด</label>
    </div>


    <!--<p>
		<input type="button" class="btn btn-outline-success btn-sm"  onclick='selectAll()' value="เลือกทั้งหมด"/>
		<input type="button" class="btn btn-outline-danger btn-sm" onclick='UnSelectAll()' value="เอาออกทั้งหมด"/>
	</p>-->

    <hr>

    <?= Html::beginForm(['/mh-construction/add-route', 'id' => $model->cons_id], 'POST'); ?>
    <?= Html::hiddenInput('step', 'provinceselector'); ?>
    <?= Html::hiddenInput('cons_id', $consid);  ?>

    <div class="d-none d-sm-block">
        <?= Html::checkboxList('selectedprovince', $selectedprovince, $province, [

            'class' => 'test ',

        ]) ?>
    </div>

    <div class="d-lg-none d-md-none d-sm-none">
        <?= Html::checkboxList('selectedprovince', $selectedprovince, $province, [

            'class' => 'test btn-group-vertical',

        ]) ?>
    </div>



    <div class="form-group">
        <?= Html::submitButton('ต่อไป', ['class' => 'btn btn-primary']); ?>
    </div>


    <?= Html::endForm(); ?>



    <div class="card">

        <h2 style="padding-left:20px; padding-top:20px;">พื้นที่ให้บริการของคุณ</h2>

        <hr>

        <div class="card-body">

            <?php

            $provtran = [];
            foreach ($serviceroutes as $key => $servicearea) {
                $provtran[$province[$servicearea]][$key] = $amphoe[$key];
            }

            if (!empty($serviceroutes)) {
            } else {

                echo ' <div class="alert alert-danger" role="alert">';
                echo "ไม่มีข้อมูล !!";
                echo '</div>';
            }




            foreach ($provtran as $key => $protran) {


                echo '<h4>' . $key . '</h4>';
                echo '<p>';
                foreach ($provtran[$key] as $distran) {

                    echo $distran . ' ';
                }

                echo '</p>';
                echo '<hr>';
            }
            ?>



        </div>
    </div>

    <br>


    <script type="text/javascript">
        $("#customCheck1").click(function() {
            $("input[type=checkbox]").prop("checked", $(this).prop("checked"));
        });

        $("input[type=checkbox]").click(function() {
            if (!$(this).prop("checked")) {
                $("#customCheck1").prop("checked", false);
            }
        });

        jackHarnerSig();
    </script>



</div>