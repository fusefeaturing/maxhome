<?php

use common\models\MhConstruction;
use yii\bootstrap4\Html;
use yii\grid\GridView;
use yii\bootstrap4\LinkPager;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */



$this->title = 'ร้านวัสดุก่อสร้าง';
$this->params['breadcrumbs'][] = $this->title;

Yii::$app->params['og_title']['content'] = $this->title;
Yii::$app->params['og_description']['content'] = $model->cons_data_other;
Yii::$app->params['og_image']['content'] = $model->getFirstPhotoURL();
Yii::$app->params['og_url']['content'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
Yii::$app->params['og_keyword']['content'] = 'ร้านวัสดุก่อสร้าง ,hub.maxhome';
Yii::$app->params['og_revisit_after']['content'] = '7 Days';
?>

<body style="background-color: #ecf0f3;">
  


<div class="mh-job-index">

  
  <?php echo $this->render('_search', ['model' => $searchModel, 'amphoe' => $amphoe, 'subpt' => $subpt,]); ?>

  <div class="col-xs-12 col-sm-12 col-md-12">
    <p> </p>
  </div>


  <div class="container">
    <div class=" row row-cols-1 row-cols-md-3">

      <?php foreach ($dataProvider->models as $model) { ?>

        <?= Html::a('<div class="card-group h-100">
                          <div class="card consd-loop" >
                            <div class="cardd"> 
                            ' . Html::img($model->getFirstPhotoURL(), ['class' => 'img-responsive rounded mx-auto d-block cons-display-loop']) . '                      
                            </div>

                            <div class="card-body card-body-list">
                              <h6 class="card-title">' . "$model->cons_name" . ' </h6>
                              <p class="card-text" style="font-size:12px;"><i class="fas fa-map-marker-alt"></i> ' . $model->pickup() . ' ' . $model->deliver() . ' </p>
                            </div>  

                          </div>
                        </div>', ['mh-construction/view', 'id' => $model->cons_id], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
        ?>

      <?php } ?>



    </div>
    <br>
  </div>


      <?php if (!empty($dataProvider->models)) : ?>
            <?php
                    echo \yii\bootstrap4\LinkPager::widget([
                    'pagination' => $pagination,
                ]);
            ?>

      <?php else : ?>
            <div class="alert alert-danger" role="alert">
              <?php echo "ไม่พบข้อมูล !!"; ?>
            </div>
      <?php endif; ?>






  <div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 ">
      <?= Html::a(
        'ลงประกาศ "ร้านวัสดุก่อสร้าง"',
        ['mh-construction/create'],
        ['class' => 'button-commany danger btn-block btn-lg btn', 'style' => '']
      ) ?>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
      <p> </p>
    </div>

  </div>

      </div>
</body>