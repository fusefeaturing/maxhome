<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhConstruction */
$userid = yii::$app->user->identity->id;
$this->title = 'เพิ่มร้านวัสดุก่อสร้าง';
if (Yii::$app->user->isGuest) {
        
    $this->params['breadcrumbs'][] = ['label' => 'รายการร้านวัสดุก่อสร้าง', 'url' => ['index']];
    
} else {

    $this->params['breadcrumbs'][] = ['label' => 'ร้านวัสดุก่อสร้างของฉัน', 'url' => ['mycons'], 'id' => $userid ];

}
//$this->params['breadcrumbs'][] = ['label' => 'ร้านวัสดุก่อสร้างของฉัน', 'url' => ['mycons']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-construction-create">

<div class="card2">
    <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
        'zipcode' => [],
    ]) ?>

</div>
