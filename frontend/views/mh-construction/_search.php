<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use common\models\MhProvince;
use common\models\MhAmphoe;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DepDrop;




$province = ArrayHelper::map(MhProvince::find()->orderBy('province_name_th')->asArray()->all(), 'province_id', 'province_name_th');
$amphoee = ArrayHelper::map(MhAmphoe::find()->asArray()->all(), 'amphoe_id', 'amphoe_name_th');




/* @var $this yii\web\View */
/* @var $model frontend\models\MhConstructionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-construction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>





    <div class="card2">

        <h1 style="padding-left:20px; padding-top:20px;">ร้านวัสดุก่อสร้าง</h1>

        <div class="row card-body">


            <div class="col-sm-3">
                <?=
                $form
                    ->field($model, 'cons_pt_id')
                    ->dropDownList(
                        $this->context->getConsTypes(),
                        [
                            'separator' => '<br>',
                            'id' => 'ddl-cons_pt_id',
                            'prompt' => 'เลือกหมวดหมู่วัสดุก่อสร้าง',

                        ]
                    )

                    ->label(false)

                ?>
            </div>

            <div class="col-md-3">
                <?=
                $form
                    ->field($model, 'cons_sub_pt_id')
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => 'ddl-cons_sub_pt_id'],
                        'data' => $subpt,
                        'pluginOptions' => [
                            'depends' => ['ddl-cons_pt_id'],
                            'placeholder' => 'เลือกประเภทย่อยของสินค้าวัสดุ',
                            'url' => Url::to(['/mh-construction/get-subpt'])
                        ]
                    ])
                    ->label(false);
                ?>
            </div>



            <div class="col-sm-3">
                <?=
                $form
                    ->field($model, 'cons_province_id')

                    ->dropdownList($province, [
                        'id' => 'ddl-province',
                        'prompt' => 'เลือกจังหวัด'
                    ])
                    ->label(false)
                ?>
            </div>

            <div class="col-sm-3">
                <?=
                $form
                    ->field($model, 'cons_amphoe_id')
                    ->widget(DepDrop::classname(), [
                        'options' => ['id' => 'ddl-amphoe'],
                        'data' => $amphoe,
                        'pluginOptions' => [
                            'depends' => ['ddl-province'],
                            'placeholder' => 'เลือกอำเภอ',
                            'url' => Url::to(['/mh-construction/get-amphoe'])
                        ]
                    ])
                    ->label(false);
                ?>
            </div>




            <div class="col-8 col-sm-4">
                <div class="form-group">
                    <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary btn-block', 'id' => 'btnSubmit']) ?>
                </div>
            </div>

            <div class="col-4 col-sm-4">
                <div class="form-group">
                    <?= Html::a(
                        'รีเซ็ต',
                        ['mh-construction/index'],
                        ['class' => 'btn btn-danger btn-block', 'style' => '']
                    ) ?>
                </div>
            </div>



            <!--<div class="col-xs-12 col-sm-12 col-md-12 ">
    <?= Html::a(
        'ลงประกาศ "ร้านวัสดุก่อสร้าง"',
        ['mh-construction/create'],
        ['class' => 'button-commany danger btn-block btn-lg btn', 'style' => '']
    ) ?>
</div>-->

            <!--<div class="col-xs-12 col-sm-12 col-md-12">
    <p> </p>
</div>-->

            <!--<div class="col-xs-12 col-sm-12 col-md-12"> 
       
       <?= Html::a(
            '<i class="fab fa-facebook-square"></i> Rogistic.com',
            'https://www.facebook.com/Rogisticcom-359767341103161',
            ['class' => 'btn btn-primary  btn-block btn-lg']

        )
        ?>

</div>-->


            <?php ActiveForm::end(); ?>


        </div>


    </div>



</div>