<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobItems */

$this->title = 'รายละเอียดงานรับเหมาก่อสร้าง';

$jobid = Yii::$app->getRequest()->getQueryParam('id');
if (Yii::$app->user->isGuest) {
    
    $this->params['breadcrumbs'][] = ['label' => 'งานรับเหมาก่อสร้าง', 'url' => ['mh-job-items/itemsstore', 'id' => $model->job_id]];
    
} else {    

    $this->params['breadcrumbs'][] = ['label' => 'งานรับเหมาก่อสร้างของฉัน', 'url' => ['mh-job-items/myitems', 'id' => $jobid, ]];

}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-items-create">

    <div class="card2">
        <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_formadd', [
        'model' => $model,
        
    ]) ?>

</div>
