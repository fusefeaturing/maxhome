<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MhProvince;
use common\models\MhAmphoe;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model frontend\models\MhjobItemsSearch */
/* @var $form yii\widgets\ActiveForm */
$province = ArrayHelper::map(MhProvince::find()->orderBy('province_name_th')->asArray()->all(), 'province_id', 'province_name_th');
$amphoee = ArrayHelper::map(MhAmphoe::find()->asArray()->all(), 'amphoe_id', 'amphoe_name_th');
?>

<div class="mh-job-items-search">

    <?php $form = ActiveForm::begin([
        'action' => ['itemsstore', 'id' => $jobid],
        'method' => 'get',
        
    ]); 
   ?>

<div class="card2" >

    <h1 style="padding-left:20px; padding-top:20px;">งานรับเหมาก่อสร้าง</h1>

<div class="row card-body">


    <div class="col-sm-3">
        <?= 
            $form
                ->field($model, 'job_pt_id')
                ->dropDownList(
                    $this->context->getjobTypes(),
                    [ 'separator' => '<br>',
                    'id' => 'ddl-job_pt_id',
                    'prompt' => 'เลือกหมวดงานรับเหมาก่อสร้าง' 
                ])
                
                ->label(false)
               
        ?>
    </div>  

    <div class="col-md-3">
        <?=
            $form
                ->field($model, 'job_sub_pt_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-job_sub_pt_id'],
                    'data' => $subpt,
                    'pluginOptions' => [
                        'depends' => ['ddl-job_pt_id'],
                        'placeholder' => 'เลือกหมู่งานรับเหมาก่อสร้าง',
                        'url' => Url::to(['/mh-jobtruction/get-subpt'])
                    ]
                ])
                ->label(false);
        ?>
    </div>


    
    <div class="col-sm-3">
        <?=
            $form
                ->field($model, 'job_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
                ->label(false)
        ?>
    </div>

    <div class="col-sm-3">
        <?=
            $form
                ->field($model, 'job_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ',
                        'url' => Url::to(['/mh-jobtruction/get-amphoe'])
                    ]
                ])
                ->label(false);
        ?>
    </div>



                    
    <div class="col-8 col-sm-4">
            <div class="form-group">
                <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary btn-block', 'id' => 'btnSubmit']) ?>
            </div>
    </div>

    <div class="col-4 col-sm-4">    
        <div class="form-group">       
            <?= Html::a(
                'รีเซ็ต',
                ['mh-job-items/itemsstore', 'id' => $jobid],
                ['class' => 'btn btn-danger btn-block', 'style' => '']
            ) ?>
        </div>
    </div>

       

<!--<div class="col-xs-12 col-sm-12 col-md-12 ">
    <?= Html::a(
        'ลงประกาศ "ร้านวัสดุก่อสร้าง"',
        ['mh-jobtruction/create'],
        ['class' => 'button-commany danger btn-block btn-lg btn', 'style' => '']
    ) ?>
</div>-->

<!--<div class="col-xs-12 col-sm-12 col-md-12">
    <p> </p>
</div>-->

<!--<div class="col-xs-12 col-sm-12 col-md-12"> 
       
       <?= Html::a(
           '<i class="fab fa-facebook-square"></i> Rogistic.com',
           'https://www.facebook.com/Rogisticcom-359767341103161',
           ['class' => 'btn btn-primary  btn-block btn-lg']

           )
       ?>

</div>-->


<?php ActiveForm::end(); ?>


    </div>


    </div>

</div>
