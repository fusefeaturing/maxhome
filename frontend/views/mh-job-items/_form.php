<?php

use common\models\MhJob;
use common\models\MhJobProduct;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\MhUser;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;


/* @var $this yii\web\View */
/* @var $model common\models\MhJobItems */
/* @var $form yii\widgets\ActiveForm */
$id = yii::$app->user->identity->id;
//$job_product_id = Yii::$app->getRequest()->getQueryParam('id');
$userid = ArrayHelper::map(MhUser::find()->asArray()->all(), 'user_id', 'user_firstname');
//$jobproduct = ArrayHelper::map(MhJobProduct::find()->asArray()->all(), 'job_product_id', 'job_product_name');
//$jobid = ArrayHelper::map(MhJob::find()->where(['user_id' => $id])->andWhere('job_id')->asArray()->all(), 'job_id', 'job_name');
$job_id = Yii::$app->getRequest()->getQueryParam('id2');

?>

<body style="background-color: #ecf0f3;">

<div class="mh-job-items-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="card2">
            <?= 
                $form
                    ->field($model, 'job_item_level')
                    ->hiddenInput(['maxlength' => true, 'value' => $model->getDefaultLevel()])
                    //->input('', ['placeholder' => ""])
                    ->label(false) 
            ?>

        <div class="card-body">

        <?= $form->field($model, 'job_item_name')->textInput(['maxlength' => true])->label('ชื่อกิจการ', ['style' => 'color:red']) ?>

            
        <div class="field" align="left">
              <?= $form->field($model, 'job_item_pic[]')->fileInput(['multiple' => true])->label('รูปถ่ายผลงาน', ['style' => 'color:red']) ?>
             <div class="well">
                 <?= $model->getPhotosViewer(); ?>        
             </div>
         
          <input type="hidden" id="files" name="job_item_pic[]" multiple />
        </div>

        <?= $form->field($model, 'job_item_description')->textarea(['maxlength' => true])->label('รายละเอียดผลงาน', ['style' => 'color:red']) ?>



        <?php
            /*echo '<label class="control-label">วันที่เริ่มงาน ถึง วันที่สิ้นสุดงาน</label>';

            echo DatePicker::widget([
                'model' => $model,
                'attribute' => 'job_item_date_start',
                'form' => $form,
                'language' => 'th',
                'name' => 'job_item_date_start',
                'value' => $model->job_item_date_start,
                'options' => ['placeholder' => 'วันเริ่มงาน'],
                'type' => DatePicker::TYPE_RANGE,
                'name2' => 'job_item_date_start',
                'attribute2' => 'job_item_date_end',
                'value2' => $model->job_item_date_end,
                'options2' => ['placeholder' => 'วันสิ้นสุดงาน'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                    
                
                ]
            ]);*/
        ?>

            <?= $form->field($model, 'job_id')
                            ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $job_id])
                            ->label(false)
            ?>

            <?= $form->field($model, 'user_id')
                            ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $id])
                            ->label(false)
            ?>
                            


            <div class="form-group">
                <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
            </div>
       

        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
</body>
