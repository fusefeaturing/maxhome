<?php

use common\models\MhJob;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobItems */
$job_id = Yii::$app->getRequest()->getQueryParam('id');
$jobid = Yii::$app->getRequest()->getQueryParam('id2');
$this->title = $model->job_item_name;

if (Yii::$app->user->isGuest) {
    $query = MhJob::find($jobid)->where(['job_id' => $jobid])->one(); 
    $name = $query->job_name;
    $this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['mh-job/view', 'id' => $query->job_id]];
        
} else {
    $userid = yii::$app->user->identity->id;
    $query = MhJob::find($userid)->where(['user_id' => $userid])->one(); 
    $name = $query->job_name;
    $this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['mh-job/view', 'id' => $query->job_id]];
}
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>


<body style="background-color: #ecf0f3;">

<div class="mh-job-items-view">
    <div class="card2">
        <div class="card-body">
        <h3><?= Html::encode($this->title) ?></h3>
        
        <div class="row">
            <?php 
                if($owner) {
                    echo '<div class="col-lg-2 col-md-2 col-sm-2 col-sx-2">';
                    echo '<div class="form-group">';
                    echo Html::a('เพิ่มผลงาน', ['create', 'id' => $model->job_id], ['class' => 'btn btn-success btn-block']); 
                    echo '</div>';
                    echo '</div>';
                    
                    echo '<div class="col-lg-2 col-md-2 col-sm-2 col-sx-2">';
                    echo '<div class="form-group">';
                    echo Html::a('แก้ไข', ['update', 'id' => $model->job_items_id, 'id2' => $model->job_id], ['class' => 'btn btn-primary btn-block']); 
                    echo '</div>';
                    echo '</div>';
                
                    echo '<div class="col-lg-1 col-md-1 col-sm-2 col-sx-1">';
                    echo '<div class="form-group">';
                    echo Html::a('ลบ', ['delete', 'id' => $model->job_items_id], [
                        'class' => 'btn btn-danger btn-block',
                        'data' => [
                            'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                            'method' => 'post',
                        ],
                    ]);    
                    echo '</div>';
                    echo '</div>';
                }
               
            ?>
        </div>

    <div class="card2" style="padding-top:10px">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                
                    <?php
                    $imagesexplode = explode(",", $model->job_item_pic);
                    $photosImage = $model->job_item_pic;
                
                    foreach ($imagesexplode as $key => $loopImage) { ?>

                            <li data-target="#carouselExampleIndicators" data-slide-to="0"></li>

                    <?php } ?>
                
            </ol>

            <div class="carousel-inner">

                <?php $count=0;
                
                foreach ($imagesexplode as $key => $loopImage) { $count++ ?>
                    <div class="carousel-item <?= ($count==1)?'active':'' ?>">
                        <img src="<?php echo Yii::$app->request->baseUrl . '/common/web/uploads/'. $loopImage ?> " 
                        class=" img-fluid rounded mx-auto d-block gImg job-display-mobile"  >
                    </div>

                    <div id="myModal" class="modal">
                        <span class="close">×</span>
                            <img class="modal-content" id="img01">
                        <div id="caption"></div>
                    </div>
                <?php } ?>
              
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
        </div>

            <div id="myModal" class="modal">
                <span class="close">×</span>
                    <img class="modal-content" id="img01">
                <div id="caption"></div>
            </div>
        
    </div>

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p></p>
                </div>
        
            <p class="card-text"><strong>ชื่องานรับเหมา</strong> : <?= $model->job_item_name ?></p>

            <!--<p class="card-text"><strong>วันเริ่มงานถึงวันสิ้นสุดงาน</strong> : <?= date("d/m/Y", strtotime($model->job_item_date_start)) .' ถึง '. date("d/m/Y", strtotime($model->job_item_date_end)) ?></p>-->
            
                <?php /*if(!$model->jobSubType->job_sup_type_name) {
                    echo 'ไม่ระบุ';
                 } else {
                    echo $model->jobSubType->job_sup_type_name;
                 }  */
                ?> </p>
            
            <div class="">
                <p class="card-text"><strong>รายละเอียดอื่นๆ</strong> <br> <?= nl2br($model->job_item_description) ?></p>
            </div>
<br>
            <div class="">
                    <?php
                    if(!$owner)
                        {
                                echo '<p>';
                                echo Html::a('<i class="fas fa-phone-alt"></i> ติดต่อกิจการ : ' . $model->job->job_name, 
                                ($model->job->job_tel1 != '') 
                                ? 'tel:'.$model->job->job_tel1 
                                : ['mh-user/view', 'id' => $model->job->user_id] , ['class' => 'btn btn-primary']);
                                //echo Html::a('Tel.', ['user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                                echo '</p>';
                        
                        }
                    ?>
            </div>

            </div>
        </div>

                <div class="">
                    <p></p>
                </div>


                
    <div class="card2"> 
        <div class="card-body">
            <h4><p class="card-text"><?= $model->job->job_name ?></p></h4>

          

           <?php
                    
                        echo '<div class="">';
                        echo '<div class="form-group">';
                        echo Html::a('ดูผลงานทั้งหมด', ['mh-job-items/myitems', 'id' => $model->job_id], ['class' => 'btn btn-primary']) ;
                        echo '</div>';
                        echo '</div>';
                   
           ?>

            <button class="btn btn-outline-dark btn-block " type="button" data-toggle="collapse" data-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample">
                <i class="fas fa-map-marker-alt"></i> ข้อมูลที่ตั้งกิจการ
            </button>

                <div class="collapse" id="collapseExample1">
                    <div class="card card-body">
                        <p class="card-text"><strong>ที่อยู่กิจการ</strong> : <?= $model->job->job_address ?> อ. <?= $model->job->deliver() ?> จ. <?= $model->job->pickup() ?> <?= $model->job->zipcode() ?></p>
                        <p class="card-text"><strong>พิกัด GPS</strong> : <?= $model->job->job_gps ?></p>
                        <p class="card-text"><strong>รัศมีพื้นที่ให้บริการ</strong> : <?= $model->job->job_km ?> กิโลเมตร</p>    
                    </div>
                </div>
             

                <div class="">
                        <p></p>
                </div>

                <button class="btn btn-outline-dark btn-block" type="button" data-toggle="collapse" data-target="#collapseExample2" aria-expanded="false" aria-controls="collapseExample">
                <i class="fas fa-route"></i> พื้นที่ให้บริการ
                </button>
                    
                <div class="collapse" id="collapseExample2">
                    <div class="card card-body">         

                    <?php
                
                            $provtran = [];
                            foreach($serviceroutes as $key => $servicearea)
                            {
                                $provtran[$province[$servicearea]][$key] = $amphoe[$key];
                            }
                        
                            if (!empty($serviceroutes)) {
                                if($owner)
                                            {
                                            
                                                echo '<div class="col-lg-4 col-md-5 col-sm-3 col-sx-3">';
                                                echo '<div class="form-group">';
                                                echo Html::a('ลบพื้นที่ให้บริการทั้งหมด', ['deleteall', 'id' => $model->job_id], [
                                                    'class' => 'btn btn-danger btn-block',
                                                    'data' => [
                                                        'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                                                        'method' => 'post',
                                                    ],
                                                ]);    
                                                echo '</div>';
                                                echo '</div>';            
                                                
                                            }
                            }else {
                                
                                          echo ' <div class="alert alert-danger" role="alert">';
                                                echo "ไม่มีข้อมูล !!"; 
                                          echo '</div>';
                            }
                        
                            foreach($provtran as $key => $protran)
                            {
                                echo '<h4>' . $key . '</h4>';
                                echo '<p>';
                                foreach($provtran[$key] as $distran)
                                {
                                    echo $distran . ' ';
                                }
                                echo '</p>';
                                echo '<hr>';
                            }
                    ?>
                
                        
                        
                    </div>
                </div>
         

                <div class="col-xs-12 col-sm-12 col-md-12">
                    <p></p>
                </div>
                        <button class="btn btn-outline-dark btn-block" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <i class="far fa-address-book"></i> ข้อมูลติดต่ออื่นๆ
                        </button>

                        <div class="collapse" id="collapseExample">
                            <div class="card card-body">
                                    <p class="card-text"><strong>พิกัด GPS</strong> : <?= $model->job->job_gps ?></p>
                                    <p class="card-text"><strong>รัศมีพื้นที่ให้บริการ</strong> : <?= $model->job->job_km ?> กิโลเมตร</p>
                                <div class="">
                                    <p><strong>เบอร์ติดต่อ 1</strong> : <?= $model->job->job_tel1 ?></p>
                                </div>
                                <div class="">
                                    <p><strong>เบอร์ติดต่อ 2</strong> : <?= $model->job->job_tel2 ?></p>
                                </div>
                                <div class="">
                                    <p><strong>line ID</strong> : <?= $model->job->job_line_id ?></p>
                                </div>
                                <div class="">
                                    <p><strong>line @</strong> : <?= $model->job->job_line_ad ?></p>
                                </div>
                                <div class="">
                                    
                                    <p><strong>Facebook Fanpage</strong> : 
                                            <br>
                                        <div class="fb-page "  
                                                data-href="<?= $model->job->job_fb_fp ?>" 
                                                data-tabs="" 
                                                data-width="280"         
                                                data-small-header="false" 
                                                data-adapt-container-width="true" 
                                                data-hide-cover="false" 
                                                data-show-facepile="true">
                                                <blockquote cite="<?= $model->job->job_fb_fp ?>" 
                                                class="fb-xfbml-parse-ignore">
                                                    <a href="<?= $model->job->job_fb_fp ?>">
                                                        Rogistic.com รถร่วม รวมรถ ขนส่ง ทั่วไทย
                                                    </a>
                                                </blockquote>
                                        </div>
                                    </p>
                                </div>
                                <div class="">
                                    <p><strong>messenger</strong> : <?= $model->job->job_messenger ?></p>
                                </div>
                                <div class="">
                                    <p><strong>email</strong> : <?= $model->job->job_email ?></p>
                                </div>
                                <div class="">
                                    <p><strong>เว็บไซต์</strong> : <?= $model->job->job_website ?></p>
                                </div>

                            </div>
                        </div>
           
                <div class="col-xs-12 col-sm-12 col-md-12">
                        <p></p>
                </div>
            
            
                <div class="row" style="margin-left:10px;">   
                    <!-- Your share button code -->
                    <div class="fb-share-button" style="margin-right:10px;"
                        data-href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                        data-layout="button_count" 
                        data-size="large">
                            <a target="_blank" href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                                class="fb-xfbml-parse-ignore">แชร์
                            </a>
                    </div>

                    <div class="line-it-button" style="margin-right:10px;"
                        data-lang="th" 
                        data-type="share-a" 
                        data-ver="3" 
                        data-url=<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>
                        data-color="default" 
                        data-size="large" 
                        style="display: none; ">
                    </div>

                    <button type="button" style="margin-left:10px;" class="btn btn-secondary btn-copy btn-sm js-tooltip js-copy " 
                    data-toggle="tooltip" data-placement="bottom" data-copy="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" title="Copy to clipboard">
                    คัดลอก URL <i class="fas fa-copy"></i>
                    </button> 
                </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <p></p>
            </div>  
   
        </div>
    </div>
      
</div>

<script>

// COPY TO CLIPBOARD
// Attempts to use .execCommand('copy') on a created text field
// Falls back to a selectable alert if not supported
// Attempts to display status in Bootstrap tooltip
// ------------------------------------------------------------------------------

function copyToClipboard(text, el) {
var copyTest = document.queryCommandSupported('copy');
var elOriginalText = el.attr('data-original-title');

if (copyTest === true) {
var copyTextArea = document.createElement("textarea");
copyTextArea.value = text;
document.body.appendChild(copyTextArea);
copyTextArea.select();
try {
  var successful = document.execCommand('copy');
  var msg = successful ? 'Copied!' : 'Whoops, not copied!';
  el.attr('data-original-title', msg).tooltip('show');
} catch (err) {
  console.log('Oops, unable to copy');
}
document.body.removeChild(copyTextArea);
el.attr('data-original-title', elOriginalText);
} else {
// Fallback if browser doesn't support .execCommand('copy')
window.prompt("Copy to clipboard: Ctrl+C or Command+C, Enter", text);
}
}

$(document).ready(function() {
// Initialize
// ---------------------------------------------------------------------

// Tooltips
// Requires Bootstrap 3 for functionality
$('.js-tooltip').tooltip();

// Copy to clipboard
// Grab any text in the attribute 'data-copy' and pass it to the 
// copy function
$('.js-copy').click(function() {
var text = $(this).attr('data-copy');
var el = $(this);
copyToClipboard(text, el);
});
});
</script>
</body>
