<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobItems */

$this->title = 'แก้ไขงานรับเหมาก่อสร้าง : ' . $model->job_items_id;
$jobid = Yii::$app->getRequest()->getQueryParam('id');
$this->params['breadcrumbs'][] = ['label' => 'งานรับเหมา', 'url' => ['index']];
if (Yii::$app->user->isGuest) {
    
    $this->params['breadcrumbs'][] = ['label' => 'สินค้าภายในร้าน', 'url' => ['mh-job-items/itemsstore', 'id' => $model->job_id]];
    
} else {    

    $this->params['breadcrumbs'][] = ['label' => 'สินค้าภายในร้านของฉัน', 'url' => ['mh-job-items/myitems', 'id' => $model->job_id]];

}
$this->params['breadcrumbs'][] = ['label' => $model->job_item_name, 'url' => ['view', 'id' => $model->job_items_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-job-items-update">
    <div class="card2">
        <div class="card-body">
        <h1><?= Html::encode($this->title) ?></h1>

        </div>
    </div>
    
    <?= $this->render('_form', [
        'model' => $model,
        
    ]) ?>

</div>
