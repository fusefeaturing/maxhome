<?php

use common\models\MhJob;
use yii\bootstrap4\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ผลงานรับเหมาก่อสร้าง';

$jobid = Yii::$app->getRequest()->getQueryParam('id');

if (Yii::$app->user->isGuest) {
    $query = MhJob::find($jobid)->where(['job_id' => $jobid])->one(); 
    $name = $query->job_name;
    $this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['mh-job/view', 'id' => $query->job_id]];
        
} else {
    $userid = yii::$app->user->identity->id;
    $query = MhJob::find($userid)->where(['user_id' => $userid])->one(); 
    $name = $query->job_name;
    $this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['mh-job/view', 'id' => $query->job_id]];
}



$this->params['breadcrumbs'][] = $this->title;

?>

<body style="background-color: #ecf0f3;">

<div class="rog-user-car-index">
    <div class="card2">
        <div class="card-body">
            <h1><?= Html::encode($this->title) ?></h1>

            <?php                         
                if (Yii::$app->user->isGuest) {
        
                } else {
                
                    echo '<div class="">';
                    echo '<div class="form-group">';
                    echo Html::a('+ เพิ่มงานรับเหมาก่อสร้าง', ['create', 'id' => $jobid ], ['class' => 'btn btn-success']); 
                    echo '</div>';
                    echo '</div>';

                }
            ?>
           
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

       
    <div class="card2">
        <div class="card-body">
            
            <div class="container">
                <div class=" row row-cols-1 row-cols-md-3">
                    <?= Html::hiddenInput('job_id', $jobid);  ?>
                        <?php foreach ($dataProvider->models as $model) { ?>
                        
                            <?= Html::a('<div class="card-group h-100">
                                            <div class="card card-loop" >
                                                <div class="cardd-job">
                                                    ' . Html::img($model->getFirstPhotoURL(), ['class' => 'img-responsive rounded mx-auto d-block job-display-loop']) . '                      
                                                </div>   

                                                <div class="card-body card-body-list">
                                                    <h6 class="card-title">' . $model->job_item_name . ' </h6>                                            
                                                </div> 
                                            </div>
                                            </div>', ['view', 'id' => $model->job_items_id, 'id2' => $jobid], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
                            ?>

                        <?php } ?>

                </div>
            </div>

            <?php if (!empty($dataProvider->models)) : ?>
                        <?php
                                echo \yii\bootstrap4\LinkPager::widget([
                                'pagination' => $pagination,
                            ]);

                            
                   ?>
                       
            <?php else : ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo "ไม่มีข้อมูล !!"; ?>
                        </div>
            <?php endif; ?>


        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>


</div>



</body>
