<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MhJobItemsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mh Job Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-items-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Mh Job Items', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'job_items_id',
            'job_item_name',
            'job_item_level',
            'job_product_id',
            'job_price',
            //'job_id',
            //'user_id',
            //'job_pt_id',
            //'job_sub_pt_id',
            //'job_province_id',
            //'job_amphoe_id',
            //'updated_id',
            //'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
