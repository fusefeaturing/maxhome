<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'งานรับเหมาก่อสร้าง';
$this->params['breadcrumbs'][] = ['label' => 'งานรับเหมาก่อสร้าง', 'url' => ['mh-job/index']];
$jobid = Yii::$app->getRequest()->getQueryParam('id');
$this->params['breadcrumbs'][] = ['label' => $model->job_name , 'url' => ['mh-job/view','id' => $jobid]];
$this->params['breadcrumbs'][] = $this->title;

?>

<body style="background-color: #ecf0f3;">

<div class="rog-user-car-index">
    

<?php echo $this->render('_searchitemsstore', ['model' => $searchModel, 'amphoe' => $amphoe, 'subpt' => $subpt, 'jobid' => $jobid, ]); ?>


<div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

    <div class="card2">

   
    
        <div class="card-body">
       
        <?= Html::hiddenInput('job_id', $jobid);  ?>

        <?='' //$model->job->job_name ?>

            <div class="container">
    <div class=" row row-cols-1 row-cols-md-3">

      <?php foreach ($dataProvider->models as $model) { ?>
        
        <?= Html::a('<div class="card-group">
           
                          <div class="card card-mobile" >
                            

                            <div class="card-body">
                              <h6 class="card-title">' . $model->jobProduct->job_product_name . ' </h6>
                              <p style="font-size:14px; color:green; " class="card-title text-right">' . $model->job_price . ' </p>
                              <p style="font-size:12px; color:blue" class="card-title"><i class="fas fa-map-marker-alt"></i> ' . $model->job->jobProvince->province_name_th . ' ' . $model->job->jobAmphoe->amphoe_name_th . ' </p>
                            </div>  

                          </div>
                        </div>', ['view', 'id' => $model->job_items_id ], ['class' => '', 'style' => 'text-decoration: none; color:black;'])
        ?>

      <?php } ?>

    </div>
    
  </div>

  <?php if (!empty($dataProvider->models)) : ?>
                <?php
                        echo \yii\bootstrap4\LinkPager::widget([
                        'pagination' => $pagination,
                    ]);
                ?>
    <?php else : ?>
                <div class="alert alert-danger" role="alert">
                    <?php echo "ไม่มีข้อมูล !!"; ?>
                </div>
    <?php endif; ?>



    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>


</div>

</body>
