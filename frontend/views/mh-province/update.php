<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhProvince */

$this->title = 'แก้ไขข้อมูลจังหวัด: ' . $model->province_name_th;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลจังหวัด', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->province_name_th, 'url' => ['view', 'id' => $model->province_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mh-province-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
