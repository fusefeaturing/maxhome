<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhProvince */

$this->title = 'เพิ่มข้อมูลจังหวัด';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลจังหวัด', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-province-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
