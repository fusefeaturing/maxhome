<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhJobProductType */

$this->title = 'แก้ไขข้อมูลประเภทสินค้า: ' . $model->job_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลประเภทสินค้าก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->job_pt_name, 'url' => ['view', 'id' => $model->job_pt_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mh-job-product-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
