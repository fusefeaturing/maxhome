<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhJobProductTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลประเภทสินค้าก่อสร้าง';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-job-product-type-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'job_pt_id',
            'job_pt_name',
            //'updated_id',
            //'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
