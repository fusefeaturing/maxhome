<?php

use yii\helpers\Html;


/* @var $this yii\web\View */


$this->title = 'hub.maxhome.co';

?>
<div class="site-login">

    <?php

    if (Yii::$app->user->isGuest) {
    } else {
        $userid = yii::$app->user->identity->id;
        echo '<h2>ยินดีต้อนรับคุณ ' . Yii::$app->user->identity->user_firstname . '</h2>';
        echo '<br>';
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'ออกจากระบบ (' . Yii::$app->user->identity->user_firstname . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }

    ?>

    <div class="container">


        <div class="row">
            <!-- <div class="col-md-4">
                <?= Html::a(
                    '<div class="card-counterv4 purple">
                                <i class="fas fa-city"></i>
                                
                                <span class="count-numbers">ดูสินค้า</span>
                                <span class="count-name">ร้านวัสดุก่อสร้าง</span>
                                </div>',
                    ['mh-cons-items/index'],
                    [/*'style' => 'background-color:#cb06ff'*/]
                ) ?>


            </div> -->


            <div class="col-md-4">
                <?= Html::a(
                    '<div class="card-counterv4 pink">
                                <i class="fas fa-city"></i>
                                
                                <span class="count-numbers">ผู้รับเหมาก่อสร้าง</span>
                                <span class="count-name">ทั่วประเทศ</span>
                                </div>',
                    ['mh-job/index'],
                    [/*'style' => 'background-color:#cb06ff'*/]
                ) ?>


            </div>

            <?php

            if (Yii::$app->user->isGuest) {



                echo '<div class="col-md-4" style="justify-content: center; align-items: center; ">';
                echo Html::a(
                    '<div class="card-counterv4 primary">
            <i class="fab fa-facebook-square"></i>
            <span class="count-numbers">Hub.MaxHome</span>
            <span class="count-name">สมัครสมาชิก/เข้าสู่ระบบ</span>
            </div>',
                    ['site/auth', 'authclient' => 'facebook'],
                    ['class' => '', 'style' => 'background-color: #3b5998;']
                );
                echo '</div>';
                echo '<br>';

                echo '<div class="col-md-4" style="justify-content: center; align-items: center; ">';
                echo Html::a(
                    '<div class="card-counterv4 danger">
                    <i class="fab fa-google-plus-square"></i>
            <span class="count-numbers">Hub.MaxHome</span>
            <span class="count-name">สมัครสมาชิก/เข้าสู่ระบบ</span>
            </div>',
                    ['site/auth', 'authclient' => 'google'],
                    ['class' => '', 'style' => 'background-color: #FFFFFF;']
                );
                echo '</div>';
                echo '<br>';
            } else {


                echo '<div class=" col-md-4" style="justify-content: center; align-items: center; ">';
                echo Html::a(
                    '<div class="card-counterv4 secondary">
            <i class="fas fa-user-edit"></i>
            <span class="count-numbers">แก้ไข</span>
            <span class="count-name">ข้อมูลส่วนตัว</span>
            </div>',
                    ['/mh-user/profile', 'id' => $userid],
                    ['class' => '']
                );
                echo '</div>';
            }

            ?>


            <!-- <div class="col-md-4">
                <?= Html::a(
                    '<div class="card-counterv4 danger">
                                <i class="fas fa-store-alt"></i>
                                
                                <span class="count-numbers">ร้านวัสดุก่อสร้าง</span>
                                <span class="count-name">ใกล้บ้านคุณ</span>
                                </div>',
                    ['mh-construction/index'],
                    [/*'style' => 'height:100px' , 'width:150px'*/]
                ) ?>

            </div> -->

        </div>


        <!-- <hr> -->

        <!-- <div class="row">
            <div class="col-md-4">
                <?= Html::a(
                    '<div class="card-counterv4 success">
                                <i class="fas fa-pencil-ruler"></i>
                                <span class="count-numbers">สถาปนิก / วิศวกร</span>
                                <span class="count-name">ออกแบบเขียนแบบ</span>
                                </div>',
                    ['mh-architect-engineer/index'],
                    [/*'style' => 'background-color:#cb06ff'*/]
                ) ?>

            </div>

            <div class="col-md-4">
                <?= Html::a(
                    '<div class="card-counterv4 bluesea">
                                <i class="fas fa-tools"></i>
                                <span class="count-numbers">รายการสินค้า</span>
                                <span class="count-name">ร้าน Maxhome.co</span>
                                </div>',
                    'https://maxhome.co/shop',
                    [/*'style' => 'background-color:#cb06ff'*/]
                ) ?>


            </div>

            <div class="col-md-4">
                <?= Html::a(
                    '<div class="card-counterv4 info">
                                <i class="fas fa-cube"></i>
                                <span class="count-numbers">สั่งคอนกรีต</span>
                                <span class="count-name">ผสมเสร็จ</span>
                                </div>',
                    ['mh-concrete/create'],
                    [/*'style' => 'background-color:#cb06ff'*/]
                ) ?>

            </div>
        </div> -->


        <!-- <hr> -->

        <div class="row">



            <!-- <?php

                    if (Yii::$app->user->isGuest) {



                        echo '<div class="col-md-4" style="justify-content: center; align-items: center; ">';
                        echo Html::a(
                            '<div class="card-counterv4 primary">
            <i class="fab fa-facebook-square"></i>
            <span class="count-numbers">Hub.MaxHome</span>
            <span class="count-name">สมัครสมาชิก/เข้าสู่ระบบ</span>
            </div>',
                            ['site/auth', 'authclient' => 'facebook'],
                            ['class' => '', 'style' => 'background-color: #3b5998;']
                        );
                        echo '</div>';



                        echo '<br>';
                    } else {


                        echo '<div class=" col-md-4" style="justify-content: center; align-items: center; ">';
                        echo Html::a(
                            '<div class="card-counterv4 secondary">
            <i class="fas fa-user-edit"></i>
            <span class="count-numbers">แก้ไข</span>
            <span class="count-name">ข้อมูลส่วนตัว</span>
            </div>',
                            ['/mh-user/profile', 'id' => $userid],
                            ['class' => '']
                        );
                        echo '</div>';
                    }

                    ?> -->

            <!-- <div class="col-md-4">
                <?= Html::a(
                    '<div class="card-counterv4 green">
                                <i class="fas fa-users"></i>
                                <span class="count-numbers">เชิญชวนเข้าร่วม</span>
                                <span class="count-name">hub maxhome</span>
                                </div>',
                    ['site/groupface'],
                    [/*'style' => 'background-color:#cb06ff'*/]
                ) ?>
            </div> -->
        </div>

        <br>





        <div class="">
            <img src="common/web/img/Hub-3.jpg" class="img-thumbnail" alt="">
        </div>

        <br>
        <button class="btn btn-outline-dark btn-block" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            <i class="far fa-address-book"> เชิญเข้าร่วมกับ Hub.MaxHome</i>
        </button>

        <div class="collapse" id="collapseExample">
            <div class="card">
                <div class="card-body">
                    <p style="text-indent: 2.5em;"> Hub maxhome คือศูนย์ร่วมกิจการรับเหมาก่อสร้าง ช่วยแนะนำ ผู้รับเหมาก่อสร้าง ร้านวัสดุก่อสร้าง ผู้ผลิต ตัวแทนจำหน่าย ที่พร้อมให้บริการทั่วประเทศไทย ให้กับลูกค้าที่กำลังสนใจสินค้าและบริการ</p>

                    <p style="text-indent: 2.5em;"> เข้าร่วม Hub maxhome ชุมชนวงการธุรกิจก่อสร้าง เสมือนว่าธุรกิจก่อสร้างของท่านตั้งอยู่ทำเลทองที่มีผู้คนสนใจสินค้าและบริการของท่านตลอดเวลา มี traffic จำนวนมากจากลูกค้าที่มีความต้องการใช้สินค้าและบริการของท่านและสามารถติดต่อไปยังธุรกิจของท่านโดยตรง</p>
                    <p style="text-indent: 2.5em;">• ระบบหน้าร้านค้าวัสดุก่อสร้างออนไลน์สำเร็จรูป ที่ให้เจ้าของธุรกิจในวงการก่อสร้างสามารถเขามาสร้างหน้าร้านของตัวเองได้ง่ายๆ ภายใน 3 นาที</p>
                    <p style="text-indent: 2.5em;">• หน้าร้านธุรกิจก่อสร้างออนไลน์ที่อยู่ใจกลางชุมชนวงการธุรกิจก่อสร้างที่มีลูกค้าค้นหาบริการของท่านตลอดเวลา</p>
                    <p style="text-indent: 2.5em;">• ระบบช่วยให้ลูกค้าของคุณสามารถค้นหาสินค้าและบริการของท่าน และติดต่อหาท่านโดยตรงทั้งทาง โทรศัพท์ , Line@ , messenger หรือดูข้อมูลของท่านเพิ่มเติมจากทาง Website หรือ Facebook fanpage ของท่าน</p>

                </div>


                </span>

            </div>





        </div>

        <br>
    </div>