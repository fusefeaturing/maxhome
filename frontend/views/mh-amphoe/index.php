<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhAmphoeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการข้อมูลอำเภอ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-amphoe-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'amphoe_id',
            'amphoe_name_th',
            'amphoe_name_en',
            //'updated_id',
            //'created_time',
            //'updated_time',
            [
                'attribute' => 'province_id',
                'value' => 'province.province_name_th',
            ],
            //'province_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
