<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhAmphoe */

$this->title = $model->amphoe_name_th;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลอำเภอ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-amphoe-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('แก้ไข', ['update', 'id' => $model->amphoe_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ลบ', ['delete', 'id' => $model->amphoe_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'amphoe_id',
            'amphoe_name_th',
            'amphoe_name_en',
            //'updated_id',
            [
                'attribute' => 'updated_id',
                'value' => $model->user_backend->firstname
            ],
            'created_time',
            'updated_time',
            //'province_id',
            [
                'attribute' => 'province_id',
                'value' => $model->province->province_name_th
            ],
        ],
    ]) ?>

</div>
