<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$userid = yii::$app->user->identity->id;
$this->title = 'ร้านค้าของฉัน';
$this->params['breadcrumbs'][] = ['label' => 'ดูและแก้ไขข้อมูลส่วนตัว', 'url' => ['mh-user/profile', 'id' => $userid]];


$this->params['breadcrumbs'][] = $this->title;
?>

<div class="rog-user-car-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>1.) เพิ่มร้านค้าของฉัน</p>
    
    <p>2.) แก้ข้อมูลสำหรับติดต่อของฉัน<?= Html::a('แก้ข้อมูลสำหรับติดต่อของฉัน', ['mh-user/update', 'id' => $userid], ['class' => 'btn btn-danger']) ?></p>
    <p>ระบุข้อมูลครบถ้วนเพื่อให้ผู้ใช้บริการจะค้นหาและติดต่อคุณได้โดยตรง</p>

    <hr>
    
    <h2>1.) ร้านค้าของฉัน</h2>

    <p>
        <?= Html::a('+ เพิ่มร้านค้า', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showHeader'=> false,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model) 
                {
                    $htmlcard = '';
                    $htmlcard .= '<div style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);  padding: 16px; background-color: #f1f1f1;">';

                    $htmlcard .= '<div class="row">';

                        $htmlcard .= '<div class="col-xs-12 col-sm-6 col-md-6">';
                        $htmlcard .= '<p><strong>ชื่อกิจการ</strong><br>' . $model->store_name . '</p>';
                        $htmlcard .= '<p><strong>ประเภทสินค้าร้านค้า</strong><br>' . $model->storetype_text() . '</p>';
                        $htmlcard .= '<p><strong>รัศมีให้บริการ</strong> : ' . $model->store_km . ' กิโลเมตร</p>';
                        $htmlcard .= '<p><strong>พื้นที่ให้บริการจังหวัด</strong> : ' . $model->location_province($model->store_province_id) . ' </p>';
                        $htmlcard .= '<p><strong>พื้นที่ให้บริการอำเภอ</strong> : ' . $model->location_amphoe($model->store_amphoe_id) . ' </p>';
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-4 col-md-4">';
                        $htmlcard .= $model->getFirstPhotos();
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-2 col-md-2" style="text-align: center; vertical-align: middle; margin-top: 10px;">';
                        $htmlcard .= Html::a('ดูข้อมูล', 
                        ['mh-store/view', 'id' => $model->store_id], 
                        ['class' => 'btn btn-success glyphicon glyphicon-user', 'style' => 'width: 100%;']);
                        $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    return $htmlcard;
                },
                'contentOptions' => ['style' => 'width:80%; height:20%; text-align: left; margin: 25px; vertical-align: middle;'],
                'headerOptions' => ['style' => 'text-align: center;'],
            ],
        ],
    ]);
    ?>

    <hr>

    <!--<h2>2.) พื้นที่ให้บริการ</h2>
    <p>
        <?= Html::a('+ ระบุเส้นทางที่ฉันให้บริการ', ['car/add-route'], ['class' => 'btn btn-success']) ?>
    </p>-->

<?php
/*
$provtran = [];
foreach($serviceroutes as $key => $servicearea)
{
    $provtran[$provinces[$servicearea]][$key] = $amphures[$key];
}

foreach($provtran as $key => $protran)
{
    echo '<h4>' . $key . '</h4>';
    echo '<p>';
    foreach($provtran[$key] as $distran)
    {
        echo $distran . ' ';
    }
    echo '</p>';
}*/
?>

</div>
