<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MhProvince;
use common\models\MhAmphoe;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DepDrop;

$province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');
$amphoe = ArrayHelper::map(MhAmphoe::find()->asArray()->all(), 'amphoe_id', 'amphoe_name_th');

/* @var $this yii\web\View */
/* @var $model frontend\models\MhStoreSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mh-store-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <?php echo $form
                ->field($model, 'store_province_id')
                ->dropdownList($province, ['prompt' => 'ไม่ระบุ']) ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
            <?php echo $form
                ->field($model, 'store_amphoe_id')
                ->dropdownList($amphoe, ['prompt' => 'ไม่ระบุ']) ?>
        </div>


    <!--
    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'store_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'store_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ...',
                        'url' => Url::to(['/mh-store/get-amphoe'])
                    ]
                ]);
        ?>
    </div>
                    -->




        <div class="form-group">
            <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary btn-block', 'id' => 'btnSubmit']) ?>
            <?= Html::a(
                'รีเซ็ตค่าค้นหา',
                ['mh-store/index'],
                ['class' => 'btn btn-danger btn-block', 'style' => '']
            ) ?>
        </div>

        <?php ActiveForm::end();
        ?>

    </div>

    <?='' //$form->field($model, 'store_id') ?>

    <?='' //$form->field($model, 'user_id') ?>

    <?='' //$form->field($model, 'store_name') ?>

    <?='' //$form->field($model, 'store_pic') ?>

    <?='' //$form->field($model, 'store_address') ?>

    <?php // echo $form->field($model, 'store_gps') ?>

    <?php // echo $form->field($model, 'store_km') ?>

    <?php // echo $form->field($model, 'store_province_id') ?>

    <?php // echo $form->field($model, 'store_amphoe_id') ?>

    <?php // echo $form->field($model, 'store_tel1') ?>

    <?php // echo $form->field($model, 'store_tel2') ?>

    <?php // echo $form->field($model, 'store_line_id') ?>

    <?php // echo $form->field($model, 'store_line_ad') ?>

    <?php // echo $form->field($model, 'store_fb_fp') ?>

    <?php // echo $form->field($model, 'store_messenger') ?>

    <?php // echo $form->field($model, 'store_email') ?>

    <?php // echo $form->field($model, 'store_website') ?>

    <?php // echo $form->field($model, 'store_other') ?>

    <?php // echo $form->field($model, 'store_data_other') ?>

    <?php // echo $form->field($model, 'store_pt_id') ?>

    <?php // echo $form->field($model, 'updated_id') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>

</div>
