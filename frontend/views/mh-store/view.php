<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MhStore */

$this->title = $model->store_name;
if (Yii::$app->user->isGuest) {
        
    $this->params['breadcrumbs'][] = ['label' => 'รายการร้านค้า', 'url' => ['index']];
    
} else {

    $this->params['breadcrumbs'][] = ['label' => 'ร้านค้าของฉัน', 'url' => ['mystore']];

}

$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="mh-store-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if($owner)
        {
            
            echo Html::a('แก้ไข', ['update', 'id' => $model->store_id], ['class' => 'btn btn-primary']); 
            echo Html::a('ลบ', ['delete', 'id' => $model->store_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'คุณต้องการลบรายการนี้หรือไม่?',
                    'method' => 'post',
                ],
            ]);
            
            
        
        }
    ?>



    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <h1>ชื่อกิจการ : <?= Html::encode($this->title) ?></h1>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <p><?= $model->getPhotosViewer() ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?php
                if(!$owner)
                    {
                        echo '<p>';
                        echo Html::a('ติดต่อ', ['mh-user/view', 'id' => $model->user_id], ['class' => 'btn btn-primary']);
                        echo '</p>';
                    }
                ?>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>ที่อยู่กิจการ</strong> : <?= $model->store_address ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>พิกัด GPS</strong> : <?= $model->store_gps ?></p>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>รัศมีพื้นที่ให้บริการ</strong> : <?= $model->store_km ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>พื้นที่ให้บริการจังหวัด</strong> : <?= $model->location_province($model->store_province_id) ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>พื้นที่ให้บริการอำเภอ</strong> : <?= $model->location_amphoe($model->store_amphoe_id) ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>เบอร์ติดต่อ 1</strong> : <?= $model->store_tel1 ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>เบอร์ติดต่อ 2</strong> : <?= $model->store_tel2 ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>Line Id</strong> : <?= $model->store_line_id ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>Line @</strong> : <?= $model->store_line_ad ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>Facebook fanpage</strong> : <?= $model->store_fb_fp ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>Messenger</strong> : <?= $model->store_messenger ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>Email</strong> : <?= $model->store_email ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>Website</strong> : <?= $model->store_website ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>อื่น ๆ</strong> : <?= $model->store_other ?></p>
        </div>
        <!--<div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>ประเภทสินค้าร้านค้า</strong> : <?= $model->storetype_text() ?></p>
        </div>-->
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>ชื่อผู้แก้ไข</strong> : <?= $model->updated_id ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>เวลาสร้าง</strong> : <?= $model->created_time ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <p><strong>เวลาแก้ไข</strong> : <?= $model->updated_time ?></p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <!-- Your share button code -->
            <div class="fb-share-button" 
                data-size="large"
                data-href="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>"
                data-layout="button_count">
            </div>
            <div class="line-it-button" 
                data-lang="en" 
                data-type="share-a" 
                data-ver="2" 
                data-url="<?= 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" 
                style="display: none;">
            </div>
        </div>       
       
    </div>




    <?=''/*DetailView::widget([
        'model' => $model,
        'attributes' => [
            'store_id',
            'store_name',
            //'store_pic',
            [
                'attribute'=>'store_pic',
                'value'=>$model->getPhotosViewerback(),
                'format' => 'html',
            ],
            'store_address',
            'store_gps',
            'store_km',
            //'store_province_id',
            //'store_amphoe_id',
            [
                'attribute' => 'store_province_id',
                'value' => $model->storeProvince->province_name_th
            ],
            [
                'attribute' => 'store_amphoe_id',
                'value' => $model->storeAmphoe->amphoe_name_th
            ],
            'store_tel1',
            'store_tel2',
            'store_line_id',
            'store_line_ad',
            'store_fb_fp',
            'store_messenger',
            'store_email:email',
            'store_website',
            'store_other',
            'store_pt_id',
           
            
            'store_data_other',
            'updated_id',
            'created_time',
            'updated_time',
        ],
    ])*/ ?>

</div>
