<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
use common\models\MhProvince;
use common\models\MhAmphoe;

$province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');
$id = yii::$app->user->identity->id;

/* @var $this yii\web\View */
/* @var $model common\models\MhStore */
/* @var $form yii\widgets\ActiveForm */
?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<!--
<script type="text/javascript">
    $(document).ready(function() {

        function showTextStatus(status) {
            return status == 'true' ? "(แตะเพื่อย่อ)" : "(แตะเพื่อขยาย)";
        }

        function loopCheckedValue() {
            var selectedLanguage = []
            $('input[type="checkbox"]:checked').each(function() {

                var text = $('span#boxitem' + this.value).html();
                selectedLanguage.push(text);
            });
            return selectedLanguage;
        }

        function showCollapsHead(selected, textStat) {
            $('#divResult').html(selected.length + "\n" + "รายการ / ประเภทสินค้าที่เลือก: " + selected + textStat);
        }

        function event(type) {
            var stat = $("#divResult[aria-expanded]").attr('aria-expanded') == "true";
            var stringStat = type == "exp" ? showTextStatus(""+!stat+"") : showTextStatus(""+stat+"");
            var selected = loopCheckedValue();
            
            if (type == "init") {
                if (selected.length != 0)  { showCollapsHead(selected, stringStat); } 
                else { return; }
            } else { showCollapsHead(selected, stringStat); }  
        }

        $("#divResult[aria-expanded]").click(function() {
            event("exp")
        });

        $('input[type="checkbox"]').click(function() {
            event("chk")
        });
        event("init")
        
    });

</script>-->

<div class="mh-store-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'store_name')->textInput(['maxlength' => true])->label('ชื่อกิจการ', ['style' => 'color:red']) ?>

    <?= $form->field($model, 'user_id')
        ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $id])
        ->label(false)
    ?>

    <?='' //$form->field($model, 'store_pic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'store_pic[]')->fileInput(['multiple' => true])->label('รูปถ่ายกิจการ', ['style' => 'color:red']) ?>
    <div class="well">
        <?= $model->getPhotosViewer(); ?>
    </div>

    <?= $form->field($model, 'store_address')->textInput(['maxlength' => true])->label('ที่อยู่กิจการ', ['style' => 'color:red']) ?>

    <?= $form->field($model, 'store_gps')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'store_km')->textInput() ?>

    <?='' //$form->field($model, 'store_province_id')->textInput() ?>

    <?='' //$form->field($model, 'store_amphoe_id')->textInput() ?>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'store_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
                ->label('จังหวัดที่ตั้งกิจการ', ['style' => 'color:red'])
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'store_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ...',
                        'url' => Url::to(['/mh-store/get-amphoe'])
                    ]
                ])
                ->label('อำเภอที่ตั้งกิจการ', ['style' => 'color:red']);
        ?>
    </div>   
</div>

    <?= $form->field($model, 'store_tel1')->textInput()->label('เบอร์ติดต่อ 1', ['style' => 'color:red']) ?>

    <?= $form->field($model, 'store_tel2')->textInput() ?>

    <?= $form->field($model, 'store_line_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'store_line_ad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'store_fb_fp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'store_messenger')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'store_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'store_website')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'store_other')->textInput(['maxlength' => true]) ?>

    <?= '' //$form->field($model, 'store_pt_id')->textInput(['maxlength' => true]) ?>

    <!--<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div id="w1" class="panel-group collapse in" aria-expanded="true" style="">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">

                            <a class="collapse-toggle" href="#w1-collapse1" data-toggle="collapse" data-parent="#w1" aria-expanded="false"  class="divResult" id="divResult">เลือกประเภทสินค้า (แตะเพื่อขยาย)</a>


                        </h4>
                    </div>

                    <div id="w1-collapse1" class="panel-collapse collapse" style="">
                        <div class="panel-body" id="divR"> 
                            <?= ''/*

                                $form
                                    ->field($model, 'store_pt_id')
                                    ->checkBoxList(
                                        $this->context->getstoreTypes(),
                                        [
                                            'separator' => '<br>',
                                            'item' => function ($index, $label, $name, $checked, $value) {
                                                $checked = $checked ? 'checked' : '';
                                                return '<div> <span id="boxitem' . $value . '"> <input type="checkbox" value=' . $value . ' name=' . $name . ' ' . $checked  . '>' . $label . '</span></div>';
                                            }
                                        ]
                                    )
                                    
                                    ->label(false)
                                    ->hint('เลือกได้หลายรายการ')

                            */?>



                        </div>
                    </div>




                </div>
            </div>

        </div>
    </div>-->

    <!--<div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <?=
                $form
                    ->field($model, 'store_pt_id')
                    ->checkBoxList(
                        $this->context->getStoreTypes(),
                        ['itemOptions' => $model->storetypeToArray(), 'separator' => '<br>']
                    )
                    ->hint(Html::a('คลิกดูประเภทสินค้า', ['mh-store-product-type/index'], ['target' => '_blank', 'class' => 'linksWithTarget']))
                    ->label(false)
                    ->hint('เลือกได้หลายรายการ')
                    ->label('เลือกประเภทสินค้า', ['style' => 'color:red'])
            ?>
        </div>
    </div>-->

    <?= $form->field($model, 'store_data_other')->textInput(['maxlength' => true]) ?>

    <?='' //$form->field($model, 'updated_id')->textInput() ?>

    <?= $form->field($model, 'updated_id')
        ->hiddenInput(['maxlength' => true, 'readonly' => true, 'value' => $id])
        ->label(false)
    ?>

    <?= $form->field($model, 'created_time')->textInput([
        'readonly' => true,
        'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
    ]) ?>

    <?= $form->field($model, 'updated_time')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

   

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
