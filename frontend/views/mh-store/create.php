<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhStore */

$this->title = 'เพิ่มข้อมูลร้านค้า';
if (Yii::$app->user->isGuest) {
        
    $this->params['breadcrumbs'][] = ['label' => 'รายการร้านค้า', 'url' => ['index']];
    
} else {

    $this->params['breadcrumbs'][] = ['label' => 'ร้านค้าของฉัน', 'url' => ['mystore']];

}
//$this->params['breadcrumbs'][] = ['label' => 'ร้านค้างาน', 'url' => ['mystore']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-store-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => [],
    ]) ?>

</div>
