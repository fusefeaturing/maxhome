<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhStore */

$this->title = 'แก้ไขข้อมูลร้านค้า: ' . $model->store_name;
if (Yii::$app->user->isGuest) {
        
    $this->params['breadcrumbs'][] = ['label' => 'รายการร้านค้า', 'url' => ['index']];
    
} else {

    $this->params['breadcrumbs'][] = ['label' => 'ร้านค้าของฉัน', 'url' => ['mystore']];

}
//$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลร้านวัสดุก่อสร้าง', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->store_name, 'url' => ['view', 'id' => $model->store_id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="mh-store-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'amphoe' => $amphoe,
    ]) ?>

</div>
