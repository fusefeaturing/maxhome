<?php

/* @var $this \yii\web\View */
/* @var $content string */


use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use common\widgets\Alert;
use frontend\assets\AppAsset;

$currentUrl = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; //Url::canonical();
$this->registerMetaTag(Yii::$app->params['og_title'], 'og_title');
$this->registerMetaTag(Yii::$app->params['og_description'], 'og_description');
$this->registerMetaTag(Yii::$app->params['og_url'], 'og_url');
$this->registerMetaTag(Yii::$app->params['og_image'], 'og_image');
$this->registerMetaTag(Yii::$app->params['og_keyword'], 'og_keyword');
$this->registerMetaTag(Yii::$app->params['og_robots'], 'og_robots');
$this->registerMetaTag(Yii::$app->params['og_revisit-after'], 'og_revisit-after');




AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>



    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <!-- Load LINE JavaScript -->
    <script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>
    <!-- End LINE JavaScript -->

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v5.0&appId=897082717338911&autoLogAppEvents=1"></script>

    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>




    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>


    <?php $this->beginBody() ?>

    <div class="wrap">

        <?php
        NavBar::begin([
            'brandLabel' => Yii::$app->name,
            'brandUrl' => Yii::$app->homeUrl,
            //'headerContent' => Html::a('089-519-7777','tel:+66618728888',['class' => 'navbar-brand navbar-link']),  
            'options' => [
                'class' => 'fixed-top navbar-expand-md navbar-dark bg-dark',
            ],
        ]);
        $menuItems = [
            //Html::a('กลับไปที่ maxhome.co',"https://line.me/R/ti/p/%40maxhome"),
            ['label' => 'หาผู้รับเหมาก่อสร้าง', 'url' => ['/mh-job/index']],
            //['label' => 'หาร้านวัสดุก่อสร้าง', 'url' => ['/mh-construction/index']],

            ['label' => 'ดูสินค้า', 'url' => 'https://maxhome.co/shop'],
            //['label' => 'สมัครสมาชิก', 'url' => ['/mh-user/register']],
            //['label' => 'ดูบทความ', 'url' => ['/site/contact']],

            //['label' => 'ดูสินค้า', 'url' => ['/mh-construction/index']],
            //['label' => 'หาสถาปนิก / วิศวกร', 'url' => ['/mh-architect-engineer/index']],
            //['label' => 'สั่งคอนกรีต', 'url' => ['/mh-concrete/create']],
        ];
        if (Yii::$app->user->isGuest) {

            $menuItems[] = ['label' => 'สมัครสมาชิก', 'url' => ['/mh-user/register']];
            $menuItems[] = ['label' => 'เข้าสู่ระบบ', 'url' => ['/site/login']];
        } else {

            $userid = yii::$app->user->identity->id;
            $menuItems[] = ['label' => 'แก้ไขข้อมูลส่วนตัว', 'url' => ['/mh-user/profile', 'id' => $userid]];


            $menuItems[] = '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'ออกจากระบบ (' . Yii::$app->user->identity->user_firstname . ')',
                    ['class' => 'btn btn-outline-danger']
                )
                . Html::endForm()
                . '</li>';
        }
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav ml-auto p-2 bd-highlight '],
            'items' => $menuItems,
        ]);
        NavBar::end();
        ?>

        <div class="container" style="padding-top: 100px;">

            <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-arrow-circle-up"></i></button>


            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],

            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>


        </div>

        <footer class="navbar fixed-bottom bg-red justify-content-center">
            <div class="wpfront-message wpfront-message576">ต้องการความช่วยเหลือติดต่อสอบถาม ทีมงาน MaxHome &nbsp;</div>
            <div>
                <a class="wpfront-button btn-sm" href="https://line.me/R/ti/p/%40maxhome" style="text-decoration: none;" target="_blank"><i class="fab fa-line"></i> LINE ID : @MaxHome</a>
                <a class="wpfront-button-blue btn-sm" href="tel:+66911039999" style="text-decoration: none;" target="_blank" rel="noopener noreferrer"><i class="fas fa-phone-alt"></i> maxhome.co</a>
            </div>
        </footer>

    </div>

    <?php
    /*

global $wpdb;
$current_user = wp_get_current_user();

$helloworld_id = $wpdb->get_var("SELECT meta_value FROM $wpdb->usermeta WHERE meta_key = 'line-id' and user_id=  $current_user->ID");

echo '<a href="https://line.me/R/ti/p/~'.$helloworld_id .'" style="text-decoration: none;" target="_blank" rel="noopener noreferrer"><i class="fab fa-line"></i> LINE ID : '.$helloworld_id.'</a>';

*/
    ?>



    <script>
        //Get the button:
        mybutton = document.getElementById("myBtn");

        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function() {
            scrollFunction()
        };

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                mybutton.style.display = "block";
            } else {
                mybutton.style.display = "none";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
    </script>
    <?php $this->endBody() ?>

</body>

</html>
<?php $this->endPage() ?>