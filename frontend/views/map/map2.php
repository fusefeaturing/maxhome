<!DOCTYPE html>
<html>
<head>
  <title>My Map</title>
  <meta name="viewport" content="initial-scale=1.0">
  <meta charset="utf-8">

  <!-- Stylesheets. -->
 
</head>
<body>

  <div class="container-fluid">
    <div class="row">
      <div class="col">
        <h1 class="display-4">My Map</h1>
      </div>
    </div>
    <div class="row mb-2">
      <div class="col">
        <input class="form-control" id="search" type="text" placeholder="Search..." />
      </div>
    </div>
    <div class="row">
      <div class="col">
        <div id="map"></div>
      </div>
    </div>
  </div>
 
  <!-- Bootstrap scripts. -->
  
  
  <!-- Google Maps scripts. -->
  <script>
  var map;

function createMap () {
  var options = {
    center: { lat: 43.654, lng: -79.383 },
    zoom: 10
  };

  map = new google.maps.Map(document.getElementById('map'), options);

  var input = document.getElementById('search');
  var searchBox = new google.maps.places.SearchBox(input);

  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  
  searchBox.addListener('places_changed', function () {
    var places = searchBox.getPlaces();

    if (places.length == 0)
      return;

    markers.forEach(function (m) { m.setMap(null); });
    markers = [];

    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(p) {
      if (!p.geometry)
        return;

      markers.push(new google.maps.Marker({
        map: map,
        title: p.name,
        position: p.geometry.location
      }));

      if (p.geometry.viewport)
        bounds.union(p.geometry.viewport);
      else
        bounds.extend(p.geometry.location);
    });
    
    map.fitBounds(bounds);
  });
}  
  </script>
  <script src="script.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoLU64RtoHP008znuvfas76KOBVfrpx_8&callback=createMap&libraries=places" async defer></script>
</body>
</html>