<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\MhProvince;
use common\models\MhAmphoe;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model backend\models\MhConcreteSearch */
/* @var $form yii\widgets\ActiveForm */
$province = ArrayHelper::map(MhProvince::find()->asArray()->all(), 'province_id', 'province_name_th');
$amphoe = ArrayHelper::map(MhAmphoe::find()->asArray()->all(), 'amphoe_id', 'amphoe_name_th');
?>

<div class="mh-concrete-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>    


    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6">
            <?php echo $form
                ->field($model, 'con_province_id')
                ->dropdownList($province, ['prompt' => 'ไม่ระบุ']) ?>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6">
            <?php echo $form
                ->field($model, 'con_amphoe_id')
                ->dropdownList($amphoe, ['prompt' => 'ไม่ระบุ']) ?>
        </div>


    <!--
    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'con_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'con_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ...',
                        'url' => Url::to(['/mh-con/get-amphoe'])
                    ]
                ]);
        ?>
    </div>
                    -->
                    <div class="form-group">
            <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary btn-block', 'id' => 'btnSubmit']) ?>
            <?= Html::a(
                'รีเซ็ตค่าค้นหา',
                ['mh-concrete/index'],
                ['class' => 'btn btn-danger btn-block', 'style' => '']
            ) ?>
        </div>


    </div>

    <?='' //$form->field($model, 'con_id') ?>

    <?='' //$form->field($model, 'con_desciption_product') ?>

    <?='' //$form->field($model, 'con_strength') ?>

    <?='' //$form->field($model, 'con_volume') ?>

    <?='' //$form->field($model, 'con_cube') ?>

    <?php // echo $form->field($model, 'con_cylender') ?>

    <?php // echo $form->field($model, 'con_gps_transpot') ?>

    <?php // echo $form->field($model, 'con_province_id') ?>

    <?php // echo $form->field($model, 'con_amphoe_id') ?>

    <?php // echo $form->field($model, 'con_district_id') ?>

    <?php // echo $form->field($model, 'con_pic_map') ?>

    <?php // echo $form->field($model, 'con_datetime') ?>

    <?php // echo $form->field($model, 'con_duration') ?>

    <?php // echo $form->field($model, 'con_name_contact') ?>

    <?php // echo $form->field($model, 'con_tel_contact') ?>

    <?php // echo $form->field($model, 'con_other') ?>

    <?php // echo $form->field($model, 'updated_id') ?>

    <?php // echo $form->field($model, 'created_time') ?>

    <?php // echo $form->field($model, 'updated_time') ?>


    <?php ActiveForm::end(); ?>

</div>
