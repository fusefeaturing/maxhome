<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MhConcreteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การจัดการการข้อมูลคอนกรีต';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-concrete-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'con_id',
            'con_desciption_product',
            'con_strength',
            'con_volume',
            'con_cube',
            'con_cylender',
            //'con_gps_transpot',
            //'con_province_id',
            //'con_amphoe_id',
            //'con_district_id',
            //'con_pic_map',
            //'con_datetime',
            //'con_duration',
            'con_name_contact',
            'con_tel_contact',
            'con_other',
            //'updated_id',
            //'created_time',
            //'updated_time',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
