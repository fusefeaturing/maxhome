<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ประวัติการสั่งคอนกรีตของฉัน';
$userid = yii::$app->user->identity->id;
//$this->params['breadcrumbs'][] = $this->title;

?>

<div class="mh-job-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>1.) สั่งคอนกรีต</p>
    
    <p>2.) แก้ข้อมูลสำหรับติดต่อของฉัน<?= Html::a('แก้ข้อมูลสำหรับติดต่อของฉัน', ['mh-user/update', 'id' => $userid], ['class' => 'btn btn-danger']) ?></p>
    <p>ระบุข้อมูลครบถ้วนเพื่อให้ผู้ใช้บริการจะค้นหาและติดต่อคุณได้โดยตรง</p>

    <hr>
    
    <h2>1.) ประวัติการสั่งคอนกรีตของฉัน</h2>

    <p>
        <?= Html::a('+ สั่งคอนกรีต', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showHeader'=> false,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model) 
                {
                    $htmlcard = '';
                    $htmlcard .= '<div style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);  padding: 16px; background-color: #f1f1f1;">';

                    $htmlcard .= '<div class="row">';

                        $htmlcard .= '<div class="col-xs-12 col-sm-6 col-md-6">';
                        $htmlcard .= '<p><strong>รายละเอียดคอนกรีต</strong><br>' . $model->con_desciption_product . '</p>';
                        $htmlcard .= '<p><strong>กำลังแรงอัด</strong> : ' . $model->con_strength . ' </p>';
                        $htmlcard .= '<p><strong>ปริมาณ</strong> : ' . $model->con_volume . ' หน่วย '.$model->con_unit.' </p>';
                        //$htmlcard .= '<p><strong>Cube</strong> : ' . $model->con_cube . ' กิโลเมตร</p>';
                        $htmlcard .= '<p><strong>พื้นที่ให้บริการจังหวัด</strong> : ' . $model->location_province($model->con_province_id) . ' </p>';
                        $htmlcard .= '<p><strong>พื้นที่ให้บริการอำเภอ</strong> : ' . $model->location_amphoe($model->con_amphoe_id) . ' </p>';
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-4 col-md-4">';
                        $htmlcard .= $model->getFirstPhotos();
                        $htmlcard .= '</div>';

                        $htmlcard .= '<div class="col-xs-12 col-sm-2 col-md-2" style="text-align: center; vertical-align: middle; margin-top: 10px;">';
                        $htmlcard .= Html::a('ดูข้อมูล', 
                        ['mh-concrete/view', 'id' => $model->con_id], 
                        ['class' => 'btn btn-success glyphicon glyphicon-user', 'style' => 'width: 100%;']);
                        $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    return $htmlcard;
                },
                'contentOptions' => ['style' => 'width:80%; height:20%; text-align: left; margin: 25px; vertical-align: middle;'],
                'headerOptions' => ['style' => 'text-align: center;'],
            ],
        ],
    ]);
    ?>



</div>
