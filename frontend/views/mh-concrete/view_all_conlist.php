<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RogUserCarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการคอนกรีต';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-concrete-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <hr>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'showHeader' => false,
        'columns' => [
            [
                
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model)  {
                    $htmlcard = '';
                    $htmlcard .= '<div style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);  padding: 16px; background-color: #f1f1f1;">';
                    /*
                    $htmlcard .= '<div class="row">';
                        $htmlcard .= '<div class="col-xs-12 col-sm-12 col-md-12">';
                        $htmlcard .= '<h4>' . ((array_key_exists($model->car_type, )) ? [$model->car_type] : 'ประเภทรถไม่ถูกต้อง') . ' </h4>';
                        $htmlcard .= '</div>';
                    $htmlcard .= '</div>';
*/
                    $htmlcard .= '<div class="row">';

                    $htmlcard .= '<div class="col-xs-12 col-sm-4 col-md-4">';
                    $htmlcard .= '<p>' . $model->getFirstPhotos() . '</p>';
                    $htmlcard .= '</div>';
                    $htmlcard .= '<div class="col-xs-12 col-sm-6 col-md-6">';                    
                    $htmlcard .= '<p><strong>ชื่อกิจการ</strong> : ' . $model->con_desciption_product . ' </p>';                    
                    $htmlcard .= '<p><strong>จังหวัด</strong> : '. $model->pickup() .'</p>';
                    $htmlcard .= '<p><strong>อำเภอ</strong> : '. $model->deliver() .'</p>';  
                    $htmlcard .= '<p><strong>รายละเอืยดคอนกรีต</strong> : '. $model->con_desciption_product .'</p>';
                    //$htmlcard .= '<p><strong>ประเภทสินค้าที่ให้บริการ</strong><br>' . $model->contype_text() . '</p>';
                    //$htmlcard .= '<p>ประเภทสินค้า : ' . $model->con_pt_id . '</p>';
                    $htmlcard .= '</div>';
                    
                    $htmlcard .= '<div class="col-xs-12 col-sm-2 col-md-2" style="text-align: center; vertical-align: middle; margin-top: 10px;">';
                    $htmlcard .= Html::a('ดูรายละเอืยด', 
                    ['mh-concrete/view', 'id' => $model->con_id], 
                    ['class' => 'btn btn-success glyphicon glyphicon-user', 'style' => 'width: 100%;']);
                    $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    $htmlcard .= '</div>';

                    return $htmlcard;
                },
                'contentOptions' => ['style' => 'width:80%; height:20%; text-align: left; margin: 25px; vertical-align: middle;'],
                'headerOptions' => ['style' => 'text-align: center;'],
            ],
        ],
    ]); ?>
</div>

