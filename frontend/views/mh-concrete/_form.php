<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DepDrop;
use yii\helpers\Url;
use common\models\MhProvince;
use yii\jui\DatePicker;







/* @var $this yii\web\View */
/* @var $model common\models\MhConcrete */
/* @var $form yii\widgets\ActiveForm */
//$id = yii::$app->user->identity->id;
$this->title = 'สั่งคอนกรีตผสมเสร็จ';
Yii::$app->params['og_title']['content'] = $this->title;
$province = ArrayHelper::map(MhProvince::find()->orderBy('province_name_th')->asArray()->all(), 'province_id', 'province_name_th');
?>

<body style="background-color: #ecf0f3;">

<div class="mh-concrete-form">



    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

   
    <div class="card2">
        <h1 style="padding-left:20px; padding-top:20px;"><?= Html::encode($this->title) ?></h1>
        
        <div class="card-body">
            <?= $form->field($model, 'con_strength')->dropDownList([ 1 => '150', 2 => '180',
                3 => '210', 4 => '240', 5 => '280', 6 => '300', 7 => '320', 8 => '350', 
                9 => '380', 10 => '400', 11 => '420', ], ['prompt' => 'เลือก Strength (cube)'])
                ->label('กำลังแรงอัด', ['style' => 'color:red']) ?>

            <?= $form->field($model, 'con_volume')->textInput()
            ->label('ปริมาณ', ['style' => 'color:red']) ?>   


            <?php $model->isNewRecord==1?
            $model->con_unit=1:$model->con_unit;?>
            <?= $form->field($model,'con_unit')->radioList([ 1 => 'cube', 2 => 'cylender', ])
            ->label('หน่วย', ['style' => 'color:red']) ?>   
        </div>
    </div>

    <?='' //$form->field($model, 'con_cube')->radio(['cube' => 'cube']) ?>

    <?='' //$form->field($model, 'con_cylender')->radio([0 => 'cylender']) ?>

    
    
    

    <?='' //$form->field($model, 'con_province_id')->textInput() ?>
   
    <?='' //$form->field($model, 'con_amphoe_id')->textInput() ?>
   
    <?='' //$form->field($model, 'con_district_id')->textInput() ?>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

    <div class="card2">
        <div class="card-body">
            <h4 style="color:red;">สถานที่จัดส่ง</h4>
            <h6 style="color:red;">*เลือกอย่างใดอย่างหนึ่ง</h6>

    <div class="">  



    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'con_province_id')
                
                ->dropdownList($province, [
                    'id' => 'ddl-province',
                    'prompt' => 'เลือกจังหวัด'
                ])
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'con_amphoe_id')
                ->widget(DepDrop::classname(), [
                    'options' => ['id' => 'ddl-amphoe'],
                    'data' => $amphoe,
                    'pluginOptions' => [
                        'depends' => ['ddl-province'],
                        'placeholder' => 'เลือกอำเภอ...',
                        'url' => Url::to(['/mh-concrete/get-amphoe'])
                    ]
                ]);
        ?>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <?=
            $form
                ->field($model, 'con_district_id')
                ->widget(DepDrop::classname(), [
                    'data' => $district,
                    'pluginOptions' => [
                        'depends' => ['ddl-province', 'ddl-amphoe'],
                        'placeholder' => 'เลือกตำบล...',
                        'url' => Url::to(['mh-concrete/get-district'])
                    ]
                ]);
        ?>
    </div>
    
    <?= $form->field($model, 'con_pic_map[]')->fileInput(['multiple' => true]) ?>
    <div class="well">
        <?= $model->getPhotosViewer(); ?>
    </div>


    <!--<?= $form->field($model, 'con_gps_transpot')->textInput(['maxlength' => true])
    ->input('con_gps_transpot', ['placeholder' => "เช่น 13.793933, 100.495085"])->label(false); ?>-->

</div>
    <div class="">
            <?= $form
                ->field($model, 'con_datetime')
                ->widget(DatePicker::classname(), [
                    'language' => 'th',
                    'dateFormat' => 'yyyy-MM-dd',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                    ],
                    'options' => ['class' => 'form-control']
                ])
                ->label('วันที่ต้องการส่ง', ['style' => 'color:red'])
            ?>
    </div>



    <?= $form->field($model, 'con_duration')->radioList([ 1 => 'เช้า', 2 => 'บ่าย', ])
    ->label('ช่วงเวลาที่ต้องการส่ง', ['style' => 'color:red']) ?>

</div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>


<div class="card2">
    <div class="card-body">


    <?= $form->field($model, 'con_name_contact')->textInput(['maxlength' => true])
    ->label('ชื่อติดต่อ', ['style' => 'color:red']) ?>

    <?= $form->field($model, 'con_tel_contact')->textInput()
    ->label('เบอร์โทรศัพท์', ['style' => 'color:red']) ?>

    <?= $form->field($model, 'con_other')->textInput(['maxlength' => true]) ?>

    <?='' //$form->field($model, 'updated_id')->textInput() ?>

    <?= $form->field($model, 'created_time')->textInput([
        'readonly' => true,
        'value' => (($model->created_time != null) && ($model->created_time != '0000-00-00 00:00:00')) ? $model->created_time : date('Y-m-d H:i:s')
    ]) ?>
    
    <?= $form->field($model, 'updated_time')->textInput([
        'readonly' => true,
        'value' => date('Y-m-d H:i:s')
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึก', ['class' => 'btn btn-outline-success']) ?>
    </div>


    </div>

    
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
            <p> </p>
        </div>

    <?php ActiveForm::end(); ?>

</div>

</body>
