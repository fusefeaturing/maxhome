<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhAeProductType */

$this->title = 'แก้ไขข้อมูลออกแบบเขียนแบบ: ' . $model->ae_pt_name;
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลประเภทสินค้าออกแบบเขียนแบบ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ae_pt_name, 'url' => ['view', 'id' => $model->ae_pt_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mh-ae-product-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
