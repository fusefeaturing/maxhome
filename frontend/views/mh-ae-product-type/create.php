<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MhAeProductType */

$this->title = 'เพิ่มข้อมูลประเภทสินค้าออกแบบเขียนแบบ';
$this->params['breadcrumbs'][] = ['label' => 'การจัดการข้อมูลประเภทสินค้าออกแบบเขียนแบบ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mh-ae-product-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
